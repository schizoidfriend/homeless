package
{
	import editor.Asset;
	import editor.EditorView;
	import editor.NPCAssetInfo;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.sampler.NewObjectSample;
	import flash.sampler.Sample;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import game.GameMapState;
	import game.GameSavedData;
	
	import inventory.InventoryItemMaker;
	import inventory.items.ItemBuzz;
	
	import map.Ad;
	import map.Buzz;
	import map.Coin;
	import map.CollectableObject;
	import map.Container;
	import map.CurrentMapParameters;
	import map.DeadRat;
	import map.ExitPoint;
	import map.Food;
	import map.Obstacle;
	import map.PlayerStartPosition;
	import map.Puke;
	import map.Shit;
	import map.SleepingPlace;
	import map.SmashableObject;
	import map.SmashablesController;
	import map.StaticNPC;
	import map.Trash;
	import map.WallWriting;
	import map.WashPlace;
	import map.characters.AfganMusicant;
	import map.characters.Bird;
	import map.characters.Dog;
	import map.characters.EnemyCharacter;
	import map.characters.Missioner;
	import map.characters.MovingNPC;
	import map.characters.NPC;
	import map.characters.Pimp;
	import map.characters.PlaceToPlaceController;
	import map.characters.PoliceManNPC;
	import map.characters.Prostitute;
	import map.characters.QuestNPC;
	import map.characters.ShopToShopNPC;
	import map.characters.TerritoryNPC;
	import map.characters.structs.QuestCharInfo;
	
	import over.OverScreen;
	
	import shops.Place;
	import shops.SimpleShop;
	
	import starling.animation.Juggler;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.ColorMatrixFilter;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.MatrixUtil;
	
	import utils.AStarLib;
	import utils.GameUtils;
	import utils.Line;
	import utils.UtilsMath;
	import utils.pathfinding.astar.AStarNodeVO;
	
	public class Background extends Sprite
	{				
		/* EDITOR PART */
		//currently mouse over tile image
		private var _currentlyTouchedTileImage:Image = null;
		//currently touched tile position
		private var _currentlyTouchedTilePos:Point = new Point();
		//currently touched tile
		private var _currentlyToucheTile:Tile = null;
		//currently touched background position
		private var _currentlyTouchedPosition:Point = new Point();
		//currently touched point on background
		private var _currentPointOnBackground:Point = new Point();
		/* END OF EDITOR PART */
		
		//redraw offset in pixels
		private var _lastRedrawnXPosOffset:int = -1;
		private var _lastRedrawnYPosOffset:int = -1;
		
		//redraw start pos in tiles
		private var _lastRedrawnXPos:int = -1;
		private var _lastRedrawnYPos:int = -1;
		
		//tiiles can fit on the screen
		private var _xTilesCanFit:int;
		private var _yTilesCanFit:int;
		
		//non walkable tiles positions
		private var _tilesWalkability:Array = new Array();
		//all tiles
		private var _tiles:Array = new Array();
		//collectable items
		private var _collectables:Vector.<CollectableObject> = new Vector.<CollectableObject>();
		private var _shopsOnMap:Array = new Array();
		//shops
		private var _shops:Vector.<SimpleShop> = new Vector.<SimpleShop>();
		//places
		private var _places:Vector.<Place> = new Vector.<Place>();
		//npcs in game
		private var _staticNPCs:Vector.<StaticNPC> = new Vector.<StaticNPC>();
		private var _walkingNPCs:Vector.<MovingNPC> = new Vector.<MovingNPC>();
		private var _birds:Vector.<Bird> = new Vector.<Bird>();
		//walkable and hitable npc-s (potential enemyes)
		private var _enemyes:Vector.<EnemyCharacter> = new Vector.<EnemyCharacter>();
		//containers in game
		private var _containers:Vector.<Container> = new Vector.<Container>();
		//places with possibility to sleep into
		private var _sleepingPlaces:Vector.<SleepingPlace> = new Vector.<SleepingPlace>();
		//trees and stuff
		private var _obstacles:Vector.<Obstacle> = new Vector.<Obstacle>();
		//smashable objects (shit)
		private var _smashable:Vector.<SmashableObject> = new Vector.<SmashableObject>();
		//walkable environment stuff
		private var _environments:Vector.<Puke> = new Vector.<Puke>();
		//fountains etc. where you can swim
		private var _washingPlaces:Vector.<WashPlace> = new Vector.<WashPlace>();
		//writings on the wall
		private var _wallWritings:Vector.<WallWriting> = new Vector.<WallWriting>();
		//trash
		private var _trash:Vector.<Trash> = new Vector.<Trash>();
		//staircases positions possible to enter
		private var _staircases:Vector.<Point> = new Vector.<Point>();
		
		
		//controller for shop-to-shop npc-s
		private var _ptpController:PlaceToPlaceController = new PlaceToPlaceController();
		//player appear positions list
		private var _playerStartPositions:Vector.<PlayerStartPosition> = new Vector.<PlayerStartPosition>();
		
		//map layers
		private var _backgroundLayer:Sprite = new Sprite();
		private var _obstaclesLayer:Sprite = new Sprite();
		private var _collectablesLayer:Sprite = new Sprite();
		private var _containersLayer:Sprite = new Sprite();
		private var _npcsLayer:Sprite = new Sprite();
		private var _writingsLayer:Sprite = new Sprite();
		
		private var _smashablesController:SmashablesController = new SmashablesController();
		
		private var _checkDeadTimer:Timer = new Timer(2000);
		
		private var _currentMapName:String = "";
		private var _previousMapName:String = "";
		
		public function Background()
		{
			super();
			
			_checkDeadTimer.addEventListener(TimerEvent.TIMER, onDeadTimer);
			_checkDeadTimer.start();
			
			/*var my_filter:ColorMatrixFilter = new ColorMatrixFilter();
			my_filter.adjustBrightness(0.1);*/
			/*my_filter.adjustSaturation(-1);
			my_filter.adjustContrast(1);*/
			
		//	this.filter = my_filter
			
			addChild(_backgroundLayer);
			addChild(_obstaclesLayer);
			addChild(_collectablesLayer)
			addChild(_containersLayer);
			addChild(_npcsLayer);
			addChild(_writingsLayer);
			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			
			//loading 1st test map
			loadMap("home.xml");
		}
		
		public function get ptpController():PlaceToPlaceController
		{
			return _ptpController;
		}

		public function get birds():Vector.<Bird>
		{
			return _birds;
		}

		public function get smashable():Vector.<SmashableObject>
		{
			return _smashable;
		}

		public function get enemyes():Vector.<EnemyCharacter>
		{
			return _enemyes;
		}

		public function get walkingNPCs():Vector.<MovingNPC>
		{
			return _walkingNPCs;
		}
		
		public function get staticNPCs():Vector.<StaticNPC>
		{
			return _staticNPCs;
		}

		public function get lastRedrawnYPosOffset():int
		{
			return _lastRedrawnYPosOffset;
		}

		public function get lastRedrawnXPosOffset():int
		{
			return _lastRedrawnXPosOffset;
		}

		public function get currentlyToucheTile():Tile
		{
			return _currentlyToucheTile;
		}

		public function get currentlyTouchedTilePos():Point
		{
			return _currentlyTouchedTilePos;
		}

		public function get currentPointOnBackground():Point
		{
			return _currentPointOnBackground;
		}

		public function get currentlyTouchedPosition():Point
		{
			return _currentlyTouchedPosition;
		}

		public function get currentlyTouchedTileImage():Image
		{
			return _currentlyTouchedTileImage;
		}

		public function get lastRedrawnYPos():int
		{
			return _lastRedrawnYPos;
		}

		public function get lastRedrawnXPos():int
		{
			return _lastRedrawnXPos;
		}
		
		/*
		* INIT AND DEINIT FUNCTIONS		
		*/
		
		public function initEnterFrame():void
		{
			addEventListener(starling.events.Event.ENTER_FRAME, onEnterFrame);
			_ptpController.startTimer();
		}
		
		public function deinitEnterFrame():void
		{
			_ptpController.stopTimer();
			removeEventListener(starling.events.Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function initTilesMap():void
		{
			//initialize map 2D array
			for ( var i:int = 0; i < CurrentMapParameters.sizeX; i++)
			{
				_tiles[i] = new Array();
				_tilesWalkability[i] = new Array();
			}
		}
		
		private function initShopsMap():void
		{
			for ( var x:int = 0; x < CurrentMapParameters.sizeX; x++)
			{
				_shopsOnMap[x] = new Array();
				for ( var y:int = 0; y < CurrentMapParameters.sizeY; y++)
				{
					_shopsOnMap[x][y] = null;
				}
			}
			
			for each (var shop:SimpleShop in _shops)
			{
				_shopsOnMap[shop.mapPosition.x][shop.mapPosition.y] = shop;
			}
			
		}
		
		public function checkNearestShopsToSetActiveState():void
		{
			for each(var shop:SimpleShop in _shops)
			{
				shop.setToDeactivatedState();
			}
			
			for each(var place:Place in _places)
			{
				place.setToDeactivatedState();
			}
			
			var curCharPos:Point = Game.instance.dude.centerTilePos;
			
			if(curCharPos.x+1 < CurrentMapParameters.sizeX)
				SimpleShop(_shopsOnMap[curCharPos.x+1][curCharPos.y]) && SimpleShop(_shopsOnMap[curCharPos.x+1][curCharPos.y]).setToActiveState();
			if(curCharPos.x-1 > 0)
				SimpleShop(_shopsOnMap[curCharPos.x-1][curCharPos.y]) && SimpleShop(_shopsOnMap[curCharPos.x-1][curCharPos.y]).setToActiveState();
			if(curCharPos.y+1 < CurrentMapParameters.sizeY)
				SimpleShop(_shopsOnMap[curCharPos.x][curCharPos.y+1]) && SimpleShop(_shopsOnMap[curCharPos.x][curCharPos.y+1]).setToActiveState();
			if(curCharPos.y-1 > 0)
				SimpleShop(_shopsOnMap[curCharPos.x][curCharPos.y-1]) && SimpleShop(_shopsOnMap[curCharPos.x][curCharPos.y-1]).setToActiveState();
		}
		
		 private function onAddedToStage(e:starling.events.Event):void
		 {
			 // counting tiles can fit on screen x and y
			 _xTilesCanFit = CommonVars.WIDTH/CommonVars.TILE_SIDE_SIZE + 1;
			 _yTilesCanFit = CommonVars.HEIGHT/CommonVars.TILE_SIDE_SIZE + 1;
			 
			 //DEBUG INFO
			 Starling.current.showStats = true;
		 }
		 
		 //clear all on-map objects lists + deleting them from display list
		 private function clearObjectLists():void
		 {
			 var snpc:StaticNPC;
			 while(_staticNPCs.length)
			 {
				 snpc = _staticNPCs.pop();
				 snpc.cleanup();
				 _npcsLayer.removeChild(snpc, true);
			 }
			 
			 var wnpc:MovingNPC;
			 while(_walkingNPCs.length)
			 {
				 wnpc = _walkingNPCs.pop();
				 wnpc.cleanup();
				 _npcsLayer.removeChild(wnpc, true);
			 }
			 
			 while(_birds.length)
			 {
				 wnpc = _birds.pop();
				 wnpc.cleanup();
				 _npcsLayer.removeChild(wnpc, true);
			 }
			 
			 while(_enemyes.length)
			 {
				 wnpc = _enemyes.pop();
				 wnpc.cleanup();
				 _npcsLayer.removeChild(wnpc, true);
			 }
			 
			 var collectable:CollectableObject;
			 while(_collectables.length)
			 {
				 collectable = _collectables.pop();
				 _collectablesLayer.removeChild(collectable, true);
			 }
			 
			 var container:Container;
			 while(_containers.length)
			 {
				 container =_containers.pop();
				 _containersLayer.removeChild(container, true);
			 }
			 
			 var obstacle:Obstacle;
			 while(_obstacles.length)
			 {
				 obstacle = _obstacles.pop();
				 _obstaclesLayer.removeChild(obstacle, true);			 
			 }
			 
			 var smashable:SmashableObject;
			 while(_smashable.length)
			 {
				 smashable = _smashable.pop();
				 smashable.cleanup();
				 _obstaclesLayer.removeChild(smashable, true);
			 }
			 
			 var puke:Puke;
			 while(_environments.length)
			 {
				 puke = _environments.pop();
				 _collectablesLayer.removeChild(puke, true);
			 }
			 
			 var wp:WashPlace;
			 while(_washingPlaces.length)
			 {
				 wp = _washingPlaces.pop();
				 _obstaclesLayer.removeChild(wp, true);
			 }
			 
			 var ww:WallWriting;
			 while(_wallWritings.length)
			 {
				 ww = _wallWritings.pop();
				 _writingsLayer.removeChild(ww, true);
			 }
			 
			 killAllTrash();
			 
			 
			 var sp:SleepingPlace;
			 while(_sleepingPlaces.length)
			 {
				sp = _sleepingPlaces.pop();
				_containersLayer.removeChild(sp, true);
			 }
			 
			 var exit:ExitPoint;
			 while(Game.instance.dude.globalGoPoints.length)
			 {
				 exit = Game.instance.dude.globalGoPoints.pop();
				 _obstaclesLayer.removeChild(exit, true);
			 }
			 
			 var psp:PlayerStartPosition;
			 while(_playerStartPositions.length)
			 {
				 psp = _playerStartPositions.pop();
				 _obstaclesLayer.removeChild(psp, true);
			 }
			 
			 var shop:SimpleShop;
			 while(_shops.length)
			 {
				 shop = _shops.pop();
				 _obstaclesLayer.removeChild(shop, true);
			 }
			 
			 var place:Place;
			 while(_places.length)
			 {
				 place = _places.pop();
				 _obstaclesLayer.removeChild(place, true);
			 }
			// _shops.splice(0, _shops.length);
			 initShopsMap();
			_ptpController.clearAllPlaces();
		 }
		 
		 public function removeAllShopToShopChars():void
		 {
			 _ptpController.removeAllCharacters();
		 }
		 
		 public function removeAllTempEnvironments():void
		 {
			 var smashable:SmashableObject;
			 while(_smashable.length)
			 {
				 smashable = _smashable.pop();
				 _obstaclesLayer.removeChild(smashable, true);
			 }
			 
			 var puke:Puke;
			 while(_environments.length)
			 {
				 puke = _environments.pop();
				 _collectablesLayer.removeChild(puke, true);
			 }
		 }
		 
		 public function initMouse():void
		 {
			 addEventListener(TouchEvent.TOUCH, onTouch);
		 }
		 
		 public function deinitMouse():void
		 {
			 removeEventListener(TouchEvent.TOUCH, onTouch);
		 }
		 
		 private function initIndexesMap():void
		 {
			 var x:int;
			 var y:int;
			 var type:int;
			 var name:String;
			 var xOffset:int;
			 var yOffset:int;
			 for(x = 0; x < CurrentMapParameters.sizeX; x++)
			 {
				 for(y = 0; y < CurrentMapParameters.sizeY; y++)
				 {
					 type = 0;
					 
					 _tiles[x][y] = new Tile(type, "TileGrass", x, y);
				 }
			 }				 
		 }
		 
		 private function initWalkabilityMap():void
		 {
			 for(var y:int = 0; y < CurrentMapParameters.sizeY; y++)
			 {
				 for(var x:int = 0; x < CurrentMapParameters.sizeX; x++)
				 {					 
					 UtilsMath.astar.walkability[x][y] = isWalkable(new Point(x, y)) ? AStarLib.WALKABLE : AStarLib.UNWALKABLE;
					 UtilsMath.rayTracing.walkability[x][y] = UtilsMath.astar.walkability[x][y];
				 }
			 }
		 }
		 
		 public function isWalkable(pos:Point):Boolean
		 {
			 if(pos.x >= CurrentMapParameters.sizeX || pos.y >= CurrentMapParameters.sizeY)
			 {
				 return false
			 }
			 return _tilesWalkability[int(pos.x)][int(pos.y)];
		 }
		 
		 public function setNPCsToInitialPositionsAndStates():void
		 {
			 Game.instance.dude.setBackToNonEnemyState();
			 for each (var npc:EnemyCharacter in _enemyes)
			 {	
				 npc.setToInitialPos();
				 npc.setToBasicStateOFMind();
			 }
		 }
		 
		 public function killAllWalkableNPCs():void
		 {
			 var wnpc:MovingNPC;
			 while(_walkingNPCs.length)
			 {
				 wnpc = _walkingNPCs.pop();
				 _npcsLayer.removeChild(wnpc, true);
			 }
		 }
		 
		 public function setNPCsToMovement():void
		 {
			 for each (var wnpc:EnemyCharacter in _enemyes)
			 {	
				 if(wnpc is PoliceManNPC)
				 {
					 (wnpc as PoliceManNPC).startMovement();
				 }
			 }
		 }
		 
		 public function clearMapAndCreateNew(sizeX:int, sizeY:int):void
		 {
			 Starling.juggler.purge();
			 CurrentMapParameters.sizeX = sizeX;
			 CurrentMapParameters.sizeY = sizeY;
			 UtilsMath.reinitAlgorithms(sizeX, sizeY);
			 CurrentMapParameters.resetTotalTiles();
			 initTilesMap();
			 initIndexesMap();
			 clearWalkability();
			 clearObjectLists();
			 Game.instance.resetGameParameters();
			 Game.instance.dude.startPos.setTo(0,0);
			 forceRedraw();
			 Game.instance.currentState = Game.EDITOR_STATE;
		 }
		 
		 private function clearWalkability():void
		 {
			 for(var i:int = 0; i < CurrentMapParameters.sizeX; i++)
			 {
				 for(var j:int = 0; j < CurrentMapParameters.sizeY; j++)
				 {
					 _tilesWalkability[i][j] = true;
				 }
			 }
		 }
		 
		 public function stopAllTimers():void
		 {
			 _ptpController.stopTimer();
		 }
		 
		 public function startAllTimers():void
		 {
			 _ptpController.startTimer();
		 }		 
		 
		 /*
		 * REDRAW AND TICKING FUNCTIONS
		 */
		 
		 //variables used in redrawEverything too!!!
		 private var snpc:StaticNPC;
		 private var wnpc:MovingNPC;
		 private var xCompareTopPos:int;
		 private var yCompareTopPos:int;
		 private var xCompareBottomPos:int;
		 private var yCompareBottomPos:int;
		 private var npcTilePosX:int;
		 private var npcTilePosY:int;
		 private function onEnterFrame(e:starling.events.Event):void
		 {				
			 _smashablesController.checkEveryCharacterOnSmashing();
			 //movement for moveable npc-s
			 //top and bottom redrawable tiles (x/y)
			 xCompareTopPos = (_lastRedrawnXPos-1);
			 yCompareTopPos = (_lastRedrawnYPos-1);
			 xCompareBottomPos = (_lastRedrawnXPos + _xTilesCanFit + 1);
			 yCompareBottomPos = (_lastRedrawnYPos + _yTilesCanFit + 1);

			 //redraw npc-s on background and ticking
			 for each (wnpc in _enemyes)
			 {
				 wnpc.tick();
				 
				 npcTilePosX = wnpc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = wnpc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if(npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos)
				 {
					/* if(!this.contains(wnpc))
					 {
						 addChild(wnpc);
					 }*/
					 wnpc.x = wnpc.globalPosition.x - _lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE;
					 wnpc.y = wnpc.globalPosition.y - _lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE;
				 }
			 }
			 
			 for each (wnpc in _walkingNPCs)
			 {
				 wnpc.tick();
				 
				 npcTilePosX = wnpc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = wnpc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if(npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos)
				 {
					/* if(!this.contains(wnpc))
					 {
						 addChild(wnpc);
					 }*/
					 wnpc.x = wnpc.globalPosition.x - _lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE;
					 wnpc.y = wnpc.globalPosition.y - _lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE;
				 }
			 }
			 
			 for each (wnpc in _birds)
			 {
				 wnpc.tick();
				 
				 npcTilePosX = wnpc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = wnpc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if(npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos)
				 {
					/* if(!this.contains(wnpc))
					 {
						 addChild(wnpc);
					 }*/
					 wnpc.x = wnpc.globalPosition.x - _lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE;
					 wnpc.y = wnpc.globalPosition.y - _lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE;
				 }
			 }
			 
			 for each (snpc in _staticNPCs)
			 {
				 npcTilePosX = snpc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = snpc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if(npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos)
				 {
					/* if(!this.contains(snpc))
					 {
						 addChild(snpc);
					 }*/
					 snpc.x = snpc.globalPosition.x - _lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE;
					 snpc.y = snpc.globalPosition.y - _lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE;
				 }
			 }
		 }
		 
		 private function onDeadTimer(e:TimerEvent):void
		 {
			 checkDeadBirds();
		 }
		 
		 private function checkDeadBirds():void
		 {
			 var len:int =_birds.length;
			 for(var i:int = 0; i < len; i++)
			 {
				 wnpc = _birds[i];
				 if(Bird(wnpc).isDead)
				 {
					 _npcsLayer.removeChild(wnpc, true);
					 _birds.splice(i,1);
					 i--;
					 len =_birds.length;
				 }
			 }
		 }
		 
		 
		 /* 
		 * EDITOR FUNCTIONS
		 */
		 private var touches:Vector.<Touch>;
		 private var touchesMoved:Vector.<Touch>
		 private var touch:Touch;
		 private var point:Point = new Point();
		 private var mapPos:Point = new Point();
		 private var touchedImage:Image = null;
		 private function onTouch(event:TouchEvent):void
		 {
			 touches = event.getTouches(this, TouchPhase.MOVED);
			 touchesMoved = event.getTouches(this, TouchPhase.HOVER);
			 if(touchesMoved.length > 0)
			 {
				 touches = touchesMoved;
			 }
			 
			 if (touches.length > 0)
			 {
				touch = touches[0];
				
				point.setTo(touch.globalX, touch.globalY);
				mapPos.setTo(int((touch.globalX-this.x)/CommonVars.TILE_SIDE_SIZE)+_lastRedrawnXPos, 
					int((touch.globalY-this.y)/CommonVars.TILE_SIDE_SIZE)+_lastRedrawnYPos);
				
				_currentlyTouchedPosition.x = (touch.globalX-this.x) + _lastRedrawnXPos * CommonVars.TILE_SIDE_SIZE;
				_currentlyTouchedPosition.y = (touch.globalY-this.y) + _lastRedrawnYPos * CommonVars.TILE_SIDE_SIZE;
				//trace(_currentlyTouchedPosition);
				
				_currentPointOnBackground.x = touch.globalX;
				_currentPointOnBackground.y = touch.globalY;
				
				_currentlyTouchedTilePos.setTo(int(mapPos.x), int(mapPos.y));
				touchedImage = null;
				if(_tiles[mapPos.x]) _currentlyToucheTile = _tiles[mapPos.x][mapPos.y];
				touchedImage = null;
				if(_currentlyToucheTile)
				{
					touchedImage = _currentlyToucheTile.image;
				}
				if(touchedImage)
				{
					_currentlyTouchedTileImage = touchedImage;
				}
				else
				{
					_currentlyTouchedTileImage = null;
				}
			 }
			 else
			 {
				 _currentlyTouchedTileImage = null;
			 }
		 } 
		 
		 //sets new tile to pos
		public function setNewMapTileAsset(type:int, imageName:String, xPos:int, yPos:int, needRedraw:Boolean = false):void
		{
			var screenPos:Point = new Point(xPos, yPos);
			
			var tile:Tile = _tiles[xPos][yPos];
			if(tile)
			{
				_ptpController.removeBasement(screenPos);
				if(type == Asset.BUILDING_PARTS || type == Asset.TILE_NON_WALKABLE || type == Asset.OBSTACLE)
				{
					setTileWalkability(screenPos, false);
					
					if(type == Asset.BUILDING_PARTS && EditorView.editorAssets.getAssetSubtype(imageName) == Tile.BASEMENT_TILE)
					{
						_ptpController.addBasement(screenPos);
					}
				}
				_ptpController.removeShop(screenPos);
				
				if(type == Asset.SHOP)
				{
					addNewShop(screenPos, imageName);
					return;
				}
				
				if(type == Asset.TILE_WALKABLE && !isWalkable(screenPos))
				{
					setTileWalkability(screenPos, true);
				}
				tile.type = type;
				tile.imageName = imageName;
				tile.redraw();
				if(needRedraw)
				{
					redrawAllBackground(_lastRedrawnXPos, _lastRedrawnYPos);
				}
			}
		}
		
		// sets walkability or non-walkability for tile
		private function setTileWalkability(pos:Point, isWalkable:Boolean):void
		{
			_tilesWalkability[int(pos.x)][int(pos.y)] = isWalkable;
			trace("tw ", pos, isWalkable);
			if(isWalkable)
			{
				UtilsMath.astar.walkability[int(pos.x)][int(pos.y)] =  AStarLib.WALKABLE;
				UtilsMath.rayTracing.walkability[int(pos.x)][int(pos.y)] = UtilsMath.astar.walkability[int(pos.x)][int(pos.y)];			
			}
			else
			{
				UtilsMath.astar.walkability[int(pos.x)][int(pos.y)] = AStarLib.UNWALKABLE;
				UtilsMath.rayTracing.walkability[int(pos.x)][int(pos.y)] = UtilsMath.astar.walkability[int(pos.x)][int(pos.y)];
			}	
			
		}
		
		public function addNewWritingObject(assetName:String, tilePos:Point):void
		{
			var ww:WallWriting = new WallWriting(assetName, tilePos);
			
			_wallWritings.push(ww);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killWritingObjectAt(pos:Point):void
		{
			var index:int = 0;
			for each (var ww:WallWriting in _wallWritings)
			{
				if(ww.tilePos.equals(pos))
				{
					ww.justKill();
					_wallWritings.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
			
		}
		
		public function addNewWashingObject(assetName:String, tilePos:Point):void
		{
			var wp:WashPlace = new WashPlace(assetName, tilePos);
			
			_washingPlaces.push(wp);
			setTileWalkability(tilePos, false);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killWashingObjectAt(pos:Point):void
		{
			var index:int = 0;
			for each (var wp:WashPlace in _washingPlaces)
			{
				if(wp.tilePos.equals(pos))
				{
					wp.justKill();
					_washingPlaces.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
			
		}
		
		public function resetAllWashingPlacesAnimation():void
		{
			for each (var wp:WashPlace in _washingPlaces)
			{
				wp.restartAnimation();
			}
		}
		
		public function addPukeUnderDude():void
		{
			addNewEnvironmentObject(0, "Puke", Game.instance.dude.globalPosition, Game.instance.dude.centerTilePos);
		}
		
		public function addNewEnvironmentObject(type:int, assetName:String, position:Point, tilePos:Point):void
		{
			var env:Puke = new Puke(position, tilePos, assetName, type);
			
			_environments.push(env);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killEnvironmentObjectAt(pos:Point):void
		{
			var index:int = 0;
			for each (var env:Puke in _environments)
			{
				if(env.tilePos.equals(pos))
				{
					env.justKill();
					_environments.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
			
		}
		
		public function killEnvironmentObject(id:int):void
		{
			var index:int = 0;
			for each (var env:Puke in _environments)
			{
				if(env.id == id)
				{
					env.justKill();
					_environments.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addNewCollectableOnMap(type:int, assetName:String, position:Point, tilePos:Point):void
		{
			var collectable:CollectableObject;
			switch(type)
			{
				case CollectableObject.FOOD:
					collectable = new Food(position, tilePos);
					break;
				case CollectableObject.BUZZ:
					collectable = new Buzz(position, tilePos);
					break;
				case CollectableObject.DEAD_RAT:
					collectable = new DeadRat(position, tilePos);
					collectable.generateGraphicObject();
					break;
				case CollectableObject.COIN:
					collectable = new Coin(position, tilePos);
					collectable.generateGraphicObject();
					break;
				default:
					collectable = new CollectableObject(position, tilePos, assetName, type);
					collectable.generateGraphicObject();
					collectable.scaleX = 0.5;
					collectable.scaleY = 0.5;
					break;
				
			}
			_collectables.push(collectable);
			var tile:Tile = _tiles[collectable.tilePos.x][collectable.tilePos.y] as Tile;
			tile.addCollectable(collectable);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addRandomTrashOnMap(position:Point, tilePos:Point):void
		{
			var asset:Asset = EditorView.editorAssets.getRandomTrash();
			
			var trash:Trash = new Trash(position, tilePos, asset.name);
			_trash.push(trash);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killAllTrash():void
		{
			var trash:Trash;
			while(_trash.length)
			{
				trash = _trash.pop();
				_collectablesLayer.removeChild(trash, true);
			}
		}
		
		public function addRandomUsefulTrash(position:Point, tilePos:Point):void
		{
			var asset:Asset = EditorView.editorAssets.getRandomUsefulTrash(); 
			
			addNewCollectableOnMap(asset.subType, asset.name, position, tilePos);
		}
		
		public function killAllUsefulTrash():void
		{
			
		}
		
		public function killCollectableAt(pos:Point):void
		{
			var index:int = 0;
			var tile:Tile = this._tiles[pos.x][pos.y];
			if(tile.collectables.length > 0)
			{
				var collectable:CollectableObject = tile.collectables[0] as CollectableObject;
				killCollectable(collectable.id);
			}
		}
		
		public function killCollectable(id:int):void
		{
			var index:int = 0;
			for each (var collectable:CollectableObject in _collectables)
			{
				if(collectable.id == id)
				{
					collectable.justKill();
					_collectables.splice(index, 1);
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function collectCollectable(id:int):Boolean
		{
			var index:int = 0;
			for each (var collectable:CollectableObject in _collectables)
			{
				if(collectable.id == id)
				{
					if(collectable.collect())
					{
						_collectables.splice(index, 1);
						redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
						return true;
					}
				}
				index++;
			}
			
			return false;
		}
		
		public function addNewNPC(type:int, tilePos:Point, assetInfo:NPCAssetInfo):NPC
		{
			var npc:NPC;
			switch(type)
			{
				case NPC.COP:
					npc = new PoliceManNPC(tilePos, assetInfo);
					_enemyes.push(npc);
					break;
				case NPC.STATIC_NPC:
					npc = new StaticNPC(tilePos, assetInfo);
					_staticNPCs.push(npc);
					setTileWalkability(tilePos, false);
					break;
				case NPC.WALKING_NPC:
					npc = new MovingNPC(tilePos, NPC.WALKING_NPC, assetInfo);
					_walkingNPCs.push(npc);
					break;
				case NPC.DOG:
					npc = new Dog(tilePos, NPC.DOG, assetInfo);
					_walkingNPCs.push(npc);
					break;
				case NPC.TERRITORY_NPC:
					npc = new TerritoryNPC(tilePos, NPC.TERRITORY_NPC, assetInfo);
					_enemyes.push(npc);
					break;
				case NPC.BIRD:
					npc = new Bird(tilePos, NPC.BIRD, assetInfo);
					_birds.push(npc);
					break;
				case NPC.PROSTITUTE:
					npc = new Prostitute(tilePos, NPC.PROSTITUTE, assetInfo);
					_walkingNPCs.push(npc);
					break;
				case NPC.AFGAN_MUSICANT:
					npc = new AfganMusicant(tilePos, NPC.AFGAN_MUSICANT, assetInfo);
					_walkingNPCs.push(npc);
					break;
				case NPC.PIMP:
					npc = new Pimp(tilePos, NPC.PIMP, assetInfo);
					_enemyes.push(npc);
					break;
				case NPC.MISSIONER:
					npc = new Missioner(tilePos, NPC.MISSIONER, assetInfo);
					_walkingNPCs.push(npc);
					break;
				case NPC.QUEST_NPC:
					npc = new QuestNPC(tilePos, assetInfo);
					_staticNPCs.push(npc);
					break;
			}
			
			
			
			var tile:Tile = _tiles[npc.tilePos.x][npc.tilePos.y] as Tile;
			tile.addNPC(npc);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
			
			return npc;
		}
		
		private var startPos:Point = new Point();
		private const NUM_POINTS_TO_CHECK:int = 9;
		public function checkInteractionWithBirds(dudePos:Point):void
		{
			startPos.setTo(dudePos.x - 1, dudePos.y - 1);
			var i:int;
			var curX:int;
			var curY:int;
			var curTile:Tile;
			var curTileCollectables:Vector.<CollectableObject>;
			var collectable:CollectableObject;
			for(i = 0; i < NUM_POINTS_TO_CHECK; i++)
			{
				for each (var bird:Bird in _birds)
				{
					if(!bird.isFlying())
					{
						curX = int(startPos.x) + i%3;
						curY = int(startPos.y) + i/3;
						curTile = getTile(curX, curY);
						if(curTile)
						{
							
							var posToCheck:Point = GameUtils.posToTile(bird.globalPosition);
							if(posToCheck.equals(new Point(curX, curY)))
							{
								bird.flyAway();
							}
						}
					}
				}
			}
		}
		
		public function addReadyNPC(npc:NPC):void
		{
			if(npc is EnemyCharacter)
			{
				_enemyes.push(npc);
			}
			else
			{
				if(npc is MovingNPC)
				{
					_walkingNPCs.push(npc);
				}
				else if(npc is Bird)
				{
					_birds.push(npc);
				}
				else
				{
					_staticNPCs.push(npc);
				}
			}

			var tile:Tile = _tiles[npc.tilePos.x][npc.tilePos.y] as Tile;
			tile.addNPC(npc);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos)
		}
		
		public function killNPC(id:int):void
		{
			var index:int = 0;
			for each (var npc:NPC in _staticNPCs)
			{
				if(npc.id == id)
				{
					if(npc.type == NPC.STATIC_NPC)
					{
						//setNonWalkable(npc.initialPos, false);
						setTileWalkability(npc.centerTilePos, true);
					}
					npc.kill();
					_staticNPCs.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
			index = 0;
			
			var wnpc:MovingNPC;
			for each (wnpc in _walkingNPCs)
			{
				if(wnpc.id == id)
				{
					wnpc.kill();
					_walkingNPCs.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return;
				}
				index++;
			}
			index = 0;
			for each (wnpc in _enemyes)
			{
				if(wnpc.id == id)
				{
					wnpc.kill();
					_enemyes.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
			
			index = 0;
			for each (wnpc in _birds)
			{
				if(wnpc.id == id)
				{
					wnpc.kill();
					_birds.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
		}
		
		public function addNewSleepingPlace(tilePos:Point, name:String, type:int):void
		{
			var sp:SleepingPlace = new SleepingPlace(name, tilePos, type);
			
			_sleepingPlaces.push(sp);
			setTileWalkability(tilePos, false);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killSleepingPlace(tilePos:Point):void
		{
			var index:int = 0;
			for each (var sp:SleepingPlace in _sleepingPlaces)
			{
				if(sp.tilePos.equals(tilePos))
				{
					setTileWalkability(tilePos, true);
					sp.justKill();
					_sleepingPlaces.splice(index, 1);
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addNewSmashable(tilePos:Point, name:String):void
		{
			var smashable:SmashableObject = new Shit(name, tilePos);
			
			_smashable.push(smashable);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killSmashable(tilePos:Point):void
		{
			var index:int = 0;
			for each (var smashable:SmashableObject in _smashable)
			{
				if(smashable.tilePos.equals(tilePos))
				{
					smashable.justKill();
					_smashable.splice(index, 1);
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		} 
		
		public function addNewObstacle(tilePos:Point, name:String, type:int):void
		{
			var obstacle:Obstacle;
			switch(type)
			{
				case Obstacle.SIMPLE:
					obstacle = new Obstacle(name, tilePos, type);
					break;
				case Obstacle.HORIZONTAL2:
					obstacle = new Ad(name, tilePos, type);
					setTileWalkability(new Point(tilePos.x, tilePos.y+1), false);
					break;
				case Obstacle.VERTICAL2:
					obstacle = new Ad(name, tilePos, type);
					setTileWalkability(new Point(tilePos.x+1, tilePos.y), false);
					break;
			}
			
			_obstacles.push(obstacle);
			setTileWalkability(tilePos, false);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killObstacle(tilePos:Point):void
		{
			var index:int = 0;
			for each (var obstacle:Obstacle in _obstacles)
			{
				if(obstacle.tilePos.equals(tilePos))
				{
					setTileWalkability(tilePos, true);
					obstacle.justKill();
					_obstacles.splice(index, 1);
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addNewShop(tilePos:Point, name:String):void
		{
			_ptpController.addShop(tilePos);
			var shop:SimpleShop = new SimpleShop(tilePos, name);
			_shops.push(shop);
			_shopsOnMap[shop.mapPosition.x][shop.mapPosition.y] = shop;
			setTileWalkability(tilePos, false);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killShop(tilePos:Point):void
		{
			var index:int = 0;
			for each (var shop:SimpleShop in _shops)
			{
				if(shop.mapPosition.equals(tilePos))
				{
					_ptpController.removeShop(tilePos);
					setTileWalkability(tilePos, true);
					_shopsOnMap[shop.mapPosition.x][shop.mapPosition.y] = null;
					shop.justKill();
					_shops.splice(index, 1);
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addNewPlace(tilePos:Point, name:String):void
		{
			_ptpController.addChurchPlace(tilePos);
			var place:Place = new Place(tilePos, name);
			_places.push(place);
			_shopsOnMap[place.mapPosition.x][place.mapPosition.y] = place;
			setTileWalkability(tilePos, false);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killPlace(tilePos:Point):void
		{
			var index:int = 0;
			for each (var place:Place in _places)
			{
				if(place.mapPosition.equals(tilePos))
				{
					_ptpController.removeChurchPlace(tilePos);
					_shopsOnMap[place.mapPosition.x][place.mapPosition.y] = null;
					setTileWalkability(tilePos, true);
					place.justKill();
					_places.splice(index, 1);
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addNewContainer(tilePos:Point, type:uint, name:String):void
		{
			var container:Container = new Container(name, type, tilePos);
			
			_containers.push(container);
			setTileWalkability(tilePos, false);
			
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function killContainer(tilePos:Point):void
		{
			var index:int = 0;
			for each (var container:Container in _containers)
			{
				if(container.tilePos.equals(tilePos))
				{
					setTileWalkability(tilePos, true);
					container.justKill();
					_containers.splice(index, 1);
				}
				index++;
			}
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addNewExitPoint(tilePos:Point, targetMap:String):void
		{
			var exit:ExitPoint = new ExitPoint(tilePos);
			exit.targetMap = targetMap;
			Game.instance.dude.addGlobalGoPoint(exit);
		}
		
		public function killExitPoint(tilePos:Point):void
		{
			Game.instance.dude.removeGlobalGoPoint(tilePos);
			redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
		}
		
		public function addNewPlayerStartPos(tilePos:Point, type:int = -1, fromMap:String = ""):void
		{
			var psp:PlayerStartPosition = new PlayerStartPosition(tilePos, type, fromMap);
			_playerStartPositions.push(psp);
		}
		
		public function killPlayerStartPos(tilePos:Point):void
		{
			var index:int = 0;
			for each (var psp:PlayerStartPosition in _playerStartPositions)
			{
				if(psp.tilePos.equals(tilePos))
				{
					psp.justKill();
					_playerStartPositions.splice(index, 1);
					redrawEverything(_lastRedrawnXPos, _lastRedrawnYPos);
					return ;
				}
				index++;
			}
		}
		
		/*
		* REDRAW FUNCTIONS
		*/
		
		public function forceRedraw(editor:Boolean = false):void
		{
			var redrawX:int;
			var redrawY:int
			if(editor)
			{
				redrawX = Game.instance.dude.centerTilePos.x - _xTilesCanFit/2 + 1;
				redrawY = Game.instance.dude.centerTilePos.y - _yTilesCanFit/2 + 1;
			}
			else
			{
				redrawX = Game.instance.dude.startPos.x - _xTilesCanFit/2 + 1;
				redrawY = Game.instance.dude.startPos.y - _yTilesCanFit/2 + 1;
				
				var addOffsetTiles:Point = new Point(0,0);
				if(Game.instance.dude.x != CommonVars.screenHalfWidth)
				{
					addOffsetTiles.x = (CommonVars.screenHalfWidth - Game.instance.dude.x) / CommonVars.TILE_SIDE_SIZE
				}
				
				if(Game.instance.dude.y != CommonVars.screenHalfHeight)
				{
					addOffsetTiles.y = (CommonVars.screenHalfHeight - Game.instance.dude.y) / CommonVars.TILE_SIDE_SIZE
				}
				//addOffsetTiles = UtilsMath.toClosestTilePos(addOffsetTiles);
				redrawX += addOffsetTiles.x;
				redrawY += addOffsetTiles.y;
			}
			redrawAllBackground(redrawX < 0 ? 0 : redrawX, redrawY < 0 ? 0 : redrawY);
			redrawEverything(redrawX < 0 ? 0 : redrawX, redrawY < 0 ? 0 : redrawY);
		}
		
		public function redrawMapWithEditor(editorPos:Point):void
		{
			//check we need to move map
			if(_lastRedrawnXPosOffset != editorPos.x || _lastRedrawnYPosOffset != editorPos.y)
			{
				_lastRedrawnXPosOffset = editorPos.x;
				_lastRedrawnYPosOffset = editorPos.y;
				
				var xStartPos:int = (editorPos.x-(CommonVars.WIDTH >> 1))/CommonVars.TILE_SIDE_SIZE;
				var yStartPos:int = (editorPos.y-(CommonVars.HEIGHT >> 1))/CommonVars.TILE_SIDE_SIZE;
				
				var needRedraw:Boolean = false;
				
				if(xStartPos != _lastRedrawnXPos || yStartPos != _lastRedrawnYPos)
				{
					needRedraw = true;
				}
				
				if(needRedraw)
				{			
					
					if(xStartPos != _lastRedrawnXPos || yStartPos != _lastRedrawnYPos)
					{
						redrawEverything(xStartPos, yStartPos);
					}
				}
				
				var startXOffset:int = (editorPos.x-(CommonVars.WIDTH >> 1))%CommonVars.TILE_SIDE_SIZE;
				var startYOffset:int = (editorPos.y-(CommonVars.HEIGHT >> 1))%CommonVars.TILE_SIDE_SIZE;
				
				this._backgroundLayer.x = -(_lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE);
				this._backgroundLayer.y = -(_lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE);
				
				this.x = -startXOffset;
				this.y = -startYOffset;
			}
		}
		 		 
		 public function redrawMapWithPos(dudePos:Point):void
		 {
			 //check we need to move map
			 if(_lastRedrawnXPosOffset != dudePos.x || _lastRedrawnYPosOffset != dudePos.y)
			 {
				 _lastRedrawnXPosOffset = dudePos.x;
				 _lastRedrawnYPosOffset = dudePos.y;
				 
				 var xStartPos:int = (dudePos.x-(CommonVars.WIDTH >> 1))/CommonVars.TILE_SIDE_SIZE;
				 var yStartPos:int = (dudePos.y-(CommonVars.HEIGHT >> 1))/CommonVars.TILE_SIDE_SIZE;
				 
				 var needRedraw:Boolean = false;
				 
				 if(xStartPos != _lastRedrawnXPos || yStartPos != _lastRedrawnYPos)
				 {
					 needRedraw = true;
				 }
				 
				 if(needRedraw)
				 {			
					 if(xStartPos < 0) xStartPos = 0;
					 if(yStartPos < 0) yStartPos = 0;
					 
					 if(xStartPos > CurrentMapParameters.sizeX - _xTilesCanFit) xStartPos = CurrentMapParameters.sizeX - _xTilesCanFit;
					 if(yStartPos > CurrentMapParameters.sizeY - _yTilesCanFit) yStartPos = CurrentMapParameters.sizeY - _yTilesCanFit;
					 
					 if(xStartPos != _lastRedrawnXPos || yStartPos != _lastRedrawnYPos)
					 { 
						 redrawEverything(xStartPos, yStartPos);
					 }
				 } 
				 
				 //counting background moving offset
				 var xTopDudePos:Number = CurrentMapParameters.sizeX*CommonVars.TILE_SIDE_SIZE - (CommonVars.WIDTH >> 1);
				 var yTopDudePos:Number = CurrentMapParameters.sizeY*CommonVars.TILE_SIDE_SIZE - (CommonVars.HEIGHT >> 1);
				 
				 var startXOffset:int; 
				 var startYOffset:int;
				 if(dudePos.x >= xTopDudePos)
				 {
					 startXOffset = CommonVars.TILE_SIDE_SIZE;
				 }
				 else
				 {
					 startXOffset = (dudePos.x-(CommonVars.WIDTH >> 1))%CommonVars.TILE_SIDE_SIZE;
				 }
				 
				 if(dudePos.y >= yTopDudePos)
				 {
					 startYOffset = CommonVars.TILE_SIDE_SIZE;
				 }
				 else
				 {
					 startYOffset = (dudePos.y-(CommonVars.HEIGHT >> 1))%CommonVars.TILE_SIDE_SIZE;
				 }
				 
				 this.x = -startXOffset;
				 this.y = -startYOffset;
				  
				 this._backgroundLayer.x = -(_lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE);
				 this._backgroundLayer.y = -(_lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE);
				// trace("backxy", _backgroundLayer.x, _backgroundLayer.y);
				 
				 if(this.x > 0) this.x = 0;
				 if(this.y > 0) this.y = 0;
			 }
		 }
		 
		 public function redrawAllBackground(xStartPos:int, yStartPos:int):void
		 {
			 _lastRedrawnXPos = xStartPos;
			 _lastRedrawnYPos = yStartPos
			 
			 while(_backgroundLayer.numChildren > 0)
			 {
				 _backgroundLayer.removeChildAt(0);
			 }
			 
			 var s:Sprite;
			 var index:int;
			 var xOffset:int;
			 var yOffset:int;
			 var image:Image;
			 var x:int;
			 var y:int;
			 
			 for(x = xStartPos; x < _xTilesCanFit+xStartPos; x++)
			 {
				 for(y = yStartPos; y < _yTilesCanFit+yStartPos; y++)
				 {
					 if( x >= 0 && y >= 0 && x < CurrentMapParameters.sizeX && y < CurrentMapParameters.sizeY)
					 {
						 _tiles[x][y].redrawIfNeeded();
						 image = _tiles[x][y].image;
						 
						 image.x = x*CommonVars.TILE_SIDE_SIZE;
						 image.y = y*CommonVars.TILE_SIDE_SIZE;
						 _backgroundLayer.addChild(image);
					 }
				 }
				 
			 }
		 }
		 
		 //redraw helper-function
		 private var img:Image;
		 private function addTileImage(x:int, y:int):void
		 {
			 if(x<0 || y<0 || x>=CurrentMapParameters.sizeX || y>=CurrentMapParameters.sizeY)
			 {
				 return ;
			 }
			 _tiles[x][y].redrawIfNeeded();
			 img = _tiles[x][y].image;
			 
			 img.x = x*CommonVars.TILE_SIDE_SIZE;
			 img.y = y*CommonVars.TILE_SIDE_SIZE;
			 
			 _backgroundLayer.addChild(img);
		 }
		 
		 private function redrawXLine(xStartPos:int, yStartPos:int):void
		 {
			 var yRDiff:int = yStartPos-_lastRedrawnYPos;
			 var yRedrawLines:int = Math.abs(yRDiff);
			 
			 var image:Image;
			 var xIndex:int;
			 var yIndex:int;
			 
			 for(var xR:int = 0; xR < yRedrawLines*_xTilesCanFit; xR++)
			 {
				 if(yRDiff > 0)
				 {
					 xIndex = _lastRedrawnXPos + xR;
					 yIndex = _lastRedrawnYPos;
					 
					 if(xIndex >= 0 && yIndex >= 0 && xIndex < CurrentMapParameters.sizeX && yIndex < CurrentMapParameters.sizeY)
					 {
						 image = _tiles[xIndex][yIndex].image;
						 if(image)
						 {
							 _backgroundLayer.removeChild(image);
						 }
					 }
						 
					 xIndex = _lastRedrawnXPos + xR;
					 yIndex = _lastRedrawnYPos+_yTilesCanFit;
					 
					 addTileImage(xIndex,yIndex);
				 }
				 else
				 {
					 xIndex = _lastRedrawnXPos + xR;
					 yIndex = yStartPos+_yTilesCanFit;
					 
					 if(xIndex >= 0 && yIndex >= 0 && xIndex < CurrentMapParameters.sizeX && yIndex < CurrentMapParameters.sizeY)
					 {
						 image = _tiles[xIndex][yIndex].image;
						 if(image)
						 {
							 _backgroundLayer.removeChild(image);
						 }
					 }
						 
					 xIndex = _lastRedrawnXPos + xR;
					 yIndex = yStartPos;
					 
					 addTileImage(xIndex,yIndex);
				 }
				
			 }
		 }
		 
		 private function redrawYLine(xStartPos:int, yStartPos:int):void
		 {
			 var xRDiff:int = xStartPos-_lastRedrawnXPos;
			 var xRedrawLines:int = Math.abs(xRDiff);
			 
			 var image:Image;
			 var xIndex:int;
			 var yIndex:int;
			 
			 for(var yR:int = 0; yR < xRedrawLines*_yTilesCanFit; yR++)
			 {
				 if(xRDiff > 0)
				 {
					 xIndex = _lastRedrawnXPos;
					 yIndex = _lastRedrawnYPos + yR;
					 
					 if(xIndex >= 0 && yIndex >= 0 && xIndex < CurrentMapParameters.sizeX && yIndex < CurrentMapParameters.sizeY)
					 {
						 image = _tiles[xIndex][yIndex].image;
						 if(image)
						 {
							 _backgroundLayer.removeChild(image);
						 }
					 }
					 
					 xIndex = _lastRedrawnXPos+_xTilesCanFit;
					 addTileImage(xIndex,yIndex);
					 
				 }
				 else
				 {
					 xIndex = xStartPos + _xTilesCanFit;
					 yIndex = _lastRedrawnYPos + yR;
					 
					 if(xIndex >= 0 && yIndex >= 0 && xIndex < CurrentMapParameters.sizeX && yIndex < CurrentMapParameters.sizeY)
					 {
						 image = _tiles[xIndex][yIndex].image;
						 if(image)
						 {
							 _backgroundLayer.removeChild(image);
						 }
					 }
					 
					 xIndex = xStartPos;
					 yIndex = _lastRedrawnYPos + yR;
					 addTileImage(xIndex,yIndex);
				 }
				 
			 }
		 }
		 
		 private var exit:ExitPoint;
		 private var psp:PlayerStartPosition;
		 private function redrawEverything(xStartPos:int, yStartPos:int):void
		 {
			 var xRDiff:int = xStartPos-_lastRedrawnXPos;
			 var yRDiff:int = yStartPos-_lastRedrawnYPos;
			
			 redrawXLine(xStartPos, yStartPos);
			 redrawYLine(xStartPos, yStartPos);
			 
			 if(xRDiff != 0 && yRDiff != 0)
			 {
			 	addTileImage(xRDiff > 0 ? _lastRedrawnXPos + _xTilesCanFit : xStartPos, yRDiff > 0 ? _lastRedrawnYPos + _yTilesCanFit : yStartPos);
			 }
			 
			 var xMinPos:Number = xStartPos*CommonVars.TILE_SIDE_SIZE;
			 var yMinPos:Number = yStartPos*CommonVars.TILE_SIDE_SIZE;
			 var xMaxPos:Number = xMinPos + _xTilesCanFit*CommonVars.TILE_SIDE_SIZE;
			 var yMaxPos:Number = yMinPos + _yTilesCanFit*CommonVars.TILE_SIDE_SIZE;
			 var collectable:CollectableObject;
			 for each (collectable in _collectables)
			 {
			 	if(collectable.initialPos.x >= xMinPos && collectable.initialPos.x <= xMaxPos &&
					collectable.initialPos.y >= yMinPos && collectable.initialPos.y <= yMaxPos)
				{
					if(!collectable.isAddedToBackground())
					{
						_collectablesLayer.addChild(collectable);
					}
					collectable.x = collectable.initialPos.x - xMinPos;
					collectable.y = collectable.initialPos.y - yMinPos;
					//trace("colinp:",collectable.initialPos.x,collectable.initialPos.y);
					//trace("mpcolinp:",collectable.x,collectable.y);
				}
				else
				{
					if(collectable.isAddedToBackground())
					{
						_collectablesLayer.removeChild(collectable);
					}
				}
			 }
			 
			 var shop:SimpleShop;
			 for each (shop in _shops)
			 {
				 if(shop.initialPos.x >= xMinPos && shop.initialPos.x <= xMaxPos &&
					 shop.initialPos.y >= yMinPos && shop.initialPos.y <= yMaxPos)
				 {
					 if(!shop.isAddedToBackground())
					 {
						 _obstaclesLayer.addChild(shop);
					 }
					 shop.x = shop.initialPos.x - xMinPos;
					 shop.y = shop.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(shop.isAddedToBackground())
					 {
						 _obstaclesLayer.removeChild(shop);
					 }
				 }
				 
			 }
			 
			 var place:Place;
			 for each (place in _places)
			 {
				 if(place.initialPos.x >= xMinPos && place.initialPos.x <= xMaxPos &&
					 place.initialPos.y >= yMinPos && place.initialPos.y <= yMaxPos)
				 {
					 if(!place.isAddedToBackground())
					 {
						 _obstaclesLayer.addChild(place);
					 }
					 place.x = place.initialPos.x - xMinPos;
					 place.y = place.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(place.isAddedToBackground())
					 {
						 _obstaclesLayer.removeChild(place);
					 }
				 }
				 
			 }
			 
			 var ww:WallWriting;
			 for each (ww in _wallWritings)
			 {
				 if(ww.initialPos.x >= xMinPos && ww.initialPos.x <= xMaxPos &&
					 ww.initialPos.y >= yMinPos && ww.initialPos.y <= yMaxPos)
				 {
					 if(!ww.isAddedToBackground())
					 {
						 _writingsLayer.addChild(ww);
					 }
					 ww.x = ww.initialPos.x - xMinPos;
					 ww.y = ww.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(ww.isAddedToBackground())
					 {
						 _writingsLayer.removeChild(ww);
					 }
				 }
			 }
			 
			 var trash:Trash;
			 for each (trash in _trash)
			 {
				 if(trash.initialPos.x >= xMinPos && trash.initialPos.x <= xMaxPos &&
					 trash.initialPos.y >= yMinPos && trash.initialPos.y <= yMaxPos)
				 {
					 if(!trash.isAddedToBackground())
					 {
						 _collectablesLayer.addChild(trash);
					 }
					 trash.x = trash.initialPos.x - xMinPos;
					 trash.y = trash.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(trash.isAddedToBackground())
					 {
						 _collectablesLayer.removeChild(trash);
					 }
				 }
			 }
			 
			 var wp:WashPlace;
			 for each (wp in _washingPlaces)
			 {
				 if(wp.initialPos.x >= xMinPos && wp.initialPos.x <= xMaxPos &&
					 wp.initialPos.y >= yMinPos && wp.initialPos.y <= yMaxPos)
				 {
					 if(!wp.isAddedToBackground())
					 {
						 _obstaclesLayer.addChild(wp);
					 }
					 wp.x = wp.initialPos.x - xMinPos;
					 wp.y = wp.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(wp.isAddedToBackground())
					 {
						 _obstaclesLayer.removeChild(wp);
					 }
				 }
			 }
			 
			 var env:Puke;
			 for each (env in _environments)
			 {
				 if(env.initialPos.x >= xMinPos && env.initialPos.x <= xMaxPos &&
					 env.initialPos.y >= yMinPos && env.initialPos.y <= yMaxPos)
				 {
					 if(!env.isAddedToBackground())
					 {
						 _collectablesLayer.addChild(env);
					 }
					 env.x = env.initialPos.x - xMinPos;
					 env.y = env.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(env.isAddedToBackground())
					 {
						 _collectablesLayer.removeChild(env);
					 }
				 }
			 }
			 
			 var npc:NPC;
			 var wnpc:MovingNPC;
			 var xCompareTopPos:int;
			 var yCompareTopPos:int;
			 var xCompareBottomPos:int;
			 var yCompareBottomPos:int;
			 var npcTilePosX:int;
			 var npcTilePosY:int;
			 
			 xCompareTopPos = (xStartPos-1);
			 yCompareTopPos = (yStartPos-1);
			 xCompareBottomPos = (xStartPos + _xTilesCanFit + 1);
			 yCompareBottomPos = (yStartPos + _yTilesCanFit + 1);
			 for each (npc in _staticNPCs)
			 {
				 npcTilePosX = npc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = npc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if((npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos) ||
					 Game.instance.currentState == Game.EDITOR_STATE)
				 {
					 if(!npc.isAddedToBackground())
					 {
					 	_npcsLayer.addChild(npc);
					 }
					 npc.x = npc.globalPosition.x - xStartPos*CommonVars.TILE_SIDE_SIZE;
					 npc.y = npc.globalPosition.y - yStartPos*CommonVars.TILE_SIDE_SIZE;
				 }
				 else
				 {
					 if(npc.isAddedToBackground())
					 {
						 _npcsLayer.removeChild(npc);
					 }
				 }
			 }
			 
			 for each (wnpc in _walkingNPCs)
			 {
				 npcTilePosX = wnpc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = wnpc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if((npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos) ||
					 Game.instance.currentState == Game.EDITOR_STATE)
				 {
					 if(!wnpc.isAddedToBackground())
					 {
						 _npcsLayer.addChild(wnpc);
					 }
					 wnpc.x = wnpc.globalPosition.x - xStartPos*CommonVars.TILE_SIDE_SIZE;
					 wnpc.y = wnpc.globalPosition.y - yStartPos*CommonVars.TILE_SIDE_SIZE;
				 }
				 else
				 {
					 if(wnpc.isAddedToBackground())
					 {
						 _npcsLayer.removeChild(wnpc);
					 }
				 }
			 }
			 
			 for each (wnpc in _birds)
			 {
				 if(Bird(wnpc).isDead)
				 {
					 continue;
				 }
				 npcTilePosX = wnpc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = wnpc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if((npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos) ||
					 Game.instance.currentState == Game.EDITOR_STATE)
				 {
					 if(!wnpc.isAddedToBackground())
					 {
						 _npcsLayer.addChild(wnpc);
					 }
					 wnpc.x = wnpc.globalPosition.x - xStartPos*CommonVars.TILE_SIDE_SIZE;
					 wnpc.y = wnpc.globalPosition.y - yStartPos*CommonVars.TILE_SIDE_SIZE;
				 }
				 else
				 {
					 if(wnpc.isAddedToBackground())
					 {
						 _npcsLayer.removeChild(wnpc);
					 }
				 }
			 }
			 
			 for each (wnpc in _enemyes)
			 {
				 npcTilePosX = wnpc.globalPosition.x/CommonVars.TILE_SIDE_SIZE;
				 npcTilePosY = wnpc.globalPosition.y/CommonVars.TILE_SIDE_SIZE;
				 if((npcTilePosX >= xCompareTopPos && npcTilePosX <= xCompareBottomPos &&
					 npcTilePosY >= yCompareTopPos && npcTilePosY <= yCompareBottomPos) ||
					 Game.instance.currentState == Game.EDITOR_STATE)
				 {
					 if(!wnpc.isAddedToBackground())
					 {
						 _npcsLayer.addChild(wnpc);
					 }
					 wnpc.x = wnpc.globalPosition.x - xStartPos*CommonVars.TILE_SIDE_SIZE;
					 wnpc.y = wnpc.globalPosition.y - yStartPos*CommonVars.TILE_SIDE_SIZE;
				 }
				 else
				 {
					 if(wnpc.isAddedToBackground())
					 {
						 _npcsLayer.removeChild(wnpc);
					 }
				 }
			 }
			 
			 var container:Container;
			 for each (container in _containers)
			 {
				 if(container.initialPos.x >= xMinPos && container.initialPos.x <= xMaxPos &&
					 container.initialPos.y >= yMinPos && container.initialPos.y <= yMaxPos)
				 {
					 if(!container.isAddedToBackground())
					 {
						 _containersLayer.addChild(container);
					 }
					 container.x = container.initialPos.x - xMinPos;
					 container.y = container.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(container.isAddedToBackground())
					 {
						 _containersLayer.removeChild(container);
					 }
				 }
			 }
			 
			 var smashable:SmashableObject;
			 for each (smashable in _smashable)
			 {
				 if(smashable.initialPos.x >= xMinPos && smashable.initialPos.x <= xMaxPos &&
					 smashable.initialPos.y >= yMinPos && smashable.initialPos.y <= yMaxPos)
				 {
					 if(!smashable.isAddedToBackground())
					 {
						 _obstaclesLayer.addChild(smashable);
					 }
					 smashable.x = smashable.initialPos.x - xMinPos;
					 smashable.y = smashable.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(smashable.isAddedToBackground())
					 {
						 _obstaclesLayer.removeChild(smashable);
					 }
				 }
			 }
			 
			 var obstalce:Obstacle;
			 for each (obstalce in _obstacles)
			 {
				 if(obstalce.initialPos.x >= xMinPos && obstalce.initialPos.x <= xMaxPos &&
					 obstalce.initialPos.y >= yMinPos && obstalce.initialPos.y <= yMaxPos)
				 {
					 if(!obstalce.isAddedToBackground())
					 {
						 _obstaclesLayer.addChild(obstalce);
					 }
					 obstalce.x = obstalce.initialPos.x - xMinPos;
					 obstalce.y = obstalce.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(obstalce.isAddedToBackground())
					 {
						 _obstaclesLayer.removeChild(obstalce);
					 }
				 }
			 }
			 
			 var sleepingPlace:SleepingPlace;
			 for each (sleepingPlace in _sleepingPlaces)
			 {
				 if(sleepingPlace.initialPos.x >= xMinPos && sleepingPlace.initialPos.x <= xMaxPos &&
					 sleepingPlace.initialPos.y >= yMinPos && sleepingPlace.initialPos.y <= yMaxPos)
				 {
					 if(!sleepingPlace.isAddedToBackground())
					 {
						 _containersLayer.addChild(sleepingPlace);
					 }
					 sleepingPlace.x = sleepingPlace.initialPos.x - xMinPos;
					 sleepingPlace.y = sleepingPlace.initialPos.y - yMinPos;
				 }
				 else
				 {
					 if(sleepingPlace.isAddedToBackground())
					 {
						 _containersLayer.removeChild(sleepingPlace);
					 }
				 }
			 }
			 
			 for each(exit in Game.instance.dude.globalGoPoints)
			 {
				 if(exit.tilePos.x >= xCompareTopPos && exit.tilePos.x <= xCompareBottomPos &&
					 exit.tilePos.y >= yCompareTopPos && exit.tilePos.y <= yCompareBottomPos)
				 {
					 if(!exit.isAddedToBackground())
					 {
					 	_obstaclesLayer.addChild(exit);
					 }
					 exit.x = exit.tilePos.x*CommonVars.TILE_SIDE_SIZE - xMinPos;
					 exit.y = exit.tilePos.y*CommonVars.TILE_SIDE_SIZE - yMinPos;		 
				 }
				 else
				 {
					 if(exit.isAddedToBackground())
					 {
						 _obstaclesLayer.removeChild(exit);
					 }
				 }
			 }
			 
			 for each(psp in _playerStartPositions)
			 {
				 if(Game.instance.currentState == Game.EDITOR_STATE)
				 {
					 if(psp.tilePos.x >= xCompareTopPos && psp.tilePos.x <= xCompareBottomPos &&
						 psp.tilePos.y >= yCompareTopPos && psp.tilePos.y <= yCompareBottomPos)
					 {
						 if(!psp.isAddedToBackground())
						 {
							 _obstaclesLayer.addChild(psp);
						 }
						 psp.x = psp.tilePos.x*CommonVars.TILE_SIDE_SIZE - xMinPos;
						 psp.y = psp.tilePos.y*CommonVars.TILE_SIDE_SIZE - yMinPos;		 
					 }
					 else
					 {
						 if(psp.isAddedToBackground())
						 {
							 _obstaclesLayer.removeChild(psp);
						 }
					 }
				 }
				 else
				 {
					 if(psp.isAddedToBackground())
					 {
						 _obstaclesLayer.removeChild(psp);
					 }
				 }
			 }
			 
			 _lastRedrawnXPos = xStartPos;
			 _lastRedrawnYPos = yStartPos;	 
		 }
		 
		 /*
		 * SCREEN/TILES FUNCTIONS
		 */
		 
		 public function getTileScreenPos(xPos:int, yPos:int):Point
		 {					 
			 var tile:Tile = _tiles[xPos][yPos];
			 if(tile && tile.image && tile.image.parent)
			 {
				 return new Point(tile.image.x + this.x, tile.image.y + this.y);
			 }
			 else
			 {
				 return null;
			 }
		 }
		 
		 public function getTile(xPos:int, yPos:int):Tile
		 {
			 if(xPos < 0 || yPos < 0 || xPos >= CurrentMapParameters.sizeX || yPos >= CurrentMapParameters.sizeY)
			 {
				 return null;
			 }
			return _tiles[xPos][yPos];
		 }
		 
		 /*
		 * SAVE/LOAD MAPS FROM XML FUNCTIONS
		 */
		 
		 public function saveXMLedBackground():void
		 {
			 var initPos:Point;
			 
			 var xml:String = new String();
			 xml+= "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n";
			 var tile:Tile;
			 
			 xml+="<background>\n"
			 
			 xml+="\t<map x=\""+CurrentMapParameters.sizeX+"\" y=\""+CurrentMapParameters.sizeY+"\" />\n";
			 for each (var psp:PlayerStartPosition in _playerStartPositions)
			 {
				 xml+="\t<start_pos x=\""+psp.tilePos.x+"\" y=\""+psp.tilePos.y+"\" type=\""+psp.type+"\" />\n";
			 }
			 
			// for(var i:int = 0; i < CommonVars.MAP_TILES_TOTAL; i++)
			for(var x:int = 0; x < CurrentMapParameters.sizeX; x++)
			{
				for(var y:int = 0; y < CurrentMapParameters.sizeY; y++)
				{
					 tile = _tiles[x][y] as Tile;
					 xml += "\t<tile x=\""+x+"\" y=\""+y+"\" type=\""+tile.type+"\" name=\""+tile.imageName+"\" />\n";
				}
			}
			
			for each (var shop:SimpleShop in _shops)
			{
				initPos = shop.mapPosition;
				xml += "\t<shop x=\""+initPos.x+"\" y=\""+initPos.y+"\" name=\""+shop.shopName+"\" />\n";
			}
			
			for each (var place:Place in _places)
			{
				initPos = place.mapPosition;
				xml += "\t<place x=\""+initPos.x+"\" y=\""+initPos.y+"\" name=\""+place.shopName+"\" />\n";
			}
			
			 for each (var collectable:CollectableObject in _collectables)
			 {
				 initPos = collectable.initialPos;
				 xml += "\t<collectable x=\""+initPos.x+
					 "\" y=\""+initPos.y+
					 "\" type=\""+collectable.type+
					 "\" name=\""+collectable.assetName+
					 "\" tilex=\""+collectable.tilePos.x+
					 "\" tiley=\""+collectable.tilePos.y+
					 "\" />\n";
			 }
			 
			 for each (var container:Container in _containers)
			 {
				 initPos = container.tilePos;
				 xml += "\t<container x=\""+initPos.x+"\" y=\""+initPos.y+"\" type=\""+0+"\" name=\""+container.objectName+"\" />\n";
			 }
			 
			 for each (var washPlace:WashPlace in _washingPlaces)
			 {
				 initPos = washPlace.tilePos;
				 xml += "\t<washing x=\""+initPos.x+"\" y=\""+initPos.y+"\" name=\""+washPlace.objectName+"\" />\n";
			 }
			 
			 for each (var ww:WallWriting in _wallWritings)
			 {
				 initPos = ww.tilePos;
				 xml += "\t<wallwriting x=\""+initPos.x+"\" y=\""+initPos.y+"\" name=\""+ww.objectName+"\" />\n";
			 }
			 
			 for each (var obstacle:Obstacle in _obstacles)
			 {
				 initPos = obstacle.tilePos;
				 xml += "\t<obstacle x=\""+initPos.x+"\" y=\""+initPos.y+"\" name=\""+obstacle.objectName+"\" type=\""+obstacle.type+"\" />\n";
			 }
			 
			 for each (var smashable:SmashableObject in _smashable)
			 {
				 initPos = smashable.tilePos;
				 xml += "\t<smashable x=\""+initPos.x+"\" y=\""+initPos.y+"\" name=\""+smashable.objectName+"\" />\n";
			 }
			 
			 for each (var sleepingPlace:SleepingPlace in _sleepingPlaces)
			 {
				 initPos = sleepingPlace.tilePos;
				 xml += "\t<sleepingPlace x=\""+initPos.x+"\" y=\""+initPos.y+"\" name=\""+sleepingPlace.objectName+"\" />\n";
			 }
			 
			 for each (var exit:ExitPoint in Game.instance.dude.globalGoPoints)
			 {
				 initPos = exit.tilePos;
				 xml += "\t<exit x=\""+initPos.x+"\" y=\""+initPos.y+"\" targetMap=\""+exit.targetMap+"\" />\n";
			 }
			 
			 var wps:Vector.<Point>;
			 var wp:Point;
			 for each (var npc:NPC in _staticNPCs)
			 {
				 initPos = npc.tilePos;
				 xml += "\t<npc x=\""+initPos.x+"\" y=\""+initPos.y+"\" type=\""+npc.type+"\" name=\""+npc.npcName+"\" >\n";
				 if(npc is PoliceManNPC)
				 {
					 wps = (npc as PoliceManNPC).waypoints;
					 for each (wp in wps)
					 {
						 xml += "\t\t<wp x=\""+wp.x+"\" y=\""+wp.y+"\" />\n";
					 }
				 }
				 xml += "\t</npc>\n";				 
			 }
			 
			 for each (var wnpc:MovingNPC in _walkingNPCs)
			 {
				 if(wnpc is Dog || wnpc is Prostitute || wnpc is AfganMusicant || wnpc is TerritoryNPC)
				 {
					 initPos = wnpc.tilePos;
					 xml += "\t<npc x=\""+initPos.x+"\" y=\""+initPos.y+"\" type=\""+wnpc.type+"\" name=\""+wnpc.npcName+"\" >\n";
					 xml += "\t</npc>\n"; 
				 }
			 }
			 
			 
			 for each (var enemy:EnemyCharacter in _enemyes)
			 {
				 initPos = enemy.tilePos;
				 xml += "\t<npc x=\""+initPos.x+"\" y=\""+initPos.y+"\" type=\""+enemy.type+"\" name=\""+enemy.npcName+"\" >\n";
				 if(enemy is PoliceManNPC)
				 {
					 wps = (enemy as PoliceManNPC).waypoints;
					 for each (wp in wps)
					 {
						 xml += "\t\t<wp x=\""+wp.x+"\" y=\""+wp.y+"\" />\n";
					 }
				 }
				 xml += "\t</npc>\n";
			 }
			 
			 for each (var bird:Bird in _birds)
			 {
				 initPos = bird.tilePos;
				 xml += "\t<npc x=\""+initPos.x+"\" y=\""+initPos.y+"\" type=\""+bird.type+"\" name=\""+bird.npcName+"\" >\n";
				 xml += "\t</npc>\n";
			 }
			 xml+="</background>\n"
			 
			 var file:FileReference = new FileReference();
			 file.save( xml, "myXML.xml" );
		 }
		 
		 private var fileRefSave:FileReference = new FileReference();
		 private var loader:Loader = new Loader();
		 private var xmlFilter:FileFilter = new FileFilter("xml", "*.xml");
		 public var urlL:URLLoader = new URLLoader();
		 public var urlR:URLRequest;
		 public function loadXMLedBackground():void
		 {
			 fileRefSave.browse([xmlFilter]);
			 fileRefSave.addEventListener(flash.events.Event.SELECT, selectedFile);
		 }
		 
		 public function forceSaveCurrentMapState():void
		 {
			 GameSavedData.instance.saveCurrentMap(_currentMapName);
		 }
		 
		 public function loadMap(name:String):void
		 {
			 if(_currentMapName.length > 0)
			 {
				 forceSaveCurrentMapState();
			 }
			 if(Game.instance.dude)
			 {
			 	Game.instance.dude.leavingMapActions();
			 }
			 if(_currentMapName.length != 0)
			 {
				 _previousMapName = _currentMapName;
			 }
			 _currentMapName = name;
			 
			 Game.instance.currentState == Game.LOADING_STATE;
			 urlR =  new URLRequest(name);            
			 urlL.addEventListener(flash.events.Event.COMPLETE, onLoadedAction);
			 urlL.load(urlR);            
		 }
		 
		 private function onLoadedAction(e:flash.events.Event):void
		 {
			 parseXML(XML(e.target.data));
			 setCurrentMapSavedData(_currentMapName);
			 Game.instance.setInitialDudePos();
			 forceRedraw();
			 Game.instance.currentState = Game.MOVING_STATE;
		 }
		 
		 private function selectedFile(e:flash.events.Event):void
		 {
			 fileRefSave.load();
			 fileRefSave.addEventListener(flash.events.Event.COMPLETE, loaded);
		 }
		 
		 private function loaded(e:flash.events.Event):void
		 {
			 var xml:XML = new XML(fileRefSave.data.readUTFBytes(fileRefSave.data.length));
			 trace(xml.toXMLString());
			 
			 parseXML(xml);
		 }
		 
		 public function resetDudePositionOnCurrentMap():void
		 {
			 if(_playerStartPositions.length > 0)
			 {
				 var wasFoundStartPt:Boolean = false;
				 for(var i:int = 0; i < _playerStartPositions.length; i++)
				 {
					 if(PlayerStartPosition(_playerStartPositions[i]).type == Game.instance.dude.lastAction)
					 {
						 if(PlayerStartPosition(_playerStartPositions[i]).fromMapName == "" || PlayerStartPosition(_playerStartPositions[i]).fromMapName == _previousMapName)
						 {
							 Game.instance.dude.resetDudePosition(PlayerStartPosition(_playerStartPositions[i]).tilePos);
							 wasFoundStartPt = true;
							 break;
						 }
					 }
				 }
				 
				 if(!wasFoundStartPt)
				 {
					 Game.instance.dude.resetDudePosition(PlayerStartPosition(_playerStartPositions[0]).tilePos);
				 }
			 }
			 else
			 {
				 Game.instance.dude.resetDudePosition(Game.instance.dude.startPos);
			 }
		 }
		 
		 private function parseXML(xml:XML):void
		 {
			 var i:int = 0;
			 var x:int;
			 var y:int;
			 var type:int;
			 var object:XML;
			 var assetName:String;
			 
				 
			CurrentMapParameters.sizeX = int(xml.map.@x);
			CurrentMapParameters.sizeY = int(xml.map.@y);
			CurrentMapParameters.resetTotalTiles();
			
			clearMapAndCreateNew(CurrentMapParameters.sizeX, CurrentMapParameters.sizeY);
			
			for each (object in xml.start_pos)
			{
				x = int(object.@x);
				y = int(object.@y);
				type = -1;
				var fromMap:String = "";
				if(object.hasOwnProperty("@type"))
				{
					type = int(object.@type)
				}
				if(object.hasOwnProperty("@fromMap"))
				{
					fromMap = String(object.@fromMap)
				}
				
				addNewPlayerStartPos(new Point(x, y), type, fromMap);
			}
			
			resetDudePositionOnCurrentMap();
			 
			 for each (object in xml.tile)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 type = int(object.@type);
				 var name:String = String(object.@name);
				 
				 setNewMapTileAsset(type, name, x, y);
			 }
			 initWalkabilityMap();
			 
			 for each (object in xml.shop)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 assetName = String(object.@name);
				 
				 addNewShop(new Point(x, y), assetName);
			 }
			 
			 initShopsMap();
			 
			 for each (object in xml.collectable)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 type = int(object.@type);
				 assetName = String(object.@name);
				 
				 var tilex:int = int(object.@tilex);
				 var tiley:int = int(object.@tiley);
				 
				 addNewCollectableOnMap(type, assetName, new Point(x, y), new Point(tilex, tiley));
			 }
			 
			 for each (object in xml.container)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 type = int(object.@type);
				 assetName = String(object.@name);
				 
				 addNewContainer(new Point(x,y), type, assetName);
			 }
			 
			 for each (object in xml.obstacle)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 assetName = String(object.@name);
				 type = int(object.@type);
				 
				 addNewObstacle(new Point(x,y), assetName, type); 
			 }
			 
			 for each (object in xml.smashable)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 assetName = String(object.@name);
				 
				 addNewSmashable(new Point(x,y), assetName); 
			 }
			 
			 for each (object in xml.sleepingPlace)
			 {
					
				 x = int(object.@x);
				 y = int(object.@y);
				 assetName = String(object.@name);
				 type = SleepingPlace.OUTSIDE_PLACE;
				 if(object.hasOwnProperty("@type"))
				 {
					 type = int(object.@type);
				 }
				 
				 addNewSleepingPlace(new Point(x,y), assetName, type);
			 }
			 
			 for each (object in xml.washing)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 assetName = String(object.@name);
				 
				 addNewWashingObject(assetName, new Point(x,y));
			 }
			 
			 for each (object in xml.wallwriting)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 assetName = String(object.@name);
				 
				 addNewWritingObject(assetName, new Point(x,y));
			 }
			 
			 for each (object in xml.npc)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 type = int(object.@type);
				 assetName = String(object.@name);
				 
				 var npc:NPC = addNewNPC(type, new Point(x, y), EditorView.editorAssets.getNPCAssetInfo(assetName));
				 
				 if(type == NPC.COP)
				 {
					 (npc as PoliceManNPC).clearWyapoints();
					 for each(var wpXML:XML in object.wp)
					 {
						 var wpX:int = int(wpXML.@x);
						 var wpY:int = int(wpXML.@y);
						 var pt:Point = new Point(wpX, wpY);
						 (npc as PoliceManNPC).addWaypoint(pt);
					 }
				 }
			 }
			 
			 for each (object in xml.exit)
			 {
				 x = int(object.@x);
				 y = int(object.@y);
				 var targetMap:String = "";
				 if(object.hasOwnProperty("@targetMap"))
				 {
					 targetMap = String(object.@targetMap)
				 }
				 
				 addNewExitPoint(new Point(x,y), targetMap);
			 }
			 
			 Game.instance.currentState = Game.MOVING_STATE;
		 }
		 
		 /*
		 * USING FUNCTIONS
		 */
		 
		 public function useInDirection(keycode:int):void
		 {
			 var pointToUse:Point = new Point();
			 Game.instance.dude.refreshPositions(CommonVars.TILE_SIDE_SIZE);

			 if(keycode == Keyboard.UP)
			 {
				 pointToUse.setTo(Game.instance.dude.centerTilePos.x, Game.instance.dude.centerTilePos.y-1);
			 }
			 if(keycode == Keyboard.DOWN)
			 {
				 pointToUse.setTo(Game.instance.dude.centerTilePos.x, Game.instance.dude.centerTilePos.y+1);
			 }
			 if(keycode == Keyboard.LEFT)
			 {
				 pointToUse.setTo(Game.instance.dude.centerTilePos.x-1, Game.instance.dude.centerTilePos.y);
			 }
			 if(keycode == Keyboard.RIGHT)
			 {
				 pointToUse.setTo(Game.instance.dude.centerTilePos.x+1, Game.instance.dude.centerTilePos.y);
			 }
			 
			 var shop:SimpleShop;
			 for each (shop in _shops)
			 {
				 if(shop.visible && shop.mapPosition.equals(pointToUse))
				 {
					 if(shop.shopName == "ShopWasted")
					 {
						 //Game.instance.wasteShop.setItems(shop);
						 //Game.instance.showWasteShopInventory();
						 Game.instance.dialogs.showWastedShop(shop);
					 }
					 else if(shop.shopName == "ShopLombard")
					 {
						 Game.instance.dialogs.showLombardShop(shop);
					 }
					 else
					 {
					 	Game.instance.dialogs.showMarketplaceWindow(shop);//shopInventory.setItems(shop);
					 }
					 Game.instance.setShopInventoryState();
				 }
			 }
			 
			 var place:Place;
			 for each (place in _places)
			 {
				 if(place.visible && place.mapPosition.equals(pointToUse))
				 {
					 Game.instance.dialogs.showDialogWith(place.placeInfo.dialogIDs, place.shopName);	
					 Game.instance.currentState = Game.DIALOG_STATE;
				 }
			 }
			 
			 var container:Container;
			 for each (container in _containers)
			 {
				 if(container.isAddedToBackground() && container.tilePos.equals(pointToUse))
				 {
					 container.openAndLoot();
				 }
			 }
			 
			 var wp:WashPlace;
			 for each (wp in _washingPlaces)
			 {
				 if(wp.isAddedToBackground() && wp.tilePos.equals(pointToUse))
				 {
					 Game.instance.setOverScreen(OverScreen.SWIMMING_IN_FOUNTAIN);
				 }
			 }
			 
			 var sleepingPlace:SleepingPlace;
			 for each (sleepingPlace in _sleepingPlaces)
			 {
				 if(sleepingPlace.isAddedToBackground() && sleepingPlace.tilePos.equals(pointToUse))
				 {
					 if(Game.instance.dude.canFallAsleep())
					 {
						 if(sleepingPlace.type == SleepingPlace.HOME_PLACE)
						 {
							 Game.instance.setOverScreen(OverScreen.SLEEING_HOME_OVER);
						 }
						 else if(sleepingPlace.type == SleepingPlace.OUTSIDE_PLACE)
						 {	 
						 	Game.instance.setOverScreen(OverScreen.SLEEPING_OUTSIDE_OVER);
						 }
					 }
					 else 
					 {
						Game.instance.dude.showPhraseOver("Ещё рано спать!");
					 }
				 }
			 }
			 
			 var npc:NPC;
			 for each (npc in _staticNPCs)
			 {
				 if(npc is QuestNPC && npc.isAddedToBackground() && npc.tilePos.equals(pointToUse))
				 {
					 if(QuestNPC(npc).isCurrentDayQuestFinished)
					 {
						 npc.showPhraseOver("Приходи позднее");
					 }
					 else
					 {
						 //here we open dialog
						 Game.instance.dialogs.showWithCharacter(npc.npcName, QuestNPC(npc).currntDay);
						 Game.instance.currentNPCTalkingTo = QuestNPC(npc);
						 Game.instance.currentState = Game.DIALOG_STATE;
					 }
				 }
			 }
			 
			 var wnpc:MovingNPC;
			 for each (wnpc in _walkingNPCs)
			 {
				 if(wnpc.isAddedToBackground() && wnpc.isStanding())
				 {
					 wnpc.refreshPositions(CommonVars.TILE_SIDE_SIZE);
					 if(wnpc.centerTilePos.equals(pointToUse))
					 {
						 wnpc.showPhraseOver("Пшов геть!");
					 }
				 }
			 }
			 
			 var exits:Vector.<ExitPoint> = Game.instance.dude.globalGoPoints
			 for each (var exit:ExitPoint in exits)
			 {
				 if(exit.tilePos.equals(pointToUse) && exit.targetMap)
				 {
					 tryToStartNewDayQuests();
					 Game.instance.dude.setEnteredAction();
					 Game.instance.background.loadMap(exit.targetMap);
				 }
			 }
			 
		 }
		 
		 public function refreshGarbage():void
		 {
			 var container:Container;
			 for each (container in _containers)
			 {
				 if(!container.isOpened)
				 {
				 	container.generateWasteds();
				 	container.close();
				 }
			 }
		 }
		 
		 public function getOpenedContainersPositions():Vector.<Point>
		 {
			 var opened:Vector.<Point> = new Vector.<Point>();
			 var container:Container;
			 for each (container in _containers)
			 {
				 if(container.isOpened)
				 {
					 opened.push(new Point(container.tilePos.x, container.tilePos.y));
				 }
			 }
			 return opened;
		 }
		 
		 public function getQuestCharactersInfo():Vector.<QuestCharInfo>
		 {
			 var questChars:Vector.<QuestCharInfo> = new Vector.<QuestCharInfo>();
			 var character:StaticNPC;
			 for each (character in _staticNPCs)
			 {
				 if(character is QuestNPC)
				 {
					 questChars.push(QuestNPC(character).infoCopy);
				 }
			 }
			 return questChars;
		 }
		 
		 public function getRandomPointNearby(curPos:Point, distFromCurrentPos:int):Point
		 {
			 var pt:Point = new Point();
			 var hasPt:Boolean = false;
			 
			 while(!hasPt)
			 {
				 var isX:Boolean = UtilsMath.getRandomWithChance(50);
				 var isPlus:Boolean = UtilsMath.getRandomWithChance(50);
				 
				 var foundRandPos:int = UtilsMath.randomRange(1, distFromCurrentPos);
				 var x:int = isX ? (isPlus ? curPos.x+foundRandPos : curPos.x-foundRandPos) : curPos.x;
				 var y:int = isX ? curPos.y : (isPlus ? curPos.y+foundRandPos : curPos.y-foundRandPos);
				 pt.setTo(x,y);
				 if(x>0 && y>0 && isWalkable(pt))
				 {
					 return pt;
				 }
			 }
			 
			 return null;
		 }
		 
		 private function setCurrentMapSavedData(mapName:String):void
		 {
			 var gms:GameMapState = GameSavedData.instance.getDataForMap(mapName);
			 if(gms)
			 {
				 var pos:Point;
				 var openedContainers:Vector.<Point> = gms.openedGarbagePos;
				 
				 for each (pos in openedContainers)
				 {
				 	openContainerAt(pos);
				 }
				 
				 var character:QuestCharInfo;
				 var questCharacters:Vector.<QuestCharInfo> = gms.questCharacters;
				 
				 for each (character in questCharacters)
				 {
					 setQuestCharacterInfo(character);
				 }
			 }
		 }
		 
		 private function setQuestCharacterInfo(info:QuestCharInfo):void
		 {
			 var character:StaticNPC;
			 for each (character in _staticNPCs)
			 {
				 if(character is QuestNPC && QuestNPC(character).npcName == info.characterName)
				 {
					 QuestNPC(character).setData(info);
					 return ;
				 }
			 }
		 }
		 
		 private function openContainerAt(pos:Point):void
		 {
			 var container:Container;
			 for each(container in _containers)
			 {
				 if(container.tilePos.equals(pos))
				 {
					 container.openAndDeleteStuff();
					 return ;
				 }
			 }
		 }
		 
		 public function getRandomFreePoint():Point
		 {
			 var pt:Point = new Point();
			 var hasPt:Boolean = false;
			 
			 while(!hasPt)
			 {
				 var x:int = UtilsMath.randomRange(0, CurrentMapParameters.sizeX-1);
				 var y:int = UtilsMath.randomRange(0, CurrentMapParameters.sizeY-1);
				 pt.setTo(x,y);
				 if(isWalkable(pt))
				 {
					 return pt;
				 }
			 }
			 
			 return null;
		 }
		 
		 public function getNearestChurchPos(pos:Point):Point
		 {
			 var charTilePos:Point = GameUtils.posToTile(pos);
			 var closestChurch:Point = new Point(-1, -1);
			 var curDist:uint = 0;
			 var place:Place;
			 
			 for each (place in _places)
			 {
				 var diff:Point =  place.initialPos.subtract(charTilePos);
				 if(closestChurch.x == -1 || diff.length < curDist)
				 {
					 curDist = diff.length;
					 closestChurch.x = place.initialPos.x;
					 closestChurch.y = place.initialPos.y;
				 }
			 }
			 closestChurch = GameUtils.posToTile(closestChurch);
			 closestChurch.y += 1;
			 return GameUtils.tileToCenterMapPos(closestChurch);
		 }
		 
		 public function getNearestPimpPos(pos:Point):Point
		 {
			 var nearestPimpID:int = -1;
			 var nearestPimp:EnemyCharacter;
			 var curDist:uint = 0;
			 for each (var enemy:EnemyCharacter in _enemyes)
			 {
				 if(enemy is Pimp)
				 {
					 enemy.refreshPositions(CommonVars.TILE_SIDE_SIZE);
					 var diff:Point = enemy.globalPosition.subtract(pos); 
					 
					 if(nearestPimpID == -1)
					 {
						 curDist = diff.length;
						 nearestPimpID = enemy.id;
						 nearestPimp = enemy;
					 }
					 else if(curDist > diff.length)
					 {
						 curDist = diff.length;
						 nearestPimpID = enemy.id;
						 nearestPimp = enemy;
					 }
					 
				 }
			 }
			 if(nearestPimpID == -1)
			 {
				 return null;
			 }
			 else
			 {
				 return nearestPimp.globalPosition;
			 }
		 }
		 
		 public function tryToStartNewDayQuests():void
		 {
			 for each (var npc:StaticNPC in _staticNPCs)
			 {
				 if(npc is QuestNPC)
				 {
					 QuestNPC(npc).startNewDayQuests();
				 }
			 }
		 }
	}
}