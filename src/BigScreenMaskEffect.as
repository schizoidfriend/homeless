package
{
	import starling.extensions.pixelmask.PixelMaskDisplayObject;
	
	public class BigScreenMaskEffect extends PixelMaskDisplayObject
	{
		public function BigScreenMaskEffect(scaleFactor:Number=-1, isAnimated:Boolean=true)
		{
			super(scaleFactor, isAnimated);
		}
	}
}