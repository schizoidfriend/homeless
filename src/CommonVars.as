package
{
	public class CommonVars
	{
		public static const WIDTH:int = 1024;
		public static const HEIGHT:int = 768;
		
		private static var _screenHalfWidth:int = WIDTH/2;
		private static var _screenHalfHeight:int = HEIGHT/2;
		
		public static const TILE_SIDE_SIZE:int = 64;
		public static const HALF_TILE_SIZE:int = TILE_SIDE_SIZE/2;
		
		public static const TIMER_FPS:int = 30;
		
		public function CommonVars()
		{
		}

		public static function get screenHalfHeight():int
		{
			return _screenHalfHeight;
		}

		public static function get screenHalfWidth():int
		{
			return _screenHalfWidth;
		}

	}
}