package 
{	
	import comunication.DialogManager;
	
	import dude.Dude;
	import dude.DudeParameters;
	
	import editor.EditorAssets;
	import editor.EditorView;
	
	import feathers.controls.ProgressBar;
	import feathers.themes.AeonDesktopTheme;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.System;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import game.GameExternalParameters;
	import game.GameSavedData;
	
	import gui.CustomImgProgressBar;
	import gui.GameInterface;
	import gui.Inventory;
	import gui.ItemsContainer;
	import gui.MainMenu;
	import gui.NewMapDialog;
	import gui.OpenableContainer;
	import gui.WasteShop;
	
	import inventory.ItemsInfoLoader;
	
	import map.CollectableObject;
	import map.CurrentMapParameters;
	import map.FightController;
	import map.characters.Bird;
	import map.characters.NPC;
	import map.characters.QuestNPC;
	import map.globalMap.GlobalMap;
	
	import over.OverScreen;
	
	import raj.soundlite.MBG;
	import raj.soundlite.MSFX;
	import raj.soundlite.MSound;
	
	import shops.PlaceInfoLoader;
	import shops.PlaceTypeCommonInfo;
	import shops.ShopsInfoLoader;
	
	import slot.SlotView;
	
	import starling.animation.Juggler;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.display.VideoPlayer;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.BlurFilter;
	import starling.filters.ColorMatrixFilter;
	import starling.text.TextField;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.AssetManager;
	import starling.utils.VAlign;
	
	import states.input.IBasicInputState;
	import states.input.InputStatesManager;
	import states.input.MovingInputState;
	
	import time.GameTime;
	
	import utils.ArtUtils;
	import utils.GameUtils;
	import utils.TilesRaytracing;
	import utils.UtilsMath;

	public class Game extends Sprite
	{		
		private static const EDITOR_SCROLL_SPEED:Number = 10.0;
		
		private static var _instance:Game;
		
		public static const MOVING_STATE:int = 0;
		public static const EDITOR_STATE:int = 1;
		public static const DIALOG_STATE:int = 2;
		public static const GLOBAL_STATE:int = 3;
		public static const LOADING_STATE:int = 4;
		public static const NEWMAP_STATE:int = 5;
		public static const INVENTORY_STATE:int = 6;
		public static const OVERSCREEN_STATE:int = 7;
		public static const SLOT_STATE:int = 8;
		public static const MAIN_MENU_STATE:int = 9;
		
		private var _isThisLoading:Boolean = false;
		
		//game objects
		private var _overBackgroundMask:BigScreenMaskEffect;
		private var _maskImage:MovieClip;
		private var _background:Background;
		private var _dude:Dude;
		private var _dialogs:DialogManager;
		private var _globalMap:GlobalMap;
		private var _gameInterface:GameInterface;
		private var _newMapDialog:NewMapDialog;
		private var _inventory:Inventory;
		private var _container:OpenableContainer;
		//private var _shopInventory:ShopInventory;
		private var _wasteShop:WasteShop;
		private var _overScreen:OverScreen;
		private var _dateTime:GameTime;
		private var _slot:SlotView;
		private var _mainMenu:MainMenu = new MainMenu();
		
		private var _isResourcesLoaded:Boolean = false;
		private var _isWaitingForGameToStart:Boolean = false;
		private var _isGameStarted:Boolean = false;
		
		//map an dude constaints
		private var _xTopBorderPos:int;
		private var _yTopBorderPos:int;
		private var _xTopDudePos:int;
		private var _yTopDudePos:int;
		private var _xTopDudeBorderPos:int;
		private var _yTopDudeBorderPos:int;
		
		//duded offset for movenent on next tick
		private var _curDudeOffsetX:Number = 0;
		private var _curDudeOffsetY:Number = 0;
		
		//assets manager
		private var _assetsManager:AssetManager;
		
		//view for editor (all menus)
		private var _editorView:EditorView;
		
		//current and previous game states
		private var _currentState:int = LOADING_STATE;
		private var _prevState:int = -1;
		
		//current state to handle input from keyboard
		private var _currentInputState:IBasicInputState;
		
		//all item types in game list loader
		private var _itemsInfo:ItemsInfoLoader;
		//all shop types in game list loader
		private var _shopInfo:ShopsInfoLoader;
		//all places info
		private var _placeInfo:PlaceInfoLoader;
		
		//test progress bar
		private var _fightController:FightController;
		
		private var _blackness:Sprite = new Sprite();
		
		private var _daylightFilter:ColorMatrixFilter;
		
		private var _loadingProgressBar:ProgressBar;
		
		private var _msound:MSound = new MSound(new Embeds.bg1_mp3(), 1000, 0.6);
		
		private var _videoPlayer:VideoPlayer;
		
		//current NPC you're talking to
		private var _currentNPCTalkingTo:QuestNPC;
		
		

		public function Game()
		{
			super();
			_instance = this;

			setupAssets();
			
			MBG.init();
			MSFX.init();
			
			//MBG.play(_msound);
			
			/*var matrix:Vector.<Number> = new Vector.<Number>[0,1,0,0,0,
															0,0,1,0,0,
															1,0,0,0,0,
															0,0,0,1,0];*/
			/*var vec:Vector.<Number> = new <Number>[ 0,1,0,0,0,
													0,0,1,0,0,
													1,0,0,0,0,
													0,0,0,1,0];*/
			/*matrix=matrix.concat([]);// red
			matrix=matrix.concat(]);// green
			matrix=matrix.concat([]);// blue
			matrix=matrix.concat([]);// alpha*/
			//var my_filter:BlurFilter=new BlurFilter(10,5,1);
		/*	var my_filter:ColorMatrixFilter = new ColorMatrixFilter();
			my_filter.adjustSaturation(-1);
			my_filter.adjustContrast(1);
			my_filter.invert();
			
			this.filter = my_filter;*/
			
			_videoPlayer = new VideoPlayer(1024, 768, 20);
			_videoPlayer.load("res/Video/LOGO_1024.mp4");
			_videoPlayer.play();
			addChild(_videoPlayer);
			
		}

		public function get currentNPCTalkingTo():QuestNPC
		{
			return _currentNPCTalkingTo;
		}

		public function set currentNPCTalkingTo(value:QuestNPC):void
		{
			_currentNPCTalkingTo = value;
		}
		
		private function setToStartGameState():void
		{
			if(_isThisLoading)
			{
				startLoading();
				_isThisLoading = false;
			}
			currentState = Game.MOVING_STATE;
		}

		public function tryToStartGame():void
		{
			if(isResourcesLoaded)
			{
				setToStartGameState();
			}
			else
			{
				_isWaitingForGameToStart = true;
				_loadingProgressBar.visible = true;
				_mainMenu.visible = false;
			}
		}

		public function get isResourcesLoaded():Boolean
		{
			return _isResourcesLoaded;
		}

		public function get daylightFilter():ColorMatrixFilter
		{
			return _daylightFilter;
		}

		public function get blackness():Sprite
		{
			return _blackness;
		}

		public function setToAlcoState():void
		{
			var alcoFilter:BlurFilter = new BlurFilter(1.5, 1.5);
			this.filter = alcoFilter;
		}
		
		public function get fightController():FightController
		{
			return _fightController;
		}
		
		public function get wasteShop():WasteShop
		{
			return _wasteShop;
		}

		public function get shopInfo():ShopsInfoLoader
		{
			return _shopInfo;
		}
		
		public function get placeInfo():PlaceInfoLoader
		{
			return _placeInfo;
		}

		public function get itemsInfo():ItemsInfoLoader
		{
			return _itemsInfo;
		}

		/*public function get shopInventory():ShopInventory
		{
			return _shopInventory;
		}*/

		public function get container():OpenableContainer
		{
			return _container;
		}

		public function get inventory():Inventory
		{
			return _inventory;
		}

		public function get editorView():EditorView
		{
			return _editorView;
		}

		public function get gameInterface():GameInterface
		{
			return _gameInterface;
		}

		public function get globalMap():GlobalMap
		{
			return _globalMap;
		}

		public function get dialogs():DialogManager
		{
			return _dialogs;
		}
		
		public function get slot():SlotView
		{
			return _slot;
		}

		public function get currentState():int
		{
			return _currentState;
		}
		
		private function deinitState(state:int):void
		{
			switch(state)
			{
				case MOVING_STATE:
					deinitMovingState();
					break;
				case DIALOG_STATE:
					deinitDialogState();
					break;
				case EDITOR_STATE:
					deinitEditorState();
					break;
				case GLOBAL_STATE:
					deinitGlobalState();
					break;
				case LOADING_STATE:
					
					break;
				case NEWMAP_STATE:
					deinitNewMapDialog();
					break;
				case INVENTORY_STATE:
					deinitInventorState();
					break;
				case OVERSCREEN_STATE:
					
					break;
				case SLOT_STATE:
					deinitSlotState();
					break;
				case MAIN_MENU_STATE:
					deinitMainMenuState();
					break;
			}
		}

		public function set currentState(value:int):void
		{
			if(_currentInputState)
			{
				_currentInputState.deinit();
			}
			_currentInputState = InputStatesManager.instance.getInputForState(value);
			_currentInputState.init();
			
			
			_prevState = _currentState;
			deinitState(_prevState);
			_currentState = value;
			switch(_currentState)
			{
				case MOVING_STATE:
					initMovingState();
					break;
				case DIALOG_STATE:
					initDialogState();
					break;
				case EDITOR_STATE:
					initEditorState();
					break;
				case GLOBAL_STATE:
					initGlobalState();
					break;
				case LOADING_STATE:
					
					break;
				case NEWMAP_STATE:
					initNewMapDialog();
					break;
				case INVENTORY_STATE:
					initInventorState();
					break;
				case OVERSCREEN_STATE:
					
					break;
				case SLOT_STATE:
					initSlotState();
					break;
				case MAIN_MENU_STATE:
					initMainMenuState();
					break;
			}
		}
		
		private function clearEvents():void
		{			
			if(hasEventListener(Event.ENTER_FRAME))
			{
				removeEventListeners(Event.ENTER_FRAME);
			}
		}
		
		private function initMovingState():void
		{
			if(!_isGameStarted)
			{
				startGame();
			}
			initMoving();
			clearEvents();
			_background.initEnterFrame();
			
			if(_prevState == EDITOR_STATE)
			{
				_background.forceRedraw();
				_dude.setDudeToStartPos();
			}
			dudePositioning();
			
			addEventListener(Event.ENTER_FRAME, onMovementEnterFrame);	
			
			_dude.visible = true;
			_gameInterface.visible = true;
			_dude.startDudeTimer();
			_dude.restartAnimations();
		}
		
		private function initSlotState():void
		{
			_slot.checkMoneyStatus();
			addChild(_slot);
		}
		
		private function deinitSlotState():void
		{
			removeChild(_slot);
		}
		
		private function initMainMenuState():void
		{
			addChild(_mainMenu);
			_mainMenu.buildMenu("main");
		}
		
		private function deinitMainMenuState():void
		{
			removeChild(_mainMenu);
			startGame();
		}
		
		private function initInventorState():void
		{
			_inventory.refresh();
			_inventory.visible = true;
		}
		
		private function deinitInventorState():void
		{
			_inventory.visible = false;
			hideItemsContainer();
			hideShopInventory();
			hideWasteShopInventory();
		}
		
		private function initNewMapDialog():void
		{
			addChild(_newMapDialog);
			_newMapDialog.visible = true;
		}
		
		private function deinitNewMapDialog():void
		{
			removeChild(_newMapDialog);
			_newMapDialog.visible = false;
		}
		
		private function deinitMovingState():void
		{
			_background.deinitEnterFrame();
			_dude.stop();
			_dude.stopDudeTimer();
		}
		
		private function initEditorState():void
		{
			_gameInterface.visible = false;
			clearEvents();
			_editorView.editorMapPosition.setTo(_background.lastRedrawnXPosOffset, _background.lastRedrawnYPosOffset);
			_editorView.visible = true;
			
			_background.removeAllShopToShopChars();
			_background.initMouse();
			initEditor();

			addEventListener(Event.ENTER_FRAME, onEditorEnterFrame);
		}
		
		private function deinitEditorState():void
		{
			_background.deinitMouse();
			_editorView.visible = false;
		}
		
		private function initDialogState():void
		{
			clearEvents();
		}
		
		private function deinitDialogState():void
		{
			_dialogs.hideCurrentDialog();
			hideShopInventory();
		}
		
		private function initGlobalState():void
		{
			clearEvents();
			_globalMap.showGlobalMap();
		}
		
		private function deinitGlobalState():void
		{
			_globalMap.hideGlobalMap();
		}
		
		public function closeInventory():void
		{
			if(_currentState == INVENTORY_STATE)
			{
				currentState = _prevState;
			}
		}

		private function setupAssets():void
		{
			setupLoadingScreen();
			
			_itemsInfo = new ItemsInfoLoader();
			_shopInfo = new ShopsInfoLoader();
			_placeInfo = new PlaceInfoLoader();
			_assetsManager = new AssetManager();
			
			var appDir:File = File.applicationDirectory;
			assets.verbose = true;
			assets.enqueue(appDir.resolvePath("res"));
			
			_loadingProgressBar.visible = false;
			
			assets.loadQueue(loadAssets);
		}
		
		private function setupLoadingScreen():void
		{
			new AeonDesktopTheme();
			
			_loadingProgressBar = new ProgressBar();
			_loadingProgressBar.minimum = 0;
			_loadingProgressBar.maximum = 1.0;
			addChild(_loadingProgressBar);
			_loadingProgressBar.validate();
			_loadingProgressBar.x = 0;
			_loadingProgressBar.y = 768/2;
			
			_loadingProgressBar.visible = false;
		}
		
		private var isPlayingGameIntro:Boolean = false;
		private var isPlayingSecondIntro:Boolean = false;
		private function loadAssets(ratio:Number):void
		{
			trace("Loading assets, progress:", ratio);
			_loadingProgressBar.value = ratio;
			if(_loadingProgressBar.x == 0 && _loadingProgressBar.width)
			{
				_loadingProgressBar.x = 1024/2 - _loadingProgressBar.width/2;
				_loadingProgressBar.visible = true;
			}
			if(_videoPlayer && !isPlayingGameIntro)
			{
				if(!_videoPlayer.player.playing)
				{
					_videoPlayer.load("res/Video/Home_Less.mp4");
					_videoPlayer.play();
					isPlayingGameIntro = true;
				}
			}
			else if(_videoPlayer && isPlayingGameIntro)
			{
				if(!_videoPlayer.player.playing)
				{
					_videoPlayer.killPlayer();
					removeChild(_videoPlayer, true);
					_videoPlayer.visible = false;
					_videoPlayer = null;
					currentState = MAIN_MENU_STATE;
				}
			}
			if (ratio == 1.0)
			{
				removeChild(_loadingProgressBar, true);
				_isResourcesLoaded = true;
				if(_isWaitingForGameToStart)
				{
					tryToStartGame();
				}
			}
		}
		private function setupGame():void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function startGame():void
		{	
			_isGameStarted = true;
			setupGame();
			
			var bgBitmapData:BitmapData = new BitmapData(CommonVars.WIDTH,CommonVars.HEIGHT, false, 0x0);
			var bgTexture:Texture = Texture.fromBitmapData(bgBitmapData,false,false);
			
			var blackImg:Image = new Image(bgTexture);
			_blackness.addChild(blackImg);
			_blackness.touchable = false;
			_blackness.visible = false;
			
			_gameInterface = new GameInterface();
			
			_background = new Background();
			/*_overBackgroundMask = new BigScreenMaskEffect();
			
			_maskImage = ArtUtils.makeMovieClip("BackgroundMaskAnim", 8, true);
			_maskImage.scaleX = 2.0;
			_maskImage.scaleY = 2.0;
			Starling.juggler.add(_maskImage);
			_maskImage.play();
			
			_overBackgroundMask.mask = _maskImage;
			addChild(_overBackgroundMask);
			_overBackgroundMask.addChild(_background);*/
			addChild(_background);
			

			_dude = new Dude();
			addChild(_dude);
			_dude.setGoToGlobalDispatch(goToGlobalHandler);
			
			_dateTime = new GameTime(_gameInterface.dateTime);
			_dateTime.setDateTime(0, 9, 0);
			
			_editorView = new EditorView();
			addChild(_editorView);
			
			_dialogs = new DialogManager();
			addChild(_dialogs);
			
			_slot = new SlotView();
			_slot.x = 1024/2;
			_slot.y = 768/2;
			
			_mainMenu = new MainMenu();
			addChild(_mainMenu);
			
			_globalMap = new GlobalMap();
			addChild(_globalMap);
			
			addChild(_gameInterface);
			
			_newMapDialog = new NewMapDialog();
			
			_inventory = new Inventory(DudeParameters.INVENTORY_SIZE);
			_inventory.x = 100;
			addChild(_inventory);
			_inventory.visible = false;
			
			_container = new OpenableContainer(2);
			addChild(_container);
			_container.visible = false;
			_container.x = 400;
			
			/*_shopInventory = new ShopInventory(4);
			addChild(_shopInventory);
			_shopInventory.visible = false;
			_shopInventory.x = 400;*/
			
			_wasteShop = new WasteShop(4);
			addChild(_wasteShop)
			_wasteShop.visible = false;
			_wasteShop.x = 400;
			
			addChild(_blackness);
			
			_overScreen = new OverScreen();
			
			_fightController = new FightController(_gameInterface.hitProgressBar, _dude);
			
			//currentState = LOADING_STATE;
		}
		
		public function resetGameParameters():void
		{
			_xTopBorderPos = CurrentMapParameters.sizeX*CommonVars.TILE_SIDE_SIZE;
			_yTopBorderPos = CurrentMapParameters.sizeY*CommonVars.TILE_SIDE_SIZE;
			
			_xTopDudePos = CurrentMapParameters.sizeX*CommonVars.TILE_SIDE_SIZE - (CommonVars.WIDTH >> 1);
			_yTopDudePos = CurrentMapParameters.sizeY*CommonVars.TILE_SIDE_SIZE - (CommonVars.HEIGHT >> 1);
			
			_xTopDudeBorderPos = CurrentMapParameters.sizeX*CommonVars.TILE_SIDE_SIZE;
			_yTopDudeBorderPos = CurrentMapParameters.sizeY*CommonVars.TILE_SIDE_SIZE;
		}
		
		private function goToGlobalHandler():void
		{
			_dude.stop();
			currentState = Game.GLOBAL_STATE;
		}
		
		public function showItemsContainer():void
		{
			_container.visible = true;	
			currentState = DIALOG_STATE;
		}
		
		public function hideItemsContainer():void
		{
			_container.visible = false;
		}
		
		public function hasOpenedContainer():Boolean
		{
			return _container.visible;
		}
		
		public function setShopInventoryState():void
		{
			//_shopInventory.visible = true;
			//_dialogs.showMarketplaceWindow();
			currentState = DIALOG_STATE;
		}
		
		public function hideShopInventory():void
		{
			_dialogs.hideMarketplaceWindow();
			_dialogs.hideWastedShop();
			_dialogs.hideLombardShop();
			//_shopInventory.visible = false;
		}
		
		public function hasOpenedShopInventory():Boolean
		{
			return false;//_shopInventory.visible;
		}
		
		public function showWasteShopInventory():void
		{
			_wasteShop.visible = true;
			currentState = DIALOG_STATE;
		}
		
		public function hideWasteShopInventory():void
		{
			_wasteShop.visible = false;
		}
		
		public function setOverScreen(screenNum:uint):void
		{
			this.addChild(_overScreen);
			_overScreen.setOverScreen(screenNum);
			currentState = OVERSCREEN_STATE;
		}
		
		public function setOverScreenByName(name:String):Boolean
		{
			this.addChild(_overScreen);
			if(_overScreen.setOverScreenByName(name))
			{
				currentState = OVERSCREEN_STATE;
				return true;	
			}
			return false;
		}
		
		public function dropOverScreen():void
		{
			this.removeChild(_overScreen);
			currentState = _prevState;
		}
		
		public function setInitialDudePos():void
		{
			_dude.addOffset(_curDudeOffsetX, _curDudeOffsetY);
			dudePositioning();
			_background.forceRedraw();
			_background.redrawMapWithPos(_dude.globalPosition);
		}
		
		public function setOverBackgroundmaskPos(x:Number, y:Number):void
		{
			_maskImage.x = x;
			_maskImage.y = y;
		}
		
		private function resetPositionsOfEverything(resetDude:Boolean = true):void
		{
			_dude.setDudeToStartPos();
			if(resetDude)
			{
				_background.setNPCsToInitialPositionsAndStates();
			}
		}
		
		private function startMovementAgain():void	
		{
			_background.setNPCsToMovement();
		}
		
		private function initEditor():void
		{
			_editorView.enableEditorMode(true);
			resetPositionsOfEverything();
			_background.forceRedraw(true);
			_background.stopAllTimers();
		}
		
		private function initMoving():void
		{
			_editorView.enableEditorMode(false);
			_editorView.editorMapPosition.setTo(CommonVars.screenHalfWidth, CommonVars.screenHalfHeight);
			if(_prevState == EDITOR_STATE)
			{
				_background.forceRedraw();
			}
			startMovementAgain();
			dudePositioning();
			_background.startAllTimers();
			_background.resetAllWashingPlacesAnimation();
		}
		
		private function onKeyDown(e:KeyboardEvent):void
		{
			if(!(e.altKey && e.ctrlKey))
			{
				_currentInputState.onKeyDown(e.keyCode);
			}
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if(!(e.altKey && e.ctrlKey))
			{
				_currentInputState.onKeyUp(e.keyCode);
			}
		}
		
		private function onMovementEnterFrame(e:Event):void
		{
			dudeMovement()
			_background.redrawMapWithPos(_dude.globalPosition);
		}
		
		private function onEditorEnterFrame(e:Event):void
		{
			editorMovement();
			_background.redrawMapWithEditor(_editorView.editorMapPosition);
			
			var dudeBackgroundPos:Point = _background.getTileScreenPos(_dude.startPos.x, _dude.startPos.y);
			if(dudeBackgroundPos)
			{
				_dude.x = dudeBackgroundPos.x - _background.lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE + CommonVars.TILE_SIDE_SIZE/2;
				_dude.y = dudeBackgroundPos.y - _background.lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE + CommonVars.TILE_SIDE_SIZE/2;
				_dude.visible = true;
			}
			else
			{
				_dude.visible = false;
			}
		}
		
		private function editorMovement():void
		{
			if(_currentInputState.keysPressed.length > 0)
			{
				var curPressed:int = -1;
				var i:int;
				
				var curEditorOffsetX:int = 0;
				var curEditorOffsetY:int = 0;
				for(i = 0; i < _currentInputState.keysPressed.length; i++)
				{
					curPressed = _currentInputState.keysPressed[i] as int;
					
					if(curPressed == Keyboard.RIGHT)
					{
						curEditorOffsetX = EDITOR_SCROLL_SPEED;
					}
					if(curPressed == Keyboard.LEFT)
					{
						curEditorOffsetX = -EDITOR_SCROLL_SPEED;
					}
					if(curPressed == Keyboard.UP)
					{
						curEditorOffsetY = -EDITOR_SCROLL_SPEED;
					}
					if(curPressed == Keyboard.DOWN)
					{
						curEditorOffsetY = EDITOR_SCROLL_SPEED;
					}
				}
				
				_editorView.addOffsetToEditorMapPosition(curEditorOffsetX, curEditorOffsetY);
			}
		}
		
		private var currentSpeed:Number;
		private function dudeMovement():void
		{
			if(_currentInputState.keysPressed.length > 0 && _dude.canMove)
			{
				var curPressed:int = -1;
				var i:int;
				//movement
				currentSpeed = _dude.currentMovingState == Dude.RUNNING_STATE ? _dude.runningSpeed : _dude.basicSpeed;
				for(i = 0; i < _currentInputState.keysPressed.length; i++)
				{
					curPressed = _currentInputState.keysPressed[i] as int;
					
					if(curPressed == Keyboard.D)
					{
						_curDudeOffsetX = currentSpeed;
					}
					if(curPressed == Keyboard.A)
					{
						_curDudeOffsetX = -currentSpeed;
					}
					if(curPressed == Keyboard.W)
					{
						_curDudeOffsetY = -currentSpeed;
					}
					if(curPressed == Keyboard.S)
					{
						_curDudeOffsetY = currentSpeed;
					}
				}
				_dude.addOffset(_curDudeOffsetX, _curDudeOffsetY);
				
				_curDudeOffsetX = 0;
				_curDudeOffsetY = 0;
				
				dudePositioning();
				if(!_dude.shouldRun)
				{
					checkDudeCollectItems();
				}
				_background.checkInteractionWithBirds(_dude.centerTilePos);
				
			}
			else if(!_dude.canMove && _dude.drunkenCantMove)
			{
				moveInRandomDirection();
			}
		}
		
		
		private function moveInRandomDirection():void
		{
			if(_dude.hasRandomDirection)
			{
				_dude.addOffset(_dude.randDirection.x, _dude.randDirection.y);
				dudePositioning();
			}
			else
			{
				_dude.resetRandomDirection();
			}
		}
		
		private function dudePositioning():void	
		{
			if(_dude.globalPosition.x - _dude.halfWidth < 0) _dude.globalPosition.x = _dude.halfWidth;
			if(_dude.globalPosition.y - _dude.halfHeight < 0) _dude.globalPosition.y = _dude.halfHeight;
			if(_dude.globalPosition.x + _dude.halfWidth > _xTopBorderPos) _dude.globalPosition.x = _xTopBorderPos - _dude.halfWidth;
			if(_dude.globalPosition.y + _dude.halfHeight > _yTopBorderPos) _dude.globalPosition.y =  _yTopBorderPos - _dude.halfHeight;
			
			if(_dude.globalPosition.x < CommonVars.screenHalfWidth)
			{
				_dude.x = _dude.globalPosition.x;
			}
			else if(_dude.globalPosition.x > _xTopDudePos)
			{
				_dude.x = CommonVars.screenHalfWidth + (_dude.globalPosition.x - _xTopDudePos);
			}
			else
			{
				_dude.x = CommonVars.screenHalfWidth;
			}
			
			if(_dude.globalPosition.y < CommonVars.screenHalfHeight)
			{
				_dude.y = _dude.globalPosition.y;
			}
			else if(_dude.globalPosition.y > _yTopDudePos)
			{
				_dude.y = CommonVars.screenHalfHeight + (_dude.globalPosition.y - _yTopDudePos);
			}
			else
			{
				_dude.y = CommonVars.screenHalfHeight;
			}
			
			//setOverBackgroundmaskPos(_dude.x-CommonVars.screenHalfWidth, _dude.y-CommonVars.screenHalfHeight);
		}
		
		public static function simpleHitTestObject(target1:DisplayObject, target2:DisplayObject):Boolean 
		{
			return target1.getBounds(target1.root).intersects(target2.getBounds(target2.root));
		}
		
		private var startPos:Point = new Point();
		private const NUM_POINTS_TO_CHECK:int = 9;
		private function checkDudeCollectItems():void
		{
			startPos.setTo(_dude.centerTilePos.x - 1, _dude.centerTilePos.y - 1);
			var i:int;
			var curX:int;
			var curY:int;
			var curTile:Tile;
			var curTileCollectables:Vector.<CollectableObject>;
			var collectable:CollectableObject;
			for(i = 0; i < NUM_POINTS_TO_CHECK; i++)
			{
				curX = int(startPos.x) + i%3;
				curY = int(startPos.y) + i/3;
				curTile = _background.getTile(curX, curY);
				if(curTile)
				{
					curTileCollectables = curTile.collectables;
					for each (collectable in curTileCollectables)
					{
						if(simpleHitTestObject(collectable, _dude))
						{
							if(_background.collectCollectable(collectable.id))
							{
								curTile.killCollectable(collectable.id);
							}
						}
					}
				}
			}
		}		
		
		private function setDudeToScreen():void
		{
			_dude.x = CommonVars.WIDTH >> 1;
			_dude.y = CommonVars.HEIGHT >> 1;
			
			dudePositioning();
		}
		
		public function cleanAtNewDay():void
		{
			_background.removeAllShopToShopChars();
			_background.removeAllTempEnvironments();
		}
		
		public function sleepingAtHome():void
		{
			_dude.sleepingAtHome();
			_dateTime.setDateTime(0,9,_dateTime.curDay+1);
			cleanAtNewDay();
			resetPositionsOfEverything(false);
			_background.resetDudePositionOnCurrentMap();
		}
		
		public function sleepingOutside():void
		{
			_dude.sleepingOutside();
			cleanAtNewDay();
			_dateTime.setDateTime(0,7,_dateTime.curDay+1);
			resetPositionsOfEverything(false);
			_background.resetDudePositionOnCurrentMap();
		}
		
		public function beatenByPolice():void
		{
			loseConsciousness();
			_background.setNPCsToInitialPositionsAndStates();
		}
		
		public function loseConsciousness():void
		{
			_dude.loseConsciousness();
			cleanAtNewDay();
			if(_dateTime.curHours<7)
			{
				_dateTime.setDateTime(0,9,_dateTime.curDay);
			}
			else if(_dateTime.curHours>22)
			{
				_dateTime.setDateTime(0,7,_dateTime.curDay+1);
			}
			else
			{
				_dateTime.setDateTime(0,_dateTime.curHours+2,_dateTime.curDay);
			}
		}
		
		public function setDaylightAccordingly(hours:uint):void
		{
			_daylightFilter = new ColorMatrixFilter();
			if(hours >= 21 || hours < 6)
			{
				_daylightFilter.adjustBrightness(-0.2); //night
			}
			else if(hours >= 6 && hours < 11)
			{
				_daylightFilter.adjustBrightness(-0.05); //morning
			}
			else if(hours >= 11 && hours < 16)
			{
				_daylightFilter.adjustBrightness(0.1); // day
			}
			else if(hours >= 16 && hours < 21)
			{
				_daylightFilter.adjustBrightness(0.0); // day
			}
			
			_background.filter = _daylightFilter;
			_dude.filter = _daylightFilter;
		}
		
		public function shouldMove():Boolean
		{
			if(_currentInputState is MovingInputState)
			{
				return MovingInputState(_currentInputState).isMovingKeysPressed();
			}
			return false;
		}
		
		public function setGameLoading():void
		{
			_isThisLoading = true;
		}
		
		public function startLoading():void
		{
			GameSavedData.instance.loadGameDataFromFile("tmpSave.xml");
		}
		
		//setters and getters
		
		public function get assets():AssetManager
		{
			return _assetsManager;
		}
		
		public function get dude():Dude
		{
			return _dude;
		}
		
		public function get background():Background
		{
			return _background;
		}
		
		static public function get instance():Game
		{
			return _instance;
		}
	}
}