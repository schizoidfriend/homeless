package
{
	import editor.EditorView;
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.pixelmask.PixelMaskDisplayObject;
	import starling.text.TextField;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	import starling.utils.Color;

	public class Game10 extends Sprite
	{
		private var test:Sprite = null;
		public function Game10()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		private function onAdded (e:Event):void
		{
		/*	var editor = new EditorView();
			addChild(editor);*/
			test = new Sprite();
			var tf:TextField = new TextField(100, 100, "SOME-SOME!", "Verdana", 24, 0xFF0000);
			test.addChild(tf);
			this.addChild(test);
			
			var blinkTextTween:Tween = new Tween(tf, 0.3);
			blinkTextTween.reverse = true;
			blinkTextTween.repeatCount = 2;
			blinkTextTween.scaleTo(1.2);
			blinkTextTween.animate("color", Color.WHITE);
			//blinkTextTween.onComplete = onTextTweenAnimComple;
			//blinkTextTween.onCompleteArgs = [blinkTextTween];
			Starling.juggler.add(blinkTextTween);
				
			/*	var blinkIconTween:Tween = new Tween(icon, 0.3);
				blinkIconTween.reverse = true;
				blinkIconTween.repeatCount = 2;
				blinkIconTween.scaleTo(1.2);
				//blinkIconTween.onComplete = onTextTweenAnimComple;
				//blinkIconTween.onCompleteArgs = [blinkIconTween];
				Starling.juggler.add(blinkIconTween);*/
			
			/*private function onTextTweenAnimComple(args:Array):void
			{
			Starling.juggler.remove(args[0] as Tween);
			}*/
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
			
		}
		
		private function onKeyDown(e:KeyboardEvent):void
		{

			if(	e.keyCode == Keyboard.E)
			{
				this.removeChild(test);
			}
			
			if(	e.keyCode == Keyboard.A)
			{
				this.addChild(test);
			}
		}
	}
}