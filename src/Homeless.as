package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DRenderMode;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	
	[SWF(width="1024", height="768", frameRate="30", backgroundColor="#000000")]
	public class Homeless extends Sprite
	{
		private var _starling:Starling;
		
		public function Homeless()
		{
			// stats class for fps
			//addChild ( new Stats() );
			stage.align = StageAlign.TOP;
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			// create our Starling instance
			_starling = new Starling(Game, stage);
			// set anti-aliasing (higher the better quality but slower performance)
			_starling.antiAliasing = 1;
			// start it!
			_starling.start();
			
			_starling.stage.addEventListener(Event.RESIZE, onResized);
			_starling.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		private function onKeyDown(e:KeyboardEvent):void
		{
			if(e.altKey && e.ctrlKey)
			{
				if(e.keyCode == Keyboard.F)
				{
					if(stage.displayState == StageDisplayState.FULL_SCREEN_INTERACTIVE)
					{
						stage.displayState = StageDisplayState.NORMAL;
					}
					else
					{
						stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
					}
				}
			}
		}
		
		private const NORMAL_WIDTH:int = 1024;
		private const NORMAL_HEIGHT:int = 768;
		private function onResized(event:Event, size:Point):void
		{
			var ratio:Number = stage.stageHeight/NORMAL_HEIGHT;
			RectangleUtil.fit(
				new Rectangle(0, 0, NORMAL_WIDTH*ratio, NORMAL_HEIGHT*ratio),
				new Rectangle(0, 0, size.x, size.y),
				ScaleMode.SHOW_ALL, true,
				Starling.current.viewPort
			);
		}
	}
}