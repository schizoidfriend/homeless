package
{
	import flash.geom.Point;
	
	import flashx.textLayout.elements.BreakElement;
	
	import map.CollectableObject;
	import map.characters.NPC;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class Tile
	{
		//sub-types
		public static const BASEMENT_TILE:int = 0;
		
		private var _type:int;
		private var _imageName:String;
		private var _pos:Point;
		
		private var _image:Image = null;
		
		private var _collectables:Vector.<CollectableObject> = new Vector.<CollectableObject>();
		private var _npcs:Vector.<NPC> = new Vector.<NPC>();
		
		public function Tile(type:int, imageName:String, posx:Number, posy:Number)
		{
			super();
			_type = type;
			_pos = new Point(posx, posy);
			_imageName = imageName;
		}
		
		public function get collectables():Vector.<CollectableObject>
		{
			return _collectables;
		}
		
		public function get npcs():Vector.<NPC>
		{
			return _npcs;
		}

		public function get imageName():String
		{
			return _imageName;
		}

		public function set imageName(value:String):void
		{
			_imageName = value;
		}

		public function get image():Image
		{
			return _image;
		}

		public function get type():int
		{
			return _type;
		}

		public function set type(value:int):void
		{
			_type = value;
		}

		public function get pos():Point
		{
			return _pos;
		}

		public function set pos(value:Point):void
		{
			_pos = value;
		}
		
		public function get x():Number
		{
			return _pos.x;
		}
		
		public function get y():Number
		{
			return _pos.y;
		}
		
		public function set x(value:Number):void
		{
			_pos.x = value;
		}
		
		public function set y(value:Number):void
		{
			_pos.y = value;
		}
		
		public function redrawIfNeeded():void
		{
			if(!_image)
			{
				redraw();
			}
		}
		
		
		public function redraw():void
		{
			killImage();
			_image = new Image(Game.instance.assets.getTexture(_imageName));
			
		}
		
		public function killImage():void
		{
			if(_image)
			{
				_image.dispose();
				_image = null;	
			}
		}
		
		public function addCollectable(collectable:CollectableObject):void
		{
			_collectables.push(collectable);
		}
		
		public function addNPC(npc:NPC):void
		{
			_npcs.push(npc);
		}
		
		public function killNPC(id:int):void
		{
			var npc:CollectableObject;
			var index:int = 0;
			for each (npc in _npcs)
			{
				if(npc.id == id)
				{
					_npcs.splice(index, 1);
					break;
				}
				index++;
			}
		}
		
		public function killCollectable(id:int):void
		{
			var collectable:CollectableObject;
			var index:int = 0;
			for each (collectable in _collectables)
			{
				if(collectable.id == id)
				{
					_collectables.splice(index, 1);
					break;
				}
				index++;
			}
		}

	}
}