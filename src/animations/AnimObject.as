package animations
{
	import flash.geom.Point;
	
	import starling.display.Sprite;

	public class AnimObject
	{
		public static const SCALE:int = 0;
		public static const ROTATE:int = 1;		
		
		private var _type:int;
		private var _object:Sprite;
		private var _framesTotal:int;
		private var _currentFrame:int = 0;
		private var _goalValue:Number;
		private var _currentValue:Number = 0;
		private var _objectPt:Point;
		
		private var _step:Number;
		
		public function AnimObject(objectPt:Point, type:int, object:Sprite, framesTotal:int, goalValue:Number)
		{
			_objectPt = objectPt;
			_type = type;
			_object = object;
			_framesTotal = framesTotal;
			_goalValue = goalValue;
			_currentFrame = currentValue;
			makeCurrentValue();
			_step = (_goalValue - _currentValue) / framesTotal;
		}
		
		public function get step():Number
		{
			return _step;
		}

		public function get objectPt():Point
		{
			return _objectPt;
		}

		public function get currentValue():Number
		{
			return _currentValue;
		}

		public function get goalValue():Number
		{
			return _goalValue;
		}

		public function get currentFrame():int
		{
			return _currentFrame;
		}

		public function get framesTotal():int
		{
			return _framesTotal;
		}

		public function get object():Sprite
		{
			return _object;
		}

		public function get type():int
		{
			return _type;
		}

		public function addFrame():void
		{
			_currentFrame++;
		}
		
		public function addStep():void
		{
			switch(_type)
			{
				case SCALE:
					if(_step > 0)
					{
						_currentValue += _step;
					}
					else
					{
						_currentValue = 1.0 + _step;
					}
					break;
				case ROTATE:
					_currentValue = _step;
					break;
			}
			
		}
		
		private function makeCurrentValue():void
		{
			switch(_type)
			{
				case SCALE:
					_currentValue = _object.scaleX;
					break;
				case ROTATE:
					_currentValue = object.rotation;
					break;
			}
		}
	}
}