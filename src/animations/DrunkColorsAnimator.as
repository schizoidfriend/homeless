package animations
{
	import flash.events.TimerEvent;
	import flash.sampler.stopSampling;
	import flash.utils.Timer;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.ColorMatrixFilter;
	import starling.filters.FragmentFilter;

	public class DrunkColorsAnimator
	{
		private var _colorsTimer:Timer;
		
		private var _colorObject:Sprite;
		private var _initialFilterMatrix:Vector.<Number>;
		private var _filterToChange:ColorMatrixFilter;
		
		public function DrunkColorsAnimator()
		{
		}
		
		public function startAnimation(object:Sprite, colorMatrix:ColorMatrixFilter, timeToColor:int):void
		{
			if(_filterToChange && _filterToChange != colorMatrix)
			{
				stopAnimation();
			}
			_initialFilterMatrix = null;
			_colorObject = object;

			var len:int = colorMatrix.matrix.length;
			_initialFilterMatrix = colorMatrix.matrix.slice(0, len);
			_filterToChange = colorMatrix;
				
			if(_colorsTimer && _colorsTimer.running)
			{
				_colorsTimer.reset();
				_colorsTimer.start();
			}
			else
			{
				_colorsTimer = new Timer(timeToColor, 1);
				_colorsTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onColorTimer, false, 0, true);
				_colorsTimer.start();
				
				_colorObject.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
		}
			
		private function stopAnimation():void
		{
			_colorsTimer.stop();
			_colorObject.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			_filterToChange.matrix = _initialFilterMatrix;
		}
		
		private function onColorTimer(e:TimerEvent):void
		{
			stopAnimation();
		}
		
		private function onEnterFrame(e:Event):void
		{
			_filterToChange.invert();
		}
	}
}