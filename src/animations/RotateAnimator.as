package animations
{
	import fl.motion.MatrixTransformer;
	
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import starling.display.Sprite;
	import starling.events.Event;

	public class RotateAnimator
	{
		private var _animObject:AnimObject;
		private var _onEndFunction:Function;
		
		public function RotateAnimator()
		{
		}
		
		public function rotateObjectAroundPoint(object:Sprite, pt:Point, rotateTo:Number, frames:int, onEndFunction:Function = null):void
		{
			object.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			_onEndFunction = onEndFunction;
			_animObject = new AnimObject(pt, AnimObject.ROTATE, object, frames, rotateTo);	
		}
		
		private function onEnterFrame(e:Event):void
		{
			
			var objMat:Matrix = _animObject.object.transformationMatrix.clone();	
			var mat:Matrix= objMat.clone();
			_animObject.addStep();
			MatrixTransformer.rotateAroundInternalPoint(mat, _animObject.objectPt.x, _animObject.objectPt.y, _animObject.currentValue);
			_animObject.addFrame();
			_animObject.object.transformationMatrix=mat;
			
			if(_animObject.currentFrame >= _animObject.framesTotal)
			{
				_animObject.object.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				_animObject = null;
				_onEndFunction && _onEndFunction();
			}
		}
	}
}