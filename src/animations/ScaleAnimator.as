package animations
{
	import fl.motion.MatrixTransformer;
	
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import starling.display.Sprite;
	import starling.events.Event;

	public class ScaleAnimator
	{
		private var _animObject:AnimObject;
		private var _onEndFunction:Function;
		public function ScaleAnimator()
		{
		}
		
		public function scaleObjectAroundPoint(object:Sprite, pt:Point, scaleTo:Number, frames:int, onEndFunction:Function = null):void
		{
			
			object.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			_onEndFunction = onEndFunction;
			_animObject = new AnimObject(pt, AnimObject.SCALE, object, frames, scaleTo);	
			_animObject.addStep();
		}
		
		private function onEnterFrame(e:Event):void
		{
			
			var objMat:Matrix = _animObject.object.transformationMatrix.clone();	
			var mat:Matrix= objMat.clone();

			MatrixTransformer.scaleArounfInternalPoint(mat, _animObject.objectPt.x, _animObject.objectPt.y, _animObject.currentValue, _animObject.currentValue);
			_animObject.addFrame();
			_animObject.object.transformationMatrix=mat;
			
			var shoulEnd:Boolean = false;
			if(_animObject.step > 0)
			{
				if(_animObject.object.scaleX >= _animObject.goalValue)
				{
					shoulEnd = true
				}
			}
			
			if(_animObject.step < 0)
			{
				if(_animObject.object.scaleX <= _animObject.goalValue)
				{
					shoulEnd = true
				}
			}
			
			if(shoulEnd)
			{
				_animObject.object.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				_animObject = null;
				_onEndFunction && _onEndFunction();
			}
		}
	}
}