package animations
{
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import starling.display.Sprite;
	import starling.events.Event;

	public class ShakeAnimator
	{
		private var _shakeTimer:Timer;
		
		private var _shakeObject:Sprite = null;
		
		private var _isMovingLeft:Boolean = false;
		private var _step:int;
		private var _initialPos:Point = new Point();
		
		public function ShakeAnimator()
		{
		}
		
		public function startShaking(shakeObject:Sprite, timeToShake:int, step:int):void
		{
			if(_shakeObject && _shakeObject != shakeObject)
			{
				stopShaking();
			}
			_shakeObject = shakeObject;
			_initialPos.setTo(_shakeObject.x, _shakeObject.y);
			_step = step;
			if(_shakeTimer && _shakeTimer.running)
			{
				_shakeTimer.reset();
				_shakeTimer.start();
			}
			else
			{
				_shakeTimer = new Timer(timeToShake, 1);
				_shakeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onShakeTimer, false, 0, true);
				_shakeTimer.start();
				
				_shakeObject.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
		}
			
		private function stopShaking():void
		{
			_shakeTimer.stop();
			_shakeObject.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			_shakeObject.x = _initialPos.x;
			_shakeObject.y = _initialPos.y;
		}
		
		private function onShakeTimer(e:TimerEvent):void
		{
			stopShaking();
		}
		
		private function onEnterFrame(e:Event):void
		{
			_shakeObject.x = _initialPos.x + (_isMovingLeft ? _step : -_step);
			_shakeObject.y = _initialPos.y + (_isMovingLeft ? _step : -_step); 
			
			_isMovingLeft = !_isMovingLeft;
		}
	}
}