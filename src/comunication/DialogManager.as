package comunication
{
	import comunication.dialog.AllDayDialogs;
	import comunication.dialog.AllDialogs;
	import comunication.dialog.AllPhrases;
	import comunication.dialog.DayDialogs;
	import comunication.dialog.DialogWindow;
	import comunication.marketplace.LombardWindow;
	import comunication.marketplace.MarketplaceWindow;
	import comunication.marketplace.WastedShopWindow;
	
	import shops.SimpleShop;
	
	import starling.display.Sprite;

	public class DialogManager extends Sprite
	{
		//private var _simpleDialog:SimpleDialog = new SimpleDialog(simpleDialogDispatch);
		//private var _manyTextDialog:ManyVariantsTextWindow = new ManyVariantsTextWindow(manyVariantsDialogDispatch);
		private var _dialog:DialogWindow;
		private var _market:MarketplaceWindow;
		private var _wasted:WastedShopWindow;
		private var _lombard:LombardWindow;
		
		private var _allDaysDialogs:AllDayDialogs = new AllDayDialogs();
		private var _allDialogs:AllDialogs = new AllDialogs();
		private var _allPhrases:AllPhrases = new AllPhrases();
		
		public function DialogManager()
		{
			/*_simpleDialog.x = CommonVars.screenHalfWidth - _simpleDialog.width/2;
			_simpleDialog.y = CommonVars.screenHalfHeight - _simpleDialog.height/2;
			
			_manyTextDialog.x = CommonVars.screenHalfWidth - _manyTextDialog.width/2;
			_manyTextDialog.y = CommonVars.screenHalfHeight - _manyTextDialog.height/2;*/
			
			_dialog = new DialogWindow(this);
			_dialog.x = CommonVars.screenHalfWidth - _dialog.width/2;
			_dialog.y = CommonVars.screenHalfHeight - _dialog.height/2;
			
			_market = new MarketplaceWindow(this);
			_market.x = CommonVars.screenHalfWidth - _market.width/2;
			_market.y = CommonVars.screenHalfHeight - _market.height/2;
			
			_wasted =  new WastedShopWindow(this);
			_wasted.x = CommonVars.screenHalfWidth - _wasted.width/2;
			_wasted.y = CommonVars.screenHalfHeight - _wasted.height/2;
			
			_lombard = new LombardWindow(this);
			_lombard.x = CommonVars.screenHalfWidth - _lombard.width/2;
			_lombard.y = CommonVars.screenHalfHeight - _lombard.height/2;
		}
		
		public function get allPhrases():AllPhrases
		{
			return _allPhrases;
		}

		/*public function showTempDialog():void
		{
			addChild(_dialog);
			var temp:Vector.<String> = new Vector.<String>();
			temp.push("day1AfganVodkaQuest");
			temp.push("day1AfganLife");
			temp.push("day1AfganInCity");
			temp.push("day1HaveVodka");
			_dialog.setDialogVariants(temp);
			_dialog.setDialogBack("Dialogue_Alko");
		}*/
		
		public function showWithCharacter(characterName:String, day:uint):void
		{
			var dialDay:DayDialogs = _allDaysDialogs.getDialogsForCharacterAtDay(characterName, day);
			if(dialDay)
			{
				addChild(_dialog);
				
				_dialog.setDialogVariants(dialDay.dialogIDs);
				_dialog.setDialogBack(dialDay.backgroundImg);
			}
		}
		
		public function isDayLastFor(characterName:String, day:int):Boolean
		{
			return _allDaysDialogs.isDayLastFor(characterName, day);
		}
		
		public function showDialogWith(dialogs:Vector.<String>, dialogBack:String):void
		{
			addChild(_dialog);
			_dialog.setDialogVariants(dialogs);
			_dialog.setDialogBack(dialogBack);
		}
		
		public function hideCurrentDialog():void
		{
			removeChild(_dialog);
		}
		
		public function showMarketplaceWindow(shop:SimpleShop):void
		{
			addChild(_market);
			_market.setItems(shop);
			if(shop.shopPicture.length)
			{
				_market.setDialogBack(shop.shopPicture);
			}
			else
			{
				_market.setDialogBack(shop.shopName);
			}
		}
		
		public function hideMarketplaceWindow():void
		{
			removeChild(_market);
		}
		
		public function showWastedShop(shop:SimpleShop):void
		{
			addChild(_wasted);
			_wasted.setWastedShop(shop);
			if(shop.shopPicture.length)
			{
				_wasted.setDialogBack(shop.shopPicture);
			}
		}
		
		public function hideWastedShop():void
		{
			removeChild(_wasted);
		}
		
		public function showLombardShop(shop:SimpleShop):void
		{
			addChild(_lombard);
			_lombard.setItems(shop);
			if(shop.shopPicture.length)
			{
				_lombard.setDialogBack(shop.shopPicture);
			}
		}
		
		public function hideLombardShop():void
		{
			removeChild(_lombard);
		}
		
		/*public function showDialog(text:String):void
		{
			addChild(_simpleDialog);
			
			_simpleDialog.setDialogText(text);
		}
		
		public function showManyTextsDialog(textsID:String):void
		{
			if(!this.contains(_manyTextDialog))
			{
				addChild(_manyTextDialog)
			}
			_manyTextDialog.createDialog(_allDialogs.getDialog(textsID));
		}
		*/

		public function get allDialogs():AllDialogs
		{
			return _allDialogs;
		}

		/*static public function simpleDialogDispatch():void
		{
			Game.instance.currentState = Game.MOVING_STATE;
		}
		
		static public function manyVariantsDialogDispatch(answer:Phrase):void
		{
			if(answer == null)
			{
				Game.instance.dialogs.exitManyVariantsDialog();
			}
			else
			{
				var actions:Vector.<Object> = answer.actionObjects;
				var nextActionConditioned:Boolean = false;
				var expectedCondition:Boolean = false;
				var onceAction:Boolean = false;
				for each (var obj:Object in actions)
				{
					if(nextActionConditioned)
					{
						var condition:Boolean =  String(obj.condition) == "true" ? true : false;
						if(condition != expectedCondition)
						{
							continue;
						}
					}
					if(obj.hasOwnProperty("once"))
					{
						if(obj.hasOwnProperty("wasOnce") && Boolean(obj.wasOnce))
						{
							continue;
							
						}
						else
						{
							onceAction = true;
							obj.wasOnce = true;
						}
					}
					
					if(obj.hasOwnProperty("always") && onceAction)
					{
						continue;
					}
					
					var at:int = obj.actionType;
					switch(at)
					{
						case Phrase.ACTION_EXIT:
							Game.instance.dialogs.exitManyVariantsDialog();
							break;
						case Phrase.ACTION_DIALOG:
							//Game.instance.dialogs.showManyTextsDialog(obj.id);
							break;
						case Phrase.ACTION_GIVE:
							expectedCondition = executeGiveTakeAction(true, String(obj.resource), int(obj.amount));
							nextActionConditioned = true;
							break;
						case Phrase.ACTION_TAKE:
							executeGiveTakeAction(false, String(obj.resource), int(obj.amount));
							break;
					}
				}
			}
		}*/
		
		//returns isSuccessfull;
		/*static private function executeGiveTakeAction(isGive:Boolean, resource:String, amount:int):Boolean
		{
			switch(resource)
			{
				case "money":
					if(isGive)
					{
						if(!Game.instance.dude.canAfford(amount))
						{
							return false;
						}
						Game.instance.dude.spendMoney(amount);
					}
					else
					{
						Game.instance.dude.earnMoney(amount);
					}
					break;
				default:
					
					break
			}
			
			return true;
		}
		
		public function exitManyVariantsDialog():void
		{
			Game.instance.currentState = Game.MOVING_STATE;
			//_manyTextDialog.killMe();
		}*/
	}
}