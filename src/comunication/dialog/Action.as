package comunication.dialog
{
	/* One action from dialog variant */
	public class Action
	{
		public static const ACTION_EXIT:int = 0;
		public static const ACTION_DIALOG:int = 1;
		public static const ACTION_GIVE:int = 2;
		public static const ACTION_TAKE:int = 3;
		public static const ACTION_CANT_BE_DONE:int = 4;
		public static const ACTION_SHOW_OVER_SCREEN:int = 5;
		public static const ACTION_END_DAY:int = 6;
		
		public static const FROM_PLAYER:int = 0;
		public static const FROM_OPPONENT:int = 1;
		
		public var type:int;
		public var dialogID:String;
		public var additional:Object = new Object();
		public var from:int;
		
		public function Action()
		{

		}
		
		public function parseAdditional(add:String):void
		{
			var parts:Array = add.split(" ");
			additional.amount = int(parts[0]);
			additional.item = String(parts[1]);
		}
		
		public function parseAdditionalScreen(add:String):void
		{
			additional.screen = add;
		}
	}
}