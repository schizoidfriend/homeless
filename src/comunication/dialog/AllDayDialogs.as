package comunication.dialog
{
	public class AllDayDialogs
	{
		[Embed(source="res/AllDayQuests.xml", mimeType="application/octet-stream")]
		public const AllDayQuestsXML:Class;
		
		private var _dayDilaogList:Vector.<DayDialogs> = new Vector.<DayDialogs>();
		
		public function AllDayDialogs()
		{
			var xml:XML = XML(new AllDayQuestsXML());
			onLoadedXML(xml);
		}
		
		private function onLoadedXML(xml:XML):void
		{
			for each (var object:XML in xml.character)
			{
				
				var charName:String = String(object.@name);
				
				for each (var dayXML:XML in object.day)
				{
					var dayDilaogsElement:DayDialogs = new DayDialogs();
					dayDilaogsElement.characterName = charName;
					
					var isLastDayVal:String = String(dayXML.@isLastDay);
					var isLastDay:Boolean = (isLastDayVal == "true") ? true : false;
					
					dayDilaogsElement.isLastDay = isLastDay;
					
					dayDilaogsElement.dayNum = int(dayXML.@num);
					
					dayDilaogsElement.backgroundImg = String(dayXML.@backgroundImg);
					
					for each (var dialogXML:XML in dayXML.dialog)
					{
						var dialog:String = String(dialogXML.@name);
						dayDilaogsElement.dialogIDs.push(dialog);
					}
					
					_dayDilaogList.push(dayDilaogsElement);
				}
				
			}
		}
		
		public function getDialogsForCharacterAtDay(characterName:String, day:uint):DayDialogs
		{
			for each (var dial:DayDialogs in _dayDilaogList)
			{
				if(dial.characterName == characterName && dial.dayNum == day)
				{
					return dial;
				}
			}
			return null;
		}
		
		public function isDayLastFor(characterName:String, day:uint):Boolean
		{
			for each (var dial:DayDialogs in _dayDilaogList)
			{
				if(dial.characterName == characterName && dial.dayNum == day)
				{
					return dial.isLastDay;
				}
			}
			return false;
		}
	}
}