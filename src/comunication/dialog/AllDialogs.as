package comunication.dialog
{
	import utils.ArtUtils;

	/* keeper for all dialogs data */
	public class AllDialogs
	{
		[Embed(source="res/AllDialogs.xml", mimeType="application/octet-stream")]
		public const AllDialogsXML:Class;
		
		private var _allDialogs:Vector.<DialogElement> = new Vector.<DialogElement>();
		
		public function AllDialogs()
		{
			var xml:XML = XML(new AllDialogsXML());
			onLoadedXML(xml);
		}
		
		private function onLoadedXML(xml:XML):void
		{
			for each (var object:XML in xml.dialog)
			{
				var dialog:DialogElement = new DialogElement();
				dialog.id = String(object.@id);
				var pictName:String = String(object.@pict);
				dialog.pict = ArtUtils.makeImage(pictName);
				
				if(object.hasOwnProperty("@ifhave"))
				{
					dialog.ifHave = true;
					dialog.parseAndSetIfHave(String(object.@ifhave));
				}
				else
				{
					dialog.ifHave = false;
				}
				for each (var resultXML:XML in object.result)
				{
					var action:Action = new Action();
					action.type = resultXML.@type;
					action.dialogID = resultXML.@dialID;
					action.from = int(resultXML.@from);
					
					switch(action.type)
					{
						case Action.ACTION_GIVE:
						case Action.ACTION_TAKE:
							action.parseAdditional(resultXML.@add);
							break;
						
						case Action.ACTION_SHOW_OVER_SCREEN:
							action.parseAdditionalScreen(resultXML.@add);
							break;
					}
					
					dialog.phrases.push(action);
				}
				
				_allDialogs.push(dialog);
			}
		}
		
		public function getDialog(id:String):DialogElement
		{
			for each(var dialogData:DialogElement in _allDialogs)
			{
				if(dialogData.id == id)
				{
					return dialogData;
				}
			}
			return null;
		}
	}
}