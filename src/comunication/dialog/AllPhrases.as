package comunication.dialog
{
	public class AllPhrases
	{
		[Embed(source="res/AllPhrases.xml", mimeType="application/octet-stream")]
		public const AllPhrasesXML:Class;
		
		private var _allPhrases:Vector.<Phrase> = new Vector.<Phrase>();
		
		public function AllPhrases()
		{
			var xml:XML = XML(new AllPhrasesXML());
			onLoadedXML(xml);
		}
		
		private function onLoadedXML(xml:XML):void
		{
			for each (var object:XML in xml.phrase)
			{
				var id:String = String(object.@id);
				var pictName:String = String(object.@pict);
				_allPhrases.push(new Phrase(id, pictName));	
			}
		}
		
		public function getPhrase(id:String):Phrase
		{
			for each(var gp:Phrase in _allPhrases)
			{
				if(gp.id == id)
				{
					return gp;
				}
			}
			return null;
		}
	}
}