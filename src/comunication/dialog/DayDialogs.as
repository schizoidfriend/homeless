package comunication.dialog
{
	public class DayDialogs
	{
		public var characterName:String;
		public var dayNum:uint;
		public var isLastDay:Boolean = false;
		public var backgroundImg:String;
		public var dialogIDs:Vector.<String> = new Vector.<String>();
		
		public function DayDialogs()
		{
		}
	}
}