package comunication.dialog
{
	import starling.display.Button;
	import starling.display.Image;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	
	import utils.ArtUtils;
	
	public class DialogButton extends Button
	{
		private var _simpleBackTex:Texture;
		
		private function combineTextures(tex1:Texture, tex2:Texture):Texture
		{
			var rt:RenderTexture = new RenderTexture(tex1.width, tex1.height);
			
			var img1:Image = new Image(tex1);
			rt.draw(img1);
			
			var img2:Image = new Image(tex2);
			img2.x = img1.width/2 - img2.width/2;
			img2.y = img1.height/2 - img2.height/2;
			rt.draw(img2);

			return rt;
		}
		
		public function DialogButton(downState:Texture=null)
		{
			_simpleBackTex = Game.instance.assets.getTexture("DialogSlotEmpty");
			super(_simpleBackTex, "", downState);
		}
		
		public function addItem(imgName:String):void
		{
			var tex:Texture = Game.instance.assets.getTexture(imgName);
			var combinedTex:Texture = combineTextures(_simpleBackTex, tex);
			removeItem();
			mUpState = combinedTex;
			mDownState = combinedTex;
			resetContents()
		}
		
		public function addItemImg(img:Image):void
		{
			var combinedTex:Texture = combineTextures(_simpleBackTex, img.texture);
			removeItem();
			mUpState = combinedTex;
			mDownState = combinedTex;
			resetContents()
		}
		
		public function removeItem():void
		{
			mUpState = _simpleBackTex;
			mDownState = _simpleBackTex;
			resetContents()
		}
	}
}