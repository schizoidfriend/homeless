package comunication.dialog
{
	import starling.display.Image;
	import starling.display.Sprite;

	/* dialog variant */
	public class DialogElement
	{
		public var id:String;
		public var pict:Image;
		public var phrases:Vector.<Action> = new Vector.<Action>();
		
		public var ifHaveItemName:String;
		public var ifHaveAmount:uint;
		public var ifHave:Boolean = false;
		
		public function DialogElement()
		{
		}
		
		public function parseAndSetIfHave(ifHave:String):void
		{
			var parts:Array = ifHave.split(" ");
			ifHaveAmount = int(parts[0]);
			ifHaveItemName = String(parts[1]);
		}
	}
}