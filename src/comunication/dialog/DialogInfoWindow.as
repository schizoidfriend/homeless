package comunication.dialog
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.filters.ColorMatrixFilter;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.ArtUtils;
	
	public class DialogInfoWindow extends Sprite
	{
		private var _background:Image;
		private var _infoText:TextField;
		private var _amountText:TextField;
		private var _objectIcon:Sprite = new Sprite();
		
		public function DialogInfoWindow()
		{
			super();
			initialize();
		}
		
		private function initialize():void
		{
			_background = ArtUtils.makeImage("descriptionField");
			addChild(_background);
			
			_infoText = ArtUtils.makeBoldTF(_background.width, 40, 0, 30, 20, 0x0, VAlign.CENTER, HAlign.CENTER);
			_amountText = ArtUtils.makeBoldTF(_background.width/2 - 5, 40, 0, 100, 16, 0x0, VAlign.CENTER, HAlign.RIGHT);
			
			addChild(_infoText);
			addChild(_amountText);
			
			_objectIcon.x = _background.width/2 + 5;
			_objectIcon.y = 100;
			addChild(_objectIcon);
		}
		
		public function setInfo(isObtained:Boolean, objectImgName:String, amount:int):void
		{
			while(_objectIcon.numChildren)
			{
				_objectIcon.removeChildAt(0, true);
			}
			_objectIcon.addChild(ArtUtils.makeImage(objectImgName));
			if(isObtained)
			{
				_infoText.text = "You obtained:";
			}
			else
			{
				_infoText.text = "You lost:";
			}
			_amountText.text = "x"+amount;
		}
		
		public function setCantInfo(isObtained:Boolean, objectImgName:String, amount:int):void
		{
			setInfo(isObtained, objectImgName, amount);
			
			if(isObtained)
			{
				_infoText.text = "Can't give:";
			}
			else
			{
				_infoText.text = "Can't take:";
			}
			
			var cantFilter:ColorMatrixFilter = new ColorMatrixFilter();
			cantFilter.adjustSaturation(-1);
			_objectIcon.filter = cantFilter;
		}
			
	}
}