package comunication.dialog
{
	import comunication.DialogManager;
	
	import flash.events.TimerEvent;
	import flash.net.drm.VoucherAccessInfo;
	import flash.utils.Timer;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	import utils.ArtUtils;
	
	public class DialogWindow extends Sprite
	{
		protected const ITEMS:int = 4;
		protected var _window:Sprite;
		protected var _dialogInfo:DialogInfoWindow = new DialogInfoWindow();
		
		protected var _background:Sprite = new Sprite();
		
		protected var _infoWindow:Sprite;
		
		protected var _curItems:Vector.<DialogButton> = new Vector.<DialogButton>();
		protected var _curVariants:Vector.<DialogElement> = new Vector.<DialogElement>();
		
		protected var _playerPhrase:Sprite = new Sprite();
		protected var _opponentPhrase:Sprite = new Sprite();
		
		protected var _dialogManager:DialogManager;
		
		protected var _currentIDsList:Vector.<String>
		
		protected var _closeButton:Button;
		public function DialogWindow(dialogManager:DialogManager)
		{
			super();
			
			_dialogManager = dialogManager;
			
			initialize();
		}
		
		private var onePhraseTimer:Timer = new Timer(1000);
		private var currentPhraseNum:int = 0;
		private var currentPhrases:Vector.<Action>;
		private function initialize():void
		{
			_window = ArtUtils.makeSimpleSprite("DialogueBG");
			addChild(_window);
			
			_background.x = 14;
			_background.y = 12;
			addChild(_background);
			
			_dialogInfo.x = _window.width/2 - _dialogInfo.width/2;
			_dialogInfo.y = _window.height/2 - _dialogInfo.height/2;
			
			var height:int = _window.height;
			for(var i:int = 0; i < ITEMS; i++)
			{
				var item:DialogButton = new DialogButton();
				item.x = item.width*i;
				item.y = height - item.height;
				_window.addChild(item); 
				item.addEventListener(Event.TRIGGERED, onItemClick);
				_curItems.push(item);
			}
			
			_playerPhrase.x = 150;
			_playerPhrase.y = 300;
			addChild(_playerPhrase);
			
			_opponentPhrase.x = 300;
			_opponentPhrase.y = 150;
			addChild(_opponentPhrase);
			
			_closeButton = new Button(Game.instance.assets.getTexture("Dialog_ButtonClose"));
			addChild(_closeButton);
			_closeButton.x = _window.width - _closeButton.width;
			_closeButton.y = _window.height;
			_closeButton.addEventListener(Event.TRIGGERED, onCloseClicked);
			
			if(!onePhraseTimer.hasEventListener(TimerEvent.TIMER))
			{
				onePhraseTimer.addEventListener(TimerEvent.TIMER, onOnePhraseTimer);
			}
		}
		
		protected function onCloseClicked(e:Event):void
		{
			Game.instance.currentState = Game.MOVING_STATE;
		}
		
		protected function getItemPosition(item:DialogButton, list:Vector.<DialogButton>):int
		{
			for(var i:int = 0; i < ITEMS; i++)
			{
				if(item == list[i])
				{
					return i;
				}
			}
			return -1;
		}
		
		protected function onItemClick(e:Event):void
		{
			var pos:int = getItemPosition(e.currentTarget as DialogButton, _curItems);
			if(pos >= 0)
			{
				showAllPhrases(_curVariants[pos].phrases);
			}
		}
		
		private function disableAllDialogVariants():void
		{
			for(var i:int = 0; i < ITEMS; i++)
			{
				_curItems[i].enabled = false;
			}
		}
		
		private function enableAllDialogVariants():void
		{
			for(var i:int = 0; i < ITEMS; i++)
			{
				_curItems[i].enabled = true;
			}
		}
		
		private function showAllPhrases(phrases:Vector.<Action>):void
		{
			currentPhraseNum = 0;
			disableAllDialogVariants();
			
			currentPhrases = phrases;
			nextAction(currentPhrases[currentPhraseNum]);
		}
		
		protected function getResourceImgName(resName:String):String
		{
			switch(resName)
			{
				case "bottles": return "IconBottle";
				case "cans": return "IconCan";
				case "cigButs": return "cigButt";
				case "money": return "IconCoin";
				case "preciousMetal": return "PreciousMetals";
				case "wastedPapper": return "IconPaper";
			}
			return null;
		}
		
		protected function nextAction(action:Action):void
		{
			clearCurrentPhrases();
			
			onePhraseTimer.start();
			
			if(_background && _background.numChildren && (_background.getChildAt(0) is MovieClip))
			{
				(_background.getChildAt(0) as MovieClip).currentFrame = 0;
				(_background.getChildAt(0) as MovieClip).pause();
			}
			
			if(action.type == Action.ACTION_GIVE)
			{
				Game.instance.dude.removeResource(action.additional.item, action.additional.amount);
				_dialogInfo.setInfo(false, action.additional.item, action.additional.amount);
				addChild(_dialogInfo);
			}
			else if(action.type == Action.ACTION_TAKE)
			{
				if(Game.instance.dude.canPutResource(action.additional.item, action.additional.amount))
				{
					Game.instance.dude.addResource(action.additional.item, action.additional.amount);
					var imgName:String = getResourceImgName(action.additional.item);
					if(imgName)
					{
						_dialogInfo.setInfo(true, imgName, action.additional.amount);
					}
					else
					{
						_dialogInfo.setInfo(true, action.additional.item, action.additional.amount);
					}
					addChild(_dialogInfo);
				}
				else
				{
					//add can't add items info window
				}
			}
			else if(action.type == Action.ACTION_CANT_BE_DONE)
			{
				_dialogInfo.setCantInfo(false, action.additional.item, action.additional.amount);
				addChild(_dialogInfo);
			}
			else if(action.type == Action.ACTION_SHOW_OVER_SCREEN)
			{
				onePhraseTimer.reset();
				killDialogs();
				Game.instance.currentState = Game.MOVING_STATE;
				Game.instance.setOverScreenByName(action.additional.screen as String)
			}
			else if(action.type == Action.ACTION_END_DAY)
			{
				Game.instance.currentNPCTalkingTo.finishCurrentDayQuest();
			}
			else
			{
				var gp:Phrase = _dialogManager.allPhrases.getPhrase(action.dialogID);
				if(action.from == Action.FROM_PLAYER)
				{
					_playerPhrase.addChild(gp.pict);
				}
				else
				{
					_opponentPhrase.addChild(gp.pict);
					if(_background.numChildren > 0)
					{
						(_background.getChildAt(0) as MovieClip).play();
					}
				}
			}
		}
		
		private function onOnePhraseTimer(e:TimerEvent):void
		{
			onePhraseTimer.reset();
			currentPhraseNum++;
			if(currentPhrases && currentPhraseNum < currentPhrases.length)
			{
				nextAction(currentPhrases[currentPhraseNum]);
			}
			else
			{
				killDialogs();
			}
		}
		
		private function killDialogs():void
		{
			if(_background && _background.numChildren && (_background.getChildAt(0) is MovieClip))
			{
				(_background.getChildAt(0) as MovieClip).currentFrame = 0;
				(_background.getChildAt(0) as MovieClip).pause();
			}
			enableAllDialogVariants();
			clearCurrentPhrases();
			resetCurrentDialog();
		}
		
		protected function clearCurrentPhrases():void
		{
			if(this.contains(_dialogInfo))
			{
				removeChild(_dialogInfo);
			}
			while(_playerPhrase.numChildren)
			{
				_playerPhrase.removeChildAt(0);
			}
			while(_opponentPhrase.numChildren)
			{
				_opponentPhrase.removeChildAt(0);
			}
		}
		
		public function setDialogBack(name:String):void
		{
			while(_background.numChildren)
			{
				_background.removeChildAt(0, true);
			}
			var backAnim:MovieClip = ArtUtils.makeMovieClip(name, 8, true);
			if(backAnim)
			{
				_background.addChild(backAnim);
				Starling.juggler.add(backAnim);
				backAnim.stop();
			}
			else
			{
				var backImg:Image = ArtUtils.makeImage(name);
				_background.addChild(backImg);
			}
		}
		
		public function setDialogVariants(IDsList:Vector.<String>):void
		{
			_currentIDsList = IDsList;
			cleanVariants();
			
			var curID:int = 0;
			for each (var id:String in IDsList)
			{
				var di:DialogElement = _dialogManager.allDialogs.getDialog(id);
				if(!di.ifHave || Game.instance.dude.doIHave(di.ifHaveItemName, di.ifHaveAmount))
				{
					_curVariants.push(di);
					_curItems[curID].addItemImg(di.pict);
					_curItems[curID].enabled = true;
					curID++;
				}
			}
			
			var disAmount:int = ITEMS - curID;
			for(var i:int = 0; i < disAmount; i++)
			{
				_curItems[curID + i].enabled = false;
			}
		}
		
		protected function resetCurrentDialog():void
		{
			setDialogVariants(_currentIDsList);
		}
		
		protected function cleanVariants():void
		{
			for(var i:int = 0; i < ITEMS; i++)
			{
				_curItems[i].removeItem();
			}
			
			if(_curVariants.length)
			{
				_curVariants.splice(0, _curVariants.length);
			}
		}
		
		
	}
}