package comunication.dialog
{
	import starling.display.Sprite;
	
	import utils.ArtUtils;

	public class Phrase
	{
		public var id:String;
		public var pict:Sprite
		
		public function Phrase(id:String, pictName:String)
		{
			init(id, pictName);
		}
		
		private function init(id:String, pictName:String):void
		{
			this.id = id;
			pict = ArtUtils.makeSimpleSprite(pictName);
		}
	}
}