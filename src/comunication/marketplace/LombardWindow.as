package comunication.marketplace
{
	import comunication.DialogManager;
	import comunication.dialog.DialogButton;
	import comunication.dialog.Action;
	import comunication.dialog.DialogElement;
	
	import inventory.items.IItem;
	
	import shops.SimpleShop;
	
	import starling.events.Event;
	import starling.filters.ColorMatrixFilter;
	import starling.text.TextField;
	
	import utils.ArtUtils;
	
	public class LombardWindow extends MarketplaceWindow
	{
		private var _sellItems:Vector.<DialogButton> = new Vector.<DialogButton>();
		private var _sellVariants:Vector.<DialogElement> = new Vector.<DialogElement>();
		private var _sellPrices:Vector.<TextField> = new Vector.<TextField>();
		private var _sellObjects:Vector.<IItem> = new Vector.<IItem>();
		
		public function LombardWindow(dialogManager:DialogManager)
		{
			super(dialogManager);
			
			initialize();
		}
		
		private function initialize():void
		{			
			for(var i:int = 0; i < ITEMS; i++)
			{
				var item:DialogButton = new DialogButton();
				item.x = item.width*i;
				item.y = 0;
				_window.addChild(item); 
				item.addEventListener(Event.TRIGGERED, onSellClick);
				_sellItems.push(item);
				
				var tf:TextField = ArtUtils.makeNumberTF(50,25,item.x,item.y,12,0xFFFFFF);
				addChild(tf);
				_sellPrices.push(tf);
			}
		}
		
		override protected function cleanVariants():void
		{
			super.cleanVariants();
			
			for(var i:int = 0; i < ITEMS; i++)
			{
				_sellItems[i].removeItem();
				
				_sellPrices[i].text = "";
				_sellObjects[i] = null;
			}
			
			if(_sellVariants.length)
			{
				_sellVariants.splice(0, _sellVariants.length);
			}
		}
		
		override public function setItems(shop:SimpleShop):void
		{	
			super.setItems(shop);
			var i:int;
			var item:IItem;
			for(i = 0; i < shop.shopInfo.dessortiment.length; i++)
			{
				item = shop.shopInfo.dessortiment[i];
				addSellItem(item, i);
			}
		}
		
		protected function addSellItem(item:IItem, pos:int):void
		{
			_sellObjects[pos] = item;
			
			var di:DialogElement  = new DialogElement();
			di.id = "selitem"+pos;
			//di.pict = ArtUtils.makeImage(item.name);
			di.ifHave = true;
			di.ifHaveAmount = 1;
			di.ifHaveItemName = item.name;
			var ga:Action = new Action();
			ga.type = Action.ACTION_GIVE;
			ga.dialogID = "sellItem";
			di.phrases.push(ga);
			if(!di.ifHave || Game.instance.dude.doIHave(di.ifHaveItemName, di.ifHaveAmount))
			{
				_sellVariants.push(di);
				_sellItems[pos].addItem(item.name);
				_sellItems[pos].filter = null;
				_sellItems[pos].enabled = true
			}
			else
			{
				_sellVariants.push(di);
				_sellItems[pos].addItem(item.name);
				var cantAffordFilter:ColorMatrixFilter = new ColorMatrixFilter();
				cantAffordFilter.adjustSaturation(-1);
				_sellItems[pos].filter = cantAffordFilter;
				_sellItems[pos].enabled = false;
			}
			
			_sellPrices[pos].text = String(item.price);
		}
		
		private function onSellClick(e:Event):void
		{
			var pos:int = getItemPosition(e.currentTarget as DialogButton, _sellItems);
			if(pos >= 0)
			{
				showSellPhrases(_sellVariants[pos].phrases[0] as Action, pos);
			}
		}
		
		private function showSellPhrases(ga:Action, itemPos:int):void
		{
			//will show action window
			var obj:IItem = _sellObjects[itemPos];
			if(obj)
			{
				var itemToRemove:IItem = obj.copy;
				//Game.instance.dude.removeFromInventory(itemToRemove.name, 1);
				Game.instance.dude.earnMoney(itemToRemove.price);
				
				ga.additional.item = itemToRemove.name;
				ga.additional.amount = 1;
				nextAction(ga);
				
				setItems(_currentShop);
			}
			
		}
	}
}