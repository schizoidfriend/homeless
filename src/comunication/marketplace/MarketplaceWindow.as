package comunication.marketplace
{
	import comunication.DialogManager;
	import comunication.dialog.DialogButton;
	import comunication.dialog.Action;
	import comunication.dialog.DialogElement;
	import comunication.dialog.DialogWindow;
	
	import inventory.items.IItem;
	
	import shops.SimpleShop;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.ColorMatrixFilter;
	import starling.text.TextField;
	
	import utils.ArtUtils;
	
	public class MarketplaceWindow extends DialogWindow
	{
		
		private var _prices:Vector.<TextField> = new Vector.<TextField>();
		
		private var _objects:Vector.<IItem> = new Vector.<IItem>();
		
		protected var _currentShop:SimpleShop;
		
		public function MarketplaceWindow(dialogManager:DialogManager)
		{
			super(dialogManager);
			
			initialize();
		}
		
		private function initialize():void
		{
			for(var i:int = 0; i < ITEMS; i++)
			{
				var tf:TextField = ArtUtils.makeNumberTF(50,25,_curItems[i].x,_curItems[i].y,12,0xFFFFFF);
				addChild(tf);
				_prices.push(tf);
			}
		}
		
		public function setItems(shop:SimpleShop):void
		{	
			_currentShop = shop;
			cleanVariants();
			var i:int;
			var item:IItem;
			for(i = 0; i < shop.shopInfo.assortiment.length; i++)
			{
				item = shop.shopInfo.assortiment[i];
				addItem(item, i);
			}
		}
		
		protected function addItem(item:IItem, pos:int):void
		{
			_objects[pos] = item;
			
			var di:DialogElement  = new DialogElement();
			di.id = "item"+pos;
			//di.pict = ArtUtils.makeImage(item.name);
			di.ifHave = true;
			di.ifHaveAmount = item.price;
			di.ifHaveItemName = "money";
			var ga:Action = new Action();
			ga.type = Action.ACTION_TAKE;
			ga.dialogID = "sellItem";
			di.phrases.push(ga);
			if(!di.ifHave || Game.instance.dude.doIHave(di.ifHaveItemName, di.ifHaveAmount))
			{
				_curVariants.push(di);
				_curItems[pos].addItem(item.name);
				_curItems[pos].filter = null;
				_curItems[pos].enabled = true
			}
			else
			{
				_curVariants.push(di);
				_curItems[pos].addItem(item.name);
				var cantAffordFilter:ColorMatrixFilter = new ColorMatrixFilter();
				cantAffordFilter.adjustSaturation(-1);
				_curItems[pos].filter = cantAffordFilter;
				_curItems[pos].enabled = false;
			}
			
			_prices[pos].text = String(item.price);
		}
		
		override protected function onItemClick(e:Event):void
		{
			var pos:int = getItemPosition(e.currentTarget as DialogButton, _curItems);
			if(pos >= 0)
			{
				showBuyPhrases(_curVariants[pos].phrases[0] as Action, pos);
			}
		}
		
		override protected function cleanVariants():void
		{
			super.cleanVariants();
			for(var i:int = 0; i < ITEMS; i++)
			{
				_prices[i].text = "";
				_objects[i] = null;
			}
		}
		
		protected function showBuyPhrases(ga:Action, itemPos:int):void
		{
			//will show action window
			var obj:IItem = _objects[itemPos];
			if(obj)
			{
				var itemToAdd:IItem = obj.copy;
				if(Game.instance.dude.canPutResource(itemToAdd.name, 1))
				{
					Game.instance.dude.spendMoney(obj.price);
					
					ga.additional.item = itemToAdd.name;
					ga.additional.amount = 1;
					nextAction(ga);
	
					setItems(_currentShop);
				}
				else
				{
					ga.type = Action.ACTION_CANT_BE_DONE;
					ga.additional.item = itemToAdd.name;
					ga.additional.amount = 1;
					nextAction(ga);
					
					setItems(_currentShop);
				}
			}
			
		}
		
		override protected function resetCurrentDialog():void
		{
			setItems(_currentShop);
		}
	}
}