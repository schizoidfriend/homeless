package comunication.marketplace
{
	import comunication.DialogManager;
	import comunication.dialog.Action;
	import comunication.dialog.DialogElement;
	import comunication.dialog.DialogWindow;
	
	import inventory.items.IItem;
	
	import shops.SimpleShop;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.ArtUtils;
	
	public class WastedShopWindow extends DialogWindow
	{
		private var _sellIcons:Vector.<Image> = new Vector.<Image>();
		private var _sellPrices:Vector.<TextField> = new Vector.<TextField>();
		
		private var _currentShop:SimpleShop;
		
		public function WastedShopWindow(dialogManager:DialogManager)
		{
			super(dialogManager);
		}
		
		private function cleanWastedPrices():void
		{
			for each (var img:Image in _sellIcons)
			{
				removeChild(img, true);
			}
			
			for each (var tf:TextField in _sellPrices)
			{
				removeChild(tf, true); 
			}
		}
		
		override protected function onItemClick(e:Event):void
		{
			var act:Action = new Action();
			act.type = Action.ACTION_TAKE;
			act.dialogID = "wasted";
			
			sellAllWasted();
			//act.additional.item = "money";
			//act.additional.amount = 
			
			nextAction(act);
		}
		
		//returns money take from selling wasted
		private function sellAllWasted():int
		{
			var moneyBefore:int = Game.instance.dude.getMoneyValue();
			var deItems:Vector.<IItem> = _currentShop.shopInfo.dessortiment;
			for each (var item:IItem in deItems)
			{
				switch(item.name)
				{
					case "Wastepappers":
						Game.instance.dude.sellAllWastePappers(item.price);
						break;
					case "Can":
						Game.instance.dude.sellAllCans(item.price);
						break;
					case "Bottle":
						Game.instance.dude.sellAllBottles(item.price);
						break;
					case "PreciousMetals":
						Game.instance.dude.sellAllPreciousMetals(item.price);
						break;
				}
			}
			
			Game.instance.dude.refreshParameters();
			
			var moneyAfter:int = Game.instance.dude.getMoneyValue();
			return moneyAfter-moneyBefore;
		}
		
		override protected function resetCurrentDialog():void
		{
			setWastedShop(_currentShop);
		}
		
		public function setWastedShop(shop:SimpleShop):void
		{
			_currentShop = shop;
			cleanWastedPrices();
			cleanVariants();
			if(shop.shopInfo.canTake)
			{
				for(var i:int = 0; i < shop.shopInfo.dessortiment.length; i++)
				{
					var item:IItem = shop.shopInfo.dessortiment[i];
					var img:Image = ArtUtils.makeImage(item.name);
					addChild(img);
					img.x = 20+img.width*i;
					var tf:TextField = ArtUtils.makeBoldTF(img.width, 20, img.x, img.y+img.height, 14, 0x0, VAlign.CENTER, HAlign.CENTER);
					addChild(tf);
					
					tf.text = String(item.price);			
				}
				

				_curItems[0].addItemImg(ArtUtils.makeImage("SellAllItem"));
				_curItems[0].enabled = true;
			}
		}
	}
}