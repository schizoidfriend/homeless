package dude
{
	import animations.DrunkColorsAnimator;
	import animations.ShakeAnimator;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.events.TimerEvent;
	import flash.filters.DisplacementMapFilter;
	import flash.filters.DisplacementMapFilterMode;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.drm.VoucherAccessInfo;
	import flash.utils.Timer;
	
	import game.GameExternalParameters;
	
	import gui.Inventory;
	
	import inventory.InventoryItemMaker;
	import inventory.items.IItem;
	
	import map.CurrentMapParameters;
	import map.ExitPoint;
	import map.characters.Character;
	
	import over.OverScreen;
	
	import starling.animation.Juggler;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.ColorMatrixFilter;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	import utils.ArtUtils;
	import utils.UtilsMath;
	
	public class Dude extends Character
	{		
		//hit directions
		public static const HIT_UP:Number = Math.PI/2;
		public static const HIT_RIGHT:Number = Math.PI;
		public static const HIT_DOWN:Number = -(Math.PI/2);
		public static const HIT_LEFT:Number = 0;
		
		//moving states
		public static const STATIC_STATE:int = 0;
		public static const WALKING_STATE:int = 1;
		public static const RUNNING_STATE:int = 2;
		public static const TOTAL_MOVING_STATES:int = 3;
		
		private var _currentMovingState:int = STATIC_STATE;
		
		//dude states
		public static const USE_STATE:int = 0;
		public static const FIGHT_STATE:int = 1;
		public static const TOTAL_STATES:int = 2;
		
		private var _currentState:int = USE_STATE;
		
		//dude states of mind
		public static const NORMAL_STATE:int = 0;
		public static const OVERALCO_STATE:int = 1;
		public static const UPINSMOKE_STATE:int = 2;
		public static const HEAVYDRUGS_STATE:int = 3;
		public static const HUNGRY_STATE:int = 4;
		public static const TOTAL_STATES_OF_MIND:int = 5;
		
		private var _currentStateOfMind:int = TOTAL_STATES_OF_MIND;
		
		private var _startPos:Point = new Point(4,4);
		
		//dude stuff
		private var _dudeWalking:MovieClip;
		private var _dudeStop:Sprite;
		
		private var _dudeFight:MovieClip;
		private var _dudeFightFrames:Vector.<Texture>;
		
		private var _fightAnim:MovieClip;
		
		private var _dudeFrames:Vector.<Texture>;
		
		private var _dudeWalkingAlco:MovieClip;
		private var _dudeStopAlco:Sprite;
		
		private var _dudeWalkingHungry:MovieClip;
		private var _dudeStopHungry:Sprite;
		
		private var _chewingNormal:MovieClip;
		private var _chewingDrunk:MovieClip;
		private var _chewingHungry:MovieClip;
		
		//current walking and stop MCs
		private var _currentWalkingMC:MovieClip;
		private var _currentStopSpr:Sprite;
		private var _currentChuwingMC:MovieClip;
		
		//private var _isWalking:Boolean = false;		
		private var _isAttacking:Boolean = false;	
		private var _shouldRun:Boolean = false;
		
		private var _canDoAnything:Boolean = true;
		
		private var _canMove:Boolean = true;
		private var _staticTimer:Timer;
		
		private var _isVomiting:Boolean = false;
		private var _vommitingTimer:Timer;
		
		private var _isChewing:Boolean = false;
		private var _chewingObject:Sprite;
		
		private var _drunkenCantMove:Boolean = false;
		private var _drunkenTimer:Timer = new Timer(2000);
		
		private var _smellAnimation:MovieClip;
		
		
		//action types
		public static const SLEPT:int = 0;
		public static const ENTERED:int = 1;
		private var _lastAction:int = -1;

		private var _goToGlobalDispatch:Function = null;
		private var _exitPoints:Vector.<ExitPoint> = new Vector.<ExitPoint>();
		
		private var _parameters:DudeParameters = new DudeParameters();
		
		private var _tickTimer:Timer = new Timer(1000);
		
		private var _stopTimerValue:Number;
		
		private var _coughtOnCrime:Boolean = false;
		private var _didKickProstitute:Boolean = false;
		
		private var _hasRandomDirection:Boolean
		private var _randDirection:Point;
		
		private var _shakeAnim:ShakeAnimator = new ShakeAnimator();
		private var _colorsAnim:DrunkColorsAnimator = new DrunkColorsAnimator();
		
		private var _blinkTween:Tween;
		private var _blinkTimer:Timer = new Timer(3000, 0);
		private var _isBlinking:Boolean = false;
		
		public function Dude()
		{
			super();
			init();
			this.touchable = false;
			
			var obj:Object = GameExternalParameters.instance.getObject("timers");
			_staticTimer = new Timer(Number(obj["static"]), 1);
			_vommitingTimer = new Timer(Number(obj["vommit"]));
			
			_stopTimerValue = Number(obj["static"]);
			
			_parameters.loseConsciousnessCallback = Dude.mustLoseConsciousnessCallback;
			_parameters.deadCallback = Dude.mustBeDeadCallback;
			_parameters.mustPukeCallback = Dude.mustPukeCallback;
			_parameters.becomeHungryCallback = Dude.becomeHungryCallback;
			_parameters.becomeNotHungryCallback = Dude.becomeNomalAfterHungryCallback;
			
			_staticTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onStaticTimerCompleted);
			_vommitingTimer.addEventListener(TimerEvent.TIMER, onVommitingTimer);
			_drunkenTimer.addEventListener(TimerEvent.TIMER, onDrunkenCantMoveTimer);
		}
		
		public function get lastAction():int
		{
			return _lastAction;
		}
		
		public function setEnteredAction():void
		{
			_lastAction = ENTERED;
		}
		
		public function setSleptFunction():void
		{
			_lastAction = SLEPT;
		}
		
		public function get didKickProstitute():Boolean
		{
			return _didKickProstitute;
		}
		
		public function set didKickProstitute(value:Boolean):void
		{
			_didKickProstitute = value;
		}
		
		public function get currentStateOfMind():int
		{
			return _currentStateOfMind;
		}

		public function set currentStateOfMind(value:int):void
		{
			if(_currentStateOfMind == value) return ;
			
			Starling.juggler.remove(_currentWalkingMC);
			deselectOldStateOfMind(_currentStateOfMind);
			
			_currentStateOfMind = value;
			
			switch(_currentStateOfMind)
			{
				case NORMAL_STATE:
					_dudeWalking.visible = true;
					_dudeStop.visible = true;
					_currentChuwingMC = _chewingNormal;
					_currentStopSpr = _dudeStop;
					_currentWalkingMC = _dudeWalking;
					break;
				case OVERALCO_STATE:
					_dudeStopAlco.visible = true;
					_dudeWalkingAlco.visible = true;
					_currentChuwingMC = _chewingDrunk;
					_currentStopSpr = _dudeStopAlco;
					_currentWalkingMC = _dudeWalkingAlco;
					break;
				case UPINSMOKE_STATE:
					
					break;
				case HEAVYDRUGS_STATE:
					
					break;
				case HUNGRY_STATE:
					_dudeStopHungry.visible = true;
					_dudeWalkingHungry.visible = true;
					_currentChuwingMC = _chewingHungry;
					_currentStopSpr = _dudeStopHungry;
					_currentWalkingMC = _dudeWalkingHungry;
					break;
			}
			
			setToWalkOrStopState();
		}
		
		private function deselectOldStateOfMind(state:int):void
		{
			switch(state)
			{
				case NORMAL_STATE:
					_chewingNormal.visible = false;
					_dudeWalking.visible = false;
					_dudeStop.visible = false;
					break;
				case OVERALCO_STATE:
					_chewingDrunk.visible = false;
					_dudeWalking.visible = false;
					_dudeStopAlco.visible = false;
					break;
				case UPINSMOKE_STATE:
					
					break;
				case HEAVYDRUGS_STATE:
					
					break;
				case HUNGRY_STATE:
					_chewingHungry.visible = false;
					_dudeStopHungry.visible = false;
					_dudeWalkingHungry.visible = false;
					break;
			}
		}

		public function get canDoAnything():Boolean
		{
			return _canDoAnything;
		}

		public function set canDoAnything(value:Boolean):void
		{
			_canDoAnything = value;
		}

		public function get canMove():Boolean
		{
			return _canMove;
		}

		public function set canMove(value:Boolean):void
		{
			_canMove = value;
			if(_canMove)
			{
				if(Game.instance.shouldMove())
				{
					currentMovingState = _shouldRun ? RUNNING_STATE : WALKING_STATE;
					forceWalk();
				}
			}
			else
			{
				stop();
			}
		}

		public function get shouldRun():Boolean
		{
			return _shouldRun;
		}

		public function set shouldRun(value:Boolean):void
		{
			_shouldRun = value;
			if(currentMovingState != STATIC_STATE)
			{
				if(currentMovingState == WALKING_STATE && _shouldRun)
				{
					run();
				}
				else if(currentMovingState == RUNNING_STATE && !_shouldRun)
				{
					walk();
				}
			}
		}

		public function get currentMovingState():int
		{
			return _currentMovingState;
		}

		public function set currentMovingState(value:int):void
		{
			if(_currentMovingState == value) return;
			
			_currentMovingState = value;
			
			switch(_currentMovingState)
			{
				case STATIC_STATE:
					parameters.staminaStep = parameters.STATIC_STAMINA_STEP;
					break;
				case WALKING_STATE:
					parameters.staminaStep = parameters.WALKING_STAMINA_STEP;
					break;
				case RUNNING_STATE:
					parameters.staminaStep = parameters.RUNNING_STAMINA_STEP;
					break;
			}
		}

		public function get currentState():int
		{
			return _currentState;
		}

		public function set currentState(value:int):void
		{
			_currentState = value;
		}

		protected function get parameters():DudeParameters
		{
			return _parameters;
		}
		
		public function get basicSpeed():Number
		{
			return parameters.realBasicSpeed;
		}
		
		public function get runningSpeed():Number
		{
			return parameters.realSpeedRunning;
		}
		
		public function get seenDistance():Number
		{
			return parameters.realDistanceCanBeSeen;
		}
		
		public function shouldBeAttackedByPolice():Boolean
		{
			return (_coughtOnCrime) || (parameters.isSmellHard() || parameters.hasAnyWeaponInHands());
		}
		
		public function hasAnyWeaponInHands():Boolean
		{
			return parameters.hasAnyWeaponInHands();
		}
		
		public function isSmellHard():Boolean
		{
			return parameters.isSmellHard();
		}
		
		public function get startPos():Point
		{
			return _startPos;
		}
		
		public function canFallAsleep():Boolean
		{
			return (parameters.stamina < 30);
		}
		
		public function resetDudePosition(newStartPos:Point):void
		{
			_startPos.setTo(newStartPos.x, newStartPos.y);
			setDudeToStartPos();
			stop();
			Game.instance.setInitialDudePos();
		}
		
		override protected function init():void
		{
			var fightAtlas:TextureAtlas = Game.instance.assets.getTextureAtlas("Fight_Swoosh");
			var fightFrames:Vector.<Texture> = fightAtlas.getTextures();
			_fightAnim = new MovieClip(fightFrames, 15);
			_fightAnim.loop = false;
			_fightAnim.addEventListener(Event.COMPLETE, onAnimCompleted);
			_fightAnim.pivotX = _fightAnim.width;
			_fightAnim.pivotY = _fightAnim.height/2;
			//_fightAnim.rotation = Math.PI/2;
			//addChild(_fightAnim);
			_fightAnim.stop();
			_fightAnim.addEventListener(Event.COMPLETE, onFightAnimCompleted);
			
			var dudeAtlas:TextureAtlas = Game.instance.assets.getTextureAtlas("HoboWalking");
			
			_dudeFrames = dudeAtlas.getTextures();
			_dudeWalking = new MovieClip(_dudeFrames);
			addChild(_dudeWalking);
			_dudeWalking.visible = false;
			
			var dudeTex:Texture = Game.instance.assets.getTexture("HoboIdle");
			var dudeImg:Image = new Image(dudeTex);
			_dudeStop = new Sprite();
			_dudeStop.addChild(dudeImg);
			_dudeStop.visible = false;
			
			addChild(_dudeStop);
			
			this.scaleX = 0.9;
			this.scaleY = 0.9;
			
			_dudeStop.x = -(_dudeStop.width >> 1);
			_dudeStop.y = -(_dudeStop.height >> 1);
			
			_dudeWalking.x = -(_dudeWalking.width >> 1);
			_dudeWalking.y = -(_dudeWalking.height >> 1);
			
			var dudeFightAtlas:TextureAtlas = Game.instance.assets.getTextureAtlas("FightTurnout");
			_dudeFightFrames = dudeFightAtlas.getTextures();
			_dudeFight = new MovieClip(_dudeFightFrames);
			
			_dudeFight.x = -(_dudeFight.width >> 1);
			_dudeFight.y = -(_dudeFight.height >> 1);
			_dudeFight.visible = false;
			addChild(_dudeFight);
			
			_dudeWalkingAlco = ArtUtils.makeMovieClip("HoboWalkingDrunk", 15, true);
			ArtUtils.centerMCOverItself(_dudeWalkingAlco);
			addChild(_dudeWalkingAlco);
			_dudeWalkingAlco.visible = false;
			
			_dudeStopAlco = ArtUtils.makeSelfCenteredSprite("HoboIdleDrunk");
			addChild(_dudeStopAlco);
			_dudeStopAlco.visible = false;
			
			_dudeWalkingHungry = ArtUtils.makeMovieClip("HoboWalkingHungry", 15, true);
			ArtUtils.centerMCOverItself(_dudeWalkingHungry);
			addChild(_dudeWalkingHungry);
			_dudeWalkingHungry.visible = false;
			
			_dudeStopHungry = ArtUtils.makeSelfCenteredSprite("HoboIdleHungry");
			addChild(_dudeStopHungry);
			_dudeStopHungry.visible = false;
			
			_chewingNormal = ArtUtils.makeMovieClip("HoboChewing", 4, true);
			ArtUtils.centerMCOverItself(_chewingNormal);
			addChild(_chewingNormal);
			_chewingNormal.visible = false;
			
			_chewingDrunk = ArtUtils.makeMovieClip("HoboChewingDrunk", 4, true);
			ArtUtils.centerMCOverItself(_chewingDrunk);
			addChild(_chewingDrunk);
			_chewingDrunk.visible = false;
			
			_chewingHungry = ArtUtils.makeMovieClip("HoboChewingHungry", 4, true);
			ArtUtils.centerMCOverItself(_chewingHungry);
			addChild(_chewingHungry);
			_chewingHungry.visible = false;
			
			_blinkTween = new Tween(this, 0.3);
			_blinkTween.reverse = true;
			_blinkTween.repeatCount = 0;
			_blinkTween.fadeTo(0.5);
			
			_blinkTimer.addEventListener(TimerEvent.TIMER, onBlinkTimer);
			
			_smellAnimation = ArtUtils.makeSelfCenteredMovieClip("HoboSmell_Sprites", 10, true);
			addChild(_smellAnimation);
			
			currentStateOfMind = Dude.NORMAL_STATE;
			
			super.init();
			
			setDudeToStartPos();
			setDudeInitialParameters();
			setSmellAnimation();
			
			stop();
			
			_tickTimer.addEventListener(TimerEvent.TIMER, onTimerTick);		
		}
		
		private function onAnimCompleted(e:starling.events.Event):void
		{
			Starling.juggler.remove(_fightAnim);
			removeChild(_fightAnim);
		}
		
		public function actionInDirection(direction:Number):void
		{
			switch(_currentState)
			{
				case USE_STATE:
					playUseAnimation(direction);
					break;
				case FIGHT_STATE:
					playHitAnimation(direction);
					break;
			}
		}
		
		public function switchBattleMode():void
		{
			if(_currentState == USE_STATE)
			{
				Game.instance.gameInterface.setBattleMode(true);
				_currentState = FIGHT_STATE;
			}
			else
			{
				Game.instance.gameInterface.setBattleMode(false);
				_currentState = USE_STATE;
			}
		}
		
		private function playHitAnimation(direction:Number):void
		{
			parameters.stamina += parameters.ATTACK_STAMINA_DECRESS;
			_isAttacking = true;
			_fightAnim.rotation = direction;
			_fightAnim.currentFrame = 0;
			Starling.juggler.add(_fightAnim);
			addChild(_fightAnim);
			_fightAnim.play();
			attack(direction);
			
		}
		
		private function playUseAnimation(direction:Number):void
		{
			attack(direction);
		}
		
		private function onFightAnimCompleted(e:Event):void
		{
			_isAttacking = false;
			_dudeFight.visible = false;
			setToWalkOrStopState();
		}
		
		private function setToWalkOrStopState():void
		{
			if(currentMovingState == WALKING_STATE || currentMovingState == RUNNING_STATE)
			{
				forceWalk();
			}
			else
			{
				forceStop();
			}
		}
		
		private function onTimerTick(e:TimerEvent):void
		{
			//trace("tick 1 sec");
			_parameters.tick();
			refreshParameters();
		}
		
		public function startDudeTimer():void
		{
			_tickTimer.start();
		}
		
		public function stopDudeTimer():void
		{
			_tickTimer.stop();
		}
		
		public function setDudeInitialParameters():void
		{
			var obj:Object = GameExternalParameters.instance.getObject("statrt_params");
			
			_parameters.alcohol = Number(obj["alcohol"]);
			_parameters.health = Number(obj["health"]);
			_parameters.hunger = Number(obj["hunger"]);
			_parameters.stamina = Number(obj["stamina"]);
			
			_parameters.money = Number(obj["money"]);
			_parameters.smell = Number(obj["smell"]);
			
			Game.instance.gameInterface.setAll(_parameters, false);
		}
		
		public function refreshParameters():void
		{
			Game.instance.gameInterface.setAll(_parameters, true);
		}
		
		public function getMoneyValue():int
		{
			return _parameters.money;
		}
		
		public function setDudeToStartPos():void
		{
			globalPosition.setTo(_startPos.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE, _startPos.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
		}

		public function stop():void
		{
			if(!_isAttacking)
			{
				forceStop();
			}
			currentMovingState = STATIC_STATE;
		}
		
		private function forceStop():void
		{
			_currentWalkingMC.visible = false;
			_currentStopSpr.visible = true;
			
			Starling.juggler.remove(_currentWalkingMC);
		}
		
		public function run():void
		{
			if(currentMovingState != RUNNING_STATE)
			{
				currentMovingState = RUNNING_STATE;
				
				if(!_isAttacking)
				{
					forceWalk();
				}
			}
		}
		
		public function walk():void
		{
			if(currentMovingState != WALKING_STATE)
			{
				currentMovingState = WALKING_STATE;
				
				if(!_isAttacking)
				{
					forceWalk();
				}
			}
		}
		
		private function forceWalk():void
		{
			//trace("forceWalk");
			_currentStopSpr.visible = false;
			_currentWalkingMC.visible = true;
			
			Starling.juggler.add(_currentWalkingMC);
		}
		
		private function attack(direction:Number):void
		{
			//stop();
			_dudeStop.visible = false;
			_dudeFight.visible = true;
			switch(direction)
			{
				case Dude.HIT_UP:
					_dudeFight.currentFrame = 3;
					break;
				case Dude.HIT_DOWN:
					_dudeFight.currentFrame = 0;
					break;
				case Dude.HIT_LEFT:
					_dudeFight.currentFrame = 1;
					break;
				case Dude.HIT_RIGHT:
					_dudeFight.currentFrame = 2;
					break;
			}
			_dudeFight.pause();
			
			_isAttacking = true;
		}
		
		private function isCurrentPositionGood():Boolean
		{
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			
			var possibleBacking:Point = new Point();
			var points:Vector.<Point> = tiles.slice();
			
			for(var i:int = 0; i < points.length; i++)
			{
				var pt:Point = points[i];
				if(!Game.instance.background.isWalkable(pt))
				{
					return false;
				}
			}
			return true;
		}
		
		private function moveSlowly(xOffset:int, yOffset:int):void
		{
			var i:int = 0;
			var xStep:int = 0;
			var xMax:int = xOffset;
			if(xOffset > 0) xStep = 1;
			else if(xOffset < 0) { xStep = -1; xMax = -xMax; }
			for(i = 0; i < xMax; i++)
			{
				_globalPosition.setTo(_globalPosition.x + xStep, _globalPosition.y);
				if(!isCurrentPositionGood())
				{
					_globalPosition.setTo(_globalPosition.x - xStep, _globalPosition.y);
					return;
				}
			}
			
			var yStep:int = 0;
			var yMax:int = yOffset;
			if(yOffset > 0) yStep = 1;
			else if(yOffset < 0) { yStep = -1; yMax = -yMax; }
			for(i = 0; i < yOffset; i++)
			{
				_globalPosition.setTo(_globalPosition.x, _globalPosition.y + yStep);
				if(!isCurrentPositionGood())
				{
					_globalPosition.setTo(_globalPosition.x, _globalPosition.y - yStep);
					return;
				}
			}
		}
		
		
		/* DO I NEED THIS */
		public function dudeKicked(hitAmount:int):void
		{
			if(!_isBlinking)
			{
				//moveSlowly(kickVector.x * 25, kickVector.y * 25);
				_parameters.health -= hitAmount;
				if(_parameters.health <= 0)
				{
					Game.instance.setOverScreen(OverScreen.BEATEN_POLICE_OVER);
				}
				else
				{
					setToHittedState()
				}
			}
		}
		
		private function setToHittedState():void
		{
			addBloodAnim();
			setToBlinkingMode();
		}
		
		protected function addBloodAnim():void
		{
			var blood:MovieClip = ArtUtils.makeMovieClip("Fight_Blood", 15);
			ArtUtils.centerMCOverItself(blood);
			this.addChild(blood);
			blood.play();
			Starling.juggler.add(blood);
			blood.addEventListener(Event.COMPLETE, onBloodAnimFinished);
		}
		
		protected function onBloodAnimFinished(e:Event):void
		{
			this.removeChild(e.currentTarget as MovieClip, true);
		}
		
		private function setToBlinkingMode():void
		{
			Starling.juggler.add(_blinkTween);
			_blinkTimer.start();
			_isBlinking = true;
		}
		
		private function onBlinkTimer(e:TimerEvent):void
		{
			Starling.juggler.remove(_blinkTween);
			_blinkTimer.reset();
			_isBlinking = false;
		}
		
		override public function refreshPositions(tileSize:int):void
		{
			var oldcenterPos:Point = new Point(_centerTilePos.x, _centerTilePos.y);
			super.refreshPositions(tileSize);
			var exit:ExitPoint = checkGlobalTransferPoint();
			if(exit)
			{
				if(exit.targetMap)
				{
					setEnteredAction();
					Game.instance.background.tryToStartNewDayQuests();
					Game.instance.background.loadMap(exit.targetMap);
				}
				else if(_goToGlobalDispatch != null)
				{
					_goToGlobalDispatch();
				}
			}
			if(!oldcenterPos.equals(_centerTilePos))
			{
				Game.instance.background.checkNearestShopsToSetActiveState();
			}
		}
		
		public function setGoToGlobalDispatch(func:Function):void
		{
			_goToGlobalDispatch = func;
		}
		
		public function cleanGoToGlobalPoints():void
		{
			_exitPoints.splice(0, _exitPoints.length);
		}
		
		public function addGlobalGoPoint(exit:ExitPoint):void
		{
			_exitPoints.push(exit);
		}
		
		public function removeGlobalGoPoint(pos:Point):void
		{
			var index:int = 0;
			for each (var exit:ExitPoint in _exitPoints)
			{
				if(exit.tilePos.equals(pos))
				{
					exit.justKill();
					_exitPoints.splice(index, 1);
					return ;
				}
				index++;
			}
		}
		
		public function get globalGoPoints():Vector.<ExitPoint>
		{
			return _exitPoints;
		}
		
		private var exit:ExitPoint;
		private function checkGlobalTransferPoint():ExitPoint
		{
			if(centerTilePos.x == 0 || centerTilePos.x == (CurrentMapParameters.sizeX-1) ||
				centerTilePos.y == 0 || centerTilePos.y == (CurrentMapParameters.sizeY-1))
			{
				
				for each(exit in _exitPoints)
				{
					if(exit.tilePos.equals(centerTilePos))
					{
						return exit;
					}
				}
			}
			return null;
		}
		
		private function getGlobalRect():Rectangle
		{
			var fightRect:Rectangle = _fightAnim.bounds;
			return new Rectangle(globalPosition.x+fightRect.x, globalPosition.y+fightRect.y,
				fightRect.width, fightRect.height);
		}
		
		public function willHit(pos:Point):Boolean
		{
			var rect:Rectangle = getGlobalRect();
			if(rect.contains(pos.x, pos.y))
			{
				return true;
			}
			return false;
		}
		
		public function spendMoney(price:int):void
		{
			parameters.spendMoney(price);
			refreshParameters();
		}
		
		public function earnMoney(amount:int):void
		{
			parameters.money += amount;
			refreshParameters();
		}
		
		public function canAfford(price:int):Boolean
		{
			return parameters.canAfford(price);
		}
		
		public function drinkEnery(effectVal:Number):void
		{
			parameters.stamina += effectVal;
		}
		
		public function drink(effectVal:Number, drinkObjectName:String = "Food"):void
		{
			parameters.alcohol += effectVal;
			refreshParameters();
			
			playEatDrinkAnimation(drinkObjectName);
		}
		
		public function eat(effectVal:Number, eatObjectName:String = "Food"):void
		{
			parameters.hunger -= effectVal;
			refreshParameters();
			
			playEatDrinkAnimation(eatObjectName);
		}
		
		private function playEatDrinkAnimation(eatObjectName:String = "Food"):void
		{
			forceStopFor(2000);
			_currentChuwingMC.visible = true;
			_currentStopSpr.visible = false;
			_currentWalkingMC.visible = false;
			_mainSprite.setChildIndex(_currentChuwingMC, _mainSprite.numChildren - 1);
			Starling.juggler.add(_currentChuwingMC);
			
			_chewingObject = ArtUtils.makeSelfCenteredSprite(eatObjectName);
			_chewingObject.scaleX = 0.4;
			_chewingObject.scaleY = 0.4;
			_chewingObject.x = 0;
			_chewingObject.y = 0;
			this.addChild(_chewingObject);
			
			_isChewing = true;
		}
		
		public function sellAllCans(pricePerOne:int):void
		{
			parameters.sellAllCans(pricePerOne);
		}
		
		public function sellAllBottles(pricePerOne:int):void
		{
			parameters.sellAllBottles(pricePerOne);
		}
		
		public function sellAllWastePappers(pricePerOne:int):void
		{
			parameters.sellAllWastePappers(pricePerOne);
		}
		
		public function sellAllPreciousMetals(pricePerOne:int):void
		{
			parameters.sellAllPreciousMetals(pricePerOne);
		}
		
		public function addWastedsLoot(wm:WastedMaterials):void
		{
			parameters.wm.addWasteds(wm);
		}
		
		public function addSmell(smellToAdd:Number):void
		{
			parameters.smell += smellToAdd;
			setSmellAnimation();
		}
		
		public function washInFountain():void
		{
			parameters.smell -= 10;
			setSmellAnimation();
		}
		
		private function setSmellAnimation():void
		{
			if(parameters.isSmell())
			{
				Starling.juggler.add(_smellAnimation);
				_smellAnimation.visible = true;
				_smellAnimation.alpha = Number(parameters.smell)/100;
			}
			else
			{
				_smellAnimation.visible = false;
				Starling.juggler.remove(_smellAnimation);
			}
		}
		
		public function isGlobalPointInNPCToSmash(pos:Point):Boolean
		{
			var rect:Rectangle = new Rectangle(_globalPosition.x - halfWidth, _globalPosition.y, this.width, halfHeight);
			return rect.containsPoint(pos);
		}
		
		public function sleepingAtHome():void
		{
			setSleptFunction();
			var obj:Object = GameExternalParameters.instance.getObject("sleeping_home");
			parameters.stamina = Number(obj["stamina"]);
			parameters.alcohol = parameters.alcohol < Number(obj["lowest_alcohol"]) ? parameters.alcohol+Number(obj["lowest_change"]) : Number(obj["other_value"]);
			parameters.health = parameters.health < Number(obj["lowest_health"]) ? Number(obj["lowest_val"]) : parameters.health+Number(obj["other_inc"]);
		}
		
		public function sleepingOutside():void
		{
			setSleptFunction();
			var obj:Object = GameExternalParameters.instance.getObject("sleeping_outside");
			parameters.stamina = Number(obj["stamina"]);
			parameters.alcohol = parameters.alcohol < Number(obj["lowest_alcohol"]) ? parameters.alcohol+Number(obj["lowest_change"]) : Number(obj["other_value"]);
			parameters.health = parameters.health < Number(obj["lowest_health"]) ? Number(obj["lowest_val"]) : parameters.health+Number(obj["other_inc"]);
		}
		
		public function loseConsciousness():void
		{
			var obj:Object = GameExternalParameters.instance.getObject("sleeping_lost_consciousness");
			parameters.stamina = Number(obj["stamina"]);
			parameters.alcohol = parameters.alcohol < Number(obj["lowest_alcohol"]) ? parameters.alcohol+Number(obj["lowest_change"]) : Number(obj["other_value"]);
			parameters.health = parameters.health < Number(obj["lowest_health"]) ? Number(obj["lowest_val"]) : parameters.health+Number(obj["other_inc"]);
		}
		
		public function forceStopFor(stopTime:uint):void
		{
			if(_staticTimer.running)
			{
				_staticTimer.reset();
			}
			canMove = false;
			_staticTimer.delay = stopTime;
			_staticTimer.start();
		}
		
		private function onStaticTimerCompleted(e:TimerEvent):void
		{
			_staticTimer.reset();
			canMove = true;
			
			if(_isChewing)
			{
				_isChewing = false;
				_currentStopSpr.visible = true;
				_currentWalkingMC.visible = true;
				_currentChuwingMC.visible = false;
				
				removeChild(_chewingObject, true);
			}
			
			if(_isVomiting)
			{
				setDrunkenMode(true);
				becomeUncontrollable();
			}
		}
		
		private function onDrunkenCantMoveTimer(e:TimerEvent):void
		{
			if(_canMove)
			{
				setDrunkenMode(true);
			}
			else
			{
				setDrunkenMode(false);
			}
		}
		
		private function setDrunkenMode(enabled:Boolean):void
		{
			if(enabled)
			{
				_canMove = false;
				_drunkenCantMove = true;
			}
			else
			{
				_canMove = true;
				becomeControllable();
			}
		}
		
		private function onVommitingTimer(e:TimerEvent):void
		{
			var obj:Object = GameExternalParameters.instance.getObject("alco");
			
			Game.instance.background.addPukeUnderDude();
			Game.instance.dude.forceStopFor(_stopTimerValue);
			parameters.alcohol -= Number(obj["vommit_dec"]);
			
			if(parameters.alcohol < Number(obj["vommiting"]))
			{
				_vommitingTimer.reset();
				_isVomiting = false;
				_canDoAnything = true;
				currentStateOfMind = Dude.NORMAL_STATE;
				setDrunkenMode(false);
				showPhraseOver("Вроде полегчало");
			}
		}
		
		public function startVommiting():void
		{
			//TODO: must turn to green
			showPhraseOver("Шота мине херова");
			currentStateOfMind = Dude.OVERALCO_STATE;
			_vommitingTimer.start();
			_canDoAnything = false;
			_isVomiting = true;
		}
		
		private function startShake():void
		{
			_shakeAnim.startShaking(Game.instance, 2000, 5);
			_colorsAnim.startAnimation(Game.instance, Game.instance.daylightFilter, 2000);
		}
		
		private function becomeUncontrollable():void
		{
			forceWalk();
			startShake();
			if(!_drunkenCantMove)
			{
				_drunkenCantMove = true;
				_drunkenTimer.start();
			}
			else
			{
				_drunkenTimer.reset();
				_drunkenTimer.start();
			}
		}
		
		private function becomeControllable():void
		{
			if(_drunkenCantMove)
			{
				setToWalkOrStopState();
				_drunkenCantMove = false;
				_drunkenTimer.reset();
				_hasRandomDirection = false;
			}
		}
		
		//returns random direction vector
		private function generateRandomDirection(step:int):Point
		{
			var xStep:Number = UtilsMath.randomRange(0, 5);
			var yStep:Number = UtilsMath.randomRange(0, 5);
			var isPlusX:Boolean = UtilsMath.getRandomWithChance(50);
			var isPlusY:Boolean = UtilsMath.getRandomWithChance(50);
			var oneSizedPt:Point = getOneSizedOfPt(new Point(xStep, yStep));
			return new Point(oneSizedPt.x*step*(isPlusX ? 1 : -1), oneSizedPt.y*step*(isPlusY ? 1 : -1));
		}
		
		private function getOneSizedOfPt(point:Point):Point
		{
			var p:Number = Math.sqrt(point.x*point.x + point.y*point.y);
			return new Point(point.x/p, point.y/p);
		}
		
		public function resetRandomDirection():void
		{
			_randDirection = generateRandomDirection(UtilsMath.randomRange(basicSpeed, runningSpeed));
			_hasRandomDirection = true;
		}
		
		public function becameHungry():void
		{
			if(currentStateOfMind != Dude.HUNGRY_STATE)
			{
				showPhraseOver("Пожрать бы чего");
				currentStateOfMind = Dude.HUNGRY_STATE;
			}
		}
		
		public function becomeNotHungry():void
		{
			if(currentStateOfMind != Dude.NORMAL_STATE)
			{
				showPhraseOver("Уже не голодный");
				if(_isVomiting)
				{
					currentStateOfMind = Dude.OVERALCO_STATE;
				}
				else
				{
					currentStateOfMind = Dude.NORMAL_STATE;
				}
			}
		}		
		
		public function setItemToHandS(item:IItem):void
		{
			if(item)
			{
				parameters.setItemToHandS(item);
			}
		}
		
		public function setItemToInventory(item:IItem, pos:int=-1):Boolean
		{
			if(item)
			{
				return parameters.addItemToInventory(item, pos);
			}
			return false;
		}
		
		public function moveInventoryItem(from:uint, to:uint):Boolean
		{
			var item:IItem = null;
			if(from < DudeParameters.INVENTORY_SIZE)
			{
				item = IItem(parameters.inventory[from]).copy;
			}
			else if(from == Inventory.INHAND_SLOT_NUM)
			{
				item = parameters.handsItem.copy;
			}
			var wasAdded:Boolean = parameters.addItemToInventory(item, to);
			parameters.removeItemFromSlot(from);
			
			return wasAdded;
		}
		
		public function exchangeInventoryItems(pos1:int, pos2:int):void
		{
			var item1:IItem = IItem(parameters.inventory[pos1]).copy;
			var item2:IItem = IItem(parameters.inventory[pos2]).copy;
			parameters.removeItemFromSlot(pos1);
			parameters.removeItemFromSlot(pos2);
			
			parameters.addItemToInventory(item1, pos2)
				
			parameters.addItemToInventory(item2, pos1)
			
		}
		
		public function removeFromInventory(itemName:String, amount:int):Boolean
		{
			if(_parameters.doIHave(itemName, amount))
			{
				for(var i:int = 0; i < amount; i++)
				{
					_parameters.removeFromInventory(itemName)
				}
				return true;
			}
			return false;
		}
		
		public function removeFromInventoryPos(pos:uint):Boolean
		{
			if(pos < DudeParameters.INVENTORY_SIZE)
			{
				if(_parameters.inventory[pos] != null)
				{
					_parameters.inventory[pos] = null;
					return true;
				}
			}
			return false;
		}
		
		public function clearInventory():void
		{
			parameters.clearInventory();
		}
		
		public function isHungry():Boolean
		{
			return parameters.isHungry;
		}
		
		public function addCan():void
		{
			parameters.cans++;
			refreshParameters();
		}
		
		public function addBottle():void
		{
			parameters.bottles++;
			refreshParameters();
		}
		
		public function addCigButt():void
		{
			parameters.cigButs++;
			refreshParameters();
		}
		
		public function canPutResource(resource:String, amount:int):Boolean
		{
			switch(resource)
			{
				case "bottles":
				case "cans":
				case "cigButs":
				case "money":
				case "preciousMetal":
				case "wastedPapper":
					return true;
				default:
					var item:IItem = Game.instance.itemsInfo.getItem(resource);
					if(item)
					{
						if(_parameters.howManyFreeSlotsDoIHave() >= amount)
						{
							return true
						}
					}
					break;
			}
			return false;
		}
		
		public function addResource(resource:String, amount:int):void
		{
			switch(resource)
			{
				case "bottles":
					_parameters.bottles += amount;
					break;
				case "cans":
					_parameters.cans += amount;
					break;
				case "cigButs":
					_parameters.cigButs += amount;
					break;
				case "money":
					_parameters.money += amount;
					break;
				case "preciousMetal":
					_parameters.preciousMetal += amount;
					break;
				case "wastedPapper":
					_parameters.wastedPapper += amount;
					break;
				default:
					for(var i:int = 0; i < amount; i++)
					{
						var item:IItem = Game.instance.itemsInfo.getItem(resource);
						_parameters.addItemToInventory(item.copy);
					}
					break;
			}
		}
		
		public function removeResource(resource:String, amount:int):Boolean
		{
			if(doIHave(resource, amount))
			{
				switch(resource)
				{
					case "bottles":
						_parameters.bottles -= amount;
						break;
					case "cans":
						_parameters.cans -= amount;
						break;
					case "cigButs":
						_parameters.cigButs -= amount;
						break;
					case "money":
						_parameters.money -= amount;
						break;
					case "preciousMetal":
						_parameters.preciousMetal -= amount;
						break;
					case "wastedPapper":
						_parameters.wastedPapper -= amount;
						break;
					default:
						removeFromInventory(resource, amount);
				}
				return true;
			}
			return false;
		}
		
		public function doIHave(resource:String, amount:int):Boolean
		{
			switch(resource)
			{
				case "bottles":
					if(_parameters.bottles >= amount) return true;
					break;
				case "cans":
					if(_parameters.cans >= amount) return true;
					break;
				case "cigButs":
					if(_parameters.cigButs >= amount) return true;
					break;
				case "money":
					if(_parameters.money >= amount) return true;
					break;
				case "preciousMetal":
					if(_parameters.preciousMetal >= amount) return true;
					break;
				case "wastedPapper":
					if(_parameters.wastedPapper >= amount) return true;
					break;
				default:
					return _parameters.doIHave(resource, amount);
			}
			return false;
		}
		
		public function leavingMapActions():void
		{
			shouldRun = false;
		}
		
		public function restartAnimations():void
		{
			addSmell(0);
		}
		
		public function get cigButs():uint
		{
			return parameters.cigButs;
		}
		
		public function set cigButs(value:uint):void
		{
			parameters.cigButs = value;
		}
		
		public function get coughtOnCrime():Boolean
		{
			return _coughtOnCrime;
		}
		
		public function set coughtOnCrime(value:Boolean):void
		{
			_coughtOnCrime = value;
		}
		
		public function get drunkenCantMove():Boolean
		{
			return _drunkenCantMove;
		}
		
		public function get randDirection():Point
		{
			return _randDirection;
		}
		
		public function set randDirection(value:Point):void
		{
			_randDirection = value;
		}
		
		public function get hasRandomDirection():Boolean
		{
			return _hasRandomDirection;
		}
		
		public function set hasRandomDirection(value:Boolean):void
		{
			_hasRandomDirection = value;
		}
		
		public function setBackToNonEnemyState():void
		{
			_didKickProstitute = false;
			_coughtOnCrime = false;
		}
		
		public function get inventory():Array
		{
			return _parameters.inventory.concat();
		}
		
		public function get handItem():IItem
		{
			return _parameters.handsItem;
		}
		
		public function useItemInSlot(slotNum:uint):Boolean
		{
			return _parameters.useItemFromSlot(slotNum);
		}
		
		public function getXMLedDudeData():String
		{
			/*
			здоровье, голод, стамина, алкоголь, 
			вонь
			деньги, бутылки, банки, макулатура, металлы, окурки
			инвентарь, в руках 
			*/
			var xml:String = "";
			
			xml+="<parameters health=\""+int(_parameters.health)+"\" "+
							 "hunger=\""+int(_parameters.hunger)+"\" "+
							 "stamina=\""+int(_parameters.stamina)+"\" "+
							 "alcohol=\""+int(_parameters.alcohol)+"\" "+
							 "smell=\""+int(_parameters.smell)+"\" />\n";
			
			xml+="<materials money=\""+_parameters.money+"\" "+
							"bottles=\""+_parameters.bottles+"\" "+
							"cans=\""+_parameters.cans+"\" "+
							"paper=\""+_parameters.wastedPapper+"\" "+
							"metal=\""+_parameters.preciousMetal+"\" "+
							"butts=\""+_parameters.cigButs+"\" />\n";
			
			xml+="<inventory>\n";
			var item:IItem;
			for(var i:int = 0; i <_parameters.inventory.length; i++)
			{
				item = _parameters.inventory[i];
				if(item)
				{
					xml+="\t<item name=\""+item.name+"\" "+
								 "effectVal=\""+item.effectVal+"\" "+
								 "group=\""+item.group+"\" "+
								 "price=\""+item.price+"\" "+
								 "type=\""+item.type+"\" />\n";
				}

			}
			xml+="</inventory>\n";
			
			item = _parameters.handsItem;
			if(item)
			{
				xml+="\t<handsItem name=\""+item.name+"\" "+
					"effectVal=\""+item.effectVal+"\" "+
					"group=\""+item.group+"\" "+
					"price=\""+item.price+"\" "+
					"type=\""+item.type+"\" />\n";
			}
			
			return xml;
		}
		
		public function setDudePrametersFromXML(xml:XML):void
		{
			var parameters:XML = XML(xml.parameters);
			var health:int = int(parameters.@health);
			var hunger:int = int(parameters.@hunger);
			var stamina:int = int(parameters.@stamina);
			var alcohol:int = int(parameters.@alcohol);
			var smell:int = int(parameters.@smell);
			
			_parameters.health = health;
			_parameters.hunger = hunger;
			_parameters.stamina = stamina;
			_parameters.alcohol = alcohol;
			_parameters.smell = smell;
			
			var materials:XML = XML(xml.materials);
			var money:int = int(materials.@money);
			var bottles:int = int(materials.@bottles);
			var cans:int = int(materials.@cans);
			var paper:int = int(materials.@paper);
			var metal:int = int(materials.@metal);
			var butts:int = int(materials.@butts);
			
			_parameters.money = money;
			_parameters.bottles = bottles;
			_parameters.cans = cans;
			_parameters.wastedPapper = paper;
			_parameters.preciousMetal = metal;
			_parameters.cigButs = butts;
			
			_parameters.clearInventory();
			var inventoryXML:XML = XML(xml.inventory);
			var invItemXML:XML;
			var index:int = 0;
			for each (invItemXML in inventoryXML.item)
			{
				var itemName:String = String(invItemXML.@name);
				var item:IItem = InventoryItemMaker.makeItem(itemName);
				
				if(item)
				{
					_parameters.inventory[index] = item;
					index++;
				}
			}
			
			_parameters.removeItemFromSlot(Inventory.INHAND_SLOT_NUM);
			if(xml.hasOwnProperty("handsItem"))
			{
				var handInvXML:XML = XML(xml.handsItem);
				if(handInvXML)
				{
					var handItemName:String = String(handInvXML.@name);
					var handItem:IItem = InventoryItemMaker.makeItem(handItemName);
					
					if(handItem)
					{
						_parameters.addItemToInventory(handItem, Inventory.INHAND_SLOT_NUM);
					}
					
				}
			}
			
		}
		
		static public function mustLoseConsciousnessCallback():void
		{
			Game.instance.setOverScreen(OverScreen.LOSE_CONSCIOUSNESS_OVER);
		}
		
		static public function mustBeDeadCallback():void
		{
			Game.instance.setOverScreen(OverScreen.HUNGER_DEAD_OVER);
		}
		
		static public function mustPukeCallback():void
		{
			Game.instance.dude.startVommiting();
			Game.instance.closeInventory();
		}
		
		static public function becomeHungryCallback():void
		{
			Game.instance.dude.becameHungry();
		}
		
		static public function becomeNomalAfterHungryCallback():void
		{
			Game.instance.dude.becomeNotHungry();	
		}
	}
}