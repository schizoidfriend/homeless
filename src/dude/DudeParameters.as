package dude
{
	import game.GameExternalParameters;
	
	import gui.Inventory;
	
	import inventory.items.IItem;
	import inventory.items.SimpleItem;

	public class DudeParameters
	{
		private var _speedBasic:Number;
		private var _speedRunning:Number;
		private var _speedMultiplier:Number = 1.0     
		
		private var _health:Number;
		private var _hunger:Number;
		private var _stamina:Number;
		private var _alcohol:Number;
		
		//change step per second
		private var _healthStep:Number;
		private var _hungerStep:Number;
		private var _staminaStep:Number;
		private var _alcoholStep:Number;
		
		private var _STATIC_STAMINA_STEP:Number;
		private var _WALKING_STAMINA_STEP:Number;
		private var _RUNNING_STAMINA_STEP:Number
		private var _ATTACK_STAMINA_DECRESS:Number;
		
		private var SMELL:int;
		private var SMELL_HARD:int;
		private var SMELL_DEADLY:int;
		private var _smell:Number = 0;
		
		private var BASIC_SEEN_DISTANCE:Number;
		private var SMELL_SEEN_DISTANCE:Number;
		private var SMELL_HARD_SEEN_DISTANCE:Number;
		private var SMELL_DEADLY_SEEN_DISTANCE:Number;
		
		private var _distanceCanBeSeen:Number;
		
		private var _money:int = 0;
		private var _wm:WastedMaterials = new WastedMaterials();
		private var _cigButs:uint = 0;
		/*
		* inventory part
		*/
		public static const INVENTORY_SIZE:int = 4;
		private var _inventory:Array = new Array(INVENTORY_SIZE);
		private var _handsItem:IItem;
		//private var _clothesItem:IItem;

		private var _loseConsciousnessCallback:Function;
		private var _deadCallback:Function;
		private var _mustPukeCallback:Function;
		private var _becomeHungryCallback:Function;
		private var _becomeNotHungryCallback:Function;
		
		private var _isHungry:Boolean = false;
		
		public function get isHungry():Boolean
		{
			return _isHungry;
		}
		
		public function get cigButs():uint
		{
			return _cigButs;
		}
		
		public function set cigButs(value:uint):void
		{
			_cigButs = value;
		}
		
		public function set becomeNotHungryCallback(value:Function):void
		{
			_becomeNotHungryCallback = value;
		}
		
		public function set becomeHungryCallback(value:Function):void
		{
			_becomeHungryCallback = value;
		}
		
		public function set mustPukeCallback(value:Function):void
		{
			_mustPukeCallback = value;
		}

		
		public function set deadCallback(value:Function):void
		{
			_deadCallback = value;
		}

		public function set loseConsciousnessCallback(value:Function):void
		{
			_loseConsciousnessCallback = value;
		}

		public function DudeParameters()
		{
			initialize();
		}
		
		public function get ATTACK_STAMINA_DECRESS():Number
		{
			return _ATTACK_STAMINA_DECRESS;
		}

		public function get RUNNING_STAMINA_STEP():Number
		{
			return _RUNNING_STAMINA_STEP;
		}

		public function get WALKING_STAMINA_STEP():Number
		{
			return _WALKING_STAMINA_STEP;
		}

		public function get STATIC_STAMINA_STEP():Number
		{
			return _STATIC_STAMINA_STEP;
		}
		
		private var objSpeed:Object;
		private var objStamina:Object;
		private var objSmell:Object;
		private var objSeenDist:Object;
		private var objBasicParams:Object;
		private function initialize():void
		{
			initParams();
			
			var objSpeed:Object = GameExternalParameters.instance.getObject("speed");
			_speedBasic = Number(objSpeed["basic"]);
			_speedRunning = Number(objSpeed["running"]);
			
			objStamina = GameExternalParameters.instance.getObject("stamina");
			_STATIC_STAMINA_STEP = Number(objStamina["static_step"]);
			_WALKING_STAMINA_STEP = Number(objStamina["walking_step"]);
			_RUNNING_STAMINA_STEP = Number(objStamina["running_step"]);
			_ATTACK_STAMINA_DECRESS = Number(objStamina["attack_step"]);
			
			objSmell = GameExternalParameters.instance.getObject("smell");
			SMELL = Number(objSmell["start_smell"]);
			SMELL_HARD = Number(objSmell["smell_hard"]);
			SMELL_DEADLY = Number(objSmell["smell_deadly"]);
			
			
			objSeenDist = GameExternalParameters.instance.getObject("seen_distance");
			BASIC_SEEN_DISTANCE = Number(objSeenDist["basic"]);
			SMELL_SEEN_DISTANCE = Number(objSeenDist["smell"]);
			SMELL_HARD_SEEN_DISTANCE = Number(objSeenDist["smell_hard"]);
			SMELL_DEADLY_SEEN_DISTANCE = Number(objSeenDist["smell_deadly"]);
			smell = 0;
			
			objBasicParams = GameExternalParameters.instance.getObject("basic_params");
			_healthStep = 0;
			_hungerStep = Number(objBasicParams["hunger_step"]);
			_staminaStep = STATIC_STAMINA_STEP;
			_alcoholStep = Number(objBasicParams["alcohol_step"]);
			
			for(var i:int = 0; i < INVENTORY_SIZE; i++)
			{
				_inventory[i] = null;		
			}
		}
		
		public function set smell(value:Number):void
		{
			_smell = value;
			if(_smell < 0) _smell = 0;
			if(_smell > SMELL_DEADLY)
			{
				_distanceCanBeSeen = SMELL_DEADLY_SEEN_DISTANCE;
			}
			else if(_smell > SMELL_HARD)
			{
				_distanceCanBeSeen = SMELL_HARD_SEEN_DISTANCE;
			}
			else if(_smell > SMELL)
			{
				_distanceCanBeSeen = SMELL_SEEN_DISTANCE;
			}
			else
			{
				_distanceCanBeSeen = BASIC_SEEN_DISTANCE;
			}
		}
		
		public function get smell():Number
		{
			return _smell;
		}
		
		public function get realDistanceCanBeSeen():Number
		{
			return _distanceCanBeSeen;
		}

		public function get speedBasic():Number
		{
			return _speedBasic;
		}
		
		public function get realBasicSpeed():Number
		{
			return _speedBasic * _speedMultiplier;
		}

		public function set speedBasic(value:Number):void
		{
			_speedBasic = value;
		}

		public function get speedRunning():Number
		{
			return _speedRunning;
		}
		
		public function get realSpeedRunning():Number
		{
			return _speedRunning * _speedMultiplier;
		}

		public function set speedRunning(value:Number):void
		{
			_speedRunning = value;
		}

		public function get speedMultiplier():Number
		{
			return _speedMultiplier;
		}

		public function set speedMultiplier(value:Number):void
		{
			_speedMultiplier = value;
		}

		public function get staminaStep():Number
		{
			return _staminaStep;
		}

		public function set staminaStep(value:Number):void
		{
			_staminaStep = value;
		}

		public function get wm():WastedMaterials
		{
			return _wm;
		}

		public function get inventory():Array
		{
			return _inventory;
		}
		
		public function get handsItem():IItem
		{
			return _handsItem;
		}

		public function get preciousMetal():int
		{
			return _wm.preciousMetal;
		}

		public function set preciousMetal(value:int):void
		{
			_wm.preciousMetal = value;
		}

		public function get wastedPapper():int
		{
			return _wm.wastedPapper;
		}

		public function set wastedPapper(value:int):void
		{
			_wm.wastedPapper = value;
		}

		public function get money():int
		{
			return _money;
		}

		public function set money(value:int):void
		{
			_money = value;
		}

		public function get bottles():int
		{
			return _wm.bottles;
		}

		public function set bottles(value:int):void
		{
			_wm.bottles = value;
		}

		public function get cans():int
		{
			return _wm.cans;
		}

		public function set cans(value:int):void
		{
			_wm.cans = value;
		}

		public function tick():void
		{
			_health += _healthStep;
			_hunger += _hungerStep;
			_stamina += _staminaStep;
			_alcohol += _alcoholStep;
			
			checkParameters();
		}
		
		private var objParamDependencies:Object;
		private var objAlco:Object;
		private var objHunger:Object;
		private var objSpeedDependencies:Object
		private function initParams():void
		{
			objParamDependencies = GameExternalParameters.instance.getObject("params_dependencies");
			objAlco = GameExternalParameters.instance.getObject("alco");
			objHunger = GameExternalParameters.instance.getObject("hunger");
			objSpeedDependencies = GameExternalParameters.instance.getObject("speed_dependencies");
		}
		
		private function checkParameters():void
		{
			if(_alcohol <= 0)
			{
				_health += _alcoholStep * Number(objParamDependencies["alco0health_multiplier"]);
			}
			if(_hunger >= 100)
			{
				_health += _hungerStep * Number(objParamDependencies["hunger100health_multiplier"]);
			}
			
			if(_health > 100)	_health = 100;
			if(_health < 0)		_health = 0;
			
			if(_hunger > 100)	_hunger = 100;
			if(_hunger < 0)		_hunger = 0;
			
			if(_stamina > 100)	_stamina = 100;
			if(_stamina < 0)	_stamina = 0;
			
			if(_alcohol > 100)	_alcohol = 100;
			if(_alcohol < 0)	_alcohol = 0;
			
			resetSpeedMultiplier();
			if(stamina <= 0)
			{
				_loseConsciousnessCallback.call();
			}
			
			if(hunger >= 100)
			{
				_deadCallback.call();
			}
			
			checkHunger(_hunger, _hunger);
			
		}

		public function get alcohol():Number
		{
			return _alcohol;
		}

		public function set alcohol(value:Number):void
		{
			_alcohol = value;
			checkParameters();
			Game.instance.gameInterface.setAlcohol(value);
			
			
			if(_alcohol > Number(objAlco["vommiting"]))
			{
				_mustPukeCallback && _mustPukeCallback.call()
			}
		}

		public function get stamina():Number
		{
			return _stamina;
		}

		public function set stamina(value:Number):void
		{
			_stamina = value;
			checkParameters();
			Game.instance.gameInterface.setStamina(value);
		}

		public function get hunger():Number
		{
			return _hunger;
		}

		public function set hunger(value:Number):void
		{
			checkHunger(_hunger, value);
			_hunger = value;
			checkParameters();
			Game.instance.gameInterface.setHunger(value);
			
		}
		
		private function checkHunger(oldVal:Number, newVal:Number):void
		{			
			if(newVal > Number(objHunger["become_hungry"]) && !_isHungry)
			{
				_isHungry = true;
				_becomeHungryCallback && _becomeHungryCallback.call();
			}
			if(oldVal > Number(objHunger["become_hungry"]) && newVal <= Number(objHunger["become_hungry"]) && _isHungry)
			{
				_isHungry = false;
				_becomeNotHungryCallback && _becomeNotHungryCallback.call();
			}
		}

		public function get health():Number
		{
			return _health;
		}

		public function set health(value:Number):void
		{
			_health = value;
			checkParameters();
			Game.instance.gameInterface.setHealth(value);
		}
		
		/*
		* replaces item in pos
		* pos if -1 means any free place
		* returns - was added or not?
		*/
		public function addItemToInventory(item:IItem, pos:int=-1):Boolean
		{
			if(pos == -1)
			{
				for(var i:int = 0; i < INVENTORY_SIZE; i++)
				{
					if(_inventory[i] == null)
					{
						_inventory[i] = item.copy;
						Game.instance.gameInterface.upateScreenInventory();
						return true;
					}
				}
				return false;
			}
			else if(pos >= 0 && pos < INVENTORY_SIZE)
			{
				_inventory[pos] = item.copy;
				Game.instance.gameInterface.upateScreenInventory();
				return true;
			}
			else if(pos == Inventory.INHAND_SLOT_NUM)
			{
				_handsItem = item.copy;
				Game.instance.gameInterface.upateScreenInventory();
				return true;
			}
			return false;
		}
		
		public function removeFromInventory(itemName:String):void
		{
			for(var i:int = 0; i < INVENTORY_SIZE; i++)
			{
				if(_inventory[i] != null)
				{
					var item:IItem = _inventory[i] as IItem;
					if(item.name == itemName)
					{
						_inventory[i] = null;
						Game.instance.gameInterface.upateScreenInventory();
						return;
					}
				}
			}
			
		}
		
		public function removeItemFromSlot(slotNum:uint):void
		{
			if(slotNum < 4)
			{
				inventory[slotNum] = null;
			}
			if(slotNum == Inventory.INHAND_SLOT_NUM)
			{
				_handsItem = null;
			}
			Game.instance.gameInterface.upateScreenInventory();
		}
		
		public function useItemFromSlot(slotNum:uint):Boolean
		{
			var usedItem:IItem = null;
			if(slotNum < 4)
			{
				usedItem = inventory[slotNum];
			}
			if(slotNum == Inventory.INHAND_SLOT_NUM)
			{
				usedItem = handsItem;
			}
			if(usedItem && usedItem.useItem() )
			{
				inventory[slotNum] = null;
				Game.instance.gameInterface.upateScreenInventory();
				return true;
			}
			else if(usedItem && (usedItem.group == SimpleItem.WEAPON_ITEM && slotNum < 4))
			{
				addItemToInventory(usedItem.copy, Inventory.INHAND_SLOT_NUM);
				removeItemFromSlot(slotNum);
				Game.instance.inventory.refresh();
				Game.instance.gameInterface.upateScreenInventory();
				return true;
			}
			return false;
		}
		
		
		public function howManyFreeSlotsDoIHave():int
		{
			var freeslots:int = 0;
			for(var i:int = 0; i < INVENTORY_SIZE; i++)
			{
				if(_inventory[i] == null)
				{
					freeslots++;
				}
			}
			return freeslots;
		}
		
		public function doIHave(itemName:String, amount:int):Boolean
		{
			var counter:int = 0;
			for(var i:int = 0; i < INVENTORY_SIZE; i++)
			{
				if(_inventory[i] != null)
				{
					var item:IItem = _inventory[i] as IItem;
					if(item.name == itemName)
					{
						counter++;
					}
				}
			}
			if(counter >= amount)
			{
				return true;
			}
			return false;
		}
		
		public function clearInventory():void
		{
			_handsItem = null;
			for(var i:int = 0; i < INVENTORY_SIZE; i++)
			{
				_inventory[i] = null;
			}
			Game.instance.gameInterface.upateScreenInventory();
		}
		
		public function setItemToHandS(item:IItem):void
		{
			_handsItem = item.copy;
			Game.instance.gameInterface.upateScreenInventory();
		}
		
		public function canAfford(price:int):Boolean
		{
			return _money >= price ? true : false;
		}
		
		public function spendMoney(price:int):void
		{
			if(canAfford(price))
			{
				_money -= price;
			}
		}
		
		public function sellAllCans(pricePerOne:int):void
		{
			_money += pricePerOne*_wm.cans;
			_wm.cans = 0;
		}
		
		public function sellAllBottles(pricePerOne:int):void
		{
			_money += pricePerOne*_wm.bottles;
			_wm.bottles = 0;
		}
		
		public function sellAllWastePappers(pricePerOne:int):void
		{
			_money += pricePerOne*_wm.wastedPapper;
			_wm.wastedPapper = 0;
		}
		
		public function sellAllPreciousMetals(pricePerOne:int):void
		{
			_money += pricePerOne*_wm.preciousMetal;
			_wm.preciousMetal = 0;
		}
		
		private function resetSpeedMultiplier():void
		{
			
		/*	Number(obj["hunger_more_than"]); //80
			Number(obj["hunger_more_than_mult_dec"]); //0.3
			Number(obj["hunger_less_than"]); //20
			Number(obj["hunger_less_than_mult_dec"]); //0.3
			Number(obj["alco_more_than"]); //80 
			Number(obj["alco_more_than_mult_dec"]); //0.2
			Number(obj["alco_less_than"]); //20
			Number(obj["alco_less_than_mult_dec"]); //0.2
			Number(obj["alco_there_mutl_inc"]); //0.2
			Number(obj["health_less_than"]); //15
			Number(obj["health_less_than_mult_dec"]); //0.4
			Number(obj["health_less_than2"]); // 50
			Number(obj["health_less_than2_mult_dec"]); //0.1
			Number(obj["health_max_mult_inc"]); //0.1*/
			
			_speedMultiplier = 1.0;
			if(hunger > Number(objSpeedDependencies["hunger_more_than"])) _speedMultiplier -= Number(objSpeedDependencies["hunger_more_than_mult_dec"]);
			if(hunger < Number(objSpeedDependencies["hunger_less_than"])) _speedMultiplier -= Number(objSpeedDependencies["hunger_less_than_mult_dec"]);
			
			if(alcohol > Number(objSpeedDependencies["alco_more_than"])) _speedMultiplier -= Number(objSpeedDependencies["alco_more_than_mult_dec"]);
			else if(alcohol < Number(objSpeedDependencies["alco_less_than"])) _speedMultiplier -= Number(objSpeedDependencies["alco_less_than_mult_dec"]);
			else _speedMultiplier += Number(objSpeedDependencies["alco_there_mutl_inc"]);
			
			if(health < Number(objSpeedDependencies["health_less_than"])) _speedMultiplier -= Number(objSpeedDependencies["health_less_than_mult_dec"]);
			else if(health < Number(objSpeedDependencies["health_less_than2"])) _speedMultiplier -= Number(objSpeedDependencies["health_less_than2_mult_dec"]);
			else if(health >= 100) _speedMultiplier += Number(objSpeedDependencies["health_max_mult_inc"]);	
		}
		
		public function isSmellHard():Boolean
		{
			return (_smell >= SMELL_HARD);
		}
		
		public function isSmell():Boolean
		{
			return (_smell >- SMELL);
		}
		
		public function hasAnyWeaponInHands():Boolean
		{
			
			if(_handsItem && _handsItem.group == SimpleItem.WEAPON_ITEM)
			{
				return true;
			}
			
			return false;
		}
			
		
	}
}