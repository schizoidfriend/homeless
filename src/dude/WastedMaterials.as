package dude
{
	public class WastedMaterials
	{
		public var cans:int = 0;
		public var bottles:int = 0;
		public var wastedPapper:int = 0;
		public var preciousMetal:int = 0;
		
		public function WastedMaterials(inCans:int = 0, inBottles:int = 0, inPapper:int = 0, inMetals:int = 0)
		{
			cans = inCans;
			bottles = inBottles;
			wastedPapper = inPapper;
			preciousMetal = inMetals;
		}
		
		public function hasAny():Boolean
		{
			return (cans>0 || bottles>0 || wastedPapper>0 || preciousMetal>0) ? true : false;
		}
		
		public function clear():void
		{
			cans = 0;
			bottles = 0;
			wastedPapper = 0;
			preciousMetal = 0;
		}
		
		public function addWasteds(wm:WastedMaterials):void
		{
			cans += wm.cans;
			bottles += wm.bottles;
			wastedPapper += wm.wastedPapper;
			preciousMetal += wm.preciousMetal;
		}
		
		public function createInfoString():String
		{
			if(!hasAny())
			{
				return "No waste here to take";
			}
			var resStr:String = "";
			if(cans > 0) resStr += cans+" Cans ";
			if(bottles > 0) resStr += bottles+" Bottles ";
			if(wastedPapper > 0) resStr += wastedPapper+" Papper ";
			if(preciousMetal > 0) resStr += preciousMetal+" Meta";
			
			return resStr;
		}
	}
}