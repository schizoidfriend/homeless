package editor
{
	public class Asset
	{
		public static const TILE_WALKABLE:int = 0;
		public static const TILE_NON_WALKABLE:int = 1;
		public static const OBSTACLE:int = 2;
		public static const COLLECTABLE:int = 3;
		public static const NPC:int = 4;
		public static const SHOP:int = 5;
		public static const BUILDING_PARTS:int = 6;
		public static const SLEEPING_PLACE:int = 7;
		public static const CONTAINER:int = 8;
		public static const SMASHABLE:int = 9;
		public static const WASHING:int = 10;
		public static const WALL_WRITINGS:int = 11;
		public static const TRASH:int = 12;
		public static const USEFUL_TRASH:int = 13;
		public static const PLACE:int = 14;
		public static const TYPES_AMOUNT:int = 15;
		
		
		private var _name:String;
		
		private var _icon:String;
		
		private var _type:uint;
		
		private var _subType:int = -1;
		
		
		public function Asset(name:String, icon:String, type:uint)
		{
			_name = name;
			_icon = icon;
			if(type >= TYPES_AMOUNT)
			{
				throw new Error("NO SUCH TYPE "+type);
			}
			else
			{
				_type = type;
			}
		}

		public function get subType():int
		{
			return _subType;
		}

		public function set subType(value:int):void
		{
			_subType = value;
		}

		public function get type():int
		{
			return _type;
		}

		public function set type(value:int):void
		{
			_type = value;
		}

		public function get icon():String
		{
			return _icon;
		}

		public function set icon(value:String):void
		{
			_icon = value;
		}

		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
		}

	}
}