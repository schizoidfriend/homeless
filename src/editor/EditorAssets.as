package editor
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import map.characters.NPC;
	
	import starling.events.Event;
	
	import utils.UtilsMath;

	public class EditorAssets extends EventDispatcher
	{
		public static const LOADED_SUCCESFULLY:String = "___LOADED_SUCCESFULLY___";
		
		[Embed(source="res/gameAssets.xml", mimeType="application/octet-stream")]
		public const GameAssetsXML:Class;
		
		private var _assetsList:Vector.<Asset>;
		private var _assetsAmounts:Array = new Array();
		private var _npcInfoAssetsList:Vector.<NPCAssetInfo>
		
		private var _trashList:Vector.<Asset>;
		private var _usefulTrashList:Vector.<Asset>;
		
		
		private var _walkableNPCs:Vector.<int> = new Vector.<int>();
		private var _rats:Vector.<int> = new Vector.<int>();
		private var _churchGuys:Vector.<int> = new Vector.<int>();
		
		public function EditorAssets()
		{
			_trashList = new Vector.<Asset>();
			_usefulTrashList = new Vector.<Asset>();
			_npcInfoAssetsList = new Vector.<NPCAssetInfo>();
			_assetsList = new Vector.<Asset>();
			
			//init amounts array
			for(var i:int = 0; i < Asset.TYPES_AMOUNT; i++)
			{
				_assetsAmounts[i] = 0;
			}
			
			var xml:XML = XML(new GameAssetsXML());
			onLoadedXML(xml);
			
		}
		
		public function get assetsAmounts():Array
		{
			return _assetsAmounts;
		}

		private function onLoadedXML(xml:XML):void
		{
			for each (var object:XML in xml.Asset)
			{
				var asset:Asset = new Asset(String(object.@name), null, int(object.@type));
				_assetsAmounts[int(object.@type)]++;
				var subType:int = -1;
				if(asset.type == Asset.NPC)
				{
					subType = object.@subtype;
					asset.subType = subType;
					var npcAssetInfo:NPCAssetInfo = new NPCAssetInfo();
					var animXML:XML;
					for each(animXML in object.animation)
					{
						npcAssetInfo.npcName = asset.name;
						var type:String = animXML.@type;
						switch(type)
						{
							case "static":
								npcAssetInfo.staticAnimName = animXML.@name;
								break;
							case "walk":
								npcAssetInfo.walkAnimName = animXML.@name;
								break;
							case "fly":
							case "play":
								npcAssetInfo.extraAnim = animXML.@name;
								break;
						}
						 
					}
					_npcInfoAssetsList.push(npcAssetInfo);
					
					if(subType == NPC.WALKING_NPC)
					{
						_walkableNPCs.push(_npcInfoAssetsList.length-1);
					}
					
					if(subType == NPC.RAT)
					{
						_rats.push(_npcInfoAssetsList.length-1);
					}
					
					if(subType == NPC.CHURCH_MAN)
					{
						_churchGuys.push(_npcInfoAssetsList.length-1);
					}
				}
				
				if(asset.type == Asset.BUILDING_PARTS && object.hasOwnProperty("@subtype"))
				{
					subType = object.@subtype;
					asset.subType = subType;
				}
				
				if(asset.type == Asset.COLLECTABLE)
				{
					subType = object.@subtype;
					asset.subType = subType;
				}
				if(asset.type == Asset.OBSTACLE)
				{
					subType = object.@subtype;
					asset.subType = subType;
				}
				if(asset.type == Asset.SLEEPING_PLACE)
				{
					subType = object.@subtype;
					asset.subType = subType;
				}
				
				if(asset.type == Asset.USEFUL_TRASH)
				{
					subType = object.@subtype;
					asset.subType = subType;
				}
				_assetsList.push(asset);
				
				if(asset.type == Asset.TRASH)
				{
					_trashList.push(asset);
				}
				
				if(asset.type == Asset.USEFUL_TRASH)
				{
					_usefulTrashList.push(asset);
				}
				
				
			}
		}
		
		public function getAssetSubtype(assetName:String):int
		{
			var asset:Asset;
			for each(asset in _assetsList)
			{
				if(asset.name == assetName)
				{
					return asset.subType;
				}
			}
			
			return -1;
		}
		
		public function getNPCAssetInfo(npcName:String):NPCAssetInfo
		{
			var npcInfo:NPCAssetInfo
			for each(npcInfo in _npcInfoAssetsList)
			{
				if(npcInfo.npcName == npcName)
				{
					return npcInfo;
				}
			}
			return null;
		}
		
		public function getRandomRat():NPCAssetInfo
		{
			var randomPos:int = UtilsMath.randomRange(0, _rats.length-1);
			var randomNPCIndex:int = _rats[randomPos] as int;
			
			return _npcInfoAssetsList[randomNPCIndex] as NPCAssetInfo
		}
		
		public function getRandomWalkingNPC():NPCAssetInfo
		{
			var randomPos:int = UtilsMath.randomRange(0, _walkableNPCs.length-1);
			var randomNPCIndex:int = _walkableNPCs[randomPos] as int;
			
			return _npcInfoAssetsList[randomNPCIndex] as NPCAssetInfo;
		}
		
		public function getRandomChurchGuy():NPCAssetInfo
		{
			var randomPos:int = UtilsMath.randomRange(0, _churchGuys.length-1);
			var randomNPCIndex:int = _churchGuys[randomPos] as int;
			
			return _npcInfoAssetsList[randomNPCIndex] as NPCAssetInfo;
			
		}
		
		public function getRandomTrash():Asset
		{
			var rand:int = UtilsMath.randomRange(0, _trashList.length-1);
			return _trashList[rand];
		}
		
		public function getRandomUsefulTrash():Asset
		{
			var rand:int = UtilsMath.randomRange(0, _usefulTrashList.length-1);
			return _usefulTrashList[rand];
		}
		
		public function get assets():Vector.<Asset>
		{
			return _assetsList;
		}
	}
}