package editor
{
	import starling.display.Button;
	import starling.textures.Texture;
	
	public class EditorButton extends Button
	{
		public function EditorButton(upState:Texture, text:String="", downState:Texture=null)
		{
			super(upState, text, downState);
		}
	}
}