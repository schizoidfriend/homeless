package editor
{
	import flash.geom.Point;
	import flash.text.engine.EastAsianJustifier;
	import flash.ui.MouseCursor;
	
	import map.CollectableObject;
	import map.Container;
	import map.Food;
	import map.characters.NPC;
	import map.characters.PoliceManNPC;
	
	import starling.display.*;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.ColorMatrixFilter;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.AssetManager;
	
	public class EditorView extends Sprite
	{
		private static const ITEMS_PER_PAGE:int = 20;
		//all placeable objects for editor
		private static var _editorAssets:EditorAssets;
		//start position
		private var _editorMapPosition:Point = new Point(CommonVars.screenHalfWidth, CommonVars.screenHalfHeight);
		
		//currently taken asset and name
		private var _currentAsset:Image;
		private var _currentAssetName:String;
		
		//cursor erser image
		private var _eraserAsset:Image;

		//private var _background:Background;
		private var _oldTouchedTile:Image;
		
		//is in cursor mode
		private var _inCursorMode:Boolean = false;
		//currently choosen NPC in cursor mode
		private var _currentNPC:NPC = null;
		
		//is in eraser mode
		private var _inEraserMode:Boolean = false;
		//eraser button
		private var _eraserButton:Button;
		
		//is in waypoints mode
		private var _inWaypointsMode:Boolean = false;
		//waypoints button
		private var _waypointsButton:Button;
		
		//textfield indicates current state
		private var _currentStateText:TextField = new TextField(250, 50, "Text Text", "Verdana", 20, 0xFFFFFF, true);
		
		//assets buttons list
		private var _currentButtonList:Vector.<Button> = new Vector.<Button>();
		//currently choosen list group page
		private var _currentGroup:int = 0;
		//check if there is more then 1 page of current group oblects
		private var _moreThenOnePage:Boolean = false;
		private var _currentPage:int = 0;
		//next page of assets list button
		private var _nextButton:Button;
		private var _prevButton:Button;
		private var _pageDown:Button;
		private var _pageUp:Button;
		
		private var _gotoglobalButton:Button;
		
		private var _playerStartPosButton:Button;
		
		public function EditorView()
		{
			super();
			visible = false;
			_editorAssets = new EditorAssets();
			onAssetsLoaded();
		}
		
		public static function get editorAssets():EditorAssets
		{
			return _editorAssets;
		}

		public function get inCursorMode():Boolean
		{
			return _inCursorMode;
		}
		
		public function get isWaypointsMode():Boolean
		{
			return _inWaypointsMode;
		}
		
		private function onTouch(e:TouchEvent):void
		{
			if(!this.visible)
			{
				return ;
			}
			
			var touches:Vector.<Touch> = e.getTouches(Game.instance.background, TouchPhase.MOVED);
			var touchesBegan:Vector.<Touch> = e.getTouches(Game.instance.background, TouchPhase.BEGAN);
			var tilePos:Point = new Point();
			var type:int;
			if(!_inWaypointsMode && !_inEraserMode && !_inCursorMode)
			{
				switch(_currentGroup)
				{
					case Asset.TILE_NON_WALKABLE:
					case Asset.TILE_WALKABLE:
					case Asset.BUILDING_PARTS:
						
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							if(_currentAssetName == "Exit")
							{
								if(touchesBegan.length > 0)
								{
									Game.instance.background.addNewExitPoint(tilePos, "");
									Game.instance.background.killCollectable(-1);
								}
							}
							else if(_currentAssetName == "PlayerStartPos")
							{
								if(touchesBegan.length > 0)
								{
									Game.instance.background.addNewPlayerStartPos(tilePos);
									Game.instance.background.killCollectable(-1);
								}
							}
							else
							{
								Game.instance.background.setNewMapTileAsset(_currentGroup, _currentAssetName, tilePos.x, tilePos.y, true);
							}
						}
						break;
					
					case Asset.SHOP:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							
							Game.instance.background.addNewShop(tilePos, _currentAssetName);
						}
						break;
					case Asset.PLACE:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							
							Game.instance.background.addNewPlace(tilePos, _currentAssetName);
						}
						break;
					case Asset.CONTAINER:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							
							Game.instance.background.addNewContainer(tilePos, Container.TRASH_CAN, _currentAssetName);
						}
						break;
					
					case Asset.WASHING:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							
							Game.instance.background.addNewWashingObject(_currentAssetName, tilePos);
						}
						break;
					
					case Asset.WALL_WRITINGS:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							
							Game.instance.background.addNewWritingObject(_currentAssetName, tilePos);
						}
						break;
					
					case Asset.SMASHABLE:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							
							Game.instance.background.addNewSmashable(tilePos, _currentAssetName);
						}
						break;
					
					case Asset.OBSTACLE:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							type = _editorAssets.getAssetSubtype(_currentAssetName);
							Game.instance.background.addNewObstacle(tilePos, _currentAssetName, type);
						}
						break;
					
					case Asset.COLLECTABLE:
						if(touchesBegan.length > 0 && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							var touchedPos:Point = new Point(
								Game.instance.background.currentlyTouchedPosition.x - _currentAsset.width / 2, 
								Game.instance.background.currentlyTouchedPosition.y - _currentAsset.height / 2);
							type = _editorAssets.getAssetSubtype(_currentAssetName);
							Game.instance.background.addNewCollectableOnMap(type, _currentAssetName, touchedPos, tilePos);
						}
						break;
					
					case Asset.NPC:
						if(touchesBegan.length > 0 && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));

							type = _editorAssets.getAssetSubtype(_currentAssetName);
							Game.instance.background.addNewNPC(type, tilePos, _editorAssets.getNPCAssetInfo(_currentAssetName));
						}
						break;
					
					case Asset.SLEEPING_PLACE:
						if ((touches.length > 0 || touchesBegan.length > 0) && _currentAsset)
						{
							tilePos.setTo(int(_oldTouchedTile.x/CommonVars.TILE_SIDE_SIZE),
								int(_oldTouchedTile.y/CommonVars.TILE_SIDE_SIZE));
							
							type = _editorAssets.getAssetSubtype(_currentAssetName);
							Game.instance.background.addNewSleepingPlace(tilePos, _currentAssetName, type);
						}
						break;
				}
			}
			
			var tile:Tile = Game.instance.background.currentlyToucheTile;

			if(_inCursorMode && touchesBegan.length > 0)
			{
				if(tile && tile.npcs.length > 0)
				{
					if(_currentNPC is PoliceManNPC)
					{
						//_currentNPC.filter = null;
						(_currentNPC as PoliceManNPC).hideWaypointsDebugInfo();
					}
					_currentNPC = tile.npcs[0] as NPC;
					
					if(_inEraserMode)
					{
						Game.instance.background.killNPC(_currentNPC.id);
					}
					else
					{
						_waypointsButton.visible = true;
						//var invertFilter:ColorMatrixFilter = new ColorMatrixFilter();
						//invertFilter.invert();
						//_currentNPC.filter = invertFilter;
						
						(_currentNPC as PoliceManNPC).showWaypointsDebugInfo();
					}
				}
				else
				{
					Game.instance.background.killExitPoint(tile.pos);
					Game.instance.background.killPlayerStartPos(tile.pos);
					Game.instance.background.killContainer(tile.pos);
					Game.instance.background.killCollectableAt(tile.pos);
					Game.instance.background.killSleepingPlace(tile.pos);
					Game.instance.background.killSmashable(tile.pos);
					Game.instance.background.killEnvironmentObjectAt(tile.pos);
					Game.instance.background.killObstacle(tile.pos);
					Game.instance.background.killWashingObjectAt(tile.pos);
					Game.instance.background.killWritingObjectAt(tile.pos);
				}
			}
			
			if(_inWaypointsMode && touchesBegan.length > 0)
			{
				if(tile && _currentNPC)
				{
					if(_inEraserMode)
					{
						(_currentNPC as PoliceManNPC).eraseWaypointAt(tile.pos);
					}
					else
					{
						(_currentNPC as PoliceManNPC).addWaypoint(tile.pos);
						(_currentNPC as PoliceManNPC).showWaypointsDebugInfo();
					}
				}					
			}
		}
		
		private function onEnterFrame(event:Event):void
		{
			if(_inEraserMode)
			{
				_eraserAsset.x = Game.instance.background.currentPointOnBackground.x;
				_eraserAsset.y = Game.instance.background.currentPointOnBackground.y;
			}
			if(Game.instance.background.currentlyTouchedTileImage && _currentAsset && !_inCursorMode)
			{
				if(_currentGroup == Asset.COLLECTABLE)
				{
					if(_oldTouchedTile != Game.instance.background.currentlyTouchedTileImage)
					{
						_oldTouchedTile = Game.instance.background.currentlyTouchedTileImage;
					}
					_currentAsset.x = Game.instance.background.currentPointOnBackground.x - _currentAsset.width / 2;
					_currentAsset.y = Game.instance.background.currentPointOnBackground.y - _currentAsset.height / 2;
					trace("curassetXY", _currentAsset.x, _currentAsset.y);
				}
				else
				{
					if(_oldTouchedTile != Game.instance.background.currentlyTouchedTileImage)
					{
						_oldTouchedTile = Game.instance.background.currentlyTouchedTileImage;
						_currentAsset.x = _oldTouchedTile.x+Game.instance.background.x - Game.instance.background.lastRedrawnXPos*CommonVars.TILE_SIDE_SIZE ;
						_currentAsset.y = _oldTouchedTile.y+Game.instance.background.y - Game.instance.background.lastRedrawnYPos*CommonVars.TILE_SIDE_SIZE;
					}
				}
			}
		}
		
		public function get editorMapPosition():Point
		{
			return _editorMapPosition;
		}

		public function set editorMapPosition(value:Point):void
		{
			_editorMapPosition = value;
		}
		
		public function addOffsetToEditorMapPosition(xOffset:int, yOffset:int):void
		{
			_editorMapPosition.x += xOffset;
			_editorMapPosition.y += yOffset;
		}

		private function onAssetsLoaded():void
		{			
			var tex:Texture = Game.instance.assets.getTexture("TileBricks");
			_eraserAsset = new Image(tex);
			_eraserAsset.touchable = false;
			_eraserAsset.alpha = 0.8;
			_eraserAsset.scaleX = 0.2;
			_eraserAsset.scaleY = 0.2;
			_eraserAsset.touchable = false;
			
			addChild(_currentStateText);
			_currentStateText.x = 300;
			_currentStateText.y = 50;
			
			var nextbuttonTexture:Texture = Game.instance.assets.getTexture("nextbutton");
			_nextButton = new Button(nextbuttonTexture, "");
			_nextButton.x = 128+40+30;
			_nextButton.y = 0;
			_nextButton.scaleX = 0.1;
			_nextButton.scaleY = 0.1;
			_nextButton.addEventListener(Event.TRIGGERED, onNextButtonTriggered);
			addChild(_nextButton);
			
			var prevbuttonTexture:Texture = Game.instance.assets.getTexture("nextbutton");
			_prevButton = new Button(prevbuttonTexture, "");
			_prevButton.x = 128+30;
			_prevButton.y = _prevButton.height/10;
			_prevButton.scaleX = 0.1;
			_prevButton.scaleY = 0.1;
			_prevButton.rotation = Math.PI;
			_prevButton.addEventListener(Event.TRIGGERED, onPrevButtonTriggered);
			addChild(_prevButton);
			
			
			
			var pagedownbuttonTexture:Texture = Game.instance.assets.getTexture("nextbutton");
			_pageDown = new Button(pagedownbuttonTexture, "");
			_pageDown.x = 128+10+25+30;
			_pageDown.y = 40;
			_pageDown.scaleX = 0.1;
			_pageDown.scaleY = 0.1;
			_pageDown.addEventListener(Event.TRIGGERED, onPageDownTriggered);
			_pageDown.rotation = Math.PI/2;
			addChild(_pageDown);
			
			var pageupbuttonTexture:Texture = Game.instance.assets.getTexture("nextbutton");
			_pageUp = new Button(pagedownbuttonTexture, "");
			_pageUp.x = 128+5+30;
			_pageUp.y = 32;
			_pageUp.scaleX = 0.1;
			_pageUp.scaleY = 0.1;
			_pageUp.addEventListener(Event.TRIGGERED, onPageUpTriggered);
			_pageUp.rotation = (Math.PI/2)*3;
			addChild(_pageUp);
			
			
			var cursorbuttonTexture:Texture = Game.instance.assets.getTexture("button");
			var cursorButton:Button = new Button(cursorbuttonTexture, "cursor");
			cursorButton.x = 900;
			cursorButton.y = 50;
			cursorButton.addEventListener(Event.TRIGGERED, onCursorButtonTriggered);
			addChild(cursorButton);
			
			var waypointsbuttonTexture:Texture = Game.instance.assets.getTexture("button");
			_waypointsButton = new Button(waypointsbuttonTexture, "waypoints");
			_waypointsButton.x = 900;
			_waypointsButton.y = 150;
			_waypointsButton.addEventListener(Event.TRIGGERED, onWaypointsButtonTriggered);
			_waypointsButton.visible = false;
			addChild(_waypointsButton);
			
			var eraserbuttonTexture:Texture = Game.instance.assets.getTexture("button");
			_eraserButton = new Button(eraserbuttonTexture, "eraser");
			_eraserButton.x = 900;
			_eraserButton.y = 250;
			_eraserButton.addEventListener(Event.TRIGGERED, onEraserButtonTriggered);
			_eraserButton.visible = true;
			addChild(_eraserButton)
			
			var gotoglobalbuttonTexture:Texture = Game.instance.assets.getTexture("button");
			_gotoglobalButton = new Button(gotoglobalbuttonTexture, "Global Map Point");
			_gotoglobalButton.x = 900;
			_gotoglobalButton.y = 350;
			_gotoglobalButton.addEventListener(Event.TRIGGERED, onGoToGlobalButtonTriggered);
			_gotoglobalButton.visible = true;
			addChild(_gotoglobalButton)
			
			var playerstartposbuttonTexture:Texture = Game.instance.assets.getTexture("button");
			_playerStartPosButton = new Button(playerstartposbuttonTexture, "PlayerStartPos");
			_playerStartPosButton.x = 900;
			_playerStartPosButton.y = 450;
			_playerStartPosButton.addEventListener(Event.TRIGGERED, onPlayerStartPosButtonTriggered);
			addChild(_playerStartPosButton)
			
			refreshAssetsList(0);
				
		}
		
		private function onNextButtonTriggered(e:Event):void
		{
			_currentGroup++;
			if(_currentGroup >= Asset.TYPES_AMOUNT)
			{
				_currentGroup = 0;
			}
			
			refreshAssetsList(_currentGroup);
			setCursorMode(true);
		}
		
		private function onPrevButtonTriggered(e:Event):void
		{
			_currentGroup--;
			if(_currentGroup < 0)
			{
				_currentGroup = Asset.TYPES_AMOUNT-1;
			}
			
			refreshAssetsList(_currentGroup);
			setCursorMode(true);
		}
		
		private function onPageUpTriggered(e:Event):void
		{
			if(_moreThenOnePage)
			{
				if(_currentPage == 0)
				{
					_currentPage = int((_editorAssets.assetsAmounts[_currentGroup]-1)/ITEMS_PER_PAGE);
				}
				else
				{
					_currentPage--;;
				}
				refreshAssetsList(_currentGroup, _currentPage);
			}
		}
		
		private function onPageDownTriggered(e:Event):void
		{
			if(_moreThenOnePage)
			{
				
				if(_currentPage == int((_editorAssets.assetsAmounts[_currentGroup]-1)/ITEMS_PER_PAGE))
				{
					_currentPage = 0;
				}
				else
				{
					_currentPage++;
				}
				refreshAssetsList(_currentGroup, _currentPage);
			}
		}
		
		private function refreshAssetsList(group:int, page:int = 0):void
		{
			var btn:Button;
			while(_currentButtonList.length > 0)
			{
				btn = _currentButtonList.pop();
				removeChild(btn, true);
			}
			
			var assets:Vector.<Asset> = _editorAssets.assets;
			var i:int = 0;
			var minVal:int = page*ITEMS_PER_PAGE;
			var maxVal:int = minVal+ITEMS_PER_PAGE;
			for each (var asset:Asset in assets)
			{
				if(asset.type == group)
				{
					if(i >= minVal && i < maxVal)
					{
						var buttonTexture:Texture = Game.instance.assets.getTexture(asset.name);
						if(!buttonTexture) buttonTexture = Game.instance.assets.getTexture("button");
						var button:Button = new Button(buttonTexture, asset.name);
						button.scaleX = 0.5;
						button.scaleY = 0.5;
						button.x = 50;
						button.y = (i-minVal) * 32;
						button.addEventListener(Event.TRIGGERED, onButtonTriggered);
						addChild(button);
						_currentButtonList.push(button);
					}
					else if(i >= ITEMS_PER_PAGE && page == 0)
					{
						_currentPage = 0;
						_pageDown.visible = true;
						_pageUp.visible = true;
						_moreThenOnePage = true;
						return ;
					}
					i++;
				}
				
				
			}
			if(page == 0)
			{
				_moreThenOnePage = false;
				_currentPage = 0;
				_pageDown.visible = false;
				_pageUp.visible = false;
			}
			
		}
		
		private function onGoToGlobalButtonTriggered(e:Event):void
		{
			setCursorMode(false);
			setEraserMode(false);
			setWaypointsMode(false);
			_currentAssetName = "Exit";
			_currentGroup = Asset.TILE_WALKABLE;
			var tex:Texture = Game.instance.assets.getTexture(_currentAssetName);
			if(_currentAsset)
			{
				this.removeChild(_currentAsset, true);
				_currentAsset = null;
			}
			_currentAsset = new Image(tex);
			_currentAsset.touchable = false;
			addChild(_currentAsset);
		}
		
		private function onPlayerStartPosButtonTriggered(e:Event):void
		{
			setCursorMode(false);
			setEraserMode(false);
			setWaypointsMode(false);
			_currentAssetName = "PlayerStartPos";
			_currentGroup = Asset.TILE_WALKABLE;
			var tex:Texture = Game.instance.assets.getTexture(_currentAssetName);
			if(_currentAsset)
			{
				this.removeChild(_currentAsset, true);
				_currentAsset = null;
			}
			_currentAsset = new Image(tex);
			_currentAsset.touchable = false;
			addChild(_currentAsset);
		}
		
		private function onEraserButtonTriggered(e:Event):void
		{
			setEraserMode(true);
		}
		
		private function setEraserMode(set:Boolean):void
		{
			_inEraserMode = set;
			changeStateText()
			
			if(_inEraserMode)
			{
				addChild(_eraserAsset);
				if(_currentAsset)
				{
					this.removeChild(_currentAsset, true);
					_currentAsset = null;
				}
			}
			else
			{
				removeChild(_eraserAsset);
			}
		}
		
		private function onCursorButtonTriggered(e:Event):void
		{
			setCursorMode(true);
			setWaypointsMode(false);
		}
		
		private function onWaypointsButtonTriggered(e:Event):void
		{
			setWaypointsMode(true);
			setCursorMode(false);
			setEraserMode(false);
		}
		
		private function onButtonTriggered(e:Event):void
		{
			setCursorMode(false);
			setEraserMode(false);
			setWaypointsMode(false);
			var name:String = Button(e.target).text;
			_currentAssetName = name;
			var tex:Texture = Game.instance.assets.getTexture(_currentAssetName);
			if(_currentAsset)
			{
				this.removeChild(_currentAsset, true);
				_currentAsset = null;
			}
			if(!tex)
			{
				var atlas:TextureAtlas = Game.instance.assets.getTextureAtlas(_currentAssetName);
				tex = atlas.getTextures()[0];
			}
			_currentAsset = new Image(tex);
			_currentAsset.touchable = false;
			_currentAsset.alpha = 0.8;
			addChild(_currentAsset);
		}
		
		private function setCursorMode(set:Boolean):void
		{
			_inCursorMode = set;
			if(_inCursorMode)
			{
				if(_currentAsset)
				{
					this.removeChild(_currentAsset);
					_currentAsset.dispose();
					_currentAsset = null;
				}
				setEraserMode(false);
				setWaypointsMode(false);
			}
			changeStateText();
		}
		
		private function clearCurrentNPC():void
		{
			if(_currentNPC)
			{
				//_currentNPC.filter = null;
				if(_currentNPC is PoliceManNPC)
				{
					(_currentNPC as PoliceManNPC).hideWaypointsDebugInfo();
				}
				_currentNPC = null;
			}
			_waypointsButton.visible = false;
		}
		
		private function setWaypointsMode(set:Boolean):void
		{
			_inWaypointsMode = set;
			if(!_inWaypointsMode)
			{
				_waypointsButton.visible = false;
			}
			else
			{
				setEraserMode(false);
			}
			changeStateText();
		}
		
		public function enableEditorMode(enable:Boolean):void
		{
			this.visible = enable;
			
			if(enable)
			{
				enableEditor();
				setCursorMode(true);
			}
			else
			{
				setCursorMode(false);
				setWaypointsMode(false);
				setEraserMode(false);
				clearCurrentNPC();
			}
				
		}
		
		private function enableEditor():void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			Game.instance.background.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function disableEditor():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			Game.instance.background.removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function changeStateText():void
		{
			_currentStateText.text = "";
			if(_inCursorMode)
			{
				_currentStateText.text += " cursor ";
			}
			if(_inEraserMode)
			{
				_currentStateText.text += " eraser ";
			}
			if(_inWaypointsMode)
			{
				_currentStateText.text += " waypoints ";
			}
			
			if(!_inWaypointsMode && !_inEraserMode && !_inCursorMode)
			{
				_currentStateText.text += " objects ";
			}
		}
		
	}
}