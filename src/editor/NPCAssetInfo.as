package editor
{
	public class NPCAssetInfo
	{
		public var npcName:String;
		public var walkAnimName:String;
		public var staticAnimName:String;
		public var extraAnim:String;
		
		public function NPCAssetInfo()
		{
		}
	}
}