package game
{
	public class GameExternalParameters
	{
		[Embed(source="../res/parameters.xml", mimeType="application/octet-stream")]
		public const GameParameters:Class;
		
		private var _allObjects:Array = new Array();
		
		private static var _instance:GameExternalParameters = new GameExternalParameters();
		
		public function GameExternalParameters()
		{
			load();
		}
		
		private function load():void
		{
			var xml:XML = XML(new GameParameters());
			
			for each (var object:XML in xml.*)
			{
				//trace(object.name())
				var obj:Object = new Object();

				var objecAttributes:XMLList = object.attributes();
				for each (var attr:XML in objecAttributes) 
				{
					//trace(attr.name());
					//trace(attr);
					var attrname:String = attr.name();
					obj[attrname] = attr;
				}
				_allObjects[object.name()] = Object(obj);
			}
		}
		
		public function getObject(name:String):Object
		{
			if(_allObjects.hasOwnProperty(name))
			{
				return _allObjects[name];
			}
			return null;
		}
		
		public static function get instance():GameExternalParameters
		{
			return _instance;
		}
	}
}