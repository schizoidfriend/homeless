package game
{
	import flash.geom.Point;
	
	import map.characters.structs.QuestCharInfo;

	public class GameMapState
	{
		private var _openedGarbagePos:Vector.<Point>;
		private var _questCharacters:Vector.<QuestCharInfo>;
		
		private var _currentMapName:String
		
		public function GameMapState(mapName:String)
		{
			_currentMapName = mapName;
		}
		
		public function get questCharacters():Vector.<QuestCharInfo>
		{
			return _questCharacters;
		}

		public function get openedGarbagePos():Vector.<Point>
		{
			return _openedGarbagePos;
		}

		public function get currentMapName():String
		{
			return _currentMapName;
		}

		public function updateCurrentMap():void
		{
			_openedGarbagePos = Game.instance.background.getOpenedContainersPositions();
			_questCharacters = Game.instance.background.getQuestCharactersInfo();
		}
		
		public function addQuestCharacter(toAdd:QuestCharInfo):void
		{
			if(!_questCharacters) _questCharacters = new Vector.<QuestCharInfo>();
			_questCharacters.push(toAdd);
		}
		
		public function addGarbagePos(toAdd:Point):void
		{
			if(!_openedGarbagePos) _openedGarbagePos = new Vector.<Point>();
			_openedGarbagePos.push(toAdd);
		}
	}
}