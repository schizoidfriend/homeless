package game
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import map.characters.structs.QuestCharInfo;
	
	import starling.events.EventDispatcher;

	public class GameSavedData extends EventDispatcher
	{
		public static const ON_LOADING_FINISHED:String = "___ON_LOADING_FINISHED___";
		
		private static var _instance:GameSavedData;
		
		private var _savedMapsStates:Vector.<GameMapState> = new Vector.<GameMapState>();
		
		public function GameSavedData()
		{
		}
		
		public var urlL:URLLoader = new URLLoader();
		public var urlR:URLRequest;
		public function loadGameDataFromFile(fileName:String):void
		{
			Game.instance.currentState == Game.LOADING_STATE;
			urlR =  new URLRequest(fileName);            
			urlL.addEventListener(flash.events.Event.COMPLETE, onLoadedAction);
			urlL.load(urlR);   
		}
		
		private function onLoadedAction(e:flash.events.Event):void
		{
			parseXML(XML(e.target.data));
		}
		
		private function parseXML(xml:XML):void
		{
			var object:XML;
			
			for each (object in xml.map)
			{
				
				var mapName:String = String(object.@name);
				var gms:GameMapState = new GameMapState(mapName);
				var characterXML:XML;
				
				for each (characterXML in object.character)
				{
					var charName:String = String(characterXML.@name);
					var day:int = int(characterXML.@day);
					var isFinished:Boolean = String(characterXML.@isFinished) == "true" ? true : false;
					var isLastDay:Boolean = String(characterXML.@isLastDay) == "true" ? true : false;
					var qci:QuestCharInfo = new QuestCharInfo(charName, day, isLastDay, isFinished);
					
					gms.addQuestCharacter(qci);
				}
				
				var pointXML:XML;
				for each (pointXML in object.openedGarbage)
				{
					var x:int = int(pointXML.@x);
					var y:int = int(pointXML.@y);
					
					gms.addGarbagePos(new Point(x,y));
				}
				
				_savedMapsStates.push(gms);
			}
			
			Game.instance.dude.setDudePrametersFromXML(xml);
			
		}
		
		public function saveGameDataToFile(fileName:String):void
		{
			var gms:GameMapState;
			var qci:QuestCharInfo;
			var pt:Point;
			
			var xml:String = new String();
			xml+= "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n";
			
			xml+="\n";
			xml+="<data>\n";
			
			for each (gms in _savedMapsStates)
			{
				xml+="\t<map name=\""+gms.currentMapName+"\">\n";
				
				for each (qci in gms.questCharacters)
				{
					xml+="\t\t<character name=\""+qci.characterName+"\" "+
										"day=\""+qci.currntDay+"\" "+
										"isFinished=\""+qci.isCurrentDayQuestFinished.toString()+"\" "+
										"isLastDay=\""+qci.isLastDay.toString()+"\" />\n";
				}
				
				for each (pt in gms.openedGarbagePos)
				{
					xml+="\t\t<openedGarbage x=\""+pt.x+"\" y=\""+pt.y+"\" />\n";
				}
				
				xml+="\t</map>\n";
			}
			
			xml+=Game.instance.dude.getXMLedDudeData();
			
			
			
			xml+="</data>\n";
			
			var file:FileReference = new FileReference();
			file.save( xml, fileName );
		}
		
		public function saveCurrentMap(mapName:String):void
		{
			var gms:GameMapState;
			for each (gms in _savedMapsStates)
			{
				if(gms.currentMapName == mapName)
				{
					gms.updateCurrentMap();
					return;
				}
			}
			gms = new GameMapState(mapName);
			gms.updateCurrentMap();
			_savedMapsStates.push(gms);
		}
		
		public function getDataForMap(mapName:String):GameMapState
		{
			var gms:GameMapState;
			for each (gms in _savedMapsStates)
			{
				if(gms.currentMapName == mapName)
				{
					return gms;
				}
			}
			return null;
		}
		
		static public function get instance():GameSavedData
		{
			if(!_instance) _instance = new GameSavedData();
			
			return _instance;
		}
	}
}