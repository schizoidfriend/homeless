package gui
{
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	import utils.ArtUtils;

	public class CustomImgProgressBar extends Sprite
	{
		private var _background:Sprite;
		private var _meter:Sprite;
		
		private var _ratio:Number;
		private var _maxMeter:Number;
		
		public function CustomImgProgressBar(backImageName:String, meterImageName:String)
		{			
			_background = ArtUtils.makeSimpleSprite(backImageName);
			_meter = ArtUtils.makeSimpleSprite(meterImageName);
			_maxMeter = _meter.width;
			
			addChild(_background);
			addChild(_meter);
			_meter.x = (_background.width-_meter.width)/2;
			_meter.y = (_background.height-_meter.height)/2;
			
			_meter.clipRect = new Rectangle(0,0,_meter.width,_meter.height);
		}
		
		public function get ratio():Number { return _ratio; }
		public function set ratio(value:Number):void 
		{ 
			_ratio = Math.max(0.0, Math.min(1.0, value)); 
			_meter.clipRect.width = _maxMeter*_ratio;
		}
	}
}