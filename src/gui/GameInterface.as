package gui
{
	import dude.DudeParameters;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.ArtUtils;

	public class GameInterface extends Sprite
	{
		[Embed(source="res/visitor1.ttf", embedAsCFF="false", fontName = "Visitor", fontFamily="Visitor")]
		private static const Visitor:Class;
		
		private static const MAIN_PARAMETERS_MAXIMUM:Number = 100.0;
		
		//private var _health:TextField = ArtUtils.makeBoldTF(250, 40, 10, 10, 16, 0xFF0000);
		//private var _hunger:TextField = ArtUtils.makeBoldTF(250, 40, 10, 50, 16, 0x0000FF); 
		//private var _stamina:TextField = ArtUtils.makeBoldTF(250, 40, 10, 90, 16, 0x00FF00);
		//private var _alcohol:TextField = ArtUtils.makeBoldTF(250, 40, 10, 130, 16, 0x000000);
		private var _health:CustomImgProgressBar;
		private var _hunger:CustomImgProgressBar;
		private var _stamina:CustomImgProgressBar;
		private var _alcohol:CustomImgProgressBar;
		private var _healthIcon:Image;
		private var _hungerIcon:Image;
		private var _staminaIcon:Image;
		private var _alcoholIcon:Image;
		private var _smell:TextField = ArtUtils.makeBoldTF(250, 40, 10, 170, 16, 0xFFFF00);
		private var _cigButs:TextField = ArtUtils.makeBoldTF(250, 40, 10, 200, 16, 0xFFFF00);
		private var _dateTime:TextField = ArtUtils.makeBoldTF(250, 50, 450, 50, 14);
		
		private var _money:Sprite = new Sprite();
		private var _moneyText:TextField = ArtUtils.makeNumberTF(150, 150, 867+46+10, 4, 50, 0x0, VAlign.TOP);
		private var _cans:Sprite = new Sprite();
		private var _cansText:TextField = ArtUtils.makeNumberTF(150, 150, 932+39+10, 95-5, 30, 0x0, VAlign.TOP);
		private var _wastedPapper:Sprite = new Sprite();
		private var _wastedPapperText:TextField = ArtUtils.makeNumberTF(150, 150, 932+39+10, 131-5, 30, 0x0, VAlign.TOP);
		private var _preciousMetals:Sprite = new Sprite();
		private var _preciousMetalsText:TextField = ArtUtils.makeNumberTF(150, 150, 932+39+10, 158-5, 30, 0x0, VAlign.TOP);
		private var _bottles:Sprite = new Sprite();
		private var _bottlesText:TextField = ArtUtils.makeNumberTF(150, 150, 932+39+10, 60-5, 30, 0x0, VAlign.TOP);
		
		private var _hitProgressBar:CustomImgProgressBar;
		private var _hitIcon:Sprite;
		private var _hitTween:Tween;
		private var _isInBattleMode:Boolean = false;
		
		private var _untouchablePart:Sprite = new Sprite();
		
		private var _screenInventory:ScreenInventory = new ScreenInventory();
		
		public function GameInterface()
		{
			addChild(_untouchablePart);
			_untouchablePart.touchable = false;
			
			/*
			* LEFT PART OF INTERFACE
			*/
			_untouchablePart.addChild(_smell);
			_untouchablePart.addChild(_cigButs);
			_untouchablePart.addChild(_dateTime);
			
			
			/*
			* RIGHT PART OF INTERFACE
			*/
			_money.addChild(ArtUtils.makeItemInPlace("IconCoin", 867, 10));
			_untouchablePart.addChild(_money);
			_untouchablePart.addChild(_moneyText);
			
			_cans.addChild(ArtUtils.makeItemInPlace("IconCan", 940, 95));
			_untouchablePart.addChild(_cans);
			_untouchablePart.addChild(_cansText);
			
			_wastedPapper.addChild(ArtUtils.makeItemInPlace("IconPaper", 932, 131));
			_untouchablePart.addChild(_wastedPapper);
			_untouchablePart.addChild(_wastedPapperText);
			
			_preciousMetals.addChild(ArtUtils.makeItemInPlace("IconPaper", 932, 158)); //metals!!!
			_untouchablePart.addChild(_preciousMetals);
			_untouchablePart.addChild(_preciousMetalsText);
			
			_bottles.addChild(ArtUtils.makeItemInPlace("IconBottle", 940, 60));
			_untouchablePart.addChild(_bottles);
			_untouchablePart.addChild(_bottlesText);
			
			_hitProgressBar = ArtUtils.createProgressBar("MeterBG", "MeterKick", 450, 10);
			_hitIcon = ArtUtils.makeSelfCenteredItemInPlace("IconKick", 400, 10);
			_hitTween = new Tween(_hitIcon, 0.5);
			_hitTween.reverse = true;
			_hitTween.repeatCount = 0;
			_hitTween.scaleTo(1.5);
			
			_health = ArtUtils.createProgressBar("MeterBG", "MeterHealth", 41, 9);
			_untouchablePart.addChild(_health);
			_hunger = ArtUtils.createProgressBar("MeterBG", "MeterFood", 41, 37);
			_untouchablePart.addChild(_hunger);
			_stamina = ArtUtils.createProgressBar("MeterBG", "MeterStamina", 41, 92);
			_untouchablePart.addChild(_stamina);
			_alcohol = ArtUtils.createProgressBar("MeterBG", "MeterBooze", 41, 65);
			_untouchablePart.addChild(_alcohol);
			
			_healthIcon = ArtUtils.makeItemInPlace("IconHealth", 13, 7);
			_untouchablePart.addChild(_healthIcon);
			_hungerIcon = ArtUtils.makeItemInPlace("IconFood", 11, 34);
			_untouchablePart.addChild(_hungerIcon);
			_staminaIcon = ArtUtils.makeItemInPlace("IconStamina", 11, 92);
			_untouchablePart.addChild(_staminaIcon);
			_alcoholIcon = ArtUtils.makeItemInPlace("IconBooze", 11, 61);
			_untouchablePart.addChild(_alcoholIcon);
			
			addChild(_screenInventory);
			_screenInventory.x = 200;
			
		}
		
		public function upateScreenInventory():void
		{
			_screenInventory.refresh();
		}
		
		public function switchBattleMode():void
		{
			setBattleMode(_isInBattleMode);
		}
		
		public function setBattleMode(activate:Boolean):void
		{
			if(_isInBattleMode == activate) return;
			_isInBattleMode = activate;
			
			if(_isInBattleMode)
			{
				_untouchablePart.addChild(_hitProgressBar);
				_untouchablePart.addChild(_hitIcon);
				Starling.juggler.add(_hitTween);
			}
			else
			{
				_untouchablePart.removeChild(_hitProgressBar);
				_untouchablePart.removeChild(_hitIcon);
				Starling.juggler.remove(_hitTween);
				_hitIcon.scaleX = 1.0;
				_hitIcon.scaleY = 1.0;
			}
		}
		
		public function get hitProgressBar():CustomImgProgressBar
		{
			return _hitProgressBar;
		}

		public function get dateTime():TextField
		{
			return _dateTime;
		}

		public function setAll(params:DudeParameters, anim:Boolean):void
		{
			setHealth(params.health);
			setHunger(params.hunger);
			setAlcohol(params.alcohol);
			setStamina(params.stamina);
			setSmell(params.smell);
			setCigButts(params.cigButs);
			
			setMoney(params.money, anim);
			setBottles(params.bottles, anim);
			setCans(params.cans, anim);
			setPreciousMetals(params.preciousMetal, anim);
			setWastedPapper(params.wastedPapper, anim);
			
			_screenInventory.refresh();
		}
		
		public function setHealth(value:int):void
		{
			_health.ratio = Number(value)/MAIN_PARAMETERS_MAXIMUM;
		}
		
		public function setHunger(value:int):void
		{
			_hunger.ratio = 1.0 - Number(value)/MAIN_PARAMETERS_MAXIMUM;
		}
		
		public function setStamina(value:int):void
		{
			_stamina.ratio = Number(value)/MAIN_PARAMETERS_MAXIMUM;
		}
		
		public function setAlcohol(value:int):void
		{
			_alcohol.ratio = Number(value)/MAIN_PARAMETERS_MAXIMUM;
		}
		
		public function setSmell(value:int):void
		{
			_smell.text = "smell:"+value;
		}
		
		public function setCigButts(value:int):void
		{
			_cigButs.text = "cig butts:"+value;
		}
		
		public function setMoney(value:int, animated:Boolean = false):void
		{
			if(animated && _moneyText.text != String(value))
			{
				animate(_moneyText, _money);
			}
			_moneyText.text = value+"";
		}
		
		public function setCans(value:int, animated:Boolean = false):void
		{
			if(animated && _cansText.text != String(value))
			{
				animate(_cansText, _cans);
			}
			_cansText.text = value+"";
		}
		
		public function setWastedPapper(value:int, animated:Boolean = false):void
		{
			if(animated && _wastedPapperText.text != String(value))
			{
				animate(_wastedPapperText, _wastedPapper);
			}
			_wastedPapperText.text = value+"";
		}
		
		public function setPreciousMetals(value:int, animated:Boolean = false):void
		{
			if(animated && _preciousMetalsText.text != String(value))
			{
				animate(_preciousMetalsText, _preciousMetals);
			}
			_preciousMetalsText.text = value+"";
		}
		
		public function setBottles(value:int, animated:Boolean = false):void
		{
			if(animated && _bottlesText.text != String(value))
			{
				animate(_bottlesText, _bottles);
			}
			_bottlesText.text = value+"";
			
		}
		
		private function animate(tf:TextField, icon:Sprite):void
		{
			Starling.juggler.removeTweens(tf);
			tf.color = 0x0;
			var blinkTextTween:Tween = new Tween(tf, 0.3);
			blinkTextTween.reverse = true;
			blinkTextTween.repeatCount = 2;
			blinkTextTween.scaleTo(1.2);
			blinkTextTween.animate("color", Color.WHITE);
			//blinkTextTween.onComplete = onTextTweenAnimComple;
			//blinkTextTween.onCompleteArgs = [blinkTextTween];
			Starling.juggler.add(blinkTextTween);
			
			/*var blinkIconTween:Tween = new Tween(icon, 0.3);
			blinkIconTween.reverse = true;
			blinkIconTween.repeatCount = 2;
			blinkIconTween.scaleTo(1.2);
			//blinkIconTween.onComplete = onTextTweenAnimComple;
			//blinkIconTween.onCompleteArgs = [blinkIconTween];
			Starling.juggler.add(blinkIconTween);*/
		}
		
		/*private function onTextTweenAnimComple(args:Array):void
		{
			Starling.juggler.remove(args[0] as Tween);
		}*/
	}
}