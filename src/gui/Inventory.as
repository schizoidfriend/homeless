package gui
{
	import dude.DudeParameters;
	
	import flash.geom.Point;
	
	import inventory.items.IItem;
	import inventory.items.ItemBuzz;
	import inventory.items.ItemFood;
	import inventory.items.SimpleItem;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	import utils.ArtUtils;
	
	public class Inventory extends ItemsContainer
	{	
		public static const INHAND_SLOT_NUM:int = 4;
		//public static const CLOTHES_SLOT_NUM:int = 5;
			
		private var _description:Sprite = new Sprite();
		private var _descriptionTF:TextField = new TextField(240,240,"","Verdana",16,0x0,true);
		
		private var _dudeDoll:Sprite = new Sprite();
		
		private var _handItemSprite:Sprite;
		private var _handPos:Point;
		
		private var _clothesItemSprite:Sprite;
		private var _clothesPos:Point;
		
		private var _garbageIcon:Sprite;
		private var _garbagePos:Point;
		
		private var _selector:Sprite;
		private var _currentSelection:int = -1;
		
		private var _sizeOfInv:int
		
		public function Inventory(invSize:int)
		{
			super(invSize);
			
			_sizeOfInv = invSize + 1;
			
			var descrImg:Image = ArtUtils.makeImage("descriptionField")
			_description.addChild(descrImg);
			_description.addChild(_descriptionTF);
			addChild(_description);
			
			var dudeDollBackImg:Image = ArtUtils.makeImage("descriptionField");
			_dudeDoll.addChild(dudeDollBackImg);
			_dudeDoll.x = 0;
			_dudeDoll.y = 256+64;
			
			var dudeDollImg:Image = ArtUtils.makeSelfCenteredImage("HoboIdle");
			dudeDollImg.scaleX = 2;	dudeDollImg.scaleY = 2;
			dudeDollImg.x = _dudeDoll.width/2 - dudeDollImg.width/2;
			dudeDollImg.y = _dudeDoll.height/2 - dudeDollImg.height/2;
			_dudeDoll.addChild(dudeDollImg);
			
			addChild(_dudeDoll);
			
			//adding slots
			for(var i:int = 0; i < 4; i++)
			{
				var elemImg:Image = ArtUtils.makeImage("inventoryElement");
				
				var spr:Sprite = new Sprite();
				spr.addChild(elemImg);
				addChild(spr);
				spr.x = i*64;
				spr.y = 256;
				_slotPositions.push(new Point(i*64, 256));
			}
			
			var handSlotImg:Image = ArtUtils.makeImage("inventoryElement");
			var handSpr:Sprite = new Sprite();
			handSpr.addChild(handSlotImg);
			handSpr.x = handSpr.width;
			handSpr.y = 0;
			_dudeDoll.addChild(handSpr);
			_handPos = new Point(_dudeDoll.x+handSpr.width, _dudeDoll.y);
			
			/*var clothesSlotImg:Image = ArtUtils.makeSelfCenteredImage("inventoryElement");
			var clothesSprite:Sprite = new Sprite();
			clothesSprite.addChild(clothesSlotImg);
			clothesSprite.x = _dudeDoll.width/2;
			clothesSprite.y = _dudeDoll.height/2;
			_dudeDoll.addChild(clothesSprite);
			_clothesPos = new Point(_dudeDoll.x+_dudeDoll.width/2-clothesSprite.width/2, _dudeDoll.y+_dudeDoll.height/2-clothesSprite.height/2);*/
				
			_slotPositions.push(_handPos);
			//_slotPositions.push(_clothesPos);
			
			_selector = ArtUtils.makeSimpleSprite("InvSelector");
			addChild(_selector);
			
			_garbageIcon = ArtUtils.makeSimpleSprite("InvNothing");
			_garbageIcon.addEventListener(TouchEvent.TOUCH, onGarbageTouch);
			_garbageIcon.x = this.width-_garbageIcon.width; _garbageIcon.y  = this.height-_garbageIcon.height;
			addChild(_garbageIcon);
				
			hideSelector();
		}
		
		public function get currentSelection():int
		{
			return _currentSelection;
		}

		private function onGarbageTouch(e:TouchEvent):void
		{
			var touchesBegan:Vector.<Touch> = e.getTouches(_garbageIcon, TouchPhase.BEGAN);
			if(touchesBegan.length)
			{
				killCurrentItem();
			}
		}
		
		public function setSelectorToPos(pos:uint):void
		{			
			_currentSelection = pos;
			
			_selector.x = _slotPositions[pos].x;
			_selector.y = _slotPositions[pos].y;
			
			_selector.visible = true;
			
			_garbageIcon.visible = true;
		}
		
		private function setSelectorToFirstValidPos():void
		{
			for(var i:int = 0; i < _sizeOfInv; i++)
			{
				if(_objects[i] != null)
				{
					setSelectorToPos(i);
					return ;
				}
			}
		}
		
		public function nextPos():void
		{
			var nextPos:uint = _currentSelection;
			var i:int;
			if(_currentSelection == _sizeOfInv-1)
			{
				i = 0;
			}
			else
			{
				i = _currentSelection+1;
			}
			
			while(nextPos == _currentSelection)
			{
				if(_objects[i] != null)
				{
					nextPos = i;
					setSelectorToPos(nextPos);
					return ;
				}
				i++;
				if(i == _sizeOfInv)
				{
					i = 0;
				}
				
				if(i == _currentSelection)
				{
					return ;
				}
			}
		}
		
		public function prevPos():void
		{
			var nextPos:uint = _currentSelection;
			var i:int;
			if(_currentSelection == 0)
			{
				i = _sizeOfInv-1;
			}
			else
			{
				i = _currentSelection-1;
			}
			
			while(nextPos == _currentSelection)
			{
				if(_objects[i] != null)
				{
					nextPos = i;
					setSelectorToPos(nextPos);
					return
				}
				i--;
				if(i < 0)
				{
					i = _sizeOfInv-1;
				}
				
				if(i == _currentSelection)
				{
					return ;
				}
			}
		}
		
		public function upPos():void
		{
			if(_currentSelection == INHAND_SLOT_NUM)
			{
				setSelectorToFirstValidPos();
			}
			else
			{
				if(_objects[INHAND_SLOT_NUM] != null)
				{
					setSelectorToPos(INHAND_SLOT_NUM);
				}
			}
		}
		
		private function hideSelector():void
		{
			_currentSelection = -1;
			_selector.visible = false;
			_garbageIcon.visible = false;
		}
		
		public function killCurrentItem():void
		{
			if(_currentSelection != -1)
			{
				if(Game.instance.dude.removeFromInventoryPos(_currentSelection))
				{
					refresh();
					hideSelector();
				}
			}			
		}
		
		override protected function onObjectMovementEnded(e:TouchEvent):void
		{
			this.parent.removeChild(_currentlyTakenObjectImg);
			addChild(_currentlyTakenObjectImg);
			_currentlyTakenObjectImg.x = _currentlyTakenObjectImg.x - this.x;
			_currentlyTakenObjectImg.y = _currentlyTakenObjectImg.y - this.y;
			
			var pos:int = getInvPosFromMousePos(new Point(_currentlyTakenObjectImg.x + _currentlyTakenObjectMouseOffset.x,
				_currentlyTakenObjectImg.y + _currentlyTakenObjectMouseOffset.y));
			if(pos != -1 && pos < _size && pos != _currentlyTakenObjectPos && _currentlyTakenObjectPos < _size)
			{
				
				if(isEmptySlot(pos))
				{
					moveItemTo(_currentlyTakenObjectPos, pos);
				}
				else
				{
					exchangeItems(_currentlyTakenObjectPos, pos);
				}
				_currentlyTakenObjectImg = null;
			}
			else if( pos != -1 && ((pos == INHAND_SLOT_NUM && canPutInhands(_currentlyTakenObject)) || _currentlyTakenObjectPos == 4 )) //check for hands
			{
				if(isEmptySlot(pos))
				{
					moveItemTo(_currentlyTakenObjectPos, pos);
				}
				else 
				{
					var exchangedItem:IItem = _objects[pos];
					if(canPutInhands(exchangedItem))
					{
						exchangeItems(_currentlyTakenObjectPos, pos);
					}
					else
					{
						onObjectNotOverValidZone(e);
						return ;
					}
				}
				_currentlyTakenObjectImg = null;
			}
			else
			{
				onObjectNotOverValidZone(e);
			}
			//setItemsToDudeInventory();
		}
		
		override protected function moveItemTo(from:int, to:int):void
		{
			super.moveItemTo(from, to);
			
			if(Game.instance.dude.moveInventoryItem(from, to))
			{
				setSelectorToPos(to);
			}
		}
		
		override protected function exchangeItems(pos1:int, pos2:int):void
		{
			super.exchangeItems(pos1, pos2);
			
			Game.instance.dude.exchangeInventoryItems(pos1, pos2);
		}
		
		/*private function setItemsToDudeInventory():void
		{
			Game.instance.dude.clearInventory();
			for(var i:int = 0; i < _objects.length; i++)
			{
				if(i == 4)
				{
					Game.instance.dude.setItemToHandS(_objects[i] as IItem);
				}
				else
				{
					Game.instance.dude.setItemToInventory(_objects[i] as IItem, i);
				}
			}
		}*/
		
		override protected function onObjectNotOverValidZone(e:TouchEvent):void
		{
			var touchesEnded:Vector.<Touch> = e.getTouches(Image(e.currentTarget), TouchPhase.ENDED);
			var movingTouch:Touch = touchesEnded[0];
			var posToCheck:Point = new Point(movingTouch.globalX - Game.instance.container.x, 
				movingTouch.globalY - Game.instance.container.y);
			
			//if(!checkUseOnDoll(movingTouch))
			{
				super.onObjectNotOverValidZone(e);
			}

		}
		
		private function canPutInhands(item:IItem):Boolean
		{
			if(item.group == SimpleItem.WEAPON_ITEM)
			{
				return true;
			}
			return false;
		}
		
		/*private function exchangeWithHands(pos1:int, pos2:int):void
		{
			if(_objects[pos1] && _objects[pos2])
			{
				var cpy1:IItem = IItem(_objects[pos1]).copy;
				var cpy2:IItem = IItem(_objects[pos2]).copy;
				_objects[pos1] = null;
				_objects[pos2] = null;
				_objects[pos1] = cpy2;
				_objects[pos2] = cpy1;
				
				redrawElement(pos1);
				redrawElement(pos2);
			}
		}
		protected function moveItemToWithHands(from:int, to:int):void
		{
			if(_objects[from] && (_objects[to] == null))
			{
				_objects[to] = _objects[from];
				removeItem(from);
				redrawElement(to);
			}
		}*/
		
		override protected function removeItem(pos:int, objects:Array, elements:Array):void
		{
			if(pos >= 0 && pos < _sizeOfInv && _objects[pos])
			{
				_objects[pos] = null;
				this.removeChild(_elements[pos], true);
				_elements[pos] = null;
			}
		}
		
		private function checkUseOnDoll(touch:Touch):Boolean
		{
			var posToCheck:Point = new Point(touch.globalX - this.x, 
				touch.globalY - this.y);
			if(isOverPlayerDoll(posToCheck))
			{
				var obj:IItem = _objects[_currentlyTakenObjectPos];
				if(obj)
				{
					if(obj.useItem())
					{
						removeItem(_currentlyTakenObjectPos, _objects, _elements);
						return true;
					}
					return false;
				}
			}
			
			return false;
		}
		
		private function isOverPlayerDoll(pos:Point):Boolean
		{
			return isInRect(pos, new Point(_dudeDoll.x, _dudeDoll.y), _dudeDoll.width, _dudeDoll.height);
		}
		
		override protected function onSimpleClick(e:TouchEvent):void
		{
			var curObj:DisplayObject = e.currentTarget as DisplayObject;
			
			super.onSimpleClick(e);
			_descriptionTF.text = String(Image(e.currentTarget).x);
			
			var pos:int = getInvPosFromMousePos(new Point(curObj.x, curObj.y));
			if(pos >= 0)
			{
				setSelectorToPos(pos);
			}
		}
		
		public function refresh():void
		{
			cleanInventory();
			
			var inv:Array = Game.instance.dude.inventory;
			for(var i:int = 0; i < inv.length; i++)
			{
				var item:IItem = inv[i];
				if(item)
				{
					addItem(item, i);
				}
			}
			var hi:IItem = Game.instance.dude.handItem;
			if(hi)
			{
				_objects[INHAND_SLOT_NUM] = hi;
				
				redrawElement(INHAND_SLOT_NUM, _elements, _objects, _slotPositions);
			}
			setSelectorToFirstValidPos();
		}
		
		private function cleanInventory():void
		{
			for(var i:int = 0; i < _sizeOfInv; i++)
			{
				_objects[i] = null;
				
				var spr:DisplayObject = _elements[i];
				if(spr)
				{
					this.removeChild(spr, true);
				}
			}
		}
	}
}