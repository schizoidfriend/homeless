package gui
{
	import flash.geom.Point;
	
	import inventory.items.IItem;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	public class ItemsContainer extends Sprite
	{
		protected var _size:int = 0;
		
		//sprites
		protected var _elements:Array = new Array();
		//IItem-s
		protected var _objects:Array = new Array();
		//positions of sprites
		protected var _slotPositions:Vector.<Point> = new Vector.<Point>();
		
		protected var _currentlyTakenObject:IItem;
		protected var _currentlyTakenObjectImg:Image = null;
		protected var _currentlyTakenObjInitialPos:Point = new Point();
		protected var _currentlyTakenObjectMouseOffset:Point = new Point();
		protected var _currentTouch:Touch;
		protected var _currentlyTakenObjectPos:int = -1;
		
		public function ItemsContainer(size:int)
		{
			super();
			_size = size;
			
			for(var i:int = 0; i < _size; i++)
			{
				_objects[i] = null;
			}
		}

		public function get size():int
		{
			return _size;
		}
		
		protected function handleMovedTouches(touches:Vector.<Touch>, e:TouchEvent):void
		{
			_currentTouch = null;
			if(_currentlyTakenObjectImg)
			{
				var movingTouch:Touch = touches[0];
				_currentlyTakenObjectImg.x = movingTouch.globalX - _currentlyTakenObjectMouseOffset.x;
				_currentlyTakenObjectImg.y = movingTouch.globalY - _currentlyTakenObjectMouseOffset.y; 
			}
		}
		
		protected function handleMouseDownTouches(touches:Vector.<Touch>, e:TouchEvent):void
		{
			var target:Image = Image(e.currentTarget);
			if(target)
			{
				_currentlyTakenObjectImg = target; 
				
				_currentlyTakenObjInitialPos.setTo(_currentlyTakenObjectImg.x, _currentlyTakenObjectImg.y);
				_currentlyTakenObjectPos = getInvPosFromMousePos(_currentlyTakenObjInitialPos);
				if(_currentlyTakenObjectPos >= 0)
				{
					removeChild(_currentlyTakenObjectImg);
					this.parent.addChild(_currentlyTakenObjectImg);
					_currentlyTakenObjectImg.x = _currentlyTakenObjectImg.x + this.x;
					_currentlyTakenObjectImg.y = _currentlyTakenObjectImg.y + this.y;
					
					_currentTouch = touches[0];
					_currentlyTakenObjectMouseOffset.setTo(_currentTouch.globalX - _currentlyTakenObjInitialPos.x - this.x,
						_currentTouch.globalY - _currentlyTakenObjInitialPos.y - this.y);
					
					var pos:int = getInvPosFromMousePos(new Point(target.x-this.x, target.y-this.y));
					if(pos >= 0)
					{
						_currentlyTakenObject = _objects[pos];
					} 
				}
				else
				{
					_currentlyTakenObjectImg = null;
				}
			}
		}
		
		protected function onSimpleClick(e:TouchEvent):void
		{
			this.parent.removeChild(_currentlyTakenObjectImg);
			addChild(_currentlyTakenObjectImg);
			_currentlyTakenObjectImg.x = _currentlyTakenObjectImg.x - this.x;
			_currentlyTakenObjectImg.y = _currentlyTakenObjectImg.y - this.y;
		}
		
		protected function onObjectMovementEnded(e:TouchEvent):void
		{
			if(!_currentlyTakenObjectImg)
				return;
			
			this.parent.removeChild(_currentlyTakenObjectImg);
			addChild(_currentlyTakenObjectImg);
			_currentlyTakenObjectImg.x = _currentlyTakenObjectImg.x - this.x;
			_currentlyTakenObjectImg.y = _currentlyTakenObjectImg.y - this.y;
			
			var pos:int = getInvPosFromMousePos(new Point(_currentlyTakenObjectImg.x + _currentlyTakenObjectMouseOffset.x,
				_currentlyTakenObjectImg.y + _currentlyTakenObjectMouseOffset.y));
			if(pos != -1 && pos != _currentlyTakenObjectPos)
			{
				if(isEmptySlot(pos))
				{
					moveItemTo(_currentlyTakenObjectPos, pos);
				}
				else
				{
					exchangeItems(_currentlyTakenObjectPos, pos);
				}
				_currentlyTakenObjectImg = null;
			}
			else
			{
				onObjectNotOverValidZone(e);
			}
		}
		
		protected function onObjectNotOverValidZone(e:TouchEvent):void
		{
			_currentlyTakenObjectImg.x = _currentlyTakenObjInitialPos.x;
			_currentlyTakenObjectImg.y = _currentlyTakenObjInitialPos.y;
		}
		
		protected function handleMouseUpTouches(touches:Vector.<Touch>, e:TouchEvent):void
		{
			if(_currentTouch)
			{
				onSimpleClick(e);
			}
			else
			{
				onObjectMovementEnded(e);
			}
		}
		
		private function onInventoryElementClicked(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(Image(e.currentTarget), TouchPhase.MOVED);
			var touchesBegan:Vector.<Touch> = e.getTouches(Image(e.currentTarget), TouchPhase.BEGAN);
			var touchesEnded:Vector.<Touch> = e.getTouches(Image(e.currentTarget), TouchPhase.ENDED);
			
			if(touchesBegan.length > 0)
			{
				handleMouseDownTouches(touchesBegan, e);
				
			}
			if(touchesEnded.length > 0)
			{
				handleMouseUpTouches(touchesEnded, e);
			}
			if(touches.length > 0)
			{
				handleMovedTouches(touches, e);
			}
		}
		
		protected function exchangeItems(pos1:int, pos2:int):void
		{
			if(_objects[pos1] && _objects[pos2])
			{
				var cpy1:IItem = IItem(_objects[pos1]).copy;
				var cpy2:IItem = IItem(_objects[pos2]).copy;
				_objects[pos1] = null;
				_objects[pos2] = null;
				_objects[pos1] = cpy2;
				_objects[pos2] = cpy1;
				
				redrawElement(pos1, _elements, _objects, _slotPositions);
				redrawElement(pos2, _elements, _objects, _slotPositions);
			}
		}
		
		protected function moveItemTo(from:int, to:int):void
		{
			if(_objects[from] && (_objects[to] == null))
			{
				_objects[to] = _objects[from];
				removeItem(from, _objects, _elements);
				redrawElement(to, _elements, _objects, _slotPositions);
			}
		}
		
		protected function addItem(item:IItem, pos:int):void
		{	
			if(pos >= 0 && pos < size && !_objects[pos])
			{
				_objects[pos] = item;
				
				redrawElement(pos, _elements, _objects, _slotPositions);
			}
		}
		
		public function addToFirstEmptyPlace(item:IItem):Boolean
		{
			for(var i:int = 0; i < _objects.length; i++)
			{
				if(_objects[i] == null)
				{
					addItem(item, i);
					return true;
				}
			}
			return false;
		}
		
		protected function removeItem(pos:int, objects:Array, elements:Array):void
		{
			if(pos >= 0 && pos < size && objects[pos])
			{
				objects[pos] = null;
				this.removeChild(elements[pos], true);
				elements[pos] = null;
			}
		}
		
		protected function isInRect(pt:Point, topPos:Point, width:int, height:int):Boolean
		{
			if(pt.x >= topPos.x && pt.y >= topPos.y &&
				pt.x < topPos.x + width && pt.y < topPos.y + height)
			{
				return true;
			}
			return false;
		}
		
		public function getInvPosFromMousePos(pos:Point):int
		{
			for(var i:int = 0; i < _slotPositions.length; i++)
			{
				var curPos:Point = _slotPositions[i];
				if(isInRect(pos, curPos, 64, 64))
				{
					return i;
				}
			}
			
			return -1;
		}
		
		public function isEmptySlot(pos:int):Boolean
		{
			return _objects[pos]==null ? true : false;
		}
		
		protected function redrawElement(pos:int, redrawElements:Array, objects:Array, slotPositions:Vector.<Point>):void
		{
			if(objects[pos])
			{
				var oldElem:Image = redrawElements[pos];
				if(oldElem)
				{
					this.removeChild(oldElem, true);
				}
				var item:IItem = objects[pos];
				var itemTex:Texture = Game.instance.assets.getTexture(item.name);
				var itemImg:Image = new Image(itemTex);
				itemImg.addEventListener(TouchEvent.TOUCH, onInventoryElementClicked);
				redrawElements[pos] = itemImg;
				
				var point:Point = slotPositions[pos] as Point;
				itemImg.x = point.x;
				itemImg.y = point.y;
				
				addChild(itemImg);
			}
		}

	}
}