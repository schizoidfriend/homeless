package gui
{
	import flash.desktop.NativeApplication;
	import flash.geom.Point;
	import flash.text.engine.FontWeight;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.ArtUtils;
	import utils.TLFSprite;
	
	public class MainMenu extends Sprite
	{
		private var _mainMenuItems:Vector.<MenuItem> = new Vector.<MenuItem>();
		private var _optionsMenuItems:Vector.<MenuItem> = new Vector.<MenuItem>();
		
		private var _currentList:Vector.<MenuItem> = null;
		
		private var _background:Image;
		
		public function MainMenu()
		{
			super();
			
			initialize();
		}
		
		public function initialize():void
		{
			//_background = ArtUtils.makeImage("DialogueBG");
			//addChild(_background);
			//_background.scaleX = 2.0; _background.scaleY = 2.0;
			
			var newGameItem:MenuItem = new MenuItem("New Game", MenuItem.ACTION_NEW_GAME);
			var loadGameItem:MenuItem = new MenuItem("Load Game", MenuItem.ACTION_LOAD_GAME);
			var optionsItem:MenuItem = new MenuItem("Options", MenuItem.ACTION_GOTO_MENU, "options");
			var exitItem:MenuItem = new MenuItem("Exit Game", MenuItem.ACTION_EXIT_GAME);
			
			_mainMenuItems.push(newGameItem);
			_mainMenuItems.push(loadGameItem);
			_mainMenuItems.push(optionsItem);
			_mainMenuItems.push(exitItem);
			
			var backToMainMenu:MenuItem = new MenuItem("Back", MenuItem.ACTION_GOTO_MENU, "main");
			_optionsMenuItems.push(backToMainMenu);
		}
		
		public function buildMenu(name:String):void
		{
			switch(name)
			{
				case "main":
					_currentList = _mainMenuItems;
					break;
				
				case "options":
					_currentList = _optionsMenuItems;
					break;
			}
			
			buildItems(_currentList);
		}
		
		private function buildItems(itemsList:Vector.<MenuItem>):void
		{
			cleanup();
			
			var startPos:Point = new Point(0,768/2);
			var yOffset:int = 100;
			
			var i:int = 0;
			for each (var item:MenuItem in itemsList)
			{
				var dp:MenuPoint = new MenuPoint(item.name, i);
				dp.x = startPos.x;
				dp.y = startPos.y+(yOffset*i);
				addChild(dp);
				
				dp.addEventListener(MenuPoint.ON_CLICKED_EVENT, onDialogPointTriggered);
				
				this.addChild(dp);
				i++;
			}
		}
		
		private function onDialogPointTriggered(e:Event):void
		{
			var curDP:MenuPoint = e.target as MenuPoint;
			if(curDP)
			{
				var curDPClickedPos:int = curDP.posInDialog;
				var item:MenuItem = _currentList[curDPClickedPos];
				executeItem(item);
			}
		}
		
		private function executeItem(item:MenuItem):void
		{
			switch(item.action)
			{
				case MenuItem.ACTION_LOAD_GAME:
					Game.instance.setGameLoading();
					Game.instance.tryToStartGame();
					break;
				case MenuItem.ACTION_NEW_GAME:
					Game.instance.tryToStartGame();
					break;
				case MenuItem.ACTION_EXIT_GAME:
					NativeApplication.nativeApplication.exit();
					break;
			}
		}
		
		private function cleanup():void
		{
			while(numChildren > 0)
			{
				removeChildAt(0, true);
			}
		}
	}
}