package gui
{
	public class MenuItem
	{
		//action types
		public static const ACTION_NEW_GAME:int = 0;
		public static const ACTION_GOTO_MENU:int = 1;
		public static const ACTION_GOTO_SCREEN:int = 2;
		public static const ACTION_EXIT_SCREEN:int = 3;
		public static const ACTION_EXIT_GAME:int = 4;
		public static const ACTION_LOAD_GAME:int = 5;
		
		private var _name:String;
		private var _action:int;
		private var _actionInfo:String;
		
		public function MenuItem(name:String, action:int, actionInfo:String = "")
		{
			_name = name;
			_action = action;
			_actionInfo = actionInfo;
		}

		public function get actionInfo():String
		{
			return _actionInfo;
		}

		public function get action():int
		{
			return _action;
		}

		public function get name():String
		{
			return _name;
		}

	}
}