package gui
{
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.ArtUtils;
	import utils.TLFSprite;
	
	public class MenuPoint extends Sprite
	{
		public static const ON_CLICKED_EVENT:String = "___ON_CLICKED_EVENT___";
		
		private var _tfContainer:Sprite = new Sprite();
		private var _textField:TLFSprite;
		
		private var _background:Image;
		private var _posInDialog:int;
		public function MenuPoint(text:String, pos:int)
		{
			super();
			_posInDialog = pos;
			init(text);
		}
		
		public function get posInDialog():int
		{
			return _posInDialog;
		}

		private function init(text:String):void
		{
			_textField = ArtUtils.makeDialogTF(text, 1024, 100, 0, 0, 16, 0xFFFFFF, VAlign.TOP, HAlign.CENTER);
			_textField.touchable = false
			//_tfContainer.addEventListener(TouchEvent.TOUCH, onTouched);
			
			addChild(_tfContainer);
			
			var bgShape:Shape = new Shape();
			bgShape.graphics.beginFill(0x0, 0.5);
			bgShape.graphics.drawRect(0, 0, _textField.width, _textField.height);
			bgShape.graphics.endFill();
			
			var bgBitmapData:BitmapData = new BitmapData(_textField.width, _textField.height, false, 0x0000FF);
			bgBitmapData.draw(bgShape);
			var bgTexture:Texture = Texture.fromBitmapData(bgBitmapData, false, false, 1.0);
			
			_background = new Image(bgTexture);
			_background.addEventListener(TouchEvent.TOUCH, onTouched);
			_background.alpha = 0;
			_tfContainer.addChild(_background);
			
			_tfContainer.addChild(_textField);
		}
		
		private function onTouched(e:TouchEvent):void
		{
			var touchesHover:Vector.<Touch> = e.getTouches(_background, TouchPhase.HOVER);			
			var touchesBegin:Vector.<Touch> = e.getTouches(_background, TouchPhase.BEGAN);
			var touchesMoved:Vector.<Touch> = e.getTouches(_background, TouchPhase.MOVED);
			var touchesEnded:Vector.<Touch> = e.getTouches(_background, TouchPhase.ENDED);
			if(touchesBegin.length > 0 || touchesMoved.length > 0)
			{
				_textField.setStyle("color", 0x000000);
			}
			else if(touchesEnded.length > 0)
			{
				var touch:Touch = touchesEnded[0];
				var localPT:Point = this.globalToLocal(new Point(touch.globalX, touch.globalY));
				if(this.getBounds(this).contains(localPT.x, localPT.y))
				{
					_textField.setStyle("color", 0x00AA00);
					dispatchEvent(new Event(ON_CLICKED_EVENT));
				}
				else
				{
					_textField.setStyle("color", 0xFFFFFF);
				}
			}
			else if(touchesHover.length > 0)
			{
				_textField.setStyle("color", 0x00AA00);
			}
			else
			{
				_textField.setStyle("color", 0xFFFFFF);
			}
		
			_textField.flatten();
		}
	}
}