package gui
{
	
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class NewMapDialog extends Sprite
	{
		private var _plusSizeXButton:Button;
		private var _minusSizeXButton:Button;
		private var _plusSizeYButton:Button;
		private var _minusSizeYButton:Button;
		
		private var _xSizeText:TextField = new TextField(50,50, "30", "Verdana", 24, 0x0, true);
		private var _ySizeText:TextField = new TextField(50,50, "30", "Verdana", 24, 0x0, true);
		
		private var _createButton:Button;
		
		private var _currentX:int = 30;
		private var _currentY:int = 30;
		
		public function NewMapDialog()
		{
			super();
			
			var nextbuttonTexture:Texture = Game.instance.assets.getTexture("nextbutton");
			
			_plusSizeXButton = new Button(nextbuttonTexture, "");
			_plusSizeXButton.x = 300;
			_plusSizeXButton.y = 300;
			_plusSizeXButton.scaleX = 0.2;
			_plusSizeXButton.scaleY = 0.2;
			_plusSizeXButton.addEventListener(Event.TRIGGERED, onPlusXButton);
			addChild(_plusSizeXButton);
			
			_minusSizeXButton = new Button(nextbuttonTexture, "");
			_minusSizeXButton.x = 200;
			_minusSizeXButton.y = 300;
			_minusSizeXButton.pivotX = _minusSizeXButton.width;
			_minusSizeXButton.pivotY = _minusSizeXButton.height;
			_minusSizeXButton.rotation = Math.PI;
			_minusSizeXButton.scaleX = 0.2;
			_minusSizeXButton.scaleY = 0.2;
			_minusSizeXButton.addEventListener(Event.TRIGGERED, onMinusXButton);
			addChild(_minusSizeXButton);
			
			_plusSizeYButton = new Button(nextbuttonTexture, "");
			_plusSizeYButton.x = 300;
			_plusSizeYButton.y = 400;
			_plusSizeYButton.scaleX = 0.2;
			_plusSizeYButton.scaleY = 0.2;
			_plusSizeYButton.addEventListener(Event.TRIGGERED, onPlusYButton);
			addChild(_plusSizeYButton);
			
			_minusSizeYButton = new Button(nextbuttonTexture, "");
			_minusSizeYButton.x = 200;
			_minusSizeYButton.y = 400;
			_minusSizeYButton.pivotX = _minusSizeYButton.width;
			_minusSizeYButton.pivotY = _minusSizeYButton.height;
			_minusSizeYButton.rotation = Math.PI;
			_minusSizeYButton.scaleX = 0.2;
			_minusSizeYButton.scaleY = 0.2;
			_minusSizeYButton.addEventListener(Event.TRIGGERED, onMinusYButton);
			addChild(_minusSizeYButton);
			
			_xSizeText.x = 255;
			_xSizeText.y = 300;
			addChild(_xSizeText);
			
			_ySizeText.x = 255;
			_ySizeText.y = 400;
			addChild(_ySizeText);
			
			var buttonTexture:Texture = Game.instance.assets.getTexture("button");
			_createButton = new Button(buttonTexture, "Create");
			_createButton.x = 220;
			_createButton.y = 500;
			_createButton.addEventListener(Event.TRIGGERED, onCreate);
			addChild(_createButton)
			
			
		}
		
		private function onPlusXButton(e:Event):void
		{
			_currentX++;
			refresh();
		}
		
		private function onMinusXButton(e:Event):void
		{
			_currentX--;
			refresh();
		}
		
		private function onPlusYButton(e:Event):void
		{
			_currentY++;
			refresh();
		}
		
		private function onMinusYButton(e:Event):void
		{
			_currentY--;
			refresh();
		}
		
		private function refresh():void
		{
			_xSizeText.text = String(_currentX);
			_ySizeText.text = String(_currentY);
		}
		
		private function onCreate(e:Event):void
		{
			Game.instance.background.clearMapAndCreateNew(_currentX, _currentY);
		}
	}
}