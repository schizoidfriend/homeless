package gui
{
	import flash.geom.Point;
	
	import inventory.items.IItem;
	import inventory.items.ItemBuzz;
	import inventory.items.ItemFood;
	import inventory.items.SimpleItem;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;

	public class OpenableContainer extends ItemsContainer
	{
		private var _takeAllButton:Button;
		
		public function OpenableContainer(size:int)
		{
			super(size);
			
			var elemTex:Texture = Game.instance.assets.getTexture("inventoryElement");
			
			for(var i:int = 0; i < size; i++)
			{
				var elemImg:Image = new Image(elemTex);
				
				var spr:Sprite = new Sprite();
				spr.addChild(elemImg);
				addChild(spr);
				spr.x = i*64;
				spr.y = 0;
				_slotPositions.push(new Point(i*64, 0));
			}
			
			var takeallbuttonTexture:Texture = Game.instance.assets.getTexture("button");
			_takeAllButton = new Button(takeallbuttonTexture, "takeAll");
			_takeAllButton.x = 0;
			_takeAllButton.y = 64;
			_takeAllButton.addEventListener(Event.TRIGGERED, onTakeAllButtonTriggered);
			addChild(_takeAllButton);
			
			//test
			//addItem(new ItemBuzz("Booze", 0, 0),1);
			//addItem(new ItemFood("Food", 0, 0),0);
			
		}
		
		/*override protected function onObjectNotOverValidZone(e:TouchEvent):void
		{
			var touchesEnded:Vector.<Touch> = e.getTouches(Image(e.currentTarget), TouchPhase.ENDED);
			var movingTouch:Touch = touchesEnded[0];
			
			var posToCheck:Point = new Point(movingTouch.globalX - Game.instance.inventory.x, 
											movingTouch.globalY - Game.instance.inventory.y);
			var invPos:int = Game.instance.inventory.getInvPosFromMousePos(posToCheck);
			if(invPos >= 0 && Game.instance.inventory.isEmptySlot(invPos))
			{
				var pos:int = getInvPosFromMousePos(_currentlyTakenObjInitialPos);
				var obj:IItem = _objects[pos];
				if(obj && Game.instance.inventory.addToFirstEmptyPlace(obj.copy))
				{
					removeItem(pos, _objects, _elements);
				}
				else
				{
					super.onObjectNotOverValidZone(e);
				}
			}
			else
			{
				super.onObjectNotOverValidZone(e);
			}
		}*/
		
		override protected function onSimpleClick(e:TouchEvent):void
		{
			super.onSimpleClick(e);
			var pos:int = getInvPosFromMousePos(new Point(Image(e.currentTarget).x, Image(e.currentTarget).y));
			if(pos >= 0)
			{
				addToInventory(pos);
			}
		}
		
		private function addToInventory(pos:int):void
		{
			var obj:IItem = _objects[pos];
			/*if(obj && Game.instance.inventory.addToFirstEmptyPlace(obj.copy))
			{
				removeItem(pos, _objects, _elements);
			}*/
		}
		
		private function onTakeAllButtonTriggered(e:Event):void
		{
			var curSize:int = _objects.length;
			for(var i:int = 0; i < curSize; i++)
			{
				var obj:IItem = _objects[i];
				/*if(obj && Game.instance.inventory.addToFirstEmptyPlace(obj.copy))
				{
					removeItem(i, _objects, _elements);
				}*/
			}
		}
	}
}