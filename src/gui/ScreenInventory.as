package gui
{
	import dude.DudeParameters;
	
	import inventory.items.IItem;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	
	import utils.ArtUtils;
	
	public class ScreenInventory extends Sprite
	{
		private var _itemCells:Vector.<Sprite> = new Vector.<Sprite>();
		private var _handsCell:Sprite;
		
		//private var _selector:Sprite;
		//private var _garbageIcon:Sprite;
		
		//private var _currentSelection:int = -1;

		public function ScreenInventory()
		{
			super();
			initialize();
		}
		
		private function initialize():void
		{
			this.scaleX = 0.5;
			this.scaleY = 0.5;
			/*_selector = ArtUtils.makeSimpleSprite("InvSelector");
			addChild(_selector);*/
			
			for(var i:int = 0; i < DudeParameters.INVENTORY_SIZE; i++)
			{
				var cell:Sprite = ArtUtils.makeSimpleSprite("InvBG");
				cell.x = i*cell.width;
				cell.y = 0;
				//cell.alpha = 0.5;
				_itemCells.push(cell);
				addChild(cell);
			//	cell.addEventListener(TouchEvent.TOUCH, onCellTouch);
				
				var tf:TextField = ArtUtils.makeBoldTF(15,15,cell.x+5,cell.height-15,10);
				tf.text = String(i+1);
				tf.touchable = false;
				addChild(tf);
			}
			
			_handsCell = ArtUtils.makeSimpleSprite("InvBG");
			//_handsCell.alpha = 0.5;
			addChild(_handsCell);
			_handsCell.x = _handsCell.width; _handsCell.y = _handsCell.height;
			
			var htf:TextField = ArtUtils.makeBoldTF(15,15,_handsCell.x+5,_handsCell.height+_handsCell.height-15,10);
			htf.text = "5";
			addChild(htf);
			
			/*_garbageIcon = ArtUtils.makeSimpleSprite("InvNothing");
			_garbageIcon.addEventListener(TouchEvent.TOUCH, onGarbageTouch);
			_garbageIcon.x = _handsCell.width*3; _garbageIcon.y  = _handsCell.height;
			addChild(_garbageIcon);
			
			hideSelector();*/
		}
		
		/*public function setSelectorToPos(pos:uint):void
		{
			if(pos >= DudeParameters.INVENTORY_SIZE || _itemCells[pos].numChildren <= 1)
			{
				return;
			}
			
			_currentSelection = pos;
			
			_selector.x = _itemCells[pos].x;
			_selector.y = _itemCells[pos].y;
			
			_selector.visible = true;
			
			_garbageIcon.visible = true;
		}
		
		private function hideSelector():void
		{
			_currentSelection = -1;
			_selector.visible = false;
			_garbageIcon.visible = false;
		}*/
		
		/*public function killCurrentItem():void
		{
			if(_currentSelection != -1)
			{
				if(Game.instance.dude.removeFromInventoryPos(_currentSelection))
				{
					refresh();
					//hideSelector();
				}
			}			
		}*/
		
	/*	private function onGarbageTouch(e:TouchEvent):void
		{
			var touchesBegan:Vector.<Touch> = e.getTouches(_garbageIcon, TouchPhase.BEGAN);
			if(touchesBegan.length)
			{
				killCurrentItem();
			}
		}
		
		private function onCellTouch(e:TouchEvent):void
		{
			//trace("cellTouched!");
			for(var i:int = 0; i < DudeParameters.INVENTORY_SIZE; i++)
			{
				var touchesBegan:Vector.<Touch> = e.getTouches(_itemCells[i], TouchPhase.BEGAN);
				if(touchesBegan.length)
				{
					setSelectorToPos(i);
				}
			}
		}*/
		
		private function getCellNum(cell:Sprite):int
		{
			for(var i:int = 0; i < DudeParameters.INVENTORY_SIZE; i++)
			{
				if(cell == _itemCells[i])
				{
					return i;
				}
			}
			return -1;
		}
		
		public function refresh():void
		{
			clear();
			if(!Game.instance.dude) return ;
			
			var inv:Array = Game.instance.dude.inventory;
			for(var i:int = 0; i < inv.length; i++)
			{
				var item:IItem = inv[i];
				if(item)
				{
					var img:Sprite = ArtUtils.makeSelfCenteredItemInPlace(item.name,0,0);
					_itemCells[i].addChild(img);
				}
			}
			
			var handItem:IItem = Game.instance.dude.handItem;
			if(handItem)
			{
				var handImg:Sprite = ArtUtils.makeSelfCenteredItemInPlace(handItem.name,0,0);
				_handsCell.addChild(handImg);
			}
		}
		
		private function clear():void
		{
			while(_handsCell.numChildren > 1)
			{
				_handsCell.removeChildAt(1, true);
			}
			
			for(var i:int = 0; i < DudeParameters.INVENTORY_SIZE; i++)
			{
				while(_itemCells[i].numChildren > 1)
				{
					_itemCells[i].removeChildAt(1, true);
				}
			}
		}
	}
}