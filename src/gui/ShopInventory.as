package gui
{
	import flash.geom.Point;
	
	import inventory.InventoryItemMaker;
	import inventory.items.IItem;
	
	import shops.SimpleShop;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.ColorMatrixFilter;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	import utils.ArtUtils;

	public class ShopInventory extends ItemsContainer
	{
		protected var _prices:Vector.<int> = new Vector.<int>();
		protected var _pricesTxts:Vector.<TextField> = new Vector.<TextField>();
		
		protected var _deElements:Array = new Array();
		protected var _dePrices:Vector.<int> = new Vector.<int>();
		protected var _dePricesTxts:Vector.<TextField> = new Vector.<TextField>();
		protected var _deObjects:Array = new Array();
		protected var _deSlotPositions:Vector.<Point> = new Vector.<Point>();
		
		protected var _canBuy:Array = new Array();
		
		protected var _currentShop:SimpleShop;
		
		public function ShopInventory(size:int)
		{
			super(size);
			
			var elemTex:Texture = Game.instance.assets.getTexture("inventoryElement");
			
			for(var i:int = 0; i < size; i++)
			{
				var elemImg:Image = new Image(elemTex);
				
				var spr:Sprite = new Sprite();
				spr.addChild(elemImg);
				addChild(spr);
				spr.x = i*64;
				spr.y = 0;
				_slotPositions.push(new Point(i*64, 0));
				
				var txt:TextField = new TextField(64, 32, "0 uah", "Verdana", 12, 0x0, true);
				txt.x = i*64;
				txt.y = 64;
				addChild(txt);
				_pricesTxts.push(txt);
				
				_prices[i] = 0;
				
				//deprices part (selling/wasted items)
				_deObjects[i] = null;
				
				txt = ArtUtils.makeBoldTF(64, 32, i*64, 192, 12);
				addChild(txt);
				_dePricesTxts.push(txt);
				
				var deelemImg:Image = new Image(elemTex);
				spr = ArtUtils.makeSprite(deelemImg, i*64, 128);
				addChild(spr);
				_deSlotPositions.push(new Point(i*64, 128));
				
			}
		}
		
		public function setPrices(prices:Array):void
		{
			for(var i:int = 0; i < prices.length; i++)
			{
				_prices[i] = prices[i];
			}
			refreshPrices();
		}
		
		public function setItems(shop:SimpleShop):void
		{	
			_currentShop = shop;
			var i:int;
			var item:IItem;
			for(i = 0; i < shop.shopInfo.assortiment.length; i++)
			{
				removeItem(i, _objects, _elements);
				item = shop.shopInfo.assortiment[i];
				addItem(item, i);
			}
			
			if(shop.shopInfo.canTake)
			{
				setDeElementsVisibility(true);
				for(i = 0; i < shop.shopInfo.dessortiment.length; i++)
				{
					removeItem(i, _deObjects, _deElements);
					item = shop.shopInfo.dessortiment[i];
					addDeItem(item, i);
				}
			}
			else
			{
				setDeElementsVisibility(false);
			}
			
			refreshPrices();
		}
		
		protected function setDeElementsVisibility(visible:Boolean):void
		{
			for each(var tf:TextField in _dePricesTxts)
			{
				tf.visible = visible;
			}
		}
		
		override protected function addItem(item:IItem, pos:int):void
		{
			super.addItem(item, pos);
			_prices[pos] = item.price;
		}
		
		protected function addDeItem(item:IItem, pos:int):void
		{
			_dePrices[pos] = item.price;
			if(pos >= 0 && pos < size && !_deObjects[pos])
			{
				_deObjects[pos] = item;
				
				redrawElement(pos, _deElements, _deObjects, _deSlotPositions);
			}
		}
		
		public function refreshItemsStatusesByPrices():void
		{
			for(var i:int = 0; i < _prices.length; i++)
			{
				if(i <_elements.length)
				{
					var price:int = _prices[i];
					var elem:Image = _elements[i];
					if(Game.instance.dude.canAfford(price))
					{
						elem.filter = null;	
						_canBuy[i] = true;
					}
					else
					{
						var cantAffordFilter:ColorMatrixFilter = new ColorMatrixFilter();
						cantAffordFilter.adjustSaturation(-1);
						elem.filter = cantAffordFilter;
						_canBuy[i] = false;
					}
				}
			}
		}
		
		public function refreshPrices():void
		{
			var i:int;
			for(i = 0; i < _pricesTxts.length; i++)
			{
				if(_prices[i] <= 0)
				{
					(_pricesTxts[i] as TextField).text = "";
				}
				else
				{
					(_pricesTxts[i] as TextField).text = String(_prices[i])+" UAH";
				}
			}
			
			for(i = 0; i < _dePricesTxts.length; i++)
			{
				if(i >= _dePrices.length || _dePrices[i] <= 0)
				{
					(_dePricesTxts[i] as TextField).text = "";
				}
				else
				{
					(_dePricesTxts[i] as TextField).text = String(_dePrices[i])+" UAH";
				}
			}
			
			refreshItemsStatusesByPrices();
		}
		
		public function setPriceFor(index:int, price:int):void
		{
			_prices[index] = price;
			refreshPrices();
		}
		
		override protected function moveItemTo(from:int, to:int):void
		{
			
		}
		
		override protected function exchangeItems(pos1:int, pos2:int):void
		{
			
		}
		
		/*override protected function onObjectNotOverValidZone(e:TouchEvent):void
		{
			var touchesEnded:Vector.<Touch> = e.getTouches(Image(e.currentTarget), TouchPhase.ENDED);
			var movingTouch:Touch = touchesEnded[0];
			
			var posToCheck:Point = new Point(movingTouch.globalX - Game.instance.inventory.x, 
				movingTouch.globalY - Game.instance.inventory.y);
			var invPos:int = Game.instance.inventory.getInvPosFromMousePos(posToCheck);
			if(invPos >= 0 && Game.instance.inventory.isEmptySlot(invPos))
			{
				var pos:int = getInvPosFromMousePos(_currentlyTakenObjInitialPos);
				var obj:IItem = _objects[pos];
				if(obj)
				{
					buyToInventory(pos, obj.price); 
				}
				
				super.onObjectNotOverValidZone(e);
			}
			else
			{
				super.onObjectNotOverValidZone(e);
			}
		}
		*/
		override protected function onSimpleClick(e:TouchEvent):void
		{
			super.onSimpleClick(e);
			var pos:int = getInvPosFromMousePos(new Point(Image(e.currentTarget).x, Image(e.currentTarget).y));
			if(pos >= 0)
			{
				//buyToInventory(pos, 0);
			}
		}
		
		/*private function buyToInventory(pos:int, price:int):void
		{
			var obj:IItem = _objects[pos];
			if(obj && _canBuy[pos])
			{
				Game.instance.inventory.addToFirstEmptyPlace(obj.copy);
				Game.instance.dude.spendMoney(obj.price);
				refreshItemsStatusesByPrices();
			}
		}*/
		
		public function getDeInvPosFromMousePos(pos:Point):int
		{
			for(var i:int = 0; i < _deSlotPositions.length; i++)
			{
				var curPos:Point = _deSlotPositions[i];
				if(isInRect(pos, curPos, 64, 64))
				{
					return i;
				}
			}
			
			return -1;
		}
		
	}
}