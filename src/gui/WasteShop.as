package gui
{
	import inventory.items.IItem;
	
	import starling.display.Button;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	
	import utils.ArtUtils;

	public class WasteShop extends ShopInventory
	{
		private var _sellAllBtn:Button;
		
		public function WasteShop(size:int)
		{
			super(size);
			_sellAllBtn = ArtUtils.createButton(0, 100, "Sell All Waste");
			_sellAllBtn.addEventListener(Event.TRIGGERED, onSellAllTriggered);
			addChild(_sellAllBtn);
		}
		
		private function onSellAllTriggered(e:Event):void
		{
			var deItems:Vector.<IItem> = _currentShop.shopInfo.dessortiment;
			for each (var item:IItem in deItems)
			{
				switch(item.name)
				{
					case "Wastepappers":
						Game.instance.dude.sellAllWastePappers(item.price);
						break;
					case "Can":
						Game.instance.dude.sellAllCans(item.price);
						break;
					case "Bottle":
						Game.instance.dude.sellAllBottles(item.price);
						break;
					case "PreciousMetals":
						Game.instance.dude.sellAllPreciousMetals(item.price);
						break;
				}
			}
			
			Game.instance.dude.refreshParameters();
		}
		
		override protected function onSimpleClick(e:TouchEvent):void
		{
		}
		
		override protected function handleMouseDownTouches(touches:Vector.<Touch>, e:TouchEvent):void
		{
			
		}
	}
}