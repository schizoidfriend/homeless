package inventory
{
	import inventory.items.IItem;
	import inventory.items.ItemBuzz;
	import inventory.items.ItemDeadRat;
	import inventory.items.ItemDrugz;
	import inventory.items.ItemFood;
	import inventory.items.SimpleItem;
	
	import map.CollectableObject;

	public class InventoryItemMaker
	{
		public function InventoryItemMaker()
		{
		}
		
		public static function makeItem(name:String):IItem
		{
			var item:IItem = Game.instance.itemsInfo.getItem(name);
			if(!item) return null;
			switch(item.group)
			{
				case SimpleItem.FOOD_ITEM: 
					if(item.type == CollectableObject.DEAD_RAT)
					{
						return new ItemDeadRat(item.name, item.type, item.effectVal, item.price);
					}
					return new ItemFood(name, item.type, item.effectVal, item.price);
				case SimpleItem.BOOZE_ITEM: return new ItemBuzz(name, item.type, item.effectVal, item.price);
				case SimpleItem.DRUGS_ITEM: return new ItemDrugz(name, item.type, item.effectVal, item.price);
				default: return null;
			}
		}
		
		public static function getTypeFromName(name:String, group:int):int
		{
			switch(name)
			{
				case "Food": 	return CollectableObject.FOOD;
				case "Booze": 	return CollectableObject.BUZZ;
				default:		return -1;
			}
		}
	}
}