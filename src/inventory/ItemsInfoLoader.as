package inventory
{
	import inventory.items.IItem;
	import inventory.items.ItemBuzz;
	import inventory.items.ItemDeadRat;
	import inventory.items.ItemDrugz;
	import inventory.items.ItemFood;
	import inventory.items.ItemWeapon;
	import inventory.items.SimpleItem;
	
	import map.CollectableObject;

	public class ItemsInfoLoader
	{
		[Embed(source="res/items.xml", mimeType="application/octet-stream")]
		public const ItemsXML:Class;
		
		private static var _itemTypes:Vector.<IItem> = new Vector.<IItem>();
		
		public function ItemsInfoLoader()
		{
			var xml:XML = XML(new ItemsXML());
			onLoadedXML(xml);
		}
		
		private function onLoadedXML(xml:XML):void
		{
			for each (var object:XML in xml.item)
			{
				var item:IItem;
				var group:int = int(object.@group);
				var name:String = String(object.@name);
				var type:int = int(object.@type);
				var effectVal:int = int(object.@effectVal);
				switch(group)
				{
					case SimpleItem.BOOZE_ITEM:
						item = new ItemBuzz(name, type, effectVal, 0);
						break;
					
					case SimpleItem.FOOD_ITEM:
						if(type == CollectableObject.DEAD_RAT)
						{
							item = new ItemDeadRat(name, type, effectVal, 0);
						}
						else
						{
							item = new ItemFood(name, type, effectVal, 0);
						}
						break;
					
					case SimpleItem.WEAPON_ITEM:
						item = new ItemWeapon(name, type, effectVal, 0);
						break;
					
					case SimpleItem.DRUGS_ITEM:
						item = new ItemDrugz(name, type, effectVal, 0);
						break;
					
					default:
						item = new SimpleItem(name, type, effectVal, 0);
						break;
				}
				
				_itemTypes.push(item);
			}
		}
		
		public function getItem(name:String):IItem
		{
			var item:IItem;
			for each (item in _itemTypes)
			{
				if(item.name == name)
				{
					return item;
				}
			}
			return null;
		}
	}
}