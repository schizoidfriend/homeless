package inventory.items
{
	public interface IItem
	{
		function get group():int;
		function get type():int;
		function get name():String;
		function get copy():IItem;
		function get effectVal():Number;
		function get price():int;
		function set price(value:int):void;
		function useItem():Boolean;
	}
}