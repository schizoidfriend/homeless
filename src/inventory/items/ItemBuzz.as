package inventory.items
{

	public class ItemBuzz extends SimpleItem
	{
		public function ItemBuzz(name:String, type:int, effectVal:int, price:int)
		{
			super(name, SimpleItem.BOOZE_ITEM, type, effectVal, price);
		}
		
		override public function useItem():Boolean
		{
			Game.instance.dude.drink(effectVal, name);
			return true;
		}	
		
		override public function get copy():IItem
		{
			return new ItemBuzz(name, type, effectVal, price);
		}
	}
}