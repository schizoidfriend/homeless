package inventory.items
{
	public class ItemDeadRat extends SimpleItem
	{
		public function ItemDeadRat(name:String, type:int, effectVal:Number=0, price:int=0)
		{
			super(name, SimpleItem.FOOD_ITEM, type, effectVal, price);
		}
		
		override public function useItem():Boolean
		{
			if(Game.instance.dude.isHungry())
			{
				Game.instance.dude.eat(effectVal, name);
				Game.instance.dude.addSmell(10);
				return true;
			}
			else
			{
				Game.instance.dude.showPhraseOver("Не буду їв тойво!");
			}
			return false;
		}
		
		override public function get copy():IItem
		{
			return new ItemDeadRat(name, type, effectVal, price);
		}
	}
}