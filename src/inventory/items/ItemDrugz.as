package inventory.items
{
	public class ItemDrugz extends SimpleItem
	{
		//types of drugz by effect
		public static const ENERGY:int = 0;
		public static const WEED:int = 1;
		public static const HEAVY:int = 2;
		public static const PSYCHO:int = 3;
		public static const DRUGZ_TOTAL:int = 4;
		
		public function ItemDrugz(name:String, type:int, effectVal:Number=0, price:int=0)
		{
			super(name,  SimpleItem.DRUGS_ITEM, type, effectVal, price);
		}
		
		override public function useItem():Boolean
		{
			Game.instance.dude.drinkEnery(effectVal);
			return true;
		}	
		
		override public function get copy():IItem
		{
			return new ItemDrugz(name, type, effectVal, price);
		}
	}
}