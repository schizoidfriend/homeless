package inventory.items
{

	public class ItemFood extends SimpleItem
	{
		public function ItemFood(name:String, type:int, effectVal:int, price:int)
		{
			super(name, SimpleItem.FOOD_ITEM, type, effectVal, price);
		}
		
		override public function useItem():Boolean
		{
			Game.instance.dude.eat(effectVal, name);
			return true;
		}
		
		override public function get copy():IItem
		{
			return new ItemFood(name, type, effectVal, price);
		}
	}
}