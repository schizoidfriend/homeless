package inventory.items
{
	public class ItemWeapon extends SimpleItem
	{
		public function ItemWeapon(name:String, type:int, effectVal:int, price:int)
		{
			super(name, SimpleItem.WEAPON_ITEM, type, effectVal, price);
		}
		
		//returns hit value
		public function hit():int
		{
			return 5;
		}
		
		override public function get copy():IItem
		{
			return new ItemWeapon(name, type, effectVal, price);
		}
	}
}