package inventory.items
{

	public class SimpleItem implements IItem
	{
		//Groups
		public static const FOOD_ITEM:int = 0;
		public static const BOOZE_ITEM:int = 1;
		public static const DRUGS_ITEM:int = 2;
		public static const WEARABLE_ITEM:int = 3;
		public static const USELESS_ITEM:int = 4;
		public static const WEAPON_ITEM:int = 5;
		public static const RECYCLE_ITEM:int = 6;
		
		public static const TOTAL_ITEMS:int = 7;
		
		
		private var _group:int;
		private var _type:int;
		private var _name:String;
		private var _effectVal:Number;
		private var _price:int
		
		public function SimpleItem(name:String, group:int, type:int, effectVal:Number = 0, price:int = 0)
		{
			_name = name;
			_group = group;
			_type = type;
			_effectVal = effectVal;
			_price = price;
		}
		
		public function get price():int
		{
			return _price;
		}
		
		public function set price(value:int):void
		{
			_price = value;
		}

		public function get effectVal():Number
		{
			return _effectVal;
		}

		public function get copy():IItem
		{
			return new SimpleItem(_name, _group, _type, _effectVal, _price);
		}
		
		public function get group():int
		{
			return _group;
		}
		
		public function get type():int
		{
			return _type;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function useItem():Boolean
		{
			return false;
		}
	}
}