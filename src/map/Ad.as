package map
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class Ad extends Obstacle
	{
		public function Ad(name:String, tilePos:Point, type:int)
		{
			super(name, tilePos, type);
		}
		
		override protected function initImages():void
		{
			var obstacleTex:Texture = Game.instance.assets.getTexture(objectName);
			var obstacleImg:Image = new Image(obstacleTex);
			var halfSize:Number = obstacleImg.width > obstacleImg.height ? (obstacleImg.height >> 1): (obstacleImg.width >> 1);
			obstacleImg.x = -(halfSize);
			obstacleImg.y = -(halfSize);
			this.addChild(obstacleImg);
		}
	}
}