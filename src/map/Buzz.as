package map
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class Buzz extends CollectableObject
	{	
		public function Buzz(initialPos:Point, tilePos:Point)
		{
			super(initialPos, tilePos, "InvBooze_Beer", CollectableObject.BUZZ);
			
			var buzzTex:Texture = Game.instance.assets.getTexture("InvBooze_Beer");
			var buzzImg:Image = new Image(buzzTex);
			this.addChild(buzzImg);
		}
	}
}