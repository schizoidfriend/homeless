package map
{
	import flash.geom.Point;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class Coin extends CollectableObject
	{
		private var _coin:MovieClip;
		private var _canBeCollected:Boolean = false;
		
		public function Coin(initialPos:Point, tilePos:Point)
		{
			super(new Point(initialPos.x, initialPos.y), new Point(tilePos.x, tilePos.y), "Coin", COIN);
		}
		
		override protected function onAdded(e:Event):void
		{
			super.onAdded(e);
		}
		
		override protected function onRemoved(e:Event):void
		{
			super.onRemoved(e);
		}
		
		override public function generateGraphicObject():void
		{
			_coin = ArtUtils.makeSelfCenteredMovieClip(_assetName, 10, true);
			Starling.juggler.add(_coin);
			addChild(_coin);
			playDropCoinAnimation();
		}
		
		private var dropTween:Tween;
		private var upTween:Tween ;
		private function playDropCoinAnimation():void
		{
			upTween = new Tween(_coin, 0.5);
			upTween.animate("y", -128);
			dropTween = new Tween(_coin, 0.5);
			dropTween.animate("y", 0);
			upTween.nextTween = dropTween;
			upTween.onComplete = onUpTweenCompleted;
			dropTween.onComplete = onDropCompleted;
			Starling.juggler.add(upTween);
		}
		
		private function onUpTweenCompleted():void
		{
			Starling.juggler.remove(upTween);
			Starling.juggler.add(dropTween);
		}
		
		private function onDropCompleted():void
		{
			Starling.juggler.remove(dropTween);
			_canBeCollected = true;
		}
		
		override public function collect():Boolean
		{
			if(_canBeCollected)	
			{
				return super.collect();
			}
			return false;
		}
	}
}