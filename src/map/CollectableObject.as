package map
{
	import flash.geom.Point;
	
	import inventory.InventoryItemMaker;
	import inventory.items.IItem;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class CollectableObject extends Sprite implements IRedwarable
	{
		public static const FOOD:int = 0;
		public static const BUZZ:int = 1;
		public static const WEAPON:int = 2;
		public static const CIGBUTT:int = 3;
		public static const DEAD_RAT:int = 4;
		public static const BOTTLE:int = 5;
		public static const CAN:int = 6;
		public static const COIN:int = 7;
		public static const NUM_OF_TYPES:int = 8;
		
		private static var nextID:int = 0;
		
		protected var _initialPos:Point = new Point();
		protected var _id:int;
		protected var _type:int;
		protected var _assetName:String;
		protected var _tilePos:Point;
		
		protected var _inventoryItem:IItem;
		
		protected var _isAddedToBackground:Boolean = false;
		
		public function CollectableObject(initialPos:Point, tilePos:Point, name:String, type:int)
		{
			super();
			_assetName = name;
			_type = type;
			_tilePos = tilePos;
			_initialPos = initialPos;
			generateID();
			
			_inventoryItem = InventoryItemMaker.makeItem(name);
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		public function generateGraphicObject():void
		{
			addChild(ArtUtils.makeSelfCenteredImage(_assetName));
		}
		
		public function get inventoryItem():IItem
		{
			return _inventoryItem;
		}

		protected function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		protected function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function get assetName():String
		{
			return _assetName;
		}

		public function set assetName(value:String):void
		{
			_assetName = value;
		}

		public function get tilePos():Point
		{
			return _tilePos;
		}

		public function set type(value:int):void
		{
			_type = value;
		}

		public function get type():int
		{
			return _type;
		}

		private function generateID():void
		{
			_id = nextID;
			nextID++;
		}
		
		public function get id():int
		{
			return _id;
		}

		public function get initialPos():Point
		{
			return _initialPos;
		}

		public function collect():Boolean
		{
			if(this.parent)
			{
				if(_inventoryItem && Game.instance.dude.setItemToInventory(_inventoryItem))
				{
					justKill()
					return true;
				}	
				else if(_type == CIGBUTT)
				{
					Game.instance.dude.addCigButt();
					justKill()
					return true;
				}
				else if(_type == BOTTLE)
				{
					Game.instance.dude.addBottle();
					justKill()
					return true;
				}
				else if(_type == CAN)
				{
					Game.instance.dude.addCan();
					justKill()
					return true;
				}
				else if(_type == COIN)
				{
					Game.instance.dude.earnMoney(1);
					justKill();
					return true;
				}
			}
			return false;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
	}
}