package map
{
	import dude.WastedMaterials;
	
	import flash.geom.Point;
	
	import inventory.items.IItem;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	import utils.UtilsMath;
	
	public class Container extends Sprite implements IRedwarable
	{
		//types of containers
		public static const TRASH_CAN:int = 0;
		public static const TRASH_TANK:int = 1;
		public static const TOTAL_CONTAINERS:int = 2;
		
		private var _type:uint;
		
		private static var _smellFromTrashType:Array = new Array(5, 10);
		
		private var _content:Vector.<IItem> = new Vector.<IItem>();
		private var _wasteds:WastedMaterials = new WastedMaterials();
		
		private var _closedState:Sprite;
		private var _openedState:Sprite;
		
		private var _isOpened:Boolean = false;
		
		private var _initialPos:Point = new Point();
		private var _tilePos:Point = new Point();
		
		private var _objectName:String;
		
		private var _isAddedToBackground:Boolean = false;
		
		private var _lootTF:TextField;
		
		public function Container(name:String, type:uint, tilePos:Point)
		{
			super();
			
			_type = type;
			
			_initialPos.setTo(tilePos.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE, 
						tilePos.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			_objectName = name;
			addStates(name);
			close();
			
			generateWasteds();
			
			_lootTF = new TextField(400, 40, _wasteds.createInfoString());
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function get objectName():String
		{
			return _objectName;
		}

		public function get tilePos():Point
		{
			return _tilePos;
		}

		public function get initialPos():Point
		{
			return _initialPos;
		}

		public function get isOpened():Boolean
		{
			return _isOpened;
		}

		private function addStates(name:String):void
		{
			var closedTex:Texture = Game.instance.assets.getTexture(name);
			var closedImg:Image = new Image(closedTex);
			_closedState = new Sprite();
			_closedState.addChild(closedImg);
			_closedState.x = -(_closedState.width >> 1);
			_closedState.y = -(_closedState.height >> 1);
			this.addChild(_closedState);
			
			var openedTex:Texture = Game.instance.assets.getTexture("opened"+name);
			var openedImg:Image = new Image(openedTex);
			_openedState = new Sprite();
			_openedState.addChild(openedImg);
			_openedState.x = -(_openedState.width >> 1);
			_openedState.y = -(_openedState.height >> 1);
			this.addChild(_openedState);
		}
		
		public function openAndLoot():void
		{
			setToOpened();
			
			//Game.instance.showItemsContainer();
			lootWasteds();
			addSmellFromContainer();
			
		}
		
		private function setToOpened():void
		{
			_isOpened = true;
			_closedState.visible = false;
			_openedState.visible = true;
		}
		
		public function openAndDeleteStuff():void
		{
			setToOpened();
			
			_wasteds.clear();
		}
		
		public function close():void
		{
			_isOpened = false;
			_closedState.visible = true;
			_openedState.visible = false;
			
		}
		
		private function addSmellFromContainer():void
		{
			if(_type < TOTAL_CONTAINERS)
			{
				Game.instance.dude.addSmell(_smellFromTrashType[_type]);
			}
		}
		
		private function lootWasteds():void
		{
			if(_wasteds.hasAny())
			{
				Game.instance.dude.addWastedsLoot(_wasteds);
				//TODO: animate loot over container
				showWastedLootAnim();
			
				_wasteds.clear();
			}
		}
		
		private function showWastedLootAnim():void
		{
			_lootTF.text = _wasteds.createInfoString();
			addChild(_lootTF);
			_lootTF.x = -_lootTF.width / 2;
			_lootTF.y = 0;
			var tween:Tween = new Tween(_lootTF, 2.0);
			tween.animate("y", -300);
			tween.onComplete = function():void { removeChild(_lootTF); };
			Starling.juggler.add(tween);
		}
		
		public function generateWasteds():void
		{
			switch(_type)
			{
				case TRASH_CAN:
					_wasteds.bottles = UtilsMath.randomRange(1, 4);
					_wasteds.cans = UtilsMath.randomRange(2,5);
					break;
				case TRASH_TANK:
					_wasteds.bottles = UtilsMath.randomRange(3, 8);
					_wasteds.cans = UtilsMath.randomRange(4,9);
					break;
			}
			
		}
		
		public function addContent(item:IItem):void
		{
			_content.push(item);
		}
		
		public function removeContent(pos:int):void
		{
			if(pos >=0 && pos < _content.length)
			{
				_content.splice(pos, 1);
			}
		}
		
		public function removeAllContent():void
		{
			_content.splice(0, _content.length);
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
	}
}