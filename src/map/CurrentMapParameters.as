package map
{
	public class CurrentMapParameters
	{
		private static var _sizeX:int;
		private static var _sizeY:int;
		
		private static var _totalTiles:int
		
		public function CurrentMapParameters()
		{
		}
		
		public static function get sizeY():int
		{
			return _sizeY;
		}

		public static function set sizeY(value:int):void
		{
			_sizeY = value;
		}

		public static function get sizeX():int
		{
			return _sizeX;
		}

		public static function set sizeX(value:int):void
		{
			_sizeX = value;
		}

		public static function resetTotalTiles():void
		{
			_totalTiles = _sizeX*_sizeY;
		}
	}
}