package map
{
	import flash.geom.Point;
	
	import inventory.InventoryItemMaker;
	
	public class DeadRat extends CollectableObject
	{
		public function DeadRat(initialPos:Point, tilePos:Point)
		{
			super(initialPos, tilePos, "deadRat", CollectableObject.DEAD_RAT);
			
			_inventoryItem = InventoryItemMaker.makeItem("InvDeadRat");
		}
	}
}