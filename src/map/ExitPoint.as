package map
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class ExitPoint extends Sprite implements IRedwarable
	{
		
		private var _isAddedToBackground:Boolean = false;
		private var _tilePos:Point = new Point();
		
		private var _targetMap:String;
		
		public function ExitPoint(tilePos:Point)
		{
			super();
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			var exitTex:Texture = Game.instance.assets.getTexture("Exit");
			var exitImg:Image = new Image(exitTex);
			this.addChild(exitImg);
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		public function get targetMap():String
		{
			return _targetMap;
		}

		public function set targetMap(value:String):void
		{
			_targetMap = value;
		}

		public function get tilePos():Point
		{
			return _tilePos;
		}

		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
	}
}