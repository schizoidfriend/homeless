package map
{
	import dude.Dude;
	
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	
	import gui.CustomImgProgressBar;
	import gui.ProgressBar;
	
	import map.characters.EnemyCharacter;
	import map.characters.MovingNPC;
	import map.characters.Pimp;
	import map.characters.PoliceManNPC;
	import map.characters.Prostitute;
	import map.characters.Rat;
	
	import starling.events.Event;

	public class FightController
	{
		private var _playerFightPB:CustomImgProgressBar;
		private var _dude:Dude;
		
		private var _canKick:Boolean = true;
		
		private var _lastKickStamp:int = 0;
		private var _expectedKickStamp:int = 0;
		private var _stampTimeDiff:int = 1000;
		
		public function FightController(pb:CustomImgProgressBar, dude:Dude)
		{
			_playerFightPB = pb;
			_dude = dude;
			
			_playerFightPB.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private var curTimer:int;
		private function onEnterFrame(e:Event):void
		{
			curTimer = getTimer();
		 	if(_expectedKickStamp <= curTimer)
			{
				_canKick = true;
				_playerFightPB.ratio = 1.0;
			}
			else
			{
				_playerFightPB.ratio = Number(curTimer - _lastKickStamp)/_stampTimeDiff;
			}
		}
		
		public function kick(keycode:int):void
		{
			if(_canKick)
			{
				_lastKickStamp = getTimer();
				_expectedKickStamp = _lastKickStamp+_stampTimeDiff;
				_canKick = false;
				_playerFightPB.ratio = 0.0;
				
				dudeKick(keycode);
			}
		}
		
		private function checkSeeingCrime():void
		{
			var enpcs:Vector.<EnemyCharacter> = Game.instance.background.enemyes;
			var enpc:EnemyCharacter;
			
			for each (enpc in enpcs)
			{
				if(enpc.isAddedToBackground() && enpc is PoliceManNPC)
				{
					if(enpc.canSeePlayer(Game.instance.dude.globalPosition))
					{
						Game.instance.dude.coughtOnCrime = true;
						PoliceManNPC(enpc).showPhraseOver("Стій, курва!!!");
					}
				}
			}
		}
		
		private function checkSeeingCrimeForPimp():void
		{
			var enpcs:Vector.<EnemyCharacter> = Game.instance.background.enemyes;
			var enpc:EnemyCharacter;
			
			for each (enpc in enpcs)
			{
				if(enpc.isAddedToBackground() && enpc is Pimp)
				{
					if(enpc.canSeePlayer(Game.instance.dude.globalPosition))
					{
						Game.instance.dude.didKickProstitute = true;
						Pimp(enpc).showPhraseOver("Не трогай моих девочек!!!");
					}
				}
			}
		}
		
		private function dudeKick(keycode:int):void
		{
			switch(keycode)
			{
				case Keyboard.UP:
					_dude.actionInDirection(Dude.HIT_UP);
					break;
				case Keyboard.DOWN:
					_dude.actionInDirection(Dude.HIT_DOWN);
					break;
				case Keyboard.LEFT:
					_dude.actionInDirection(Dude.HIT_LEFT);
					break;
				case Keyboard.RIGHT:
					_dude.actionInDirection(Dude.HIT_RIGHT);
					break;
			}
			
			var positions:Vector.<Point>;
			var pt:Point;
			
			var npcs:Vector.<MovingNPC> = Game.instance.background.walkingNPCs;
			var npc:MovingNPC;
			for each (npc in npcs)
			{
				if(npc.isAddedToBackground() && !(npc is Rat))
				{
					positions = npc.globalTiles;
					for each (pt in positions)
					{
						if(_dude.willHit(pt))
						{
							npc.runWithPanic();
							if(npc is Prostitute)
							{
								checkSeeingCrimeForPimp();
							}
							else
							{
								checkSeeingCrime();
							}
						}
					}
				}
			}
			
			var enpcs:Vector.<EnemyCharacter> = Game.instance.background.enemyes;
			var enpc:EnemyCharacter;
			for each (enpc in enpcs)
			{
				if(enpc.isAddedToBackground())
				{
					positions = enpc.globalTiles;
					for each (pt in positions)
					{
						if(_dude.willHit(pt))
						{
							enpc.stun();
						}
					}
				}
			}
			
			var rats:Vector.<Rat> = Game.instance.background.ptpController.currentRats;
			for each (var rat:Rat in rats)
			{
				if(rat.isAddedToBackground())
				{
					if(_dude.willHit(rat.globalPosition) && !rat.isDead())
					{
						rat.refreshPositions(CommonVars.TILE_SIDE_SIZE);
						Game.instance.background.addNewCollectableOnMap(CollectableObject.DEAD_RAT, 
																			"deadRat", 
																			rat.globalPosition,
																			rat.centerTilePos);
						Game.instance.background.ptpController.killRat(rat);
					}
				}
			}
			
			
		}
		
		public function canKick():Boolean
		{
			return _canKick;
		}
	}
}