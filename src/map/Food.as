package map
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.textures.Texture;

	public class Food extends CollectableObject
	{		
		public function Food(initialPos:Point, tilePos:Point)
		{
			super(initialPos, tilePos, "InvFood_Baton", CollectableObject.FOOD);
			
			var foodTex:Texture = Game.instance.assets.getTexture("InvFood_Baton");
			var foodImg:Image = new Image(foodTex);
			this.addChild(foodImg);
		}
	}
}