package map
{
	public interface IRedwarable
	{
		function isAddedToBackground():Boolean;
	}
}