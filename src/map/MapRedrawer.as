package map
{
	import dude.Dude;

	public class MapRedrawer
	{
		private var _background:Background;
		private var _dude:Dude;
		
		public function MapRedrawer(background:Background, dude:Dude)
		{
			_background = background;
			_dude = dude;
		}
		
		public function redrawCallForGame():void
		{
			
		}
		
		public function redrawCallForEditor():void
		{
			
		}
	}
}