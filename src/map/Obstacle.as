package map
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class Obstacle extends Sprite implements IRedwarable
	{
		//obstacle types:
		public static const SIMPLE:int = 0;
		public static const VERTICAL2:int = 1;
		public static const HORIZONTAL2:int = 2;
		
		private var _type:int;
		
		private var _initialPos:Point = new Point();
		private var _tilePos:Point = new Point();
		
		private var _objectName:String;
		
		private var _isAddedToBackground:Boolean = false;
		
		
		
		public function Obstacle(name:String, tilePos:Point, type:int)
		{
			super();
			
			_type = type;
			
			_initialPos.setTo(tilePos.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE, 
				tilePos.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			_objectName = name;
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			
			initImages();
		}
		
		public function get type():int
		{
			return _type;
		}

		protected function initImages():void
		{

			var obstacleTex:Texture = Game.instance.assets.getTexture(_objectName);
			var obstacleImg:Image = new Image(obstacleTex);
			obstacleImg.x = -(obstacleImg.width >> 1);
			obstacleImg.y = -(obstacleImg.height >> 1);
			this.addChild(obstacleImg);
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function get objectName():String
		{
			return _objectName;
		}
		
		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		public function get initialPos():Point
		{
			return _initialPos;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
	}
}