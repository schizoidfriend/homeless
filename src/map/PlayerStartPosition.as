package map
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class PlayerStartPosition extends Sprite implements IRedwarable
	{
		private var _isAddedToBackground:Boolean = false;
		private var _tilePos:Point = new Point();
		
		private var _type:int;
		
		private var _fromMapName:String;
		
		public function PlayerStartPosition(tilePos:Point, type:int = -1, fromMap:String = "")
		{
			super();
			
			_type = type;
			_fromMapName = fromMap;
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			var exitTex:Texture = Game.instance.assets.getTexture("PlayerStartPos");
			var exitImg:Image = new Image(exitTex);
			this.addChild(exitImg);
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		public function get fromMapName():String
		{
			return _fromMapName;
		}

		public function set fromMapName(value:String):void
		{
			_fromMapName = value;
		}

		public function get type():int
		{
			return _type;
		}

		public function set type(value:int):void
		{
			_type = value;
		}

		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
	}
}