package map
{
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class Puke extends Sprite implements IRedwarable
	{
		private static var nextID:int = 0;
		
		private var _initialPos:Point = new Point();
		private var _id:int;
		private var _type:int;
		private var _assetName:String;
		private var _tilePos:Point = new Point();
		
		private var _isAddedToBackground:Boolean = false;
		
		public function Puke(initialPos:Point, tilePos:Point, name:String, type:int)
		{
			super();
			
			_assetName = name;
			_type = type;
			_tilePos.setTo(tilePos.x, tilePos.y);
			_initialPos.setTo(initialPos.x, initialPos.y);
			generateID();
			
			var pukeImg:Image = ArtUtils.makeSelfCenteredImage(_assetName);
			addChild(pukeImg);
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function get assetName():String
		{
			return _assetName;
		}
		
		public function set assetName(value:String):void
		{
			_assetName = value;
		}
		
		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		public function set type(value:int):void
		{
			_type = value;
		}
		
		public function get type():int
		{
			return _type;
		}
		
		private function generateID():void
		{
			_id = nextID;
			nextID++;
		}
		
		public function get id():int
		{
			return _id;
		}
		
		public function get initialPos():Point
		{
			return _initialPos;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
	}
}