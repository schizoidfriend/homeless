package map
{
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	
	import utils.ArtUtils;
	
	public class Shit extends SmashableObject
	{
		private var _flyesMC:MovieClip;
		
		private var _flyesAppearTimer:Timer = new Timer(20000, 1);
		
		public function Shit(name:String, tilePos:Point)
		{
			super(name, tilePos);
			
			addAdditionalAnimation();
			_flyesAppearTimer.start();
			_flyesAppearTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onFliesAppearTimer);
		}
		
		override public function cleanup():void
		{
			if(_flyesAppearTimer)
			{
				_flyesAppearTimer.reset();
			}
		}
		
		private function addAdditionalAnimation():void
		{
			_flyesMC = ArtUtils.makeMovieClip("flyesAnim", 4, true);
			ArtUtils.centerMCOverItself(_flyesMC);
			_flyesMC.visible = false;
			this.addChild(_flyesMC);
		}
		
		private function onFliesAppearTimer(e:TimerEvent):void
		{
			Starling.juggler.add(_flyesMC);
			_flyesMC.visible = true;
			
			_flyesAppearTimer.stop();
			_flyesAppearTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onFliesAppearTimer);
			_flyesAppearTimer = null;
		}
		
		override public function smash():void
		{
			super.smash();
			if(_flyesMC.parent)
			{
				Starling.juggler.remove(_flyesMC);
				removeChild(_flyesMC);
			}
		}
	}
}