package map
{	
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class SleepingPlace extends Sprite implements IRedwarable
	{
		public static const HOME_PLACE:int = 0;
		public static const OUTSIDE_PLACE:int = 1;
		
		private var _isAddedToBackground:Boolean = false;
		
		private var _isOpened:Boolean = false;
		
		private var _initialPos:Point = new Point();
		private var _tilePos:Point = new Point();
		
		private var _name:String;
		private var _type:int;
		
		private var _notTouchedState:Sprite;
		private var _touchedState:Sprite;
		
		public function SleepingPlace(name:String, tilePos:Point, type:int)
		{
			super();
			
			_initialPos.setTo(tilePos.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE, 
				tilePos.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			_name = name;
			_type = type;
			
			addStates(name);
			
			setUnTouched();
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		public function get type():int
		{
			return _type;
		}

		public function get objectName():String
		{
			return _name;
		}

		public function get initialPos():Point
		{
			return _initialPos;
		}

		public function get tilePos():Point
		{
			return _tilePos;
		}

		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
		
		private function addStates(name:String):void
		{
			var notTouchedTex:Texture = Game.instance.assets.getTexture(name);
			var notTouchedImg:Image = new Image(notTouchedTex);
			_notTouchedState = new Sprite();
			_notTouchedState.addChild(notTouchedImg);
			_notTouchedState.x = -(_notTouchedState.width >> 1);
			_notTouchedState.y = -(_notTouchedState.height >> 1);
			this.addChild(_notTouchedState);
			
			var touchedTex:Texture = Game.instance.assets.getTexture("touched"+name);
			var touchedImg:Image = new Image(touchedTex);
			_touchedState = new Sprite();
			_touchedState.addChild(touchedImg);
			_touchedState.x = -(_touchedState.width >> 1);
			_touchedState.y = -(_touchedState.height >> 1);
			this.addChild(_touchedState);
		}
		
		private function setTouched():void
		{
			_notTouchedState.visible = false;
			_touchedState.visible = true;
		}
		
		private function setUnTouched():void
		{
			_notTouchedState.visible = true;
			_touchedState.visible = false;
		}
	}
}