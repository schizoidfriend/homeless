package map
{
	import flash.geom.Point;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class SmashableObject extends Sprite implements IRedwarable
	{
		private var _initialPos:Point = new Point();
		private var _tilePos:Point = new Point();
		
		private var _objectName:String;
		
		private var _isAddedToBackground:Boolean = false;
		
		private var _simpleState:Sprite;
		private var _smashedState:Sprite;
		
		private var _isSmashed:Boolean = false;
		
		public function SmashableObject(name:String, tilePos:Point)
		{
			super();
			
			_initialPos.setTo(tilePos.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE, 
				tilePos.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			_objectName = name;
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			
			addStates();
		}
		
		public function cleanup():void
		{
			
		}
		
		public function get isSmashed():Boolean
		{
			return _isSmashed;
		}

		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function get objectName():String
		{
			return _objectName;
		}
		
		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		public function get initialPos():Point
		{
			return _initialPos;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
		
		private function addStates():void
		{
			_simpleState = ArtUtils.makeSelfCenteredSprite(_objectName);
			this.addChild(_simpleState);
			_smashedState = ArtUtils.makeSelfCenteredSprite("smashed" + _objectName);
			this.addChild(_smashedState);
			
			unsmash();
			
		}
		
		public function smash():void
		{
			_simpleState.visible = false;
			_smashedState.visible = true;
			_isSmashed = true;
		}
		
		public function unsmash():void
		{
			_simpleState.visible = true;
			_smashedState.visible = false;
			_isSmashed = false;
		}
	}
}