package map
{
	import flash.net.dns.AAAARecord;
	
	import map.characters.Dog;
	import map.characters.EnemyCharacter;
	import map.characters.MovingNPC;
	import map.characters.NPC;
	import map.characters.Rat;
	
	import utils.GameUtils;

	public class SmashablesController
	{
		public function SmashablesController()
		{
		}
		
		public function checkEveryCharacterOnSmashing():void
		{
			
			var smashables:Vector.<SmashableObject> = Game.instance.background.smashable;
			
			
			var smashable:SmashableObject;
			for each (smashable in smashables)
			{
				if(!smashable.isSmashed)
				{
					checkAllMovableNPCs(smashable);
					checkAllEnemyes(smashable);
					checkDude(smashable);
					
				}
			}
		}
		
		private var npc:NPC;
		private var mnpc:MovingNPC;
		private function checkAllMovableNPCs(smashable:SmashableObject):void
		{
			var npcs:Vector.<MovingNPC> = Game.instance.background.walkingNPCs;
			for each (mnpc in npcs)
			{
				if((!(mnpc is Dog)) && (!(mnpc is Rat)) && mnpc.isInTile(smashable.tilePos.x, smashable.tilePos.y))
				{
					if(mnpc.isGlobalPointInNPCToSmash(GameUtils.tileToCenterMapPos(smashable.tilePos)))
					{
						smashable.smash();
						mnpc.showPhraseOver("ОТ ЛАЙНО!!!!");
					}
				}
			}
		}
		
		private function checkAllEnemyes(smashable:SmashableObject):void
		{
			var npcs:Vector.<EnemyCharacter> = Game.instance.background.enemyes;
			for each (mnpc in npcs)
			{
				if(mnpc.isInTile(smashable.tilePos.x, smashable.tilePos.y))
				{
					if(mnpc.isGlobalPointInNPCToSmash(GameUtils.tileToCenterMapPos(smashable.tilePos)))
					{
						smashable.smash();
						mnpc.showPhraseOver("ОТ ЛАЙНО!!!!");
						EnemyCharacter(mnpc).stopFor(1000);
					}
				}
			}
		}
		
		private function checkDude(smashable:SmashableObject):void
		{
			if(Game.instance.dude.isInTile(smashable.tilePos.x, smashable.tilePos.y))
			{
				if(Game.instance.dude.isGlobalPointInNPCToSmash(GameUtils.tileToCenterMapPos(smashable.tilePos)))
				{
					smashable.smash();
					Game.instance.dude.addSmell(10);
					Game.instance.dude.forceStopFor(1000);
					Game.instance.dude.showPhraseOver("ОТ ЛАЙНО!!!!");
				}
			}
		}
		
		
	}
}