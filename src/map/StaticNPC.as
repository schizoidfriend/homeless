package map
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	import map.characters.NPC;
	
	public class StaticNPC extends NPC
	{
		public function StaticNPC(initialTilePos:Point, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, NPC.STATIC_NPC, assetInfo);
			init();
		}
		
		override protected function initAnimations(assetInfo:NPCAssetInfo):void
		{
			addStaticAnimation(assetInfo.staticAnimName);
			
			_charStatic.scaleX = 0.9;
			_charStatic.scaleY = 0.9;
			
			forceStop();
			
			visible = true;
		}
	}
}