package map
{
	import flash.geom.Point;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class Trash extends Sprite implements IRedwarable
	{
		private var _initialPos:Point = new Point();
		private var _assetName:String;
		private var _tilePos:Point;
		
		private var _isAddedToBackground:Boolean = false;
		
		public function Trash(initialPos:Point, tilePos:Point, name:String)
		{
			super();
			
			_assetName = name;
			_tilePos = tilePos;
			_initialPos = initialPos;
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			
			generateGraphicObject();
		}
		
		private function generateGraphicObject():void
		{
			addChild(ArtUtils.makeSelfCenteredImage(_assetName));
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function get assetName():String
		{
			return _assetName;
		}
		
		public function set assetName(value:String):void
		{
			_assetName = value;
		}
		
		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		public function get initialPos():Point
		{
			return _initialPos;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this);
			}
		}
	}
}