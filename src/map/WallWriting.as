package map
{
	
	import flash.geom.Point;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class WallWriting extends Sprite implements IRedwarable
	{
		private var _isAddedToBackground:Boolean = false;
		
		private var _tilePos:Point = new Point();
		
		private var _initialPos:Point = new Point();
		
		private var _objectName:String;
		
		public function WallWriting(name:String, tilePos:Point)
		{
			super();
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			_initialPos.setTo(tilePos.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE, 
				tilePos.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
			
			_objectName = name;
			
			addChild(ArtUtils.makeSelfCenteredImage(_objectName));
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		public function get initialPos():Point
		{
			return _initialPos;
		}

		public function get objectName():String
		{
			return _objectName;
		}
		
		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
	}
}