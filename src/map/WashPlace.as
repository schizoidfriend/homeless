package map
{
	import flash.geom.Point;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class WashPlace extends Sprite implements IRedwarable
	{
		private var _initialPos:Point = new Point();
		private var _tilePos:Point = new Point();
		
		private var _objectName:String;
		
		private var _isAddedToBackground:Boolean = false;
		
		private var _waterAnim:MovieClip;
		
		public function WashPlace(name:String, tilePos:Point)
		{
			super();
			
			_objectName = name;
			
			_initialPos.setTo(tilePos.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE, 
				tilePos.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
			
			_tilePos.setTo(tilePos.x, tilePos.y);
			
			addAnimation();
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		public function get objectName():String
		{
			return _objectName;
		}
		
		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		public function get initialPos():Point
		{
			return _initialPos;
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		private function addAnimation():void
		{
			_waterAnim = ArtUtils.makeMovieClip(_objectName, 12, true);
			ArtUtils.centerMCOverItself(_waterAnim);
			addChild(_waterAnim);
			
			Starling.juggler.add(_waterAnim)
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}
		
		public function restartAnimation():void
		{
			_waterAnim.currentFrame = 0;
			_waterAnim.play();
		}
	}
}