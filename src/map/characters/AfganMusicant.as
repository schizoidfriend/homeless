package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	
	import starling.display.MovieClip;
	
	public class AfganMusicant extends MovingNPC
	{
		private var _playAnim:MovieClip;
		
		public function AfganMusicant(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
			
			setupPlayAnim(assetInfo.extraAnim);
			
			sitAndPlay();
		}
		
		private function setupPlayAnim(atlasName:String):void
		{
			if(_playAnim && _playAnim.parent)
			{
				this.removeChild(_playAnim, true);
				_playAnim = null;
			}
			_playAnim = getAnimation(atlasName, 4);
			
			_playAnim.visible = false;
		}
		
		private function sitAndPlay():void
		{
			setNewCurrentAnimation(_playAnim);
			
			setState(STATIC_STATE);
		}
	}
}