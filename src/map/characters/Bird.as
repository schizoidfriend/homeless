package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.net.drm.VoucherAccessInfo;
	import flash.utils.Timer;
	
	import map.CurrentMapParameters;
	import map.globalMap.GlobalMap;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	
	import utils.ArtUtils;
	import utils.GameUtils;
	import utils.UtilsMath;
	
	public class Bird extends MovingNPC
	{
		protected static const MOVE_TO_TILE_POS:int = 2;	
		protected static const FLY_AWAY:int = 3;
		
		private var _staticTimer:Timer = new Timer(1500);
		
		private var _ptMovingTo:Point;
		
		private var _flyAnimation:MovieClip;
		
		private var _isDead:Boolean = false;
		
		public function Bird(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
			
			_staticTimer.addEventListener(TimerEvent.TIMER, onTimer);
			
			_ptMovingTo = GameUtils.tileToCenterMapPos(initialTilePos);
			
			addFlyAnimation(assetInfo.extraAnim);
			
			makeNextDecision();
		}
		
		override public function cleanup():void
		{
			super.cleanup();
			_staticTimer.reset();
		}
		
		public function get isDead():Boolean
		{
			return _isDead;
		}

		public function isFlying():Boolean
		{
			return _flyAnimation.visible;
		}
		
		
		public function addFlyAnimation(atlasName:String):void
		{
			if(_flyAnimation && _flyAnimation.parent)
			{
				this.removeChild(_flyAnimation, true);
				_flyAnimation = null;
			}
			_flyAnimation = getAnimation(atlasName, 4);
			
			_flyAnimation.visible = false;
		}
		
		public function flyAway():void
		{
			_staticTimer.reset();
			
			setNewCurrentAnimation(_flyAnimation);
			
			setState(FLY_AWAY);
			
			_ptMovingTo = getRandomPointOutsideMap();
		}
		
		private function getRandomPointOutsideMap():Point
		{
			var isXOutside:Boolean = UtilsMath.getRandomWithChance(50);
			var isOutsideMinus:Boolean = UtilsMath.getRandomWithChance(50);
			
			var xOut:int; var yOut:int;
			
			if(isXOutside)
			{
				yOut = isOutsideMinus ? -CommonVars.TILE_SIDE_SIZE : (CurrentMapParameters.sizeY+1)*CommonVars.TILE_SIDE_SIZE;
				xOut = UtilsMath.randomRange(0, CurrentMapParameters.sizeX*CommonVars.TILE_SIDE_SIZE);
			}
			else
			{
				xOut = isOutsideMinus ? -CommonVars.TILE_SIDE_SIZE : (CurrentMapParameters.sizeX+1)*CommonVars.TILE_SIDE_SIZE;
				yOut = UtilsMath.randomRange(0, CurrentMapParameters.sizeY*CommonVars.TILE_SIDE_SIZE);
			}
			
			return new Point(xOut, yOut);
		}
		
		private function makeNextDecision():void
		{
			if(UtilsMath.getRandomWithChance(20))
			{
				var newPT:Point = findNearEmptyTile(tilePos);
				if(newPT)
				{
					makeRandPtIntTile(newPT);	
				}
				else
				{
					makeRandPtIntTile(tilePos);	
				}
			}
			else
			{
				makeRandPtIntTile(tilePos);				
			}
		}
		
		private function makeRandPtIntTile(tile:Point):void
		{
			//move to random point in this tile
			var newXPos:uint = UtilsMath.randomRange(10,54);
			var newYPos:uint = UtilsMath.randomRange(10,54);
			
			var oldPt:Point = new Point(_ptMovingTo.x, _ptMovingTo.y);
			
			_ptMovingTo = GameUtils.tileToCenterMapPos(tile);
			_ptMovingTo.setTo(_ptMovingTo.x - 27 + newXPos, _ptMovingTo.y - 27 + newYPos);
			setState(MOVE_TO_TILE_POS);
			changeDirrection(oldPt, _ptMovingTo);
		}
		
		private function findNearEmptyTile(curTile:Point):Point
		{
			var emptyTiles:Array = new Array();
			
			if(Game.instance.background.isWalkable(new Point(curTile.x+1, curTile.y))) emptyTiles.push(new Point(curTile.x+1, curTile.y));
			if(Game.instance.background.isWalkable(new Point(curTile.x-1, curTile.y))) emptyTiles.push(new Point(curTile.x-1, curTile.y));
			if(Game.instance.background.isWalkable(new Point(curTile.x, curTile.y+1))) emptyTiles.push(new Point(curTile.x, curTile.y+1));
			if(Game.instance.background.isWalkable(new Point(curTile.x, curTile.y-1))) emptyTiles.push(new Point(curTile.x, curTile.y-1));
			
			if(emptyTiles.length)
			{
				return emptyTiles[UtilsMath.randomRange(0, emptyTiles.length-1)] as Point;
			}
			return null;
		}
		
		private function changeDirrection(lastPt:Point, nextPt:Point):void
		{
			if(lastPt.x < nextPt.x)
			{
				setMovementDirection(true);
			}
			else if(lastPt.x > nextPt.x)
			{
				setMovementDirection(false);
			}
		}
		
		private function setMovementDirection(isLeft:Boolean):void
		{
			if(isLeft)
			{
				_mainSprite.scaleX = -1.0;
			}
			else
			{
				_mainSprite.scaleX = 1.0;
			}
		}
		
		private function stayHere():void
		{
			setState(STATIC_STATE);
			_staticTimer.start();
		}
		
		private function onTimer(e:TimerEvent):void
		{
			_staticTimer.reset();
			makeNextDecision();
		}
		
		protected function movementTilePosTick():void
		{
			if(moveToPoint(this, _ptMovingTo, 1))
			{
				stayHere();
			}
		}
		
		private var _target:Point = new Point();
		protected function moveToPointNoObstacles(obj:Object, target:Point, speed:Number = 1):Boolean
		{
			_target.setTo(target.x, target.y);
			// get the difference between obj and target points.
			var diff:Point = _target.subtract(new Point(_globalPosition.x, _globalPosition.y)); 
			var dist:int = diff.length;
			
			if (dist <= speed)  // if we will go past when we move just put it in place.
			{
				_globalPosition.x = _target.x;
				_globalPosition.y = _target.y;
				return true;
			}
			else // If we are not there yet. Keep moving.
			{ 
				diff.normalize(1);
				_globalPosition.setTo(_globalPosition.x+(diff.x * speed), _globalPosition.y+(diff.y * speed));
			}
			return false;
		}
		
		protected function movementFlyTick():void
		{
			if(moveToPointNoObstacles(this, _ptMovingTo, 10))
			{
				kill();
				_isDead = true;
			}
		}
		
		override public function tick():void
		{
			switch(_currentState)
			{
				case STATIC_STATE:
					break;
				
				case MOVING_TO_WAYPOINT_STATE:
					movementWaytopintTick();
					break;
				case MOVE_TO_TILE_POS:
					movementTilePosTick();
					break;
				case FLY_AWAY:
					movementFlyTick();
					break;
			}
			
		}
	}
}