package map.characters
{
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.ArtUtils;
	import utils.TLFSprite;

	//Just an informational class useful for dude and npc-s
	public class Character extends Sprite
	{
		protected var _globalPosition:Point = new Point();
		
		protected var _mainSprite:Sprite = new Sprite();
		
		protected var _halfWidth:Number;
		protected var _halfHeight:Number;
		
		protected var _charPositions:Vector.<Point> = new Vector.<Point>();
		protected var _charGlobalPositions:Vector.<Point> = new Vector.<Point>();
		
		//position of the center in tiles
		protected var _centerTilePos:Point = new Point(0, 0);
		
		protected var _dialogWindow:Sprite = new Sprite();
		protected var _dialogTF:TLFSprite;
		protected var _phraseTimer:Timer = new Timer(3000, 1);
		
		public function Character()
		{
			super.addChild(_mainSprite);
		}
		
		public function cleanup():void
		{
			_phraseTimer.reset();
		}
		
		override public function addChild(child:DisplayObject):DisplayObject
		{
			_mainSprite.addChild(child);
			return child;
		}
		
		override public function removeChild(child:DisplayObject, dispose:Boolean=false):DisplayObject
		{
			_mainSprite.removeChild(child, dispose);
			return child;
		}
		
		override public function get width():Number { return _mainSprite.width }
		
		override public function get height():Number { return _mainSprite.height }
		
		protected function init():void
		{
			_halfWidth = (width >> 1);
			_halfHeight = (height >> 1);
			initDialogWindow();
		}
		
		protected function initDialogWindow():void
		{
			_phraseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onPhraseTimerCompleted);
			var dialogShape:Shape = new Shape();
			dialogShape.graphics.beginFill(0xFFFFFF, 0.8);
			dialogShape.graphics.drawRoundRect(0, 0, 200, 100, 20, 20);
			dialogShape.graphics.endFill();
			
			var dialogBitmapData:BitmapData = new BitmapData(200, 100, true, 0x0);
			dialogBitmapData.draw(dialogShape);
			var dialogTexture:Texture = Texture.fromBitmapData(dialogBitmapData, false, false);
			
			var dialogImg:Image = new Image(dialogTexture);
			_dialogWindow.addChild(dialogImg);
			
			_dialogTF = ArtUtils.makeDialogTF("test", 160, 80, 20, 20, 16, 0x0, VAlign.CENTER, HAlign.CENTER); 
			_dialogWindow.addChild(_dialogTF);
		}
		
		protected function onPhraseTimerCompleted(e:TimerEvent):void
		{
			_phraseTimer.reset();
			_phraseTimer.stop();
			hidePhraseOver();
		}
		
		public function showPhraseOver(text:String):void
		{
			_dialogWindow.y = -height*2;
			_dialogTF.text = text;
			_dialogTF.flatten();
			super.addChild(_dialogWindow);
			if(_phraseTimer.running)
			{
				_phraseTimer.reset();
			}
			_phraseTimer.start();
		}
		
		protected function hidePhraseOver():void
		{
			super.removeChild(_dialogWindow);
			if(_phraseTimer.running)
			{
				_phraseTimer.reset();
			}
		}
		
		public function get centerTilePos():Point
		{
			return _centerTilePos;
		}
		
		public function get globalPosition():Point
		{
			return _globalPosition;
		}

		public function set globalPosition(value:Point):void
		{
			_globalPosition = value;
		}
		
		public function setGlobalPos(x:Number, y:Number):void
		{
			_globalPosition.setTo(x,y);
		}
		
		public function get halfWidth():Number
		{
			return _halfWidth;
		}
		
		public function get halfHeight():Number
		{
			return _halfHeight;
		}
		
		public function addOffset(xOffset:Number, yOffset:Number):void
		{
			var oldPos:Point = new Point(_globalPosition.x, _globalPosition.y);
			_globalPosition.setTo(_globalPosition.x + xOffset, _globalPosition.y + yOffset);
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			var canMoveX:Boolean = true;
			var canMoveY:Boolean = true;
			var possibleBacking:Point = new Point();
			var points:Vector.<Point> = tiles.slice();
			for(var i:int = 0; i < points.length; i++)
			{
				var pt:Point = points[i];
				if(!Game.instance.background.isWalkable(pt))
				{
					_globalPosition.setTo(oldPos.x + xOffset, oldPos.y);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					if(isInTile(pt.x,pt.y))
					{
						canMoveX = false;
					}
					_globalPosition.setTo(oldPos.x, oldPos.y + yOffset);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					if(isInTile(pt.x,pt.y))
					{
						canMoveY = false;
					}
					
					_globalPosition.setTo(oldPos.x, oldPos.y);
				}
			}
			
			if(!canMoveX || !canMoveY)
			{
				_globalPosition.setTo(oldPos.x, oldPos.y);
				if(canMoveX)
				{
					globalPosition.setTo(oldPos.x + xOffset, oldPos.y);
				}
				if(canMoveY)
				{
					globalPosition.setTo(oldPos.x, oldPos.y + yOffset);
				}
				
			}
		}
		
		private const SIZE_OFFSET:int = 8;
		public function refreshPositions(tileSize:int):void
		{
			_charPositions.splice(0, _charPositions.length);
			_charGlobalPositions.splice(0, _charGlobalPositions.length);
			
			var halfWidth:int = this.width >> 1;
			var halfHeight:int = this.height >> 1;
			var posTL:Point = getTileFromPos(tileSize, _globalPosition.x - halfWidth + SIZE_OFFSET, _globalPosition.y - halfHeight + SIZE_OFFSET);
			var posTR:Point = getTileFromPos(tileSize, _globalPosition.x + halfWidth - SIZE_OFFSET, _globalPosition.y - halfHeight + SIZE_OFFSET);
			var posBL:Point = getTileFromPos(tileSize, _globalPosition.x - halfWidth + SIZE_OFFSET, _globalPosition.y + halfHeight - SIZE_OFFSET);
			var posBR:Point = getTileFromPos(tileSize, _globalPosition.x + halfWidth - SIZE_OFFSET, _globalPosition.y + halfHeight - SIZE_OFFSET);
			
			var globalPosTL:Point = new Point(_globalPosition.x - halfWidth + SIZE_OFFSET, _globalPosition.y - halfHeight + SIZE_OFFSET);
			var globalPosTR:Point = new Point(_globalPosition.x + halfWidth - SIZE_OFFSET, _globalPosition.y - halfHeight + SIZE_OFFSET);
			var globalPosBL:Point = new Point(_globalPosition.x - halfWidth + SIZE_OFFSET, _globalPosition.y + halfHeight - SIZE_OFFSET);
			var globalPosBR:Point = new Point(_globalPosition.x + halfWidth - SIZE_OFFSET, _globalPosition.y + halfHeight - SIZE_OFFSET);
			_charGlobalPositions.push(globalPosTL, globalPosTR, globalPosBL, globalPosBR);
			
			_centerTilePos = getTileFromPos(tileSize, _globalPosition.x, _globalPosition.y);
			
			_charPositions.push(posTL, posTR, posBL, posBR);
		}
		
		private function getTileFromPos(tileSize:int, x:Number, y:Number):Point
		{
			return new Point(int(x/tileSize), int(y/tileSize));
		}
		
		public function get tiles():Vector.<Point>
		{
			return _charPositions;
		}
		
		public function get globalTiles():Vector.<Point>
		{
			return _charGlobalPositions;
		}
		
		public function isInTile(x:int, y:int):Boolean
		{
			if(Math.abs(_centerTilePos.x - x) > 2 || Math.abs(_centerTilePos.y - y) > 2)
			{
				return false;
			}
			for(var i:int = 0; i < _charPositions.length; i++)
			{
				if( (_charPositions[i] as Point).equals(new Point(x, y)) )
				{
					return true;
				}
			}
			return false;
		}

	}
}