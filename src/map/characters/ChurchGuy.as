package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	
	public class ChurchGuy extends ShopToShopNPC
	{
		public function ChurchGuy(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo, shopToPos:Point, isSmoking:Boolean=false)
		{
			_movementSpeed = 12.0;
			super(initialTilePos, type, assetInfo, shopToPos, isSmoking);
			
			_isSmoking = false;
		}
	}
}