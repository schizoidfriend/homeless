package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import utils.AStarLib;
	import utils.UtilsMath;
	
	public class Dog extends MovingNPC
	{		
		private static const MOVEMENT_SPEED:Number = 9.0;
		
		private var _staticTimer:Timer = new Timer(1500);
		
		public function Dog(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
			
			_staticTimer.addEventListener(TimerEvent.TIMER, onTimer);
			
			init();
			findAndMoveToNextPoint();
			
			
		}
		
		override public function cleanup():void
		{
			super.cleanup();
			_staticTimer.reset();
		}
		
		private function setMovementDirection(isLeft:Boolean):void
		{
			if(isLeft)
			{
				_mainSprite.scaleX = -1.0;
			}
			else
			{
				_mainSprite.scaleX = 1.0;
			}
		}
		
		private function onTimer(e:TimerEvent):void
		{
			_staticTimer.reset();
			if(UtilsMath.getRandomWithChance(20))
			{
				deficateHere();
			}
			findAndMoveToNextPoint();
		}
		
		private function findAndMoveToNextPoint():void
		{
			walk();
			UtilsMath.astar.InitializePathfinder();
			
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			_currentPoint = new Point(_centerTilePos.x, _centerTilePos.y);
			
			var pathFound:int;
			var pt:Point;
			while(pathFound != AStarLib.FOUND)
			{
				pt = Game.instance.background.getRandomFreePoint();
				pathFound = UtilsMath.astar.FindPath(0, _currentPoint.x, _currentPoint.y, pt.x, pt.y);
			}
			
			_nextPathPointIndex = 0;
			
			_currentPath.splice(0, _currentPath.length);
			
			_nextPoint = new Point();
			_nextPoint.setTo(pt.x, pt.y);
			
			setState(MOVING_TO_WAYPOINT_STATE);
			setPath(UtilsMath.astar.pathBank[0]);
				
			changeNextPoint();
			
		}
		
		private function changeDirrection(lastPt:Point, nextPt:Point):void
		{
			if(lastPt.x < nextPt.x)
			{
				setMovementDirection(false);
			}
			else if(lastPt.x > nextPt.x)
			{
				setMovementDirection(true);
			}
		}
		
		override protected function changeNextPoint():void
		{	
			if(_currentPath.length == 0)
			{
				return ;
			}
			_lastPathPoint = _currentPath[_nextPathPointIndex];
			_nextPathPointIndex++;
			if(_nextPathPointIndex < _currentPath.length)
			{			
				_nextPathPoint = _currentPath[_nextPathPointIndex];
				changeDirrection(_lastPathPoint, _nextPathPoint);
			}
			else
			{
				justStopHere();
			}
		}
		
		override protected function movementWaytopintTick():void
		{
			var moveTo:Point = new Point(_nextPathPoint.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				_nextPathPoint.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, MOVEMENT_SPEED))
			{
				//justStopHere();
				changeNextPoint();
			}
		}
		
		private function deficateHere():void
		{
			Game.instance.background.addNewSmashable(_centerTilePos, "shit");
			showPhraseOver("Гав Гав!!!!");
		}
		
		private function justStopHere():void
		{
			_staticTimer.start();
			setState(STATIC_STATE);
		}
		
		override public function showPhraseOver(text:String):void
		{
			super.showPhraseOver(text);
			_dialogWindow.y = -height*4;
		}
	}
}