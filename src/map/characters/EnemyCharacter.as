package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import starling.animation.Juggler;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.filters.ColorMatrixFilter;
	import starling.textures.Texture;
	
	import utils.ArtUtils;
	import utils.UtilsMath;
	
	public class EnemyCharacter extends MovingNPC
	{
		protected static const STUNNED_STATE:int = 2;
		
		
		//if got into obstacle - position to move from it
		protected var _awayFromObstaclePoint:Point = new Point();
		
		protected var _canMoveX:Boolean = false;
		protected var _canMoveY:Boolean = false;
		protected var _neededXOffset:Number;
		protected var _neededYOffset:Number;
		
		protected var _playerCurrentPos:Point = new Point();
		
		protected var _prevState:int = -1;
		
		protected var _curObstaclePoint:Point = new Point();
		
		private var _stunnedTimer:Timer = new Timer(1500, 1);
		
		private var _stopTimer:Timer;
		
		//will be common for all enemy characters
		protected var _stunAnim:MovieClip;
		
		protected var _isStunned:Boolean = false;
		
		public function EnemyCharacter(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
			
			_stunnedTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onStunnedTimerCompleted);
		}
		
		override public function cleanup():void
		{
			super.cleanup();
			_stunnedTimer.reset();
			if(_stopTimer)
			{
				_stopTimer.reset();
			}
		}
		
		override protected function init():void
		{
			super.init();
			
			_stunAnim = ArtUtils.makeSelfCenteredMovieClip("HeadBeaten", 15, true);
			_stunAnim.visible = false;
			addChild(_stunAnim);
			
		}
		
		protected function stunnedState():void
		{
			if(!_stunnedTimer.running)
			{
				_stunnedTimer.start();
				this.stop();
			}
		}
		
		public function stopFor(time:int):void
		{
			if(_stopTimer && _stopTimer.running)
			{
				_stopTimer.reset();
				_stopTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onStopTimer);
				_stopTimer = null;
			}
			_stopTimer = new Timer(time, 1);
			_stopTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onStopTimer);
			_stopTimer.start();
			setState(STUNNED_STATE);
		}
		
		protected function onStopTimer(e:TimerEvent):void
		{
			_stopTimer.reset();
			_stopTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onStopTimer);
			_stopTimer = null;
		}
		
		protected function addBloodAnim():void
		{
			var blood:MovieClip = ArtUtils.makeMovieClip("Fight_Blood", 15);
			ArtUtils.centerMCOverItself(blood);
			this.addChild(blood);
			blood.play();
			Starling.juggler.add(blood);
			blood.addEventListener(Event.COMPLETE, onBloodAnimFinished);
		}
		
		protected function addStunAnim():void
		{
			_stunAnim.visible = true;
			Starling.juggler.add(_stunAnim);
		}
		
		protected function removeStunAnim():void
		{
			_stunAnim.visible = false;
			Starling.juggler.remove(_stunAnim);
		}
		
		protected function onBloodAnimFinished(e:Event):void
		{
			this.removeChild(e.currentTarget as MovieClip, true);
		}
		
		protected function onStunnedTimerCompleted(e:TimerEvent):void
		{
			//setState(MOVING_TO_LAST_SEEN_POINT);
			_isStunned = false;
			removeStunAnim();
		}
		
		public function stun():void
		{
			if(_currentState == STUNNED_STATE)
			{
				_stunnedTimer.reset();
				_stunnedTimer.start();
			}
			else
			{
				stop();
				setState(STUNNED_STATE);
				
			}
			_isStunned = true;
			/*if(this.filter != null)
			{
				(this.filter as ColorMatrixFilter).adjustBrightness(1);
			}
			else
			{
				var cf:ColorMatrixFilter = new ColorMatrixFilter();
				cf.adjustBrightness(1);
				this.filter = cf;
			}
			var tex:Texture;
			tex.
			*/	
			addBloodAnim();
			addStunAnim();
		}
		
		protected function isCurrentPositionGood():Boolean
		{
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			
			var possibleBacking:Point = new Point();
			var points:Vector.<Point> = tiles.slice();
			
			for(var i:int = 0; i < points.length; i++)
			{
				var pt:Point = points[i];
				if(!Game.instance.background.isWalkable(pt))
				{
					return false;
				}
			}
			return true;
		}
		
		protected function moveSlowly(xOffset:int, yOffset:int):void
		{
			var i:int = 0;
			var xStep:int = 0;
			var xMax:int = xOffset;
			if(xOffset > 0) xStep = 1;
			else if(xOffset < 0) { xStep = -1; xMax = -xMax; }
			for(i = 0; i < xMax; i++)
			{
				_globalPosition.setTo(_globalPosition.x + xStep, _globalPosition.y);
				if(!isCurrentPositionGood())
				{
					_globalPosition.setTo(_globalPosition.x - xStep, _globalPosition.y);
					return;
				}
			}
			
			var yStep:int = 0;
			var yMax:int = yOffset;
			if(yOffset > 0) yStep = 1;
			else if(yOffset < 0) { yStep = -1; yMax = -yMax; }
			for(i = 0; i < yOffset; i++)
			{
				_globalPosition.setTo(_globalPosition.x, _globalPosition.y + yStep);
				if(!isCurrentPositionGood())
				{
					_globalPosition.setTo(_globalPosition.x, _globalPosition.y - yStep);
					return;
				}
			}
		}
		
		protected function findingNearestAwayFromObstacleTile(canMoveX:Boolean, canMoveY:Boolean, xOffset:Number, yOffset:Number):Point
		{
			var nearestTile:Point = new Point(Math.round(_globalPosition.x / CommonVars.TILE_SIDE_SIZE), 
				Math.round(_globalPosition.y / CommonVars.TILE_SIDE_SIZE));
			if(!canMoveX)
			{
				nearestTile.y += yOffset > 0 ? 1 : -1
			}
			
			if(!canMoveY)
			{
				nearestTile.x += xOffset > 0 ? 1 : -1
			}
			
			return nearestTile;
		}
		
		protected function addSpecialOffset():void
		{
			var oldPos:Point = new Point(_globalPosition.x, _globalPosition.y);
			if(_canMoveX)
			{
				_globalPosition.setTo(oldPos.x, oldPos.y + _neededYOffset);
			}
			if(_canMoveY)
			{
				_globalPosition.setTo(oldPos.x + _neededXOffset, oldPos.y);
			}
		}
		
		protected function canMoveXAndY():Boolean
		{
			var oldPos:Point = new Point(_globalPosition.x, _globalPosition.y);
			if(_canMoveX)
			{
				_globalPosition.setTo(oldPos.x + _neededXOffset, oldPos.y);
			}
			if(_canMoveY)
			{
				_globalPosition.setTo(oldPos.x, oldPos.y + _neededYOffset);
			}
			
			if(!isCurrentPositionGood())
			{
				_globalPosition.setTo(oldPos.x, oldPos.y);
				if(_canMoveX)
				{
					moveSlowly(_neededXOffset, 0);
				}
				if(_canMoveY)
				{
					moveSlowly(0, _neededYOffset);
				}
				
				refreshPositions(CommonVars.TILE_SIDE_SIZE);
				points = tiles.slice();
				for(i = 0; i < points.length; i++)
				{
					var pt:Point = points[i];
					if(!Game.instance.background.isWalkable(points[i] as Point))
					{
						return false;
					}
				}
				
			}
			else
			{
				oldPos.setTo(_globalPosition.x, _globalPosition.y);
				
				var points:Vector.<Point>;
				var i:int;
				if(_canMoveX)
				{
					_globalPosition.setTo(oldPos.x, oldPos.y + _neededYOffset);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					points = tiles.slice();
					for(i = 0; i < points.length; i++)
					{
						var pt:Point = points[i];
						if(!Game.instance.background.isWalkable(points[i] as Point))
						{
							_globalPosition.setTo(oldPos.x, oldPos.y);
							return false;
						}
					}
				}
				if(_canMoveY)
				{
					_globalPosition.setTo(oldPos.x + _neededXOffset, oldPos.y);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					points = tiles.slice();
					for(i = 0; i < points.length; i++)
					{
						if(!Game.instance.background.isWalkable(points[i] as Point))
						{
							_globalPosition.setTo(oldPos.x, oldPos.y);
							return false;
						}
					}
				}
			}
			
			
			return true;
		}
		
		protected function toNearestIntPoint(num:Number):int
		{
			return (num - int(num)) > .5 ? int(num)+1 : int(num);
		}
		
		public function canSeePlayer(playerPos:Point):Boolean
		{
			var diff:Point = playerPos.subtract(new Point(_globalPosition.x, _globalPosition.y)); 
			var pts:Vector.<Point> = Game.instance.dude.globalTiles;
			var viewDist:Number = Game.instance.dude.seenDistance;
			for each (var pt:Point in pts)
			{
				if(UtilsMath.rayTracing.canSee(pt.x, pt.y, _globalPosition.x, _globalPosition.y) && diff.length < viewDist)
				{
					_playerCurrentPos.setTo(playerPos.x, playerPos.y);
					return true;
				}
			}
			
			return false;
		}
		
		override protected function setState(state:int):void
		{
			if(_isStunned)
			{
				return ;
			}
			_prevState = _currentState;
			
			_currentState = state;
			if(state == STATIC_STATE || state == STUNNED_STATE)
			{
				stop();
			}
			else
			{
				walk();
			}
		}
		
		public function setToBasicStateOFMind():void
		{
			
		}
	}
}