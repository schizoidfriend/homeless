package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	
	import utils.UtilsMath;
	
	public class Missioner extends Prostitute
	{
		public function Missioner(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
		}
		
		override protected function showMessages():void
		{
			if(UtilsMath.getRandomWithChance(50))
			{
				showPhraseOver("Увіруй у бозю и будєт тобі шастя!");
			}
			else
			{
				showPhraseOver("Покайтесь, а то пекло вас жде!");
			}
		}
		
		override protected function showPanicPhrase():void
		{
			showPhraseOver("Бозя тєбя покараєт!");
		}
		
		override protected function getRunWithPanicPos():Point
		{
			return Game.instance.background.getNearestChurchPos(this.globalPosition);
		}
	}
}