package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	import utils.AStarLib;
	import utils.ArtUtils;
	import utils.UtilsMath;
	
	public class MovingNPC extends NPC
	{
		protected static const STATIC_STATE:int = 0;
		protected static const MOVING_TO_WAYPOINT_STATE:int = 1;
		
		protected static const BASIC_MOVEMENT_SPEED:Number = 6.0;
		
		protected var _movementSpeed:Number = BASIC_MOVEMENT_SPEED;
		
		//images of waypoints
		protected var _waypointsImages:Vector.<Sprite> = new Vector.<Sprite>();
		
		//current state
		protected var _currentState:int = STATIC_STATE;
		
		//current path based on nextPoint
		protected var _currentPath:Vector.<Point> = new Vector.<Point>();
		
		//current point cop moving to
		protected var _currentPoint:Point;
		
		//next point to move to
		protected var _nextPoint:Point;
		//next point inedex in points array
		protected var _nextPointIndex:int = 1;
		
		//prev path point
		protected var _lastPathPoint:Point = new Point();
		//next path point
		protected var _nextPathPoint:Point = new Point();
		//next path point index in array
		protected var _nextPathPointIndex:int = 0;
		
		//waypoint of com movemen
		protected var _waypoints:Vector.<Point> = new Vector.<Point>();
		
		public function MovingNPC(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
			init();
		}
		
		override public function cleanup():void
		{
		}
		
		//return isReached 
		private var _target:Point = new Point();
		protected var _lastDiffBeforeReach:Point;

		public function set movementSpeed(value:Number):void
		{
			_movementSpeed = value;
		}

		protected function moveToPoint(obj:Object, target:Point, speed:Number = 1):Boolean
		{
			_target.setTo(target.x, target.y);
			// get the difference between obj and target points.
			var diff:Point = _target.subtract(new Point(_globalPosition.x, _globalPosition.y)); 
			var dist:int = diff.length;
			
			if (dist <= speed)  // if we will go past when we move just put it in place.
			{
				_globalPosition.x = _target.x;
				_globalPosition.y = _target.y;
				return true;
			}
			else // If we are not there yet. Keep moving.
			{ 
				diff.normalize(1);
				_lastDiffBeforeReach = new Point(diff.x, diff.y);
				addOffset(diff.x * speed, diff.y * speed);
				
				/*if (objRot) // If we want to rotate with our movement direction.
				{ 
				obj.rotation = Math.atan2(diff.y, diff.x) * (180 / Math.PI) + 90;
				}*/
			}
			return false;
		}
		
		override public function setToInitialPos():void
		{
			if(_waypoints && _waypoints.length > 0)
			{
				_currentPoint = _waypoints[0] as Point;
			}
			
			super.setToInitialPos();
			
			setState(STATIC_STATE);
		}
		
		public function get waypoints():Vector.<Point>
		{
			return _waypoints;
		}
		
		public function get nextPoint():Point
		{
			return _nextPoint;
		}
		
		public function get currentPoint():Point
		{
			return _currentPoint;
		}
		
		public function startMovement():void
		{
			if(_waypoints.length < 2)
			{
				stop();
				return ;
			}
							
			walk();
			UtilsMath.astar.InitializePathfinder();
			
			var pathFound:int = UtilsMath.astar.FindPath(0, _currentPoint.x, _currentPoint.y, _nextPoint.x, _nextPoint.y);
			_currentPath.splice(0, _currentPath.length);
			if ( pathFound == AStarLib.FOUND ) //path found
			{
				setState(MOVING_TO_WAYPOINT_STATE);
				setPath(UtilsMath.astar.pathBank[0]);
				
				changeNextPoint();
			}
			
		}
		
		protected function setState(state:int):void
		{
			//trace("state - "+_currentState);
			_currentState = state;
			if(state == STATIC_STATE)
			{
				stop();
			}
			else
			{
				walk();
			}
		}
		
		protected function setPath(path:Array):void
		{
			_currentPath.push(_currentPoint);
			for(var i:int = 0; i < path.length; i+=2)
			{
				_currentPath.push(new Point(path[i], path[i+1]));
			}
			_currentPath.push(_nextPoint);
			if(_nextPoint == null)
			{
				return ;
			}
		}
		
		protected function changeNextPoint():void
		{	
			if(_currentPath.length == 0)
			{
				return ;
			}
			_lastPathPoint = _currentPath[_nextPathPointIndex];
			_nextPathPointIndex++;
			if(_nextPathPointIndex < _currentPath.length)
			{			
				_nextPathPoint = _currentPath[_nextPathPointIndex];
			}
			else
			{
				_nextPathPointIndex = 0;
				
				_currentPoint = _nextPoint;
				_nextPointIndex++;
				if(_nextPointIndex >= _waypoints.length)
				{
					_nextPointIndex = 0;
				}
				_nextPoint = _waypoints[_nextPointIndex];
				//startMovement();
				setState(STATIC_STATE);
			}			
		}
		
		public function tick():void
		{
			switch(_currentState)
			{
				case STATIC_STATE:
					break;
				
				case MOVING_TO_WAYPOINT_STATE:
					movementWaytopintTick();
					break;
			}
				
		}
		
		protected function movementWaytopintTick():void
		{
			var moveTo:Point = new Point(_nextPathPoint.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				_nextPathPoint.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, _movementSpeed))
			{
				changeNextPoint();
			}
		}
		
		public function addWaypoint(point:Point):void
		{
			if(_waypoints.length == 0)
			{
				_currentPoint = new Point(point.x, point.y);
			}
			else if(_waypoints.length == 1)
			{
				_nextPoint = new Point(point.x, point.y);
			}
			_waypoints.push(point);
			
			if(_waypoints.length == 2)
			{
				startMovement();
			}
		}
		
		public function eraseWaypointAt(pos:Point):void
		{
			var wp:Point;
			var index:int = 0;
			for each (wp in _waypoints)
			{
				if(wp.equals(pos))
				{
					_waypoints.splice(index, 1);
					showWaypointsDebugInfo();
					return ;
				}
				index++;
			}
		}
		
		public function clearWyapoints():void
		{
			_waypoints.splice(0, _waypoints.length);
		}
		
		public function hideWaypointsDebugInfo():void
		{
			killWaypointsImages();
		}
		
		protected function killWaypointsImages():void
		{
			while(_waypointsImages.length > 0)
			{
				this.removeChild(_waypointsImages[0] as Sprite, true);
				_waypointsImages.splice(0, 1);
			}
		}
		
		public function showWaypointsDebugInfo():void
		{
			killWaypointsImages();
			var wp:Point;
			var index:int = 0;
			for each (wp in _waypoints)
			{
				var copTex:Texture = Game.instance.assets.getTexture("CharPoliceman1");
				var copImg:Image = new Image(copTex);
				var wpCop:Sprite = new Sprite();
				copImg.scaleX = 0.2;
				copImg.scaleY = 0.2;
				wpCop.addChild(copImg);
				
				var numTF:TextField = new TextField(35, 35, String(index), "Verdana", 24, 0xFFFFFF, true);
				index++;
				wpCop.addChild(numTF);
				wpCop.x = (wp.x - tilePos.x)*CommonVars.TILE_SIDE_SIZE;
				wpCop.y = (wp.y - tilePos.y)*CommonVars.TILE_SIDE_SIZE;

				this.addChild(wpCop);
				
				_waypointsImages.push(wpCop);
			}
		}
		
		public function isGlobalPointInNPCToSmash(pos:Point):Boolean
		{
			var rect:Rectangle = new Rectangle(_globalPosition.x - halfWidth, _globalPosition.y, this.width, halfHeight);
			return rect.containsPoint(pos);
		}
		
		public function runWithPanic():void
		{
			_movementSpeed = BASIC_MOVEMENT_SPEED * 2;
			showPhraseOver("Не займай мене!!! Міліція!!!");
		}
		
		public function isStanding():Boolean
		{
			return _currentState == STATIC_STATE;
		}
	}
}