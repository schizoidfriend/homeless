package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	
	import map.IRedwarable;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class NPC extends Character implements IRedwarable
	{
		public static const COP:int = 0;
		public static const WALKING_NPC:int = 1;
		public static const STATIC_NPC:int = 2;
		public static const DOG:int = 3;
		public static const TERRITORY_NPC:int = 4;
		public static const BIRD:int = 5;
		public static const RAT:int = 6;
		public static const PROSTITUTE:int = 7;
		public static const AFGAN_MUSICANT:int = 8;
		public static const PIMP:int = 9;
		public static const CHURCH_MAN:int = 10;
		public static const MISSIONER:int = 11;
		public static const QUEST_NPC:int = 12;
		public static const NUM_OF_TYPES:int = 13;
		
		protected var _charStatic:MovieClip;
		protected var _charMovement:MovieClip;
		
		protected var _currentAnimation:MovieClip;
		
		private static var nextID:int = 0;
		
		private var _tilePos:Point = new Point();
		private var _id:int;
		private var _type:int;
		
		private var _isWalking:Boolean = false;
		
		private var _npcName:String;
		
		private var _isAddedToBackground:Boolean = false;
		
		public function NPC(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super();
			
			_tilePos = initialTilePos;
			generateID();
			_type = type;
			
			_npcName = assetInfo.npcName;
			
			initAnimations(assetInfo);
			setToInitialPos();
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
			hidePhraseOver();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		override public function showPhraseOver(text:String):void
		{
			if(isAddedToBackground())
			{
				super.showPhraseOver(text);
			}
		}
		
		public function setToInitialPos():void
		{
			_globalPosition.setTo(tilePos.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE,
				tilePos.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
		}
		
		protected function initAnimations(assetInfo:NPCAssetInfo):void
		{
			addStaticAnimation(assetInfo.staticAnimName);
			addMovementAnimation(assetInfo.walkAnimName);
			
			//_charStatic.scaleX = 0.9;
			//_charStatic.scaleY = 0.9;
			_charStatic.visible = false;

			//_charMovement.scaleX = 0.9;
			//_charMovement.scaleY = 0.9;
			_charMovement.visible = false;
			
			forceStop();
		}
		
		public function get npcName():String
		{
			return _npcName;
		}

		public function addStaticAnimation(atlasName:String):void
		{
			if(_charStatic && _charStatic.parent)
			{
				this.removeChild(_charStatic, true);
				_charStatic = null;
			}
			_charStatic = getAnimation(atlasName);
			if(!_charStatic)
			{
				_charStatic = getStaticSprite(atlasName);
			}
		}
		
		public function addMovementAnimation(atlasName:String):void
		{
			if(_charMovement && _charMovement.parent)
			{
				this.removeChild(_charMovement, true);
				_charMovement = null;
			}
			_charMovement = getAnimation(atlasName);
		}
		
		protected function getAnimation(atlasName:String, fps:int = 12):MovieClip
		{
			var object:MovieClip;
			var atlas:TextureAtlas = Game.instance.assets.getTextureAtlas(atlasName);
			if(!atlas)
			{
				return null;
			}
			
			var atlasFrames:Vector.<Texture>;
			atlasFrames = atlas.getTextures();
			object = new MovieClip(atlasFrames, fps);
			addChild(object);
			
			object.x = -(object.width >> 1);
			object.y = -(object.height >> 1);
			
			return object;
		}
		
		private function getStaticSprite(spriteName:String):MovieClip
		{
			var object:MovieClip;			
			var tex:Texture = Game.instance.assets.getTexture(spriteName);
			var texVec:Vector.<Texture> = new Vector.<Texture>();
			texVec.push(tex);
			object = new MovieClip(texVec);
			this.addChild(object);
			
			object.x = -(object.width >> 1);
			object.y = -(object.height >> 1);
			
			return object;
		}
		
		protected function stop():void
		{
			if(_isWalking)
			{
				forceStop();
				_isWalking = false;
			}
		}
		
		protected function setNewCurrentAnimation(anim:MovieClip):void
		{
			if(_currentAnimation)
			{
				_currentAnimation.visible = false;
				Starling.juggler.remove(_currentAnimation);
			}
			
			anim.visible = true;
			Starling.juggler.add(anim);
			_currentAnimation = anim;
		}
		
		protected function forceStop():void
		{
			setNewCurrentAnimation(_charStatic);
		}
		
		protected function walk():void
		{
			if(!_isWalking)
			{
				_isWalking = true;
				
				setNewCurrentAnimation(_charMovement);
			}
		}
		
		public function get type():int
		{
			return _type;
		}

		public function set type(value:int):void
		{
			_type = value;
		}

		private function generateID():void
		{
			_id = nextID;
			nextID++;
		}
		
		public function get id():int
		{
			return _id;
		}
		
		public function get tilePos():Point
		{
			return _tilePos;
		}
		
		public function kill():void
		{
			if(this.parent)
			{
				Starling.juggler.remove(_currentAnimation);
				this.parent.removeChild(this, true);
			}
		}
	}
}