package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import over.OverScreen;
	
	import utils.AStarLib;
	import utils.GameUtils;
	import utils.UtilsMath;
	
	public class Pimp extends EnemyCharacter
	{
		private static const MOVING_AFTER_PLAYER_STATE:int = 3;
		private static const MOVING_TO_NEAREST_TILE_STATE:int = 4;
		private static const MOVING_TO_INITIAL_POSITION:int = 5;
		private static const MOVING_AWAY_FROM_OBSTACLE:int = 6;
		private static const MOVING_TO_LAST_SEEN_POINT:int = 7;
		private static const TOTAL_AMOUNT_STATES:int = 8;
		
		private static const MOVEMENT_SPEED:Number = 2.0;
		private static const MOVEMENT_AFTER_PLAYER_SPEED:Number = 7.0;
		
		private var _isEnemy:Boolean = false;
		
		private var _initialPosition:Point = new Point();
		
		private var _isMovingFromInitialPt:Boolean = true;
		
		private var _staticTimer:Timer = new Timer(4000);
		
		//last position we see player
		private var _lastSeenPlayerPos:Point = new Point();
		
		public function Pimp(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
			
			_initialPosition.setTo(initialTilePos.x, initialTilePos.y);
			
			_staticTimer.addEventListener(TimerEvent.TIMER, onTimer);
			
			init();
			findAndMoveToNextPoint();
		}
		
		override public function cleanup():void
		{
			super.cleanup();
			_staticTimer.reset();
		}
		
		override protected function init():void
		{
			_movementSpeed = MOVEMENT_SPEED;
			super.init();
		}
		
		private function onTimer(e:TimerEvent):void
		{
			_staticTimer.reset();
			findAndMoveToNextPoint();
		}
		
		override public function tick():void
		{
			switch(_currentState)
			{
				case STATIC_STATE:	
					staticStateTick()
					break;
				
				case MOVING_TO_WAYPOINT_STATE:
					movementWaytopintTick();
					break;
				
				case MOVING_AFTER_PLAYER_STATE:
					movementAfterPlayer();
					break;
				
				case MOVING_TO_NEAREST_TILE_STATE:
					findAndMoveToNearestTile();
					break;
				
				case MOVING_TO_INITIAL_POSITION:
					findWayBackToInitialPt();
					break;
				
				case MOVING_AWAY_FROM_OBSTACLE:
					movingAwayFromObstacle();
					break;
				
				case STUNNED_STATE:
					stunnedState();
					break;
				
				case MOVING_TO_LAST_SEEN_POINT:
					movementToLastSeenPoint();
					break;
			}			
		}
		
		private function movementToLastSeenPoint():void
		{
			UtilsMath.astar.InitializePathfinder();
			
			if(moveToPoint(this, _lastSeenPlayerPos, _movementSpeed))
			{
				if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
				{
					_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
					setState(MOVING_AFTER_PLAYER_STATE);
				}
				else
				{
					setState(MOVING_TO_NEAREST_TILE_STATE);
				}
			}
			if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
			{
				_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
				setState(MOVING_AFTER_PLAYER_STATE);
			}
			
		}
		
		private function findWayBackToInitialPt():void
		{
			walk();
			UtilsMath.astar.InitializePathfinder();
			
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			_currentPoint = new Point(_centerTilePos.x, _centerTilePos.y);
			
			var pathFound:int;
			var pt:Point;
			while(pathFound != AStarLib.FOUND)
			{
				pathFound = UtilsMath.astar.FindPath(0, _currentPoint.x, _currentPoint.y, _initialPosition.x, _initialPosition.y);
				if(pathFound == AStarLib.NON_EXISTENT)
				{
					_nextPathPointIndex = 0;
					
					_currentPath.splice(0, _currentPath.length);
					
					setState(STATIC_STATE);
					return ;
				}
			}
			
			_nextPathPointIndex = 0;
			
			_currentPath.splice(0, _currentPath.length);
			
			_nextPoint = new Point();
			_nextPoint.setTo(_initialPosition.x, _initialPosition.y);
			
			setState(MOVING_TO_WAYPOINT_STATE);
			setPath(UtilsMath.astar.pathBank[0]);
			
			changeNextPoint();
			
		}
		
		
		
		private function findAndMoveToNextPoint():void
		{
			walk();
			UtilsMath.astar.InitializePathfinder();
			
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			_currentPoint = new Point(_centerTilePos.x, _centerTilePos.y);
			
			var pathFound:int;
			var pt:Point;
			while(pathFound != AStarLib.FOUND)
			{
				if(_isMovingFromInitialPt)
				{
					pt = Game.instance.background.getRandomPointNearby(tilePos, 3);
				}
				else
				{
					pt = new Point(tilePos.x, tilePos.y);
				}
				if(_currentPoint.equals(pt))
				{
					pt = Game.instance.background.getRandomPointNearby(tilePos, 3);
				}
				pathFound = UtilsMath.astar.FindPath(0, _currentPoint.x, _currentPoint.y, pt.x, pt.y);
			}
			
			_isMovingFromInitialPt = !_isMovingFromInitialPt;
			
			_nextPathPointIndex = 0;
			
			_currentPath.splice(0, _currentPath.length);
			
			_nextPoint = new Point();
			_nextPoint.setTo(pt.x, pt.y);
			
			setState(MOVING_TO_WAYPOINT_STATE);
			setPath(UtilsMath.astar.pathBank[0]);
			
			changeNextPoint();
			
		}
		
		override protected function changeNextPoint():void
		{	
			if(_currentPath.length == 0)
			{
				return ;
			}
			_lastPathPoint = _currentPath[_nextPathPointIndex];
			_nextPathPointIndex++;
			if(_nextPathPointIndex < _currentPath.length)
			{			
				_nextPathPoint = _currentPath[_nextPathPointIndex];
			}
			else
			{
				justStopHere();
			}
		}
		
		public override function addOffset(xOffset:Number, yOffset:Number):void
		{			
			var oldPos:Point = new Point(_globalPosition.x, _globalPosition.y);
			_globalPosition.setTo(_globalPosition.x + xOffset, _globalPosition.y + yOffset);
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			var canMoveX:Boolean = true;
			var canMoveY:Boolean = true;
			var possibleBacking:Point = new Point();
			var points:Vector.<Point> = tiles.slice();
			//simple adding offset and gathering information about possibility to move x and y
			for(var i:int = 0; i < points.length; i++)
			{
				var pt:Point = points[i];
				if(!Game.instance.background.isWalkable(pt))
				{
					_curObstaclePoint.setTo(pt.x, pt.y);
					_globalPosition.setTo(oldPos.x + xOffset, oldPos.y);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					if(isInTile(pt.x,pt.y))
					{
						canMoveX = false;
					}
					_globalPosition.setTo(oldPos.x, oldPos.y + yOffset);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					if(isInTile(pt.x,pt.y))
					{
						canMoveY = false;
					}
					
					_globalPosition.setTo(oldPos.x, oldPos.y);
				}
			}
			
			if(canMoveX && canMoveY)
			{
				return ;	
			}
			
			//if can't move one of directions
			var charPT:Point;
			var posToLookFor:Point = new Point();
			var diff:Point;
			var distFromTop:Number;
			var distFromBottom:Number;
			if(canMoveX)
			{
				if(Math.abs(xOffset) < 1.0)
				{
					_canMoveX = canMoveX;
					_canMoveY = canMoveY;
					_neededYOffset =  yOffset;
					
					var topXPoint:Point = new Point(_curObstaclePoint.x+1, _curObstaclePoint.y);
					var bottomXPoint:Point = new Point(_curObstaclePoint.x-1, _curObstaclePoint.y);
					
					var isTopXWalkable:Boolean = Game.instance.background.isWalkable(topXPoint);
					var isBottomXWalkable:Boolean = Game.instance.background.isWalkable(bottomXPoint);
					if(isTopXWalkable && isBottomXWalkable)
					{
						var topXGlobalPoint:Point = GameUtils.tileToCenterMapPos(topXPoint);
						var bottomXGlobalPoint:Point = GameUtils.tileToCenterMapPos(bottomXPoint);
						
						if(_prevState == MOVING_TO_LAST_SEEN_POINT)
						{
							posToLookFor.setTo(_lastSeenPlayerPos.x, _lastSeenPlayerPos.y);
						}
						else
						{
							posToLookFor.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
						}
						
						diff = posToLookFor.subtract(topXGlobalPoint); 
						distFromTop = diff.length;
						
						diff = posToLookFor.subtract(bottomXGlobalPoint); 
						distFromBottom = diff.length;
						
						if(distFromTop < distFromBottom)
						{
							_neededXOffset = _movementSpeed;
						}
						else
						{
							_neededXOffset = -_movementSpeed;
						}
					}
					else if(isTopXWalkable)
					{
						_neededXOffset = _movementSpeed;
					}
					else
					{
						_neededXOffset = -_movementSpeed;
					}
					
					setState(MOVING_AWAY_FROM_OBSTACLE);
				}
				else
				{
					xOffset = (xOffset < 0) ? -_movementSpeed : _movementSpeed;
					_globalPosition.setTo(oldPos.x + xOffset, oldPos.y);
					
					if(!isCurrentPositionGood())
					{
						_globalPosition.setTo(oldPos.x - xOffset, oldPos.y);
						moveSlowly(_movementSpeed, 0);
					}
					
				}
				
			}
			else if(canMoveY)
			{
				if(Math.abs(yOffset) < 1.0)
				{
					_canMoveX = canMoveX;
					_canMoveY = canMoveY;
					_neededXOffset = xOffset;
					
					var topYPoint:Point = new Point(_curObstaclePoint.x, _curObstaclePoint.y+1);
					var bottomYPoint:Point = new Point(_curObstaclePoint.x, _curObstaclePoint.y-1);
					
					var isTopYWalkable:Boolean = Game.instance.background.isWalkable(topYPoint);
					var isBottomYWalkable:Boolean = Game.instance.background.isWalkable(bottomYPoint);
					if(isTopYWalkable && isBottomYWalkable)
					{
						var topYGlobalPoint:Point = GameUtils.tileToCenterMapPos(topYPoint);
						var bottomYGlobalPoint:Point = GameUtils.tileToCenterMapPos(bottomYPoint);
						
						if(_prevState == MOVING_TO_LAST_SEEN_POINT)
						{
							posToLookFor.setTo(_lastSeenPlayerPos.x, _lastSeenPlayerPos.y);
						}
						else
						{
							posToLookFor.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
						}
						
						diff = posToLookFor.subtract(topYGlobalPoint); 
						distFromTop = diff.length;
						
						diff = posToLookFor.subtract(bottomYGlobalPoint); 
						distFromBottom = diff.length;
						
						if(distFromTop < distFromBottom)
						{
							_neededYOffset = _movementSpeed;
						}
						else
						{
							_neededYOffset = -_movementSpeed;
						}
					}
					else if(isTopYWalkable)
					{
						_neededYOffset = _movementSpeed;
					}
					else
					{
						_neededYOffset = -_movementSpeed;
					} 
					
					setState(MOVING_AWAY_FROM_OBSTACLE);
				}
				else
				{
					yOffset = (yOffset < 0) ? -_movementSpeed : _movementSpeed;
					_globalPosition.setTo(oldPos.x, oldPos.y + yOffset);
					
					if(!isCurrentPositionGood())
					{
						_globalPosition.setTo(oldPos.x, oldPos.y - yOffset);
						moveSlowly(0, _movementSpeed);
					}
				}
			}
			else
			{
				setState(MOVING_TO_NEAREST_TILE_STATE);
			}
		}
		
		override protected function movementWaytopintTick():void
		{
			var moveTo:Point = new Point(_nextPathPoint.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				_nextPathPoint.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, _movementSpeed))
			{
				if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
				{
					_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
					setState(MOVING_AFTER_PLAYER_STATE);
					return ;
				}
				//justStopHere();
				changeNextPoint();
			}
		}
		
		private function moveAfterPlayer(playerPos:Point):Boolean
		{
			return moveToPoint(this, playerPos, _movementSpeed);
		}
		
		private function staticStateTick():void
		{			
			if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
			{
				_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
				setState(MOVING_AFTER_PLAYER_STATE);
			}

		}
		
		private function movingAwayFromObstacle():void
		{
			if(canMoveXAndY())
			{
				addSpecialOffset();
				
				if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
				{
					_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
					setState(MOVING_AFTER_PLAYER_STATE);
				}
				else
				{
					setState(MOVING_TO_NEAREST_TILE_STATE);
				}
			}
		}
		
		override protected function onStunnedTimerCompleted(e:TimerEvent):void
		{
			super.onStunnedTimerCompleted(e);
			if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
			{
				setState(MOVING_AFTER_PLAYER_STATE);
				showPhraseOver("Я те дам, падла!");
			}
			else
			{
				setState(MOVING_TO_NEAREST_TILE_STATE);
				showPhraseOver("Де ты, падла!");
			}
			
		}
		
		private function findAndMoveToNearestTile():void
		{
			var nearestTile:Point = new Point(int(_globalPosition.x / CommonVars.TILE_SIDE_SIZE), 
				int(_globalPosition.y / CommonVars.TILE_SIDE_SIZE));
			
			
			if(moveToPoint(this, new Point(nearestTile.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE,
				nearestTile.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE), _movementSpeed))
			{
				setState(MOVING_TO_INITIAL_POSITION);
			}
			
		}
		
		private function movementAfterPlayer():void
		{
			if(canSeePlayer(Game.instance.dude.globalPosition))
			{
				_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
				var diff:Point = _globalPosition.subtract(new Point(_initialPosition.x*CommonVars.TILE_SIDE_SIZE, _initialPosition.y*CommonVars.TILE_SIDE_SIZE)); 
				var dist:int = diff.length;
				if(moveAfterPlayer(_lastSeenPlayerPos))
				{
					Game.instance.setOverScreen(OverScreen.BEATEN_POLICE_OVER); //TODO: Create beaten by someone else state
					setState(MOVING_TO_NEAREST_TILE_STATE);
				}
			}
			else
			{
				//setState(MOVING_TO_NEAREST_TILE_STATE);
				setState(MOVING_TO_LAST_SEEN_POINT);
			}
		}
		
		private function justStopHere():void
		{
			_staticTimer.start();
			setState(STATIC_STATE);
			
			if(UtilsMath.getRandomWithChance(50))
			{
				showPhraseOver("Лучшие девочки!");
			}
			else
			{
				showPhraseOver("Подходи поторогай сосцы!");
			}
		}
		
		private function isDudeEnemy():Boolean
		{
			return _isEnemy || Game.instance.dude.didKickProstitute;
		}
		
		public function canSeePlayerAndBeAttacked(playerPos:Point):Boolean
		{
			if(isDudeEnemy() && canSeePlayer(playerPos))
			{
				return true;
			}
			return false;
		}
		
		override public function stun():void
		{
			_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
			_movementSpeed = MOVEMENT_AFTER_PLAYER_SPEED;
			_isEnemy = true;
			super.stun();
		}
		
		override protected function setState(state:int):void
		{
			super.setState(state);
			
			if(state == MOVING_AFTER_PLAYER_STATE ||
				state == MOVING_TO_LAST_SEEN_POINT)
			{
				_movementSpeed = MOVEMENT_AFTER_PLAYER_SPEED;
			}
			else if(state == MOVING_TO_INITIAL_POSITION||
				state == MOVING_TO_WAYPOINT_STATE ||
				state == MOVING_TO_NEAREST_TILE_STATE)
			{
				_movementSpeed = MOVEMENT_SPEED;
			}
		}
		
		override public function setToBasicStateOFMind():void
		{
			_isEnemy = false;
		}
		
	}
}