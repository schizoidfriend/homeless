 package map.characters
{
	import editor.EditorAssets;
	import editor.EditorView;
	
	import feathers.controls.Radio;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import org.osmf.events.TimeEvent;
	
	import utils.UtilsMath;

	public class PlaceToPlaceController
	{
		private const MIN_TIME_TILL_NEXT_STS:int = 1000;
		private const MAX_TIME_TILL_NEXT_STS:int = 2000;
		
		private var _shopsList:Vector.<Point> = new Vector.<Point>();
		private var _currentSTSCharacters:Vector.<ShopToShopNPC> = new Vector.<ShopToShopNPC>();
		
		private var _basementsList:Vector.<Point> = new Vector.<Point>();
		private var _currentRats:Vector.<Rat> = new Vector.<Rat>();
		
		private var _churchesList:Vector.<Point> = new Vector.<Point>();
		private var _currentChurchGuys:Vector.<ShopToShopNPC> = new Vector.<ShopToShopNPC>();
		
		private var _timerTillNextSTS:Timer = new Timer(1000);
		private var _timerTillNextRat:Timer = new Timer(2000);
		private var _timerTillNextChurchGuy:Timer = new Timer(5000);
		
		public function PlaceToPlaceController()
		{
			_timerTillNextSTS.reset();
			_timerTillNextSTS.addEventListener(TimerEvent.TIMER, onTimer);
			
			_timerTillNextRat.reset();
			_timerTillNextRat.addEventListener(TimerEvent.TIMER, onRatTimer);
			
			_timerTillNextChurchGuy.reset();
			_timerTillNextChurchGuy.addEventListener(TimerEvent.TIMER, onChurchGuyTimer);
		}
		
		public function cleanup():void
		{
			_timerTillNextSTS.reset();
			_timerTillNextRat.reset();
			_timerTillNextChurchGuy.reset();
		}
		
		public function get currentRats():Vector.<Rat>
		{
			return _currentRats;
		}

		public function stopTimer():void
		{
			_timerTillNextSTS.stop();
			_timerTillNextRat.stop();
			_timerTillNextChurchGuy.stop();
		}
		
		public function startTimer():void
		{
			_timerTillNextSTS.start();
			_timerTillNextRat.start();
			_timerTillNextChurchGuy.start();
		}
		
		private function onRatTimer(e:TimerEvent):void
		{
			generateNewRandomRat();
			cleanDeadRats();
			
			_timerTillNextSTS.reset();
			_timerTillNextSTS.delay = UtilsMath.randomRange(MIN_TIME_TILL_NEXT_STS, MAX_TIME_TILL_NEXT_STS);
			_timerTillNextSTS.start();
		}
		
		private function onTimer(e:TimerEvent):void
		{
			generateNewRandomSTS();
			cleanDeadSTS();
			
			_timerTillNextSTS.reset();
			_timerTillNextSTS.delay = UtilsMath.randomRange(MIN_TIME_TILL_NEXT_STS, MAX_TIME_TILL_NEXT_STS);
			_timerTillNextSTS.start();
		}
		
		private function onChurchGuyTimer(e:TimerEvent):void
		{
			generateNewRandomChurchGuy();
			cleanDeadChurchGuys();
			
			_timerTillNextChurchGuy.reset();
			_timerTillNextChurchGuy.delay = UtilsMath.randomRange(MIN_TIME_TILL_NEXT_STS, MAX_TIME_TILL_NEXT_STS);
			_timerTillNextChurchGuy.start();
		}
		
		private function cleanDeadChurchGuys():void
		{
			var churchGuy:ChurchGuy;
			for each (churchGuy in _currentChurchGuys)
			{
				if(churchGuy.isDead())
				{
					Game.instance.background.killNPC(churchGuy.id);
					removeSTS(churchGuy.id);
				}
			}
		}
		
		private function cleanDeadRats():void
		{
			var rat:Rat;
			for each (rat in _currentRats)
			{
				if(rat.isDead())
				{
					Game.instance.background.killNPC(rat.id);
					removeSTS(rat.id);
				}
			}
		}
		
		public function killRat(rat:Rat):void
		{
			rat.killMyself();
			Game.instance.background.killNPC(rat.id);
			removeSTS(rat.id);
		}
		
		private function cleanDeadSTS():void
		{
			var sts:ShopToShopNPC;
			for each (sts in _currentSTSCharacters)
			{
				if(sts.isDead())
				{
					Game.instance.background.killNPC(sts.id);
					removeSTS(sts.id);
				}
			}
		}
		
		public function addBasement(pos:Point):void
		{
			_basementsList.push(new Point(pos.x, pos.y));
		}
		
		public function removeBasement(basePos:Point):void
		{
			var index:int = 0;
			for each (var pos:Point in _basementsList)
			{
				if(pos.equals(basePos))
				{
					_basementsList.splice(index, 1);
					return ;
				}
				index++;
			}
		}
		
		public function addShop(pos:Point):void
		{
			_shopsList.push(new Point(pos.x, pos.y));
		}
		
		public function removeShop(shopPos:Point):void
		{
			var index:int = 0;
			for each (var pos:Point in _shopsList)
			{
				if(pos.equals(shopPos))
				{
					_shopsList.splice(index, 1);
					return ;
				}
				index++;
			}
		}
		
		public function addChurchPlace(pos:Point):void
		{
			_churchesList.push(new Point(pos.x, pos.y));
		}
		
		public function removeChurchPlace(churchPos:Point):void
		{
			var index:int = 0;
			for each (var pos:Point in _churchesList)
			{
				if(pos.equals(churchPos))
				{
					_churchesList.splice(index, 1);
					return ;
				}
				index++;
			}
		}
		
		public function isThereABasement(pos:Point):Boolean
		{
			var index:int = 0;
			for each (var pos:Point in _basementsList)
			{
				if(pos.equals(pos))
				{
					return true;
				}
			}
			return false;
		}
		
		public function isThereAShop(pos:Point):Boolean
		{
			var index:int = 0;
			for each (var pos:Point in _shopsList)
			{
				if(pos.equals(pos))
				{
					return true;
				}
			}
			return false;
		}
		
		private function addRat(rat:Rat):void
		{
			_currentRats.push(rat);
			rat.startNPC();
			Game.instance.background.addReadyNPC(rat);
		}
		
		private function removeRat(id:int):void
		{
			var index:int = 0;
			for each (var rat:Rat in _currentRats)
			{
				if(rat.id == id)
				{
					_currentRats.splice(index, 1);
				}
				index++;
			}
		}
		
		private function addSTS(sts:ShopToShopNPC):void
		{
			_currentSTSCharacters.push(sts);
			sts.startNPC();
			Game.instance.background.addReadyNPC(sts);
		}
		
		private function removeSTS(id:int):void
		{
			var index:int = 0;
			for each (var sts:ShopToShopNPC in _currentSTSCharacters)
			{
				if(sts.id == id)
				{
					_currentSTSCharacters.splice(index, 1);
				}
				index++;
			}
		}
		
		private function addChurchGuy(cg:ChurchGuy):void
		{
			_currentChurchGuys.push(cg);
			cg.startNPC();
			Game.instance.background.addReadyNPC(cg);
		}
		
		private function removeChurchGuy(id:int):void
		{
			var index:int = 0;
			for each (var cg:ChurchGuy in _currentChurchGuys)
			{
				if(cg.id == id)
				{
					_currentChurchGuys.splice(index, 1);
				}
				index++;
			}
		}
		
		private function generateNewRandomChurchGuy():void
		{
			if(_churchesList.length >= 2)
			{
				var firstShopIndex:int = UtilsMath.randomRange(0, _churchesList.length-1);
				var secondShopIndex:int = firstShopIndex;
				while(secondShopIndex == firstShopIndex)
				{
					secondShopIndex = UtilsMath.randomRange(0, _churchesList.length-1);
				}
				
				var cg:ChurchGuy = new ChurchGuy(_churchesList[firstShopIndex] as Point, 0, 
					EditorView.editorAssets.getRandomChurchGuy(), _churchesList[secondShopIndex] as Point, true);
				
				addSTS(cg);
			}
		}
		
		private function generateNewRandomSTS():void
		{
			if(_shopsList.length >= 2)
			{
				var firstShopIndex:int = UtilsMath.randomRange(0, _shopsList.length-1);
				var secondShopIndex:int = firstShopIndex;
				while(secondShopIndex == firstShopIndex)
				{
					secondShopIndex = UtilsMath.randomRange(0, _shopsList.length-1);
				}
				
				var sts:ShopToShopNPC = new ShopToShopNPC(_shopsList[firstShopIndex] as Point, 0, 
					EditorView.editorAssets.getRandomWalkingNPC(), _shopsList[secondShopIndex] as Point, true);
				
				addSTS(sts);
			}
		}
		
		private function generateNewRandomRat():void
		{
			if(_basementsList.length >= 2)
			{
				var firstBaseIndex:int = UtilsMath.randomRange(0, _basementsList.length-1);
				var secondBaseIndex:int = firstBaseIndex;
				while(secondBaseIndex == firstBaseIndex)
				{
					secondBaseIndex = UtilsMath.randomRange(0, _basementsList.length-1);
				}
				
				var rat:Rat = new Rat(_basementsList[firstBaseIndex] as Point, 0, 
					EditorView.editorAssets.getRandomRat(), _basementsList[secondBaseIndex] as Point, false);
				
				addRat(rat);
			}
		}
		
		public function removeAllCharacters():void
		{
			cleanDeadSTS();
			cleanDeadRats();
			while(_currentSTSCharacters.length > 0)
			{
				var sts:ShopToShopNPC = _currentSTSCharacters.pop();
				Game.instance.background.killNPC(sts.id)
			}
			
			while(_currentRats.length > 0)
			{
				var rat:Rat = _currentRats.pop();
				Game.instance.background.killNPC(rat.id)
			}
		}
		
		public function clearAllPlaces():void
		{
			removeAllCharacters();
			
			_shopsList.splice(0, _shopsList.length);
			_basementsList.splice(0, _basementsList.length);
		}
			
	}
}