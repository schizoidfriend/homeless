package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import over.OverScreen;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	import utils.AStarLib;
	import utils.GameUtils;
	import utils.UtilsMath;
	import utils.pathfinding.astar.AStarNodeVO;

	public class PoliceManNPC extends EnemyCharacter
	{
		//poloiceman states
		private static const MOVING_AFTER_PLAYER_STATE:int = 3;
		private static const MOVING_TO_LAST_SEEN_POINT:int = 4;
		private static const MOVING_TO_NEAREST_TILE_STATE:int = 5;
		private static const MOVING_TO_LAST_WAYPOINT:int = 6;
		private static const MOVING_AWAY_FROM_OBSTACLE:int = 7;
		private static const GOING_BACK_TO_PLACE_STATE:int = 8;
		
		private static const TOTAL_AMOUNT_STATES:int = 9;
		
		//policemam basic movement speed
		private static const MOVEMENT_SPEED:Number = 6.0;	
		
		private var _disappointedAnim:MovieClip;
		private var _lookaroundLeftAnim:MovieClip;
		private var _lookaroundRightAnim:MovieClip;
		
		//last position we see player
		private var _lastSeenPlayerPos:Point = new Point();
		
		private var _expectedStateAfterPause:int = MOVING_TO_WAYPOINT_STATE;
		private var _expectedStateAfterObstacle:int;
		
		
		
		public function PoliceManNPC(initialTilePos:Point, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, COP, assetInfo);
			
			_waypoints.push(initialTilePos);
			_currentPoint = initialTilePos;
			
			setToInitialPos();
			
			//_waitPause.addEventListener(TimerEvent.TIMER_COMPLETE, onStaticTimer);
		}
		
		override protected function initAnimations(assetInfo:NPCAssetInfo):void
		{
			super.initAnimations(assetInfo);
			
			_disappointedAnim = getAnimation("P_Disappointed", 1);
			_disappointedAnim.visible = false;
			_lookaroundLeftAnim = getAnimation("P_Lookaround_Left-Right");
			_lookaroundLeftAnim.visible = false;
			_lookaroundRightAnim = getAnimation("P_Lookaround_Right-Left");
			_lookaroundRightAnim.visible = false;
			
			_disappointedAnim.addEventListener(Event.COMPLETE, onWaitingAnimCompleted);
			_lookaroundLeftAnim.addEventListener(Event.COMPLETE, onWaitingAnimCompleted);
			_lookaroundRightAnim.addEventListener(Event.COMPLETE, onWaitingAnimCompleted);
			
		}
		
		private function onWaitingAnimCompleted(e:Event):void
		{
			setState(_expectedStateAfterPause);
			if(_expectedStateAfterPause != MOVING_TO_NEAREST_TILE_STATE)
			{
				startMovement();
			}
		}
		
		private function lookaround():void
		{
			setNewCurrentAnimation(_lookaroundLeftAnim);
		}
		
		private function disappointed():void
		{
			setNewCurrentAnimation(_disappointedAnim);
		}
		

		public function get lastSeenPlayerPos():Point
		{
			return _lastSeenPlayerPos;
		}
		
		public override function addOffset(xOffset:Number, yOffset:Number):void
		{			
			var oldPos:Point = new Point(_globalPosition.x, _globalPosition.y);
			_globalPosition.setTo(_globalPosition.x + xOffset, _globalPosition.y + yOffset);
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			var canMoveX:Boolean = true;
			var canMoveY:Boolean = true;
			var possibleBacking:Point = new Point();
			var points:Vector.<Point> = tiles.slice();
			//simple adding offset and gathering information about possibility to move x and y
			for(var i:int = 0; i < points.length; i++)
			{
				var pt:Point = points[i];
				if(!Game.instance.background.isWalkable(pt))
				{
					_curObstaclePoint.setTo(pt.x, pt.y);
					_globalPosition.setTo(oldPos.x + xOffset, oldPos.y);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					if(isInTile(pt.x,pt.y))
					{
						canMoveX = false;
					}
					_globalPosition.setTo(oldPos.x, oldPos.y + yOffset);
					refreshPositions(CommonVars.TILE_SIDE_SIZE);
					if(isInTile(pt.x,pt.y))
					{
						canMoveY = false;
					}
					
					_globalPosition.setTo(oldPos.x, oldPos.y);
				}
			}
			
			if(canMoveX && canMoveY)
			{
				return ;	
			}
			
			//if can't move one of directions
			var charPT:Point;
			var posToLookFor:Point = new Point();
			var diff:Point;
			var distFromTop:Number;
			var distFromBottom:Number;
			if(canMoveX)
			{
				if(Math.abs(xOffset) < 1.0)
				{
					_canMoveX = canMoveX;
					_canMoveY = canMoveY;
					_neededYOffset =  yOffset;
					
					var topXPoint:Point = new Point(_curObstaclePoint.x+1, _curObstaclePoint.y);
					var bottomXPoint:Point = new Point(_curObstaclePoint.x-1, _curObstaclePoint.y);
					
					var isTopXWalkable:Boolean = Game.instance.background.isWalkable(topXPoint);
					var isBottomXWalkable:Boolean = Game.instance.background.isWalkable(bottomXPoint);
					if(isTopXWalkable && isBottomXWalkable)
					{
						var topXGlobalPoint:Point = GameUtils.tileToCenterMapPos(topXPoint);
						var bottomXGlobalPoint:Point = GameUtils.tileToCenterMapPos(bottomXPoint);
						
						if(_prevState == MOVING_TO_LAST_SEEN_POINT)
						{
							posToLookFor.setTo(_lastSeenPlayerPos.x, _lastSeenPlayerPos.y);
						}
						else
						{
							posToLookFor.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
						}
						
						diff = posToLookFor.subtract(topXGlobalPoint); 
						distFromTop = diff.length;
						
						diff = posToLookFor.subtract(bottomXGlobalPoint); 
						distFromBottom = diff.length;
						
						if(distFromTop < distFromBottom)
						{
							_neededXOffset = MOVEMENT_SPEED;
						}
						else
						{
							_neededXOffset = -MOVEMENT_SPEED;
						}
					}
					else if(isTopXWalkable)
					{
						_neededXOffset = MOVEMENT_SPEED;
					}
					else
					{
						_neededXOffset = -MOVEMENT_SPEED;
					}
					
					_expectedStateAfterObstacle = _currentState;
					
					setState(MOVING_AWAY_FROM_OBSTACLE);
				}
				else
				{
					xOffset = (xOffset < 0) ? -MOVEMENT_SPEED : MOVEMENT_SPEED;
					_globalPosition.setTo(oldPos.x + xOffset, oldPos.y);
					
					if(!isCurrentPositionGood())
					{
						_globalPosition.setTo(oldPos.x - xOffset, oldPos.y);
						moveSlowly(MOVEMENT_SPEED, 0);
					}
					
				}
				
			}
			else if(canMoveY)
			{
				if(Math.abs(yOffset) < 1.0)
				{
					_canMoveX = canMoveX;
					_canMoveY = canMoveY;
					_neededXOffset = xOffset;
					
					var topYPoint:Point = new Point(_curObstaclePoint.x, _curObstaclePoint.y+1);
					var bottomYPoint:Point = new Point(_curObstaclePoint.x, _curObstaclePoint.y-1);
					
					var isTopYWalkable:Boolean = Game.instance.background.isWalkable(topYPoint);
					var isBottomYWalkable:Boolean = Game.instance.background.isWalkable(bottomYPoint);
					if(isTopYWalkable && isBottomYWalkable)
					{
						var topYGlobalPoint:Point = GameUtils.tileToCenterMapPos(topYPoint);
						var bottomYGlobalPoint:Point = GameUtils.tileToCenterMapPos(bottomYPoint);
						
						if(_prevState == MOVING_TO_LAST_SEEN_POINT)
						{
							posToLookFor.setTo(_lastSeenPlayerPos.x, _lastSeenPlayerPos.y);
						}
						else
						{
							posToLookFor.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
						}
						
						diff = posToLookFor.subtract(topYGlobalPoint); 
						distFromTop = diff.length;
						
						diff = posToLookFor.subtract(bottomYGlobalPoint); 
						distFromBottom = diff.length;
						
						if(distFromTop < distFromBottom)
						{
							_neededYOffset = MOVEMENT_SPEED;
						}
						else
						{
							_neededYOffset = -MOVEMENT_SPEED;
						}
					}
					else if(isTopYWalkable)
					{
						_neededYOffset = MOVEMENT_SPEED;
					}
					else
					{
						_neededYOffset = -MOVEMENT_SPEED;
					} 
					
					_expectedStateAfterObstacle = _currentState;
					setState(MOVING_AWAY_FROM_OBSTACLE);
				}
				else
				{
					yOffset = (yOffset < 0) ? -MOVEMENT_SPEED : MOVEMENT_SPEED;
					_globalPosition.setTo(oldPos.x, oldPos.y + yOffset);
					
					if(!isCurrentPositionGood())
					{
						_globalPosition.setTo(oldPos.x, oldPos.y - yOffset);
						moveSlowly(0, MOVEMENT_SPEED);
					}
				}
			}
			else
			{
				setState(MOVING_TO_NEAREST_TILE_STATE);
			}
		}

		
		
		private function moveAfterPlayer(playerPos:Point):Boolean
		{
			return moveToPoint(this, playerPos, MOVEMENT_SPEED);
		}
		
		override protected function changeNextPoint():void
		{	
			if(_currentPath.length == 0)
			{
				return ;
			}
			_lastPathPoint = _currentPath[_nextPathPointIndex];
			_nextPathPointIndex++;
			if(_nextPathPointIndex < _currentPath.length)
			{			
				_nextPathPoint = _currentPath[_nextPathPointIndex];
			}
			else
			{
				_nextPathPointIndex = 0;
				
				_currentPoint = _nextPoint;
				_nextPointIndex++;
				if(_nextPointIndex >= _waypoints.length)
				{
					_nextPointIndex = 0;
				}
				_nextPoint = _waypoints[_nextPointIndex];
				//startMovement();
				_expectedStateAfterPause = MOVING_TO_WAYPOINT_STATE
				setState(STATIC_STATE);
				//restartWaitingTimer(2000); //след точка (осмотреться)
				lookaround();
			}			
		}
		
		override public function tick():void
		{
			switch(_currentState)
			{
				case STATIC_STATE:
					staticStateTick();	
					break;
				
				case MOVING_TO_WAYPOINT_STATE:
					movementWaytopintTick();
					break;
				
				case MOVING_AFTER_PLAYER_STATE:
					movementAfterPlayer();
					break;
				
				case MOVING_TO_LAST_SEEN_POINT:
					movementToLastSeenPoint();
					break;
				
				case MOVING_TO_NEAREST_TILE_STATE:
					findAndMoveToNearestTile();
					break;
				
				case MOVING_TO_LAST_WAYPOINT:
					movementToLastWaypoint();
					break;
				
				case MOVING_AWAY_FROM_OBSTACLE:
					movingAwayFromObstacle();
					break;
				
				case STUNNED_STATE:
					stunnedState();
					break;
			}
			
		}
		
		override public function stun():void
		{
			_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
			Game.instance.dude.coughtOnCrime = true;
			super.stun();
		}
		
		
		override protected function onStunnedTimerCompleted(e:TimerEvent):void
		{
			super.onStunnedTimerCompleted(e);
			setState(MOVING_TO_LAST_SEEN_POINT);
		}
		
		override protected function onStopTimer(e:TimerEvent):void
		{
			super.onStopTimer(e);
			setState(MOVING_TO_LAST_SEEN_POINT);
		}
		
		private function staticStateTick():void
		{			
			if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
			{
				_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
				setState(MOVING_AFTER_PLAYER_STATE);
				return ;
			}
		}
		
		public function canSeePlayerAndBeAttacked(playerPos:Point):Boolean
		{
			if(Game.instance.dude.shouldBeAttackedByPolice() && canSeePlayer(playerPos))
			{
				return true;
			}
			return false;
		}
		
		/*private function restartWaitingTimer(timerValue:int = 1000):void
		{
			_waitPause.stop();
			_waitPause.reset();
			_waitPause.delay = timerValue;
			_waitPause.start();
		}*/
		
		private function movingAwayFromObstacle():void
		{
			if(canMoveXAndY())
			{
				addSpecialOffset();
				
				if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
				{
					_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
					setState(MOVING_AFTER_PLAYER_STATE);
				}
				else
				{
					setState(_expectedStateAfterObstacle);
				}
			}
			/*if(moveToPoint(this, _awayFromObstaclePoint, MOVEMENT_SPEED))
			{
				if(canSeePlayer(Game.instance.dude.globalPosition))
				{
					_lastSeenPlayerPos.setTo(_lastSeenPlayerPoint.x, _lastSeenPlayerPoint.y);
					setState(MOVING_AFTER_PLAYER_STATE);
				}
				else
				{
					setState(_expectedStateAfterObstacle);
				}
			}*/
		}
		
		private function movementToLastSeenPoint():void
		{
			UtilsMath.astar.InitializePathfinder();
			
			if(moveToPoint(this, lastSeenPlayerPos, MOVEMENT_SPEED))
			{
				if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
				{
					_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
					setState(MOVING_AFTER_PLAYER_STATE);
				}
				else
				{
					_expectedStateAfterPause = MOVING_TO_NEAREST_TILE_STATE;
					setState(STATIC_STATE);
					disappointed();
				}
			}
			if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
			{
				_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
				setState(MOVING_AFTER_PLAYER_STATE);
			}
			
		}
		
		private function movementToLastWaypoint():void
		{
			UtilsMath.astar.InitializePathfinder();
			
			var curPT:Point = new Point(int(_globalPosition.x/CommonVars.TILE_SIDE_SIZE), int(_globalPosition.y/CommonVars.TILE_SIDE_SIZE));
			var pathFound:int = UtilsMath.astar.FindPath(0, curPT.x, curPT.y, _nextPoint.x, _nextPoint.y);
			_currentPath.splice(0, _currentPath.length);
			_nextPathPointIndex = 0;
			if ( pathFound == AStarLib.FOUND ) //path found
			{
				setState(MOVING_TO_WAYPOINT_STATE);
				_currentPoint = curPT;
				setPath(UtilsMath.astar.pathBank[0]);
				_nextPathPointIndex = 0;
				changeNextPoint();
			}
		}
		
		private function findAndMoveToNearestTile():void
		{
			var nearestTile:Point = new Point(int(_globalPosition.x / CommonVars.TILE_SIDE_SIZE), 
				int(_globalPosition.y / CommonVars.TILE_SIDE_SIZE));
			
			
			if(moveToPoint(this, new Point(nearestTile.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE,
				nearestTile.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE), MOVEMENT_SPEED))
			{
				setState(MOVING_TO_LAST_WAYPOINT);
			}
			
		}
		
		override protected function setState(state:int):void
		{
			if(state == MOVING_AFTER_PLAYER_STATE)
			{
				if(Game.instance.dude.hasAnyWeaponInHands())
				{
					showPhraseOver("Кинь оружиє!!");
				}
				else if(Game.instance.dude.isSmellHard())
				{
					showPhraseOver("Я тебе щас, курва, помию!!");
				}
					
			}
			super.setState(state);
		}
		
		private function movementAfterPlayer():void
		{
			if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
			{
				_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
				if(moveAfterPlayer(_lastSeenPlayerPos))
				{
					//Game.instance.setOverScreen(OverScreen.BEATEN_POLICE_OVER);
					Game.instance.dude.dudeKicked(50);
					setState(MOVING_TO_NEAREST_TILE_STATE);
				}
			}
			else
			{
				setState(MOVING_TO_LAST_SEEN_POINT);
			}
		}
		
		override protected function movementWaytopintTick():void
		{
			var moveTo:Point = new Point(_nextPathPoint.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
										_nextPathPoint.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, MOVEMENT_SPEED))
			{
				if(canSeePlayerAndBeAttacked(Game.instance.dude.globalPosition))
				{
					_lastSeenPlayerPos.setTo(_playerCurrentPos.x, _playerCurrentPos.y);
					setState(MOVING_AFTER_PLAYER_STATE);
					return ;
				}
				changeNextPoint();
			}
		}
		
		/*public function canSeePlayerFully(playerPos:Point):Boolean
		{
			var diff:Point = playerPos.subtract(new Point(_globalPosition.x, _globalPosition.y)); 
			var pts:Vector.<Point> = Game.instance.dude.globalTiles;
			var seeAllPoints:Boolean = true;
			for each (var pt:Point in pts)
			{
				if(UtilsMath.rayTracing.canSee(pt.x, pt.y, _globalPosition.x, _globalPosition.y) && diff.length < 600)
				{
					_lastSeenPlayerPoint = pt;
					return true;
				}
			}
		}*/
	}
}