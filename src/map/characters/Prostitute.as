package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import map.CollectableObject;
	
	import utils.AStarLib;
	import utils.GameUtils;
	import utils.UtilsMath;
	
	public class Prostitute extends MovingNPC
	{
		private var _isMovingFromInitialPt:Boolean = true;
		
		private static const MOVEMENT_SPEED:Number = 2.0;
		
		private var _staticTimer:Timer = new Timer(4000);
		
		private var _isRunningWithPanic:Boolean = false;
		
		public function Prostitute(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, type, assetInfo);
			
			_staticTimer.addEventListener(TimerEvent.TIMER, onTimer);
			
			init();
			findAndMoveToNextPoint();
		}
		
		override public function cleanup():void
		{
			super.cleanup();
			_staticTimer.reset();
		}
		
		private function onTimer(e:TimerEvent):void
		{
			_staticTimer.reset();
			findAndMoveToNextPoint();
		}
		
		protected function getRunWithPanicPos():Point
		{
			return Game.instance.background.getNearestPimpPos(this.globalPosition);
		}
		
		private function findAndMoveToNextPoint():void
		{
			walk();
			UtilsMath.astar.InitializePathfinder();
			
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			_currentPoint = new Point(_centerTilePos.x, _centerTilePos.y);
			
			var pathFound:int;
			var pt:Point;
			if(_isRunningWithPanic)
			{
				var pos:Point = getRunWithPanicPos();
				pt = GameUtils.posToTile(pos);
			}
			else
			{
				if(_isMovingFromInitialPt)
				{
					pt = Game.instance.background.getRandomPointNearby(tilePos, 3);
				}
				else
				{
					pt = new Point(tilePos.x, tilePos.y);
				}
			}
			pathFound = UtilsMath.astar.FindPath(0, _currentPoint.x, _currentPoint.y, pt.x, pt.y);
			
			if(pathFound == AStarLib.NON_EXISTENT)
			{
				pt = Game.instance.background.getRandomPointNearby(tilePos, 3);
				UtilsMath.astar.FindPath(0, _currentPoint.x, _currentPoint.y, pt.x, pt.y);
			}
			
			_isMovingFromInitialPt = !_isMovingFromInitialPt;
			
			_nextPathPointIndex = 0;
			
			_currentPath.splice(0, _currentPath.length);
			
			_nextPoint = new Point();
			_nextPoint.setTo(pt.x, pt.y);
			
			setState(MOVING_TO_WAYPOINT_STATE);
			setPath(UtilsMath.astar.pathBank[0]);
			
			changeNextPoint();
			
		}
		
		override protected function changeNextPoint():void
		{	
			if(_currentPath.length == 0)
			{
				return ;
			}
			_lastPathPoint = _currentPath[_nextPathPointIndex];
			_nextPathPointIndex++;
			if(_nextPathPointIndex < _currentPath.length)
			{			
				_nextPathPoint = _currentPath[_nextPathPointIndex];
			}
			else
			{
				justStopHere();
			}
		}
		
		override protected function movementWaytopintTick():void
		{
			var moveTo:Point = new Point(_nextPathPoint.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				_nextPathPoint.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, _movementSpeed))
			{
				//justStopHere();
				changeNextPoint();
			}
		}
		
		protected function justStopHere():void
		{
			if(_isRunningWithPanic)
			{
				_isRunningWithPanic = false;
				_movementSpeed = MOVEMENT_SPEED;
			}
			_staticTimer.start();
			setState(STATIC_STATE);
			
			showMessages();
		}
		
		protected function showMessages():void
		{
			if(UtilsMath.getRandomWithChance(50))
			{
				showPhraseOver("Парниша, хочешь развлечься?");
			}
			else
			{
				showPhraseOver("Кому продажной любви?");
			}
		}
		
		protected function showPanicPhrase():void
		{
			showPhraseOver("Батя, он міня абіжаєт!!!");
		}
		
		override public function runWithPanic():void
		{	
			_isRunningWithPanic = true;
			_movementSpeed = MOVEMENT_SPEED * 3;
			showPanicPhrase();
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			
			findAndMoveToNextPoint();
			
			_staticTimer.reset();
			
			Game.instance.background.addNewCollectableOnMap(CollectableObject.COIN, 
				"Coin", 
				this.globalPosition,
				this.centerTilePos);
		}
	}
}