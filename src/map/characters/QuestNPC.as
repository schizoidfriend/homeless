package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	
	import map.StaticNPC;
	import map.characters.structs.QuestCharInfo;
	
	public class QuestNPC extends StaticNPC
	{
		protected var _info:QuestCharInfo;
		
		public function QuestNPC(initialTilePos:Point, assetInfo:NPCAssetInfo)
		{
			super(initialTilePos, assetInfo);
			_info = new QuestCharInfo(assetInfo.npcName);
			type = NPC.QUEST_NPC
			checkCurrentDayLast();
		}
		
		public function get isCurrentDayQuestFinished():Boolean
		{
			return _info.isCurrentDayQuestFinished;
		}
		public function set isCurrentDayQuestFinished(value:Boolean):void
		{
			_info.isCurrentDayQuestFinished = value;
		}

		public function get currntDay():uint
		{
			return _info.currntDay;
		}
		public function set currntDay(value:uint):void
		{
			_info.currntDay = value;
		}
		
		public function startNewDayQuests():void
		{
			if(!_info.isLastDay && _info.isCurrentDayQuestFinished)
			{
				_info.isCurrentDayQuestFinished = false;
				_info.currntDay++;
				checkCurrentDayLast();
			}
		}

		protected function checkCurrentDayLast():void
		{
			_info.isLastDay = Game.instance.dialogs.isDayLastFor(this.npcName, _info.currntDay);
		}
		
		public function finishCurrentDayQuest():void
		{
			_info.isCurrentDayQuestFinished = true;
		}
		
		public function get infoCopy():QuestCharInfo
		{
			return _info.copy();
		}
		
		public function setData(info:QuestCharInfo):void
		{
			_info.currntDay = info.currntDay;
			_info.isCurrentDayQuestFinished = info.isCurrentDayQuestFinished;
			_info.isLastDay = info.isLastDay;			
		}
	}
}