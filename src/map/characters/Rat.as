package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.geom.Point;
	
	public class Rat extends ShopToShopNPC
	{
		public function Rat(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo, holeToPos:Point, isSmoking:Boolean=false)
		{
			_movementSpeed = 12.0;
			super(initialTilePos, type, assetInfo, holeToPos, isSmoking);
		}
		
		private function setMovementDirection(isLeft:Boolean):void
		{
			if(isLeft)
			{
				_mainSprite.scaleX = -1.0;
			}
			else
			{
				_mainSprite.scaleX = 1.0;
			}
		}
		
		private function changeDirrection(lastPt:Point, nextPt:Point):void
		{
			if(lastPt.x < nextPt.x)
			{
				setMovementDirection(false);
			}
			else if(lastPt.x > nextPt.x)
			{
				setMovementDirection(true);
			}
		}
		
		override protected function changeNextPoint():void
		{
			super.changeNextPoint();
			if(_currentState == STATIC_STATE)
			{
				setState(MOVING_TO_SHOP_STATE);
			}
			if(_nextPathPointIndex < _currentPath.length)
			{			
				changeDirrection(_lastPathPoint, _nextPathPoint);
			}
		}
		
		private function justStopHere():void
		{
			setState(STATIC_STATE);
		}		
		
		override public function startNPC():void
		{
			_globalPosition.setTo(tilePos.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE,
				(tilePos.y+1) * CommonVars.TILE_SIDE_SIZE);
			super.startNPC();
		}
		
		override protected function movementToShopTick():void
		{
			var moveTo:Point = new Point(_shopMovingToPos.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				(_shopMovingToPos.y+1) * CommonVars.TILE_SIDE_SIZE);
			if(moveToPoint(this, moveTo, _movementSpeed/2))
			{
				shopReached()
			}
		}
		
	}
}