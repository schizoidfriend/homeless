 package map.characters
{
	import editor.NPCAssetInfo;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import map.CollectableObject;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	
	import utils.ArtUtils;
	import utils.UtilsMath;
	
	public class ShopToShopNPC extends MovingNPC
	{		
		protected static const MOVING_FROM_SHOP_STATE:int = 2;
		protected static const MOVING_TO_SHOP_STATE:int = 3;
		protected static const DEAD_STATE:int = 4;
		protected static const SMOKING_STATE:int = 5;
		
		protected var _shopMovingFromPos:Point = new Point();
		protected var _shopMovingToPos:Point = new Point();
		
		protected var _isSmoking:Boolean;
		protected var _smokingTimer:Timer = new Timer(5000);
		protected var _preSmokingState:int = -1;
		protected var _smokingAnim:MovieClip;
		
		public function ShopToShopNPC(initialTilePos:Point, type:int, assetInfo:NPCAssetInfo, shopToPos:Point, isSmoking:Boolean = false)
		{
			super(initialTilePos, type, assetInfo);
			
			_shopMovingFromPos.setTo(initialTilePos.x, initialTilePos.y);
			_shopMovingToPos.setTo(shopToPos.x, shopToPos.y);
			
			_isSmoking = isSmoking;
			if(_isSmoking)
			{
				_smokingAnim = ArtUtils.makeMovieClip("smoking", 3, true);
				ArtUtils.centerMCOverItself(_smokingAnim);
				_smokingTimer.addEventListener(TimerEvent.TIMER, onSmokingTimer);
			}
			////////////////////////////////////////////////
			addWaypoint(new Point(_shopMovingFromPos.x, _shopMovingFromPos.y+1));
			addWaypoint(new Point(_shopMovingToPos.x, _shopMovingToPos.y+1));
			
			setToInitialPos();
			
			this.visible = false;
		}
		
		override public function cleanup():void
		{
			super.cleanup();
			_smokingTimer.reset();
		}
		
		private function onSmokingTimer(e:TimerEvent):void
		{
			_smokingTimer.reset();
			_isSmoking = false;
			walk();
			setState(_preSmokingState)
			
			removeChild(_smokingAnim);
			Starling.juggler.remove(_smokingAnim);
			dropCigButt();
		}
		
		private function startSmoking():void
		{
			_smokingTimer.start();
			_preSmokingState = _currentState;
			setState(SMOKING_STATE);
			stop();
			
			addChild(_smokingAnim);
			Starling.juggler.add(_smokingAnim);
		}
		
		public function startNPC():void
		{
			this.visible = true;
			setState(MOVING_FROM_SHOP_STATE);
		}
		
		override protected function changeNextPoint():void
		{
			super.changeNextPoint();
			if(_currentState == STATIC_STATE)
			{
				setState(MOVING_TO_SHOP_STATE);
			}
			else if(_isSmoking && _currentState == MOVING_TO_WAYPOINT_STATE)
			{
				var shouldStartToSmoke:Boolean = UtilsMath.getRandomWithChance(5);
				if(shouldStartToSmoke)
				{
					startSmoking();
				}
				var shouldDropGarbage:Boolean = UtilsMath.getRandomWithChance(5);
				if(shouldDropGarbage)
				{
					dropRandomGarbage();
				}
				var shouldDropUsefulGarbage:Boolean = UtilsMath.getRandomWithChance(5);
				if(shouldDropUsefulGarbage)
				{
					dropRandomUsefulGarbage();
				}
			}
		}
		
		private function dropCigButt():void
		{			
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			var randX:int = UtilsMath.randomRange(0,50);
			var randY:int = UtilsMath.randomRange(0,50);
			if(Game.instance.background.isWalkable(new Point(_centerTilePos.x, _centerTilePos.y)))
			{
				Game.instance.background.addNewCollectableOnMap(CollectableObject.CIGBUTT, 
					"cigButt", 
					new Point(globalPosition.x-25+randX, globalPosition.y-25+randY), 
					new Point(_centerTilePos.x, _centerTilePos.y));
			}
		}
		
		private function dropRandomGarbage():void
		{
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			var randX:int = UtilsMath.randomRange(0,50);
			var randY:int = UtilsMath.randomRange(0,50);
			if(Game.instance.background.isWalkable(new Point(_centerTilePos.x, _centerTilePos.y)))
			{
				Game.instance.background.addRandomTrashOnMap(new Point(globalPosition.x-25+randX, globalPosition.y-25+randY), 
															new Point(_centerTilePos.x, _centerTilePos.y));
			}
		}
		
		private function dropRandomUsefulGarbage():void
		{
			refreshPositions(CommonVars.TILE_SIDE_SIZE);
			var randX:int = UtilsMath.randomRange(0,50);
			var randY:int = UtilsMath.randomRange(0,50);
			if(Game.instance.background.isWalkable(new Point(_centerTilePos.x, _centerTilePos.y)))
			{
				Game.instance.background.addRandomUsefulTrash(new Point(globalPosition.x-25+randX, globalPosition.y-25+randY), 
					new Point(_centerTilePos.x, _centerTilePos.y));
			}
		}
		
		override public function tick():void
		{
			switch(_currentState)
			{
				case STATIC_STATE:
					break;
				
				case MOVING_TO_WAYPOINT_STATE:
					movementWaytopintTick();
					break;
				
				case MOVING_FROM_SHOP_STATE:
					movementFromShopTick();
					break;
				
				case MOVING_TO_SHOP_STATE:
					movementToShopTick();
					break;
				
				case SMOKING_STATE:
					
					break;
			}
			
		}
		
		override protected function movementWaytopintTick():void
		{
			var moveTo:Point = new Point(_nextPathPoint.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				_nextPathPoint.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, _movementSpeed))
			{
				changeNextPoint();
			}
		}
		
		protected function movementFromShopTick():void
		{
			var moveTo:Point = new Point(_shopMovingFromPos.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				(_shopMovingFromPos.y+1) * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, _movementSpeed/2))
			{
				setState(MOVING_TO_WAYPOINT_STATE);
				startMovement();
			}
		}
		
		protected function movementToShopTick():void
		{
			var moveTo:Point = new Point(_shopMovingToPos.x * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE, 
				_shopMovingToPos.y * CommonVars.TILE_SIDE_SIZE + CommonVars.HALF_TILE_SIZE);
			if(moveToPoint(this, moveTo, _movementSpeed/2))
			{
				shopReached()
			}
		}
		
		override public function addOffset(xOffset:Number, yOffset:Number):void
		{
			if(_currentState == MOVING_FROM_SHOP_STATE || _currentState == MOVING_TO_SHOP_STATE)
			{
				_globalPosition.setTo(_globalPosition.x + xOffset, _globalPosition.y + yOffset);
			}
			else
			{
				super.addOffset(xOffset, yOffset);
			}
		}			
		
		protected function shopReached():void
		{
			this.visible = false;
			killMyself();
		}
		
		override public function startMovement():void
		{
			this.visible = true;
			super.startMovement();
		}
		
		public function killMyself():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
			setState(DEAD_STATE);
		}
		
		public function isDead():Boolean
		{
			return _currentState == DEAD_STATE ? true : false;
		}
		
		override public function runWithPanic():void
		{
			super.runWithPanic();
			if(_currentState == SMOKING_STATE)
			{
				onSmokingTimer(null);
			}
		}
		
		override public function isStanding():Boolean
		{
			return _currentState == STATIC_STATE || _currentState == SMOKING_STATE;
		}
	}
}