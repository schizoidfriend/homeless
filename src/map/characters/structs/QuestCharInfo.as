package map.characters.structs
{
	public class QuestCharInfo
	{
		public var currntDay:uint = 1;
		public var isLastDay:Boolean = false;
		public var isCurrentDayQuestFinished:Boolean = false;
		public var characterName:String;
		
		public function QuestCharInfo(name:String, currntDayValue:uint = 1, isLastDayValue:Boolean = false, isCurrentDayQuestFinishedValue:Boolean = false)
		{
			characterName = name;
			currntDay = currntDayValue;
			isLastDay = isLastDayValue;
			isCurrentDayQuestFinished = isCurrentDayQuestFinishedValue;
		}
		
		public function copy():QuestCharInfo
		{
			return new QuestCharInfo(characterName, currntDay, isLastDay, isCurrentDayQuestFinished);
		}
	}
}