package map.globalMap
{	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;

	public class GlobalMap extends Sprite
	{
		public function GlobalMap()
		{
			
		}
		
		public function showGlobalMap():void
		{
			var gmTex:Texture = Game.instance.assets.getTexture("GlobalMap");
			var gmImg:Image = new Image(gmTex);
			this.addChild(gmImg);
			
			var mapbuttonTexture:Texture = Game.instance.assets.getTexture("button");
			var firstMapBtn:Button = new Button(mapbuttonTexture, "First");
			firstMapBtn.x = 300;
			firstMapBtn.y = 300;
			firstMapBtn.addEventListener(Event.TRIGGERED, onFirstButtonTriggered);
			addChild(firstMapBtn)
			
			var secondMapBtn:Button = new Button(mapbuttonTexture, "Second");
			secondMapBtn.x = 700;
			secondMapBtn.y = 300;
			secondMapBtn.addEventListener(Event.TRIGGERED, onSecondButtonTriggered);
			addChild(secondMapBtn)
			
			this.visible = true;
		}
		
		public function hideGlobalMap():void
		{
			while(this.numChildren > 0)
			{
				removeChildAt(0, true);
			}
			
			this.visible = false;
		}
		
		private function onFirstButtonTriggered(e:Event):void
		{
			Game.instance.background.loadMap("myXML.xml");
		}
		
		private function onSecondButtonTriggered(e:Event):void
		{
			Game.instance.background.loadMap("myXML2.xml");
		}
	}
}