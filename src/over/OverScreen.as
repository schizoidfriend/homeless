package over
{
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	import utils.ArtUtils;
	
	public class OverScreen extends Sprite
	{
		public static const DRINKING_OVER:int = 0;
		public static const SLEEING_HOME_OVER:int = 1;
		public static const SLEEPING_OUTSIDE_OVER:int = 2;
		public static const BEATEN_POLICE_OVER:int = 3;
		public static const LOSE_CONSCIOUSNESS_OVER:int = 4;
		public static const HUNGER_DEAD_OVER:int = 5;
		public static const SWIMMING_IN_FOUNTAIN:int = 6;
		public static const DRINKING_NICE:int = 7;
		public static const TOTAL_OVERS:int = 8;
		
		private static const SCREENS:Array = new Array("Drinking", "SleepingHome", "SleepingOutside", "BeatenByPolice", "LoseConsciousness", "HungerDead", "SwimmingInFountain", "DrinkingNice");
		
		private var _overScreens:Vector.<Image> = new Vector.<Image>();
		private var _currentScreen:Image = null;
		private var _currentScreenNum:int;
		
		public function OverScreen()
		{
			super();
			
			for each (var name:String in SCREENS)
			{
				var screenImg:Image = ArtUtils.makeSelfCenteredImage(name)
				screenImg.x = CommonVars.screenHalfWidth - screenImg.width/2;
				screenImg.y = CommonVars.screenHalfHeight - screenImg.height/2;
				_overScreens.push(screenImg);
			}
		}
		
		private var touches:Vector.<Touch>;
		private function onTouch(e:TouchEvent):void
		{
			touches = e.getTouches(this, TouchPhase.BEGAN);
			
			if (touches.length > 0)
			{
				if(_currentScreen)
				{
					afterOverScreenFunctionality(_currentScreenNum);
					
					_currentScreen.scaleX = 1.0;
					_currentScreen.scaleY = 1.0;
					_currentScreen.x = CommonVars.screenHalfWidth - _currentScreen.width/2;
					_currentScreen.y = CommonVars.screenHalfHeight - _currentScreen.height/2;
					
					this.removeChild(_currentScreen);
					_currentScreen.removeEventListener(TouchEvent.TOUCH, onTouch);
					
					Game.instance.dropOverScreen();
					Game.instance.blackness.visible = false;
				}
			}
		}
		
		private function afterOverScreenFunctionality(screenNum:int):void
		{
			switch(screenNum)
			{
				case DRINKING_OVER:
					
					break;
				case SLEEING_HOME_OVER:
					Game.instance.sleepingAtHome();
					break;
				case SLEEPING_OUTSIDE_OVER:
					Game.instance.sleepingOutside();
					break;
				case BEATEN_POLICE_OVER:
					Game.instance.beatenByPolice();
					break;
				case LOSE_CONSCIOUSNESS_OVER:
					Game.instance.loseConsciousness();
					break;
				case HUNGER_DEAD_OVER:
					Game.instance.loseConsciousness();
					break;
				case SWIMMING_IN_FOUNTAIN:
					Game.instance.dude.washInFountain();
					break;
			}
		}
		
		public function setOverScreenByName(name:String):Boolean
		{
			for(var i:int = 0; i < SCREENS.length; i++)
			{
				if(name == SCREENS[i])
				{
					setOverScreen(i);
					return true;
				}
			}
			return false;
		}
		
		private var tween:Tween;
		private var blackTween:Tween
		public function setOverScreen(screenNum:uint):void
		{
			if(screenNum >= TOTAL_OVERS) return;
			_currentScreenNum = screenNum;
			
			if(_currentScreen)
			{
				this.removeChild(_currentScreen);
			}
			_currentScreen = _overScreens[screenNum];
			this.addChild(_currentScreen);
			
			Game.instance.blackness.alpha = 0.0;
			Game.instance.blackness.visible = true;
			blackTween = new Tween(Game.instance.blackness, 0.5);
			blackTween.animate("alpha", 1.0);
			
			
			tween = new Tween(_currentScreen, 1.0);
			tween.animate("scaleX", 2.0);
			tween.animate("scaleY", 2.0);
			tween.animate("x", CommonVars.screenHalfWidth - _currentScreen.width);
			tween.animate("y", CommonVars.screenHalfHeight - _currentScreen.height);
			tween.onComplete = onTweenCompleted;
			
			blackTween.nextTween = tween;
			
			Starling.juggler.add(blackTween);
		}
		
		private function onTweenCompleted():void
		{
			Starling.juggler.remove(blackTween);
			Starling.juggler.remove(tween);
			
			_currentScreen.addEventListener(TouchEvent.TOUCH, onTouch);
		}
	}
}