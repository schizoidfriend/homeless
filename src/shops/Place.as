package shops
{
	import flash.geom.Point;
	
	public class Place extends SimpleShop
	{
		protected var _placeInfo:PlaceTypeCommonInfo;
		
		public function Place(mapPos:Point, name:String)
		{
			super(mapPos, name);
		}
		
		public function get placeInfo():PlaceTypeCommonInfo
		{
			return _placeInfo;
		}

		override protected function init():void
		{
			_placeInfo = Game.instance.placeInfo.getPlace(shopName);
			
			addShopSprite();
			addActiveLightbulb();
			
		}
	}
}