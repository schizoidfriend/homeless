package shops
{
	public class PlaceInfoLoader
	{
		[Embed(source="res/placesinfo.xml", mimeType="application/octet-stream")]
		public const PlacesInfoXML:Class;
		
		private var _placeTypes:Vector.<PlaceTypeCommonInfo> = new Vector.<PlaceTypeCommonInfo>();
		
		public function PlaceInfoLoader()
		{
			var xml:XML = XML(new PlacesInfoXML());
			onLoadedXML(xml);
		}
		
		private function onLoadedXML(xml:XML):void
		{
			for each (var object:XML in xml.shop)
			{
				var dialogs:Vector.<String> = new Vector.<String>();
				var placeXML:XML;
				for each (placeXML in object.item)
				{
					dialogs.push(String(placeXML.@id));
				}
				
				var place:PlaceTypeCommonInfo = new PlaceTypeCommonInfo(String(object.@name), 
					int(object.@NPC_timer_min), 
					int(object.@NPC_timer_max),
					dialogs);
				
				if(object.hasOwnProperty("@shopPict"))
				{
					place.placePict = String(object.@shopPict);
				}
				
				_placeTypes.push(place);
			}
		}
		
		public function getPlace(name:String):PlaceTypeCommonInfo
		{
			var place:PlaceTypeCommonInfo;
			for each (place in _placeTypes)
			{
				if(place.name == name)
				{
					return place.copy();
				}
			}
			
			return null;
		}
	}
}