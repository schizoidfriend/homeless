package shops
{
	public class PlaceTypeCommonInfo
	{
		private var _name:String;
		private var _minTimer:int;
		private var _maxTimer:int;
		private var _dialogIDs:Vector.<String>;
		private var _placePict:String = "";
		
		public function PlaceTypeCommonInfo(name:String, minTimer:int, maxTimer:int, dialogs:Vector.<String>, placePict:String = "")
		{
			_dialogIDs = new Vector.<String>();
			_name = name;
			_minTimer = minTimer;
			_maxTimer = maxTimer;
			_placePict = placePict;
			
			for each (var id:String in dialogs)
			{
				_dialogIDs.push(id);
			}
		}
		
		public function copy():PlaceTypeCommonInfo
		{
			return new PlaceTypeCommonInfo(name, minTimer, maxTimer, _dialogIDs, _placePict);
		}

		public function get placePict():String
		{
			return _placePict;
		}

		public function set placePict(value:String):void
		{
			_placePict = value;
		}

		public function get dialogIDs():Vector.<String>
		{
			return _dialogIDs;
		}

		public function get maxTimer():int
		{
			return _maxTimer;
		}

		public function get minTimer():int
		{
			return _minTimer;
		}

		public function get name():String
		{
			return _name;
		}

	}
}