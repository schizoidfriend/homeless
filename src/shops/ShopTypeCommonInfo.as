package shops
{
	import inventory.items.IItem;

	public class ShopTypeCommonInfo
	{
		private var _name:String;
		private var _minTimer:int;
		private var _maxTimer:int;
		private var _canTake:Boolean;
		private var _shopPict:String = "";
		private var _assortiment:Vector.<IItem> = new Vector.<IItem>();
		private var _dessortiment:Vector.<IItem> = new Vector.<IItem>();
		
		public function ShopTypeCommonInfo(name:String, minTimer:int, maxTimer:int, canTake:Boolean = false, assortiment:Vector.<IItem> = null, dessortiment:Vector.<IItem> = null, shopPict:String = "")
		{
			_shopPict = shopPict;
			_name = name;
			_minTimer = minTimer;
			_maxTimer = maxTimer;
			_canTake = canTake;
			
			var item:IItem;
			if(assortiment)
			{
				for each (item in assortiment)
				{
					addItem(item.copy);
				}
			}
			
			if(dessortiment)
			{
				for each (item in dessortiment)
				{
					addDesortimentItem(item.copy);
				}
			}
		}
		
		public function get shopPict():String
		{
			return _shopPict;
		}

		public function set shopPict(value:String):void
		{
			_shopPict = value;
		}

		public function get dessortiment():Vector.<IItem>
		{
			return _dessortiment;
		}

		public function get canTake():Boolean
		{
			return _canTake;
		}

		public function get assortiment():Vector.<IItem>
		{
			return _assortiment;
		}

		public function get maxTimer():int
		{
			return _maxTimer;
		}

		public function get minTimer():int
		{
			return _minTimer;
		}

		public function get name():String
		{
			return _name;
		}

		public function addItem(item:IItem):void
		{
			_assortiment.push(item);
		}
		
		public function addDesortimentItem(item:IItem):void
		{
			_dessortiment.push(item);
		}
		
		public function copy():ShopTypeCommonInfo
		{
			return new ShopTypeCommonInfo(name, minTimer, maxTimer, _canTake, _assortiment, _dessortiment, _shopPict);
		}
	}
}