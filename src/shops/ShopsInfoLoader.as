package shops
{
	import inventory.items.IItem;
	import inventory.items.SimpleItem;

	public class ShopsInfoLoader
	{
		public static const LOADED_SUCCESFULLY:String = "___LOADED_SUCCESFULLY___";
		
		[Embed(source="res/shopsAssortiment.xml", mimeType="application/octet-stream")]
		public const ShopsAssortimentXML:Class;
		
		private var _shopTypes:Vector.<ShopTypeCommonInfo> = new Vector.<ShopTypeCommonInfo>();
		
		public function ShopsInfoLoader()
		{
			
			var xml:XML = XML(new ShopsAssortimentXML());
			onLoadedXML(xml);
		}
		
		private function onLoadedXML(xml:XML):void
		{
			for each (var object:XML in xml.shop)
			{
				var canTakeVal:String = String(object.@canTake);
				var canTake:Boolean = (canTakeVal == "true") ? true : false;
				var shop:ShopTypeCommonInfo = new ShopTypeCommonInfo(String(object.@name), 
																		int(object.@NPC_timer_min), 
																		int(object.@NPC_timer_max),
																		canTake);
				if(object.hasOwnProperty("@shopPict"))
				{
					shop.shopPict = String(object.@shopPict);
				}
				
				var shopXML:XML;
				var itemName:String;
				var newItem:IItem;
				
				for each (shopXML in object.item)
				{
					itemName = String(shopXML.@name);
					
					newItem = Game.instance.itemsInfo.getItem(itemName);
					newItem.price = int(shopXML.@price);
					shop.addItem(newItem);
				}
				
				if(canTake)
				{
					for each (shopXML in object.item_buy)
					{
						itemName = String(shopXML.@name);
						
						newItem = Game.instance.itemsInfo.getItem(itemName);
						newItem.price = int(shopXML.@price);
						shop.addDesortimentItem(newItem);
					}
				}
				
				_shopTypes.push(shop);
				
			}
		}
		
		public function getShop(name:String):ShopTypeCommonInfo
		{
			var shop:ShopTypeCommonInfo;
			for each (shop in _shopTypes)
			{
				if(shop.name == name)
				{
					return shop.copy();
				}
			}
			
			return null;
		}
	}
}