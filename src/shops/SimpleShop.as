package shops
{
	import flash.geom.Point;
	
	import inventory.items.IItem;
	
	import map.IRedwarable;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	import utils.GameUtils;
	import utils.UtilsMath;

	public class SimpleShop extends Sprite implements IRedwarable
	{
		private var _shopInfo:ShopTypeCommonInfo;
		private var _mapPosition:Point = new Point();
		private var _name:String;
		private var _shopPicture:String = "";
		
		private var _activeStateLightbulb:MovieClip;
		
		private var _initialPos:Point = new Point();
		
		private var _isAddedToBackground:Boolean = false;
		
		public function SimpleShop(mapPos:Point, name:String)
		{
			_mapPosition.setTo(mapPos.x, mapPos.y);
			_initialPos = GameUtils.tileToCenterMapPos(_mapPosition);
			_name = name;
			
			init();
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		public function get initialPos():Point
		{
			return _initialPos;
		}

		private function onAdded(e:Event):void
		{
			addToBackground();
		}
		
		private function onRemoved(e:Event):void
		{
			removeFromBackground();
		}
		
		public function isAddedToBackground():Boolean
		{
			return _isAddedToBackground;
		}
		
		private function addToBackground():void
		{
			_isAddedToBackground = true;
		}
		
		private function removeFromBackground():void
		{
			_isAddedToBackground = false;
		}
		
		protected function init():void
		{
			_shopInfo = Game.instance.shopInfo.getShop(_name);
			
			addShopSprite();
			addActiveLightbulb();
			
		}
		
		protected function addShopSprite():void
		{
			var shopSprite:Sprite = ArtUtils.makeSelfCenteredSprite(_name);
			addChild(shopSprite);
		}
		
		protected function addActiveLightbulb():void
		{
			_activeStateLightbulb = ArtUtils.makeSelfCenteredMovieClip("Indicator_lamp", 8, true);
			_activeStateLightbulb.x = - CommonVars.TILE_SIDE_SIZE/2;
			_activeStateLightbulb.visible = false;
		}
		
		public function setToActiveState():void
		{
			if(!_activeStateLightbulb.visible)
			{
				Starling.juggler.add(_activeStateLightbulb);
				_activeStateLightbulb.visible = true;
				this.addChild(_activeStateLightbulb);
			}
		}
		
		public function setToDeactivatedState():void
		{
			if(_activeStateLightbulb.visible)
			{
				Starling.juggler.remove(_activeStateLightbulb);
				_activeStateLightbulb.visible = false;
				this.removeChild(_activeStateLightbulb);
			}
			
		}

		public function get shopPicture():String
		{
			return _shopInfo.shopPict;
		}

		public function get shopName():String
		{
			return _name;
		}

		public function get mapPosition():Point
		{
			return _mapPosition;
		}

		public function get shopInfo():ShopTypeCommonInfo
		{
			return _shopInfo;
		}
		
		public function justKill():void
		{
			if(this.parent)
			{
				this.parent.removeChild(this, true);
			}
		}

	}
}