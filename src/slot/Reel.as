package slot
{
	import flash.net.drm.VoucherAccessInfo;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import utils.ArtUtils;
	
	public class Reel extends Sprite
	{
		public static const REEL_STOPPED:String = "Reel_stopped";
		
		private var _symbolNames:Array = new Array("SYM0", "SYM1", "SYM2", "SYM3", "SYM4");
		private var _symbolsTotal:int = _symbolNames.length;
		private var _nextSymbolIndex:int;
		
		private var _symbols:Vector.<Image> = new Vector.<Image>();
		
		private var _currentSym:Image;
		private var _nextSym:Image;
		
		private var _hasStopPos:Boolean = false;
		private var _stopPos:int;
		
		private var _id:int;
		
		public function Reel(startPos:int, id:int)
		{
			super();
			init(startPos);
			_id = id;
		}
		
		public function get stopPos():int
		{
			return _stopPos;
		}

		public function set stopPos(value:int):void
		{
			_stopPos = value;
		}

		public function get id():int
		{
			return _id;
		}

		public function spin():void
		{
			if(!hasEventListener(Event.ENTER_FRAME))
			{
				addEventListener(Event.ENTER_FRAME, onEnterFrame);	
			}
		}
		
		public function stopAt(pos:int):void
		{
			_stopPos = pos;
			_hasStopPos = true;
		}
		
		private function stop():void
		{
			if(hasEventListener(Event.ENTER_FRAME))
			{
				removeEventListener(Event.ENTER_FRAME, onEnterFrame);	
			}
			_hasStopPos = false;
			dispatchEvent(new Event(REEL_STOPPED));
		}
		
		private function init(startPos:int, needRemakeSymbls:Boolean = true):void
		{
			_nextSymbolIndex = startPos+1;
			if(_nextSymbolIndex >= _symbolsTotal)
			{
				_nextSymbolIndex = 0;
			}
			
			if(needRemakeSymbls)
			{
				for each(var name:String in _symbolNames)
				{
					_symbols.push(ArtUtils.makeImage(name));
				}
			}
			
			_currentSym = _symbols[startPos] as Image;
			_nextSym = _symbols[_nextSymbolIndex] as Image;
			
			_nextSym.y = -_currentSym.height;
			
			addChild(_currentSym);
			addChild(_nextSym);
		}
		
		public function reset():void
		{
			removeChild(_currentSym);
			init(_nextSymbolIndex, false);
		}
		
		private function onEnterFrame(e:Event):void
		{
			_currentSym.y += 8;
			_nextSym.y += 8;
			
			if(_nextSym.y >= 0)
			{
				changeSymbol();
			}
		}
		
		private function changeSymbol():void
		{
			removeChild(_currentSym);
			_currentSym = _nextSym;
			if(_hasStopPos && _nextSymbolIndex == _stopPos)
			{
				_currentSym.y = 0;
				stop();
			}
			else
			{
				_nextSymbolIndex++;
				if(_nextSymbolIndex >= _symbolsTotal)
				{
					_nextSymbolIndex = 0;
				}
				
				_nextSym = _symbols[_nextSymbolIndex] as Image;
				_nextSym.y = -_currentSym.height;
				addChild(_nextSym);
			}
		}
	}
}