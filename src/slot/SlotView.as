package slot
{
	import flash.display.Shape;
	import flash.geom.Rectangle;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.ArtUtils;
	import utils.GameUtils;
	import utils.UtilsMath;
	
	public class SlotView extends Sprite
	{
		private var _reels:Vector.<Reel> = new Vector.<Reel>();
		private var _startStopBtn:Button;
		private var _exitButton:Button;
		
		private var _headerText:TextField;
		
		private var _isSpinning:Boolean = false;
		
		private var _stoppedReelsCount:int = 0;
		
		public function SlotView()
		{
			super();
			init();
		}
		
		public function checkMoneyStatus():void
		{
			if(Game.instance.dude.canAfford(10))
			{
				_startStopBtn.enabled = true;
			}
			else
			{
				_startStopBtn.enabled = false;
			}
		}
		
		private function init():void
		{
			_reels.push(new Reel(1, 1));
			_reels.push(new Reel(2, 2));
			_reels.push(new Reel(3, 3));
			
			for (var i:int = 0; i < _reels.length; i++)
			{
				var reel:Reel = _reels[i];
				reel.addEventListener(Reel.REEL_STOPPED, onReelStopped);
				reel.x = i*reel.width;
				
				addChild(reel);
				reel.clipRect = new Rectangle(0,0,reel.width,64);
			}
			
			_startStopBtn = ArtUtils.createButton(64,64,"Start");
			addChild(_startStopBtn);
			_startStopBtn.addEventListener(Event.TRIGGERED, onStop);
			
			_exitButton = ArtUtils.createButton(64, 128, "Exit");
			addChild(_exitButton);
			_exitButton.addEventListener(Event.TRIGGERED, onExit);
			
			_headerText = ArtUtils.makeBoldTF(192, 64, 0, -64, 24, 0x0, VAlign.CENTER, HAlign.CENTER);
			addChild(_headerText);
			_headerText.text = "КазIно \"НаIбалово\"";
		}
		
		private function onReelStopped(e:Event):void
		{
			_stoppedReelsCount++;
			
			if(_stoppedReelsCount >= 3)
			{
				_stoppedReelsCount = 0;
				checkWin();	
				checkMoneyStatus();
				_exitButton.enabled = true;
				_startStopBtn.text = "Start";
				_isSpinning = false;
				for (var i:int = 0; i < _reels.length; i++)
				{
					var reel:Reel = _reels[i];
					reel.reset();
				}
			}
		}
		
		private function checkWin():void
		{
			if((Reel(_reels[0]).stopPos == Reel(_reels[1]).stopPos) && 
				(Reel(_reels[1]).stopPos == Reel(_reels[2]).stopPos))
			{
				Game.instance.dude.earnMoney(100);
			}
			else if( 	(Reel(_reels[0]).stopPos == Reel(_reels[1]).stopPos) ||
						(Reel(_reels[1]).stopPos == Reel(_reels[2]).stopPos) ||
						(Reel(_reels[0]).stopPos == Reel(_reels[2]).stopPos) 	)
			{
				Game.instance.dude.earnMoney(10);
			}
				
		}
		
		private function onExit(e:Event):void
		{
			Game.instance.currentState = Game.MOVING_STATE;
		}
		
		private function onStop(e:Event):void
		{
			var i:int;
			var reel:Reel;
			if(_isSpinning)
			{
				_startStopBtn.enabled = false;
				for (i = 0; i < _reels.length; i++)
				{
					reel = _reels[i];
					reel.stopAt(UtilsMath.randomRange(0, 4));
				}
			}
			else
			{
				_isSpinning = true;
				_exitButton.enabled = false;
				Game.instance.dude.spendMoney(10);
				_startStopBtn.text = "Stop";
				for (i = 0; i < _reels.length; i++)
				{
					reel = _reels[i];
					reel.spin();	
				}
			}
		}
	}
}