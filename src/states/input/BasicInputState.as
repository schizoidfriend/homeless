package states.input
{
	public class BasicInputState implements IBasicInputState
	{
		private var _parentStateID:int;
		
		//currently pressed keys
		protected var _keysPressed:Vector.<int> = new Vector.<int>();
		
		public function BasicInputState(parentStateID:int)
		{
			_parentStateID = parentStateID;
		}
		
		public function get keysPressed():Vector.<int>
		{
			return _keysPressed;
		}

		public function get stateID():int
		{
			return _parentStateID;
		}
		
		public function onKeyDown(keyCode:uint):void
		{
		}
		
		public function onKeyUp(keyCode:uint):void
		{
		}
		
		public function releaseAllKeys():void
		{
			_keysPressed.splice(0, _keysPressed.length);
		}
		
		public function init():void
		{
			releaseAllKeys();
		}
		
		public function deinit():void
		{
			releaseAllKeys();	
		}
	}
}