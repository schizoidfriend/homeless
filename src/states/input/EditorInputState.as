package states.input
{
	import flash.ui.Keyboard;

	public class EditorInputState extends BasicInputState
	{
		public function EditorInputState(parentStateID:int)
		{
			super(parentStateID);
		}
		
		override public function onKeyDown(keyCode:uint):void
		{
			if(_keysPressed.indexOf(keyCode) == -1)
			{
				if(	keyCode == Keyboard.G)
				{
					Game.instance.currentState = Game.MOVING_STATE;
				}
				
				if( keyCode == Keyboard.N )
				{
					Game.instance.currentState = Game.NEWMAP_STATE;
				}
				
				if( keyCode == Keyboard.S)
				{
					Game.instance.background.saveXMLedBackground();
				}
				
				if( keyCode == Keyboard.L)
				{
					Game.instance.background.loadXMLedBackground();
				}
				
				if(	keyCode == Keyboard.LEFT || 
					keyCode == Keyboard.RIGHT || 
					keyCode == Keyboard.UP || 
					keyCode == Keyboard.DOWN)
				{
					_keysPressed.push(keyCode);					
				}
			}
		}
		
		override public function onKeyUp(keyCode:uint):void
		{
			var index:int = _keysPressed.indexOf(keyCode);
			if(index != -1)
			{
				_keysPressed.splice(index, 1);
			}
		}
	}
}