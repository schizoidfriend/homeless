package states.input
{
	public interface IBasicInputState
	{
		function get stateID():int;
		function get keysPressed():Vector.<int>;
		
		function onKeyDown(keyCode:uint):void;
		function onKeyUp(keyCode:uint):void;
		function releaseAllKeys():void;
		
		function init():void;
		function deinit():void;
	}
}