package states.input
{
	public class InputStatesManager
	{
		private var _inputStates:Vector.<IBasicInputState> = new Vector.<IBasicInputState>();
		
		private var _simpleState:BasicInputState = new BasicInputState(-1);
		
		private static var _instance:InputStatesManager = new InputStatesManager();
		
		public function InputStatesManager()
		{
			_inputStates.push(new EditorInputState(Game.EDITOR_STATE));
			_inputStates.push(new InventoryInputState(Game.INVENTORY_STATE));
			_inputStates.push(new MovingInputState(Game.MOVING_STATE));
		}
		
		public function getInputForState(stateID:int):IBasicInputState
		{
			for each (var state:IBasicInputState in _inputStates)
			{
				if(state.stateID == stateID)
				{
					return state;
				}
			}
			return _simpleState;
		}
		
		public static function get instance():InputStatesManager
		{
			return _instance;
		}
		
	}
}