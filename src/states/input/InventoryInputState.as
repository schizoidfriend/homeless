package states.input
{
	import flash.ui.Keyboard;

	public class InventoryInputState extends BasicInputState
	{
		public function InventoryInputState(parentStateID:int)
		{
			super(parentStateID);
		}
		
		override public function onKeyDown(keyCode:uint):void
		{
			if(	keyCode == Keyboard.LEFT)
			{
				Game.instance.inventory.prevPos();
			}
			else if(keyCode == Keyboard.RIGHT)
			{
				Game.instance.inventory.nextPos();
			}
			else if (keyCode == Keyboard.UP ||
					 keyCode == Keyboard.DOWN)
			{
				Game.instance.inventory.upPos();
			}
			else if(keyCode == Keyboard.ENTER)
			{
				Game.instance.dude.useItemInSlot(Game.instance.inventory.currentSelection);
			}
			else
			{
				Game.instance.currentState = Game.MOVING_STATE;
			}
		}
		
		override public function onKeyUp(keyCode:uint):void
		{
			
		}
	}
}