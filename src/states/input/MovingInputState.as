package states.input
{
	import animations.RotateAnimator;
	import animations.ScaleAnimator;
	import animations.ShakeAnimator;
	
	import dude.Dude;
	
	import fl.motion.MatrixTransformer;
	import fl.transitions.Transition;
	import fl.transitions.TransitionManager;
	import fl.transitions.Zoom;
	import fl.transitions.easing.Elastic;
	
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import game.GameSavedData;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.pixelmask.PixelMaskDisplayObject;
	import starling.textures.TextureSmoothing;
	
	import utils.ArtUtils;

	public class MovingInputState extends BasicInputState
	{
		private var _scaleAnim:ScaleAnimator = new ScaleAnimator();
		private var _rotAnim:RotateAnimator = new RotateAnimator();
		private var _shakeAnim:ShakeAnimator = new ShakeAnimator();
		
		public function MovingInputState(parentStateID:int)
		{
			super(parentStateID);
		}

		private function onScaleDone():void
		{
			var gameObj:Game = Game.instance;
			//gameObj.setToAlcoState();
			_rotAnim.rotateObjectAroundPoint(gameObj, new Point(gameObj.dude.x, gameObj.dude.y), 360.0, 30, onRescale);
		}
		
		private function onRescale():void
		{
			var gameObj:Game = Game.instance;
			
			_scaleAnim.scaleObjectAroundPoint(gameObj, new Point(gameObj.dude.x, gameObj.dude.y), 1.0, 30, onRescaleCompleted);
		}
		
		private function onRescaleCompleted():void
		{
			var gameObj:Game = Game.instance;
			
			gameObj.x = 0;
			gameObj.y = 0;
			
			gameObj.scaleX = 1.0;
			gameObj.scaleY = 1.0;
		}
		
		private function startShake():void
		{
			_shakeAnim.startShaking(Game.instance, 5000, 5);
		}
		
		public function isMovingKeysPressed():Boolean
		{
			for(var i:int = 0; i < keysPressed.length; i++)
			{
				var curPressed:int = keysPressed[i] as int;
				
				if(curPressed == Keyboard.D || curPressed == Keyboard.A || curPressed == Keyboard.W || curPressed == Keyboard.S)
				{
					return true;
				}
			}
			return false;
		}
		
		private function userActionLogic(keyCode:uint):void
		{
			if(Game.instance.dude.canDoAnything)
			{
				if(Game.instance.dude.currentState == Dude.USE_STATE)
				{
					Game.instance.background.useInDirection(keyCode);	
				}
				else
				{
					Game.instance.fightController.kick(keyCode);
				}
			}
			else
			{
				Game.instance.dude.showPhraseOver("Ничё нимагу. Шота мне плоха...");
			}
		}
		
		override public function onKeyDown(keyCode:uint):void
		{
			if(_keysPressed.indexOf(keyCode) == -1)
			{
				if( keyCode == Keyboard.F5)
				{
					Game.instance.background.forceSaveCurrentMapState();
					GameSavedData.instance.saveGameDataToFile("tmpSave.xml");
				}
			
				if(	keyCode == Keyboard.E )
				{
					Game.instance.currentState = Game.EDITOR_STATE;					
				}
				
				if(	keyCode == Keyboard.K )
				{
					startShake();
				}
				
				if ( keyCode == Keyboard.T )
				{
					// myCustomDisplayObject and myCustomMaskDisplayObject can be any Starling display object:
					var myCustomDisplayObject:Sprite = Game.instance;
					var myCustomMaskDisplayObject:Sprite = ArtUtils.makeSimpleSprite("pixelmask");
					
					
					// for masks with no animation (note, MUCH better performance!)
					var maskedDisplayObject:PixelMaskDisplayObject = new PixelMaskDisplayObject(-1, false);
					maskedDisplayObject.addChild(myCustomDisplayObject);
					
					// Apply the masking as you would in classic flash.display style.
					// Note: the mask display object should not be added to the display list.
					
					maskedDisplayObject.mask = myCustomMaskDisplayObject;
					//Game.instance.addChild(maskedDisplayObject);
				}
				
				if( keyCode == Keyboard.R)
				{
					
					var gameObj:Game = Game.instance;
				/*	
					gameObj.pivotX = -gameObj.width/2;
					gameObj.pivotY = -gameObj.height/2;
					gameObj.x = -gameObj.width/2;
					gameObj.y = -gameObj.height/2;*/
					_scaleAnim.scaleObjectAroundPoint(gameObj, new Point(gameObj.dude.x, gameObj.dude.y), 2.0, 30, onScaleDone);
					//var backMat:Matrix=back.transformationMatrix.clone();
					
					/*var tween:Tween = new Tween(back, 2.0);
					//tween.animate("rotation", Math.PI);
					tween.animate("scaleX", 2.0);
					tween.animate("scaleY", 2.0);
					tween.reverse = true;
					tween.repeatCount = 4;
					Starling.juggler.add(tween);*/
					

					/*var mat:Matrix= backMat.clone();
					MatrixTransformer.scaleArounfInternalPoint(mat,back.dude.x, back.dude.y, 2.0, 2.0);
					back.transformationMatrix=mat;*/
				}
				
				if( keyCode == Keyboard.F )
				{
					Game.instance.dude.switchBattleMode();
				}
				
				if( keyCode == Keyboard.Z )
				{
					Game.instance.background.forceRedraw(false);
				}
				
				if( keyCode == Keyboard.I )
				{
					Game.instance.currentState = Game.INVENTORY_STATE;
				}
				
				if( keyCode == Keyboard.J )
				{
					if(Game.instance.currentState != Game.SLOT_STATE)
					{
						Game.instance.currentState = Game.SLOT_STATE;
					}
					else
					{
						Game.instance.currentState = Game.MOVING_STATE;
					}
				}
				
				if( keyCode == Keyboard.NUMBER_1 ||
					keyCode == Keyboard.NUMBER_2 ||
					keyCode == Keyboard.NUMBER_3 ||
					keyCode == Keyboard.NUMBER_4 ||
					keyCode == Keyboard.NUMBER_5)
				{
					Game.instance.dude.useItemInSlot(keyCode - Keyboard.NUMBER_1)
				}
			
				if( keyCode == Keyboard.UP ||
					keyCode == Keyboard.DOWN ||
					keyCode == Keyboard.LEFT || 
					keyCode == Keyboard.RIGHT)
				{
					userActionLogic(keyCode);
				}
				
				if(keyCode == Keyboard.SHIFT)
				{
					Game.instance.dude.shouldRun = true;
					_keysPressed.push(keyCode);
				}
				
				if(	(keyCode == Keyboard.D || 
					keyCode == Keyboard.A || 
					keyCode == Keyboard.W || 
					keyCode == Keyboard.S) &&
					Game.instance.dude.canMove)
				{
					_keysPressed.push(keyCode);
					if(Game.instance.dude.shouldRun)
					{
						Game.instance.dude.run();
					}
					else
					{
						Game.instance.dude.walk();
					}		
				}
			}
		}
		
		override public function onKeyUp(keyCode:uint):void
		{
			var index:int = _keysPressed.indexOf(keyCode);
			if(index != -1)
			{
				_keysPressed.splice(index, 1);
				
				if(keyCode == Keyboard.SHIFT)
				{
					Game.instance.dude.shouldRun = false;
				}
				
				if(_keysPressed.length == 0)
				{
					Game.instance.dude.stop();
				}
			}
		}
	}
}