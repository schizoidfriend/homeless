package time
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import starling.text.TextField;

	public class GameTime
	{
		public static const DAY_HOURS:int = 24;
		public static const MINUTES_IN_HOUR:int = 60;
		
		
		private var _curHours:int;
		private var _curMinutes:int;
		
		private var _curDay:int;
		
		private var _oneSecTimer:Timer = new Timer(1000);
		
		private var _timerTF:TextField;
		
		public function GameTime(timerTF:TextField)
		{
			_timerTF = timerTF;
			initializeTimer();
		}
		
		public function get curDay():int
		{
			return _curDay;
		}

		public function get curHours():int
		{
			return _curHours;
		}

		private function initializeTimer():void
		{
			_oneSecTimer.addEventListener(TimerEvent.TIMER, onOneSecTimer);
			startTimer();
		}
		
		public function startTimer():void
		{
			_oneSecTimer.start();
		}
		
		public function stopTimer():void
		{
			_oneSecTimer.stop();
		}
		
		private function onOneSecTimer(e:TimerEvent):void
		{
			_curMinutes++;
			checkTime();
			_timerTF.text = getTimeSting();
		}
		
		private function checkTime():void
		{
			if(_curMinutes >= MINUTES_IN_HOUR)
			{
				_curMinutes = 0;
				_curHours++;
				
				Game.instance.setDaylightAccordingly(_curHours);
			}
			
			if(_curHours >= DAY_HOURS)
			{
				_curHours = 0;
				_curDay++;
				Game.instance.background.refreshGarbage();
			}
		}
		
		public function setDateTime(min:int, hours:int, day:int):void
		{
			if(_curDay < day)
			{
				Game.instance.background.refreshGarbage();
			}
			_curHours = hours;
			_curMinutes = min;
			_curDay = day;
			
			checkTime();
			
			Game.instance.setDaylightAccordingly(_curHours);
		}
		
		public function getTimeSting():String
		{
			return "day:"+_curDay+" time:"+buildTimeNum(_curHours)+":"+buildTimeNum(_curMinutes);
		}
		
		private function buildTimeNum(value:int):String
		{
			return value >= 10 ? String(value) : "0"+value;
		}
		
		public function addTimer(days:int, hours:int, min:int):void
		{
			_curDay += days;
			_curHours += hours;
			_curMinutes += min;
			
			checkTime();
		}
		
		
	}
}