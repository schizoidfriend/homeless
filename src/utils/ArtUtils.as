package utils
{
	import flash.net.dns.AAAARecord;
	
	import gui.CustomImgProgressBar;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	public class ArtUtils
	{
		public function ArtUtils()
		{
		}
		
		public static function makeSprite(img:Image, x:int, y:int):Sprite
		{
			var spr:Sprite = new Sprite();
			spr.addChild(img);
			spr.x = x;
			spr.y = y;
			
			return spr;
		}
		
		public static function makeSelfCenteredImage(name:String):Image
		{
			var img:Image = makeImage(name);
			img.x = -(img.width >> 1);
			img.y = -(img.height >> 1);
			
			return img;
		}
		
		public static function makeSelfCenteredSprite(name:String):Sprite
		{
			var img:Image = makeImage(name);
			var spr:Sprite = makeSprite(img,0,0);
			spr.x = -(spr.width >> 1);
			spr.y = -(spr.height >> 1);
			
			return spr;
		}
		
		public static function makeSimpleSprite(name:String):Sprite
		{
			var img:Image = makeImage(name);
			var spr:Sprite = makeSprite(img,0,0);
			
			return spr;
		}
		
		public static function makeMovieClip(name:String, speed:int = 30, loop:Boolean = false):MovieClip
		{
			var mcAtlas:TextureAtlas = Game.instance.assets.getTextureAtlas(name);
			if(mcAtlas)
			{
				var mcFrames:Vector.<Texture> = mcAtlas.getTextures();
				var mc:MovieClip = new MovieClip(mcFrames, speed);
				mc.loop = loop;
			
				return mc;
			}
			return null;
		}
		
		public static function makeSelfCenteredMovieClip(name:String, speed:int = 30, loop:Boolean = false):MovieClip
		{
			var mc:MovieClip = makeMovieClip(name, speed, loop);
			centerMCOverItself(mc);
			
			return mc;
		}
		
		public static function centerMCOverItself(object:MovieClip):void
		{
			object.x = -(object.width >> 1);
			object.y = -(object.height >> 1);
		}
			
		
		public static function makeImage(name:String):Image
		{
			var tex:Texture = Game.instance.assets.getTexture(name);
			var img:Image = new Image(tex);
			return img;
		}
		
		public static function makeItemInPlace(name:String, x:int, y:int):Image
		{
			var img:Image = makeImage(name);
			img.x = x;
			img.y = y;
			
			return img;
		}
		
		public static function makeSelfCenteredItemInPlace(name:String, x:int, y:int):Sprite
		{
			var img:Image = ArtUtils.makeItemInPlace(name,0,0);
			img.x = -(img.width >> 1);
			img.y = -(img.height >> 1);
			var spr:Sprite = makeSprite(img,x+(img.width >> 1),y+(img.height >> 1));
			
			return spr;
		}
		
		public static function makeBoldTF(sizex:int, sizey:int, posx:int, posy:int, fontSize:int, color:uint=0x0, vAlign:String=VAlign.CENTER, hAlign:String=HAlign.LEFT):TextField
		{
			var tf:TextField = new TextField(sizex, sizey, "temp text", "Verdana", fontSize, color, true);
			tf.x = posx;
			tf.y = posy;
			tf.vAlign = vAlign;
			tf.hAlign = hAlign;
			
			return tf;
		}
		
		public static function makeNumberTF(sizex:int, sizey:int, posx:int, posy:int, fontSize:int, color:uint=0x0, vAlign:String=VAlign.CENTER, hAlign:String=HAlign.LEFT):TextField
		{
			var tf:TextField = new TextField(sizex, sizey, "temp text", "Visitor", fontSize, color, true);
			tf.x = posx;
			tf.y = posy;
			tf.vAlign = vAlign;
			tf.hAlign = hAlign;
			
			return tf;
		}
		
		public static function makeDialogTF(text:String, sizex:int, sizey:int, posx:int, posy:int, fontSize:int, color:uint=0x0, vAlign:String=VAlign.CENTER, hAlign:String=HAlign.LEFT):TLFSprite
		{
			var dialogTF:TLFSprite = TLFSprite.fromPlainText(text);
			dialogTF.setStyle("fontSize", fontSize);
			dialogTF.setStyle("color", color);
			dialogTF.compositionWidth = sizex;
			dialogTF.compositionHeight = sizey;
			dialogTF.x = posx;
			dialogTF.y = posy;
			dialogTF.flatten();
			
			return dialogTF;
		}
		
		public static function createProgressBar(backImgName:String, barImgName:String, x:int, y:int):CustomImgProgressBar
		{
			var pb:CustomImgProgressBar = new CustomImgProgressBar(backImgName, barImgName)
			pb.x = x;
			pb.y = y;
			pb.ratio = 1.0;
			
			return pb;
		}
		
		public static function createButton(posx:int, posy:int, text:String):Button
		{
			
			var buttonTexture:Texture = Game.instance.assets.getTexture("button");
			var btn:Button = new Button(buttonTexture, text);
			btn.x = posx;
			btn.y = posy;
			return btn;			
		}
	}
}