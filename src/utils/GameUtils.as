package utils
{
	import flash.geom.Point;

	public class GameUtils
	{
		public function GameUtils()
		{
		}
		
		static public function tileToCenterMapPos(tile:Point):Point
		{
			return new Point(tile.x*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE,
				tile.y*CommonVars.TILE_SIDE_SIZE+CommonVars.HALF_TILE_SIZE);
		}
		
		static public function posToTile(pos:Point):Point
		{
			return new Point(int(pos.x/CommonVars.TILE_SIDE_SIZE),
				int(pos.y/CommonVars.TILE_SIDE_SIZE));
		}
	}
}