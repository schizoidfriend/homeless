package utils
{
	public class TilesRaytracing
	{
		private var _cellSizeX:Number;
		private var _cellSizeY:Number;
		
		public var walkability:Array = new Array();
		
		public function TilesRaytracing(cellSizeX:Number, cellSizeY:Number, mapWidth:int)
		{
			for ( var i:int = 0; i < mapWidth; i++)
			{
				walkability[i] = new Array();
			}
				
			_cellSizeX = cellSizeX;
			_cellSizeY = cellSizeY;
		}
		
		public function onTraverse(cx:int, cy:int):Boolean
		{
			if(walkability[cx][cy] == AStarLib.UNWALKABLE)
			{
				return false;
			}
			
			return true;
		}
		
		public function canSee(x1:Number, y1:Number, x2:Number, y2:Number):Boolean
		{
			var gridPosX:int = int(x1 / _cellSizeX);
			var gridPosY:int = int(y1 / _cellSizeY);
			if (!onTraverse(gridPosX, gridPosY))
				return false;
			
			var dirX:Number = x2 - x1;
			var dirY:Number = y2 - y1;
			const distSqr:Number = dirX * dirX + dirY * dirY;
			if (distSqr < 0.00000001)
				return false;
			
			const nf:Number = 1 / Math.sqrt(distSqr);
			dirX *= nf;
			dirY *= nf;
			
			const deltaX:Number = _cellSizeX / Math.abs(dirX);
			const deltaY:Number = _cellSizeY / Math.abs(dirY);
			
			var maxX:Number = gridPosX * _cellSizeX - x1;
			var maxY:Number = gridPosY * _cellSizeY - y1;
			if (dirX >= 0) maxX += _cellSizeX;
			if (dirY >= 0) maxY += _cellSizeY;
			maxX /= dirX;
			maxY /= dirY;
			
			const stepX:int = dirX < 0 ? -1 : 1;
			const stepY:int = dirY < 0 ? -1 : 1;
			const gridGoalX:int = int(x2 / _cellSizeX);
			const gridGoalY:int = int(y2 / _cellSizeY);
			var currentDirX:int = gridGoalX - gridPosX;
			var currentDirY:int = gridGoalY - gridPosY;
			while (currentDirX * stepX > 0 || currentDirY * stepY > 0)
			{
				if (maxX < maxY)
				{
					maxX += deltaX;
					gridPosX += stepX;
					currentDirX = gridGoalX - gridPosX;
				}
				else
				{
					maxY += deltaY;
					gridPosY += stepY;
					currentDirY = gridGoalY - gridPosY;
				}
				
				if (!onTraverse(gridPosX, gridPosY))
					return false;
			}
			
			return true;
		}
	}
}