package utils
{
	import flash.geom.Point;

	public class UtilsMath
	{
		private static var _pathLib:AStarLib;
		private static var _rayTracing:TilesRaytracing;
		
		public function UtilsMath()
		{
		}
		
		public static function reinitAlgorithms(sizeX:int, sizeY:int):void
		{
			_pathLib = new AStarLib( sizeX, sizeY, 1);
			_rayTracing = new TilesRaytracing(CommonVars.TILE_SIDE_SIZE, CommonVars.TILE_SIDE_SIZE, sizeX)
		}
		
		public static function makePointAbs(point:Point):Point
		{
			if(point.x < 0) point.x *= -1;
			if(point.y < 0) point.y *= -1;
			
			return point;
		}
		
		public static function randomRange(minNum:int, maxNum:int):int 
		{
			return int(Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		
		public static function getRandomWithChance(chanceOf100:uint):Boolean
		{
			if(chanceOf100 >= 100) return true;
			
			return randomRange(1,100) <= chanceOf100 ? true : false;
		}
		
		public static function toClosestTilePos(pos:Point):Point
		{
			var res:Point = new Point();
			if(pos.x > 0)
			{
				if(pos.x - Math.floor(pos.x) < 0.5) res.x = Math.floor(pos.x);
				else								res.x = Math.floor(pos.x)+1;
			}
			else
			{
				if(pos.x - Math.floor(pos.x) < -0.5) res.x = Math.floor(pos.x);
				else								 res.x = Math.floor(pos.x)-1;
			}
			
			if(pos.y > 0)
			{
				if(pos.y - Math.floor(pos.y) < 0.5) res.y = Math.floor(pos.y);
				else								res.y = Math.floor(pos.y)+1;
			}
			else
			{
				if(pos.y - Math.floor(pos.y) < -0.5) res.y = Math.floor(pos.y);
				else								 res.y = Math.floor(pos.y)-1;
			}
			
			return res;
		}
		
		public static function get astar():AStarLib
		{
			return _pathLib;
		}
		
		public static function get rayTracing():TilesRaytracing
		{
			return _rayTracing;
		}
	}
}