package starling.display
{
	import away3d.materials.utils.SimpleVideoPlayer;
	
	import flash.display.BitmapData;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.getNextPowerOfTwo;
	
	/**
	 * ...
	 * @author Frederico Garcia
	 */
	public class VideoPlayer extends Sprite
	{
		//--------------------------------------------------------------------------
		//
		//  Constants
		//
		//--------------------------------------------------------------------------
		
		public static const COLUMNS:uint = 4;
		
		public static const ROWS:uint = 4;
		
		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------
		
		public function VideoPlayer(videoWidth:Number, videoHeight:Number, fps:Number = 20)
		{
			// set parameters
			_videoWidth = videoWidth;
			_videoHeight = videoHeight;
			_fps = fps;
			
			init();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Variables
		//
		//--------------------------------------------------------------------------
		
		private var _videoWidth:Number;
		
		private var _videoHeight:Number;
		
		private var _fps:Number;
		
		private var _player:SimpleVideoPlayer;
		
		private var _canvas:BitmapData;
		
		private var _timer:Timer;
		
		private var _images:Array;
		
		private var _matrixes:Array;
		
		//--------------------------------------------------------------------------
		//
		//  Methods
		//
		//--------------------------------------------------------------------------
		
		public function get player():SimpleVideoPlayer
		{
			return _player;
		}

		private function init():void
		{
			// init player
			_player = new SimpleVideoPlayer();
			_player.width = _videoWidth;
			_player.height = _videoHeight;
			
			// we'll draw each tile on this bitmapData
			_canvas = new BitmapData(getNextPowerOfTwo(_videoWidth / COLUMNS), getNextPowerOfTwo(_videoHeight / ROWS), true);
			
			// init images
			_images = [];
			_matrixes = [];
			
			var imageFrame:Rectangle = new Rectangle(0, 0, _videoWidth / COLUMNS, _videoHeight / ROWS);
			for (var i:int = 0; i < COLUMNS; i++)
			{
				var imageColumn:Array = [];
				var matrixColumn:Array = [];
				for (var j:int = 0; j < ROWS; j++)
				{
					var image:Image = new Image(Texture.fromBitmapData(_canvas, false, false/*, imageFrame*/));
					image.x = i * _videoWidth / COLUMNS;
					image.y = j * _videoHeight / ROWS;
					addChild(image);
					
					imageColumn.push(image);
					matrixColumn.push(new Matrix(1, 0, 0, 1, -i * _videoWidth / COLUMNS, -j * _videoHeight / ROWS));
				}
				
				_images.push(imageColumn);
				_matrixes.push(matrixColumn);
			}
			
			// the video can be updated at a different framerate than the rest of the app
			_timer = new Timer(1000 / _fps);
			_timer.addEventListener(TimerEvent.TIMER, onTimerHandler);
		}
		
		public function load(url:String):void
		{
			_player.source = url;
		}
		
		public function play():void
		{
			_player.play();
			_timer.start();
		}
		
		public function pause():void
		{
			_player.pause();
			_timer.stop();
		}
		
		public function stop():void
		{
			_player.stop();
			_timer.stop();
		}
		
		public function killPlayer():void
		{
			_player.dispose();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Event handlers
		//
		//--------------------------------------------------------------------------
		
		private function onTimerHandler(e:TimerEvent):void
		{
			if(!_player.playing)
			{
				return ;
			}
			var imageFrame:Rectangle = new Rectangle(0, 0, _videoWidth / COLUMNS, _videoHeight / ROWS);
			for (var i:int = 0; i < COLUMNS; i++)
			{
				for (var j:int = 0; j < ROWS; j++)
				{
					_canvas.draw(_player.container, _matrixes[i][j]);
					
					Image(_images[i][j]).texture.dispose();
					Image(_images[i][j]).texture = Texture.fromBitmapData(_canvas, false, false/*, imageFrame*/);
				}
			}
		}
	}
	
}